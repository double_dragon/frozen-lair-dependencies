# Additional Dependencies for the Frozen Lair project

In this repository we put some extra tools which we modified to get a working environment between Blender and OGRE.

* io_ogre  
    A modified version of the [blender2ogre](https://github.com/OGRECave/blender2ogre) exporter. Installation instructions are the same as the original.
    
* OgreCommandLineTools  
	A compiled version of the OGRE conversion tools. Place in C:\OgreCommandLineTools to make the exporter work (without further configuration).
    
- export_script  
	A batch script that automatically exports all .blend file inside the folder where it's placed. Requires the Blender executable to be inside the system path environment variable.
    
- dotsceneinterface  
    A modified version of the original [dotsceneinterface](https://bitbucket.org/ogreaddons/dotsceneinterface) importer. Our version accounts for user_data tags; follow the same instructions as the original version, and link the resulting library (including tinyxml) to the project.