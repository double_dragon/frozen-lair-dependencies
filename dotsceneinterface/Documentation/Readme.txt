
This document is best when viewed in 1152x864 screen resolution in a maximazied Total Commander lister or in Visual Studio's text
editor with solution explorer/Class View/Resource View and output window/Bookmarks/Command Window/etc. either closed or hidden.


                                                     Copyright for Readme.txt                                                      
                                                     ========================

File Readme.txt
Copyright (C) 2007 Balazs Hajdics

Permission is granted to make and distribute verbatim
copies of this manual provided the copyright notice and
this permission notice are preserved on all copies.

Permission is granted to copy and distribute modified
versions of this manual under the conditions for verbatim
copying, provided that the entire resulting derived work is
distributed under the terms of a permission notice
identical to this one.

This work is licenced under the Creative Commons Attribution-ShareAlike 2.5 License.
To view a copy of this licence, visit http://creativecommons.org/licenses/by-sa/2.5/
or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.




                                                         Copyright notices                                                         
                                                         =================
dotSceneInterface
-----------------
	Copyright (C) 2007 Bal�zs Hajdics, cTH
	Licensed under LGPL. Please see license.txt for more information.
	
dotSceneViewer
--------------
	Copyright (C) 2007 Bal�zs Hajdics, cTH
	Licensed under LGPL. Please see license.txt for more information.
	
dotSceneOctree
--------------
	Copyright (C) 2007 Doug Wolanick, Bal�zs Hajdics
	Licensed under LGPL. Please see license.txt for more information.
	
dotSceneToDotSceneOctreeConverter
---------------------------------
	Copyright (C) 2007 Doug Wolanick, Bal�zs Hajdics
	Licensed under LGPL. Please see license.txt for more information.
	
XmlNodeProcessor
----------------
	Copyright (C) 2007 Tibor Tihon, Bal�zs Hajdics
	Licensed under LGPL. Please see license.txt for more information.
	
tree01S.tga, tree02S.tga and tree35S.tga
----------------------------------------
 	Copyright (C) 2000 Microsoft Corporation
	You may reuse, copy, modify tree01S.tga, tree02S.tga and tree35S.tga according to the Microsoft DirectX SDK 8.1 license.
	In the short term it means that you are allowed to reuse, copy these files in non-commercial projects, but you
	are not allowed to reuse them in commercial products. They aren't licensed under LGPL.


                                                             Stability                                                              
                                                             =========
Although the new DotSceneInterface is currently in stage alpha 1 it was heavily tested for almost all special cases. The source
code is really large thanks to that (and to some other things), however it is really stable. Currently there are no known bugs.
The only way to make DotSceneInterface freeze is to give it to an xml file that is not according to the 20001006 W3C Recommendation.
E.g. no ending tag for an opening tag, etc. This is because currently DotSceneInteface is compiled against an old version of tiny
xml, which freezes incase a non-standard .xml file were given for parsing.
Currently DotSceneInterface catches all exceptions thrown by Ogre because of incorrect parameters specified in the .scene. It only
logs errors. If the given .scene file is not according to the alpha stage 1 dotScene format specification (see dotScene.dtd for it)
DotSceneInterface will also log errors, but it won't throw an exception, until it is really necessary (a fatal error has occured,
which makes it impossible to continue the loading of the scene). This decision was made to make DotSceneInterface more compatible
with the phylosophy behind Extensible Markup Language.
However DotSceneInterface doesn't meant to protect you from semantical mistakes. For example: trying to use or reference to a
resource (e.g. a mesh) in one of the xml elements which you haven't declared a resource location for using the <resourceGroup> and
<resourceLocation> xml elements will result in an exception.

DotSceneViewer has one known bug, see Roadmap.txt for more information on that.


                                                            Installation                                                            
                                                            ============

- If you compile from source, then please see section "Compiling the source" for more information.
- If you use a binary release:
	Before running dotSceneViewer.exe you must copy the content of the included "media/ogre_sdk" directory to the
	directory where you installed Ogre SDK. The correct directory structure already exists under media/ogre_sdk
	so you don't have to care about copying different files to different locations.


                                                        Compiling the source                                                        
                                                        ====================

Prerequisites
-------------
For compiling the source you have to download and install the latest Ogre SDK (at least version 1.4.1) from
http://www.ogre3d.org. You either have to install the downloaded Ogre SDK to Dependencies/trunk/main/Ogre or
you have to add the paths of your Ogre SDK's include and lib directory to the include and lib paths of your
Visual Studio. To do this start Visual Studio in the Tools menu select Options, then open
"Projects and Solutions" in the left pane. Then click on the "VC++ Directories" child element. In the panel
appearing in the right select "Include files", create a item in the list in the right pane. Repeat the whole
process to "Library files" too.


Supported Platforms, Compilers
------------------------------
Currently all parts of the dotScene project only compiles under Windows.
However dotScene projects either contain no platform specific source code, or when they do it is put
between the correct preprocessor condition statements, so say for example Windows specific code
wouldn't compile under Linux system.
According to this you must be able to compile the source under different operating systems, however
no makefiles or project files are avaible for platforms other then Windows MSVC8.

Currently the only supported compiler is Microsoft Visual Studio 8.0 with Service Pack 1.


Notes
-----
	After compilation dotSceneViewer.exe and dotSceneInterface.dll are copyied either to the
$(OGRE_SDK)/bin/debug or $(OGRE_SDK)/bin/release directory, depending on the build configuration
type you have selected. OGRE_SDK is an environment variable that must consist the path where you
have installed Ogre SDK (at least version 1.4.1). See http://www.ogre3d.org for more information
about downloading the Ogre SDK. Copying is needed, so dotSceneViewer and dotSceneInterface can
find the .dll-s they were linked against. (e.g. OgreMain.dll)


Important
---------
As porting from the previous dotScene projects is not yet finished, project
DotSceneToDotSceneOctreeConverter will not compile.


Compile
-------
Open dotSceneFormat_VC80.sln. Select the configuration type you want. Press the F7 button.
Configurations "Release" and "Debug" are valid, configurations "ReleaseOctreeSM" and "DebugOctreeSM"
are not tested and may not function as expected.



                                                            dotScene.dtd                                                            
                                                            ============
The new .scene format (version 1.0.0) is not yet finished. It is in stage alpha 1. Interface breaking changes will take place both
in the source code and in the format itself.
See Roadmap.txt for more information on the expectable interface breaking changes in the format.
In general it can be said that the current format is stable, mostly it is going to be extended (new optional child elements
will appear for many elements), not modified.
For the full specification of the new dotscene format v1.0.0 please see dotScene.dtd.




                                                   dotSceneInterface Documentation                                                  
                                                   ===============================


Concepts for creating xml specifications for the new extensible loader
-----------------------------------------------------------------------
Please see file Common/trunk/main/XmlNodeProcessor/Documentation/Design concept & philosophy behind XMLNodeProcessor.txt



Using dotSceneLoader from code
------------------------------



Remarks for colours
-------------------
Currently colors are specified as r,g,b,a components. Each is a normalized colour value. I.e. a floating point noumber between range 0,1
and not an integer between 0,255.


Remarks for angles
------------------
All xml elements which have an attribute consisting an angle value, also have an attribute named "angleUnit" which you can set either
to "degree" or to "radian" to specify the unit in which the angle was given. If you omit attribute "angleUnit" angles for the given
element will be treated as if they were in degrees. See dotScene.dtd for more information.


Remarks for creating different .scene xml elements
--------------------------------------------------

Common remarks:
- All element names and all attribute names of all elements are case sensitive!

Element "scene":
- Attribute "sceneManagerName" is ignored if attribute "sceneManagerType" is not present.

- Element "renderTargets"
	- The names of render windows, render textures, multi render targets are never prefixed. The common parameter "NamePrefix" will be
	  ignored in their case. So you can load the same .scene files multiple times to different scene nodes without creating too many
	  unwanted render windows, or creating render textures which will be never used by any material.

Element "renderWindow":
- If attribute "primary" is set to true, then the following cases are possible:
	- A primary render window has been created before. This is possible in many cases. Maybe attribute "primary" was set to "true"
	  for more then one "renderWindow" elements in the .scene file. Perhaps it was created by a previously loaded .scene file.
	  The most frequent case is that it has been created automatically by Ogre. If a primary window already exists, dotSceneLoader
	  will not destroy it (because it would mean the destroyment of the entire RenderSystem, which would cause all the previously
	  loaded textures, meshes, etc. to be destroyed too), instead it will try to tweak its properties through
	  Ogre::RenderSystem::setConfigOptions() calls.
	  The following attributes of xml element "renderWindow" will be taken into account if attribute "primary" is set to "true" and
	  a primary window already exists:
		"width"
		"height"
		"fullscreen"
		"active"
		"autoUpdated"
		"primary"
		"visible"
		
		"left"
		"top"
	  The following attributes of xml element "renderWindow" will be ignored if attribute "primary" is set to "true" and a primary
	  window already exists:
		"name"
		"priority"
		
		"colourDepth"
		"FSAA"
		"displayFrequency"
		"vsync"
		"border"
		"outerDimensions"
		"depthBuffer"
		"title"

	- A primary render window hasn't been created before. In this case it will be created with the parameters specified in the
	  .scene file.
	  
Element "viewports":
- Element "viewports" have to be defined after all cameras have been created. Because camera declarations can be nested into node
  declarations usually this means, that element "viewports" have to be defined after element "nodes".
  Element "viewports" also have to be defined after element "renderTargets".
  
Element "viewport":
- If a viewport at the given ZOrder already exists in the refered RenderTarget, the following will happen:
	- If the camera used to render to the viewport at the given ZOrder is different than the camera specified
	  in the .scene file a warning will be logged and the current camera will be overwritten with the one specified in the .scene
	  file.
	- The properties of the viewport will be changed according to the .scene file.
  This gives the user the ability to modify the viewports of a previously created render window, render texture or multi render
  target. If modifying the already existing viewport of the render target is not a purpose, then either use different ZOrders in
  your .scene file, or do not try to overwrite the viewport at all.
- Possible values for attribute "clearBuffers" include "colour", "depth", "stencil". You can concatenate two or more of these
  values using the "|" character (just like in C++). E.g. "colour|depth".
  
Element "HDRCompositorInstance":
- Currently values "cross" and "plusCross" are not supported for attribute "starType". This is due to the lack of implementation
  of these star types in the current version HDRLib.
		
		 
Element "renderTexture":
- If attribute "usage" set to value "default", it equals with the combination of "autoMipmap" and "staticWriteOnly".
- Attribute "numberofMipmaps" can be set to either a number representing the number of mipmaps to be created with the textuer, or
  it can be set to special words:
	- "default": use the TextureManager default number of mipmaps previously set with a call to TextureManager::setDefaultNumMipmaps().
	  This is equivalent with setting the num_mips parameter in a TextureManager::CreateManual() function call to
	  Ogre::TextureMipmap::MIP_DEFAULT.
	- "unlimited": mipmaps will be generated up to the 1x1 resolution level.
	  This is equivalent with setting the num_mips parameter in a TextureManager::CreateManual() function call to
	  Ogre::TextureMipmap::MIP_UNLIMITED.
  Please see the documentation of enumerated type TextureMipmap in Ogre for more information.

Element "viewport":
- Attribute "renderQueueInvocationSequenceName" is parsed and its value is set through
  Ogre::Viewport::setRenderQueueInvocationSequenceName(), however currently the dotScene format does not support creating render
  queue invocation sequences through the xml file, so the given render queue invocation sequence must be created in code by the time
  dotSceneLoader loads a viewport with a valid "renderQueueInvocationSequenceName" attribute set.

Element "orientation":
- If more then one sub-element is present then all sub-elements will be parsed and validated but only the last one will affect the
  object which consist the "orientation" element. Although it is illegal to emit more then sub-element for element "orientation", this
  error will be ignored silently.
  
Element "skyBox", "skyDome", "skyPlane":
- All skyboxes, skydomes and skyplanes are stored in dotSceneInfo, identified by a unique name. You can use dotSceneInfo to query a
  given skybox/skydome/skyplane and then set it to your scene manager.
  This is especially usefull in some cases, e.g. when you want to store all skyboxes of your map for different times of day, and
  you want to switch between them according to the gameplay (when the night comes, etc.). This functionality should become the
  part of the Ogre Core in the future.
- If there are more than one "skyBox"/"skyDome"/"skyPlane" elements with the property "enabled" set to true, all of them will be
  parsed but only the last one will be applied to the scene.
  IMPORTANT!!! You should declare the sky box/dome you want to use with your current scene with attribute "enabled" set to
  "true" lastly. This is because even disabled sky boxes/domes can have an optional child element orientation, which will modify
  the orientation of the current sky box/dome. However Ogre currently only handles one sky box/dome instances with one scene so
  this will modify the orientation of the previously set, enabled, sky box/dome. This is a behaviour you surely do not want.
  
Element "camera":
- Please note that although attribute "aspectRatio" will be set on the given camera, applications should override this when the camera
  is attached to a viewport to reflect the aspect ratio of the render surface. A typical example might look like this:
  @code
		using namespace Ogre;
		RenderWindow* 	mWindow;
		Camera*			mCamera;
		[...]
  
  		Viewport* vp = mWindow->addViewport( mCamera, 0, 0.0f, 0.0f, 1.0f, 1.0f );

		vp->setBackgroundColour( ColourValue( 0.0f, 0.0f, 1.0f ) );
		vp->setClearEveryFrame( true );
		mCamera->setAspectRatio( Real(vp->getActualWidth()) / Real(vp->getActualHeight()) );
  @endcode

- Element "billboardSet":
	- If both sub-element "textureCoords" and "textureStacksAndSlices" are present, the one defined later in the .scene file will be
	  used as an element for texture coordinate set generation for the give billboard set. Although "textureCoords" and
	  "textureStacksAndSlices" are mutually exclusive, this error will be ignored silently.
- Element "billboard":
	- Sub-element "textureCoords" can only contain one "floatRect" sub-element. If it contains more then one, this error will be
	  ignored silently and the one defined last will be used.
	- Attribute "texcoordIndex" and sub-element "textureCoords" are mutually exclusive. If both is present in the same billboard
	  declaration, both of them will be parsed, but only the texture rectangles specified by the sub-element "textureCoords" will be
	  used.
	- If you specify attribute "width", you have to specifiy attribute "height" and vica-versa. Otherwise Billboad::setDimensions()
	  won't be called.
 
- Element "plane":
	- "plane" only creates a mesh, you have to create an entity based on that mesh to have movable object representing the given plane. 
 
- Element "resourceLocation" as a child element of "resourceGroup":
	- Attribute "relativeTo" determines how the path specified in attribute "name" should be treated:
		- "dotSceneFileLocation": the path specified is relative to the location of the dotscene file.
		  E.g. if "name"="../../media" and the dotscene file can be found at "E:/3D Engines/OGRE/OgreSDK/bin/debug/", then
		  dotscene loader will concatenate the two paths and the resource location will be:
		  "E:/3D Engines/OGRE/OgreSDK/bin/debug/../../media"
		- "exeFileLocation": the path specified is relative to the location of the executable running dotScene loader.
		  E.g. if "name"="../../media" and the .exe file location is "E:/3D Engines/OGRE/OgreSDK/bin/debug/", then
		  dotscene loader will concatenate the two paths and the resource location will be:
		  "E:/3D Engines/OGRE/OgreSDK/bin/debug/../../media"
		  If you specify your resource location paths relative to your executable's path, then you should use this value instead of
		  the "currentWorkingDir", since the location of the .exe file will be searched for both on Windows and on MAC OS operating
		  systems. 
		- "currentWorkingDir": the path specified is relative to the current working directory.
		  E.g. if "name"="../../media" and the current working directory is "E:/3D Engines/OGRE/OgreSDK/bin/debug/", then
		  dotscene loader will concatenate the two paths and the resource location will be:
		  "E:/3D Engines/OGRE/OgreSDK/bin/debug/../../media"
		  IMPORTANT!!! DO NOT RELY ON THE CURRENT WORKING DIRECTORY ON MAC OS OPERATING SYSTEM, SINCE IT WON'T BE THE DIRECTORY OF
		  THE .EXE FILE.
		- "absolute": the path specified is absolute and dotScene loader will leave it intact.

- Element "trackTarget":
	- Child element "localDirectionVector" is only valid for elements of type "node". It is going to be ignored for elements of
	  type "camera".

- Element "lookTarget":
	- If child element "position" and attribute "nodeName" are present simultaneously, then position will be treated as an offset
	  relative to the given scene node.
	- Child element "localDirectionVector" is only valid for elements of type "node". It is going to be ignored for elements of
	  type "camera".
	- Attribute "relativeTo" is valid both on cameras and scene nodes. Although Ogre basically doesn't support lookAt positions in
	  different transform spaces for cameras, dotScene loader does.
	  
- Element "sceneManagerOption":
	- If attribute "type" has the value "camera", "light", "entity", or anything similar which indicates that the attribute "value"
	  stores the name (as a string) of some kind of Ogre related object, which needs to be resolved to a pointer to that object, then
	  define the given "sceneManagerOption" element *after* all other sibling xml elements in the "scene" element. Otherwise the name
	  of the object wouldn't get resolved to a pointer to the object, because the object does not exist and an exception will be
	  thrown.

- Element "animations":
	- If declared as a child element of "scene", it must be declared after the element "nodes", since it will store animations
	  referencing to nodes, which must first become loaded.
	
- Element "NodeAnimationTrack"/"transformKeyFrame"/"orientation":
	- Using child-element "direction" won't work. All other child-element of "orientation" element will work.

- Element "animationStates":
	- Must always be declared after "animations" element both if declared as a child element of "entity" or if declared as a child
	  element of "scene".
	
- Element "entity":
	- If attribute "displaySkeleton" is true, the skeleton of the given entity will be displayed. However this displayed skeleton
	  do not reflect the transforms of the parent nodes, so it is always displayed in the origo of the scene.
	  Here are some comments from OgreEntity.cpp/_updateRenderQueue() function:
        // HACK to display bones
        // This won't work if the entity is not centered at the origin
        // TODO work out a way to allow bones to be rendered when Entity not centered
        
- Element "meshLODBias" and "materialLODBias":
	- If attribute "maxDetailIndex" is greater then Entity::getMesh()::getNumLodLevels(), an error will be logged and it will be
	  truncated to be equal with Entity::getMesh()::getNumLodLevels().
	- If attribute "minDetailIndex" is lesser then "maxDetailIndex", they will be set to be equal.





                                                  dotSceneViewer.exe Documentation                                                  
                                                  ================================
                                                  
Running dotSceneViewer.exe                                                     
==========================
Starting dotSceneViewer.exe without a command line parameter will make dotSceneViewer open a scene file opening dialog window.
You have to select a scene file compatible with the new 1.0.0 dotScene format standard (see dotScene.dtd) for more information) and
then you have to click Ok.
Starting dotSceneViewer.exe with the name of a valid dotScene 1.0.0 standard compatible file will make dotSceneViewer start the
loading of the file immediately.


Recommendations
---------------
We recommend running dotSceneViewer in windowed mode, so you can see both the scene file opening dialog window and the exception
window currently appearing to notify you about the informations/errors which have occured during the loading of the given .scene file.
These informations/error messages are written to the log file of Ogre too.


Hardware Recommendations
------------------------
The hardware requirements will heavily depend on the scene you have assembled using dotScene format specification, no general
requirements can be established.


Running in fullscreen mode
--------------------------
If you start dotSceneViewer.exe in full screen mode, without a command line parameter you will be unable to see the scene file
opening window. To do so, you will have to press Alt+Shift+Tab and wait for a while, until your operating system switches to the
dialog window. After selecting a scene file click Ok, to begin loading it. Passing the name of a valid dotScene 1.0.0 standard
compatible file as a command line parameter to dotSceneViewer.exe will avoid this problem.
After the loading has finished dotSceneViewer will display an exception window with the informations/errors occured during the
loading of the file. This window is also can't be seen in full screen mode. You can choose to either press the Alt+Shift+Tab
keyboard combination again and wait until the operating system displays this window so you can read its contents, or you can simply
ignore it and press Enter or Esc.
Loading may take a while depending on the scene you have assembled (especially on the resource locations you have added to your
scene file, since Ogre searches for scripts and parses them in resource locations, which could lead to the loading of many
probably unused textures as in the case of MyScene.scene), so be patient.
These difficulties will be fixed in the final release. Anyway currently we do not recommend running dotSceneViewer in full screen
mode.


Resource locations
------------------
You can specify resource locations both in your .scene file or in resources.cfg (located in the folder of OgreMain.dll), but not
in both of them!
The resources.cfg attached to this release will contain a corrected version Ogre's original resources.cfg.
It only declares OgreCore.zip as a resource location. All other locations are expected to be declared in the scene file itself.
See elements "resourceGroup" and "resourceLocation" in the dotScene.dtd for more information.
     
                                                  
Controls
--------
The following are the controls for the dotSceneViewer.exe application:
Mouse:					Freelook.
Mouse wheel up/down:	Move forward/backward along local Z axis.
W or Up:				Forward.
S or Down:				Backward.
A:						Step left.
D:						Step right.
PgUp:					Move upwards.
PgDown:					Move downwards.
F:						Toggle frame rate stats on/off.
R:						Render mode (solid, wireframe, points).
T:						Cycle texture filtering:
							- Bilinear,
							- Trilinear,
							- Anisotropic(8).

M:						Switch between buffered and unbuffered mouse input.
K:						Switch between buffered and unbuffered keyboard input.

H:						Show/Hide scene info overlay. The default is on.
P:						Toggle on/off display of overlay consisting camera position / orientation. The default is off.
C:						Cycle through cameras in the scene.
L:						Toggle lights on/off.
E:						Toggle entities on/off.
I:						Toggle dotSceneInfo objects display ON/OFF. The default is on..
B:						Toggle AABB display ON/OFF.
X:						Toggle collision detection displaying in debug text ON/OFF.
NUMPAD MINUS:			Increase camera move speed.
NUMPAD PLUS:			Decrease camera move speed.
NUMPAD MULTIPLY:		Increase info objects size.
NUMPAD DIVIDE:			Decrease info objects size.
