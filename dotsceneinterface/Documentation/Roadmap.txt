- Implement windowResized() event for non-primary RenderWindows. Also register my own implementation of RenderSystem::Listener 
  instances for non-primary RenderWindows too. Currently its only instance is only registered for the primary render window.
  This is needed because internal render textures in compositor instances don't get resized when their owner viewport's  actual
  dimensions are changed (e.g. due to a window resize), they continue to render into the same texture with the same resolution
  which may result in distorted output, so we have to disable and re-enable all compositors corresponding to all viewports of a given
  render window, which was resized.

- Finish DotSceneInfo. Currently not all information about the parsed scene is reported to dotSceneInfo instances.
  Currently not all information reported from DotSceneProcessor is accessible by the client application. The main reason is that
  I run out of time, and I couldn't write these simple functions :-).
  - get...() and has...() functions for movable objects.
  - render window, render texture, multi render target, viewport, compositor report...() get...() has...() functions.
  - Anything else?
  
- Displaying info objects in DotSceneViewer is temporary disabled due to the reorganization of DotSceneInfo.
- Function StringTransformSpaceToEnumTransformSpace() must be reimplemented to reflect the design of the other global string
  converter functions.

- Use progress listeners, implemented as XmlNodeProcessor listeners (ParseListener and SerializeListener).

- Reorganize dotSceneUtils.

- Compiling with the latest stable version of Ogre. Currently dotScene projects are tested against Ogre 1.4.1, however they should
  compile and run without any problems with any 1.4.x Ogre version.

- Further develop DotSceneNodeProcessorTreeBuilder to have functions needed to easily add XmlNodeProcessor derived classes'
  instances as child element processors to one of the currently existing element processors.
  Also further develop DotSceneProcessor and DotSceneProcessorImpl classes to tap the interface required to reach this
  functionality.
- Create demos demonstrating the easyness of adding new element processors to the loader without recompiling it!

- Create and take into account "LoadStaticEntities" common parameter while processing the scene.

- Further develop and fix info data display (nodes, lights, camera) in DotSceneViewer. The old display routines and materials
  weren't modified too deeply, so they produce strange or annoying things sometimes (e.g. when many node info panels are overlap,
  or when you try to increase or decrease the size of info objects and the size of all other objects are increased/decreased with
  them too).

- Integrate DotSceneOctreeSceneManager with DotSceneInterface. Make DotSceneInterface load octrees using DotSceneOctreeSceneManager.
  Also simplify DotSceneOctreeSceneManager. SceneMain is not needed anymore, since 

- Upgrade DotSceneToDotSceneOctreeConverter to use DotSceneInterface for scene loading and then the pure Ogre objects to get the
  data needed for conversion. This will make this project really modular, because it won't depend on the actual structure or format
  of the .scene files. Once written well, this will minimize the work required to update it when other parts of DotSceneFormat are
  updated, further developed. Currently DotSceneToDotSceneOctreeManager does not compile (I haven't had time to finish the integration
  of this project into the others). Solve this problem.
  
- Place copyright notices into the beginning of all files relating to DotSceneToDotSceneOctreeConverter.

- Make project DotSceneInterface compile with a precompiled header too. (Currently it is turned off, since it causes compiler
  errors.)

- Elements "skyBox", "skyDome", "skyPlane":
	- All skyboxes, skydomes and skyplanes should be stored in dotSceneInfo, identified by a unique name. You can use dotSceneInfo to
	  query a given skybox/skydome/skyplane and then set it to your scene manager.
      This is especially usefull in some cases, e.g. when you want to store all skyboxes of your map for different times of day, and
	  you want to switch between them according to the gameplay (when the night comes, etc.).
  
	  A better solution would be to make Ogre support more then one skyboxes, skydomes, skyplanes at one time, each identified by a
	  unique name.

- Currently the old version of tinyxml is still used. TODO: update it.

- Implement a .DTD parser and let dotSceneNodeProcessorTreeBuilder build the tree of element processors based on a given dtd and a
  map of xml element names and pointers to derives of XmlNodeProcessor instances. This will make DotSceneInterface really modular
  and flexible, since redefining which elements can be contained by which elements could be done in run-time. Extending
  DotSceneLoader would also be much easier.

- RenderWindow, Viewport loading/serializing?

- Element "environment":
	- Sub-element "clipping" is not parsed.
	
- Element "terrain":
	- Not parsed.
	- Make it possible to parse element "octree" into a terrain scene manager.
	- Support different types of terrain scene managers.

- Element "node":
	- Do something with attribute "isTarget". Perhaps notify dotSceneInfo.
	
- Element "shadowSettings":
	- Add new child element "shadowCameraSetup" and support parsing custom shadow camera setup.
	
- Element "light":
	- Add new child element "shadowCameraSetup" and support parsing custom shadow camera setup.
	- Add elements "trackTarget" and "lookTarget" as an optional child element. Would be especially usefull for spotlights.
	
- Element "poseReference":
	- Add new attribute "poseName", so poses can be referenced by name to, not only by index. However this requires the further
	  development of Ogre, because currently there is no way to determine the index of a given pose of given submesh. So we can
	  search for a given pose by name using Mesh::getPose(name), but then we can't figure out its handle. However
	  VertexPoseKeyFrame expects pose references *only* by handle.
	  
- Element "numericAnimationTrack":
	- Support parsing, perhaps store the parsed animated numeric values in DotSceneInfo, or give user a callback to get them.

- Element "vertexMorphKeyFrame":
	- Should we support parsing VertexMorphKeyFrame anyway? It would be a re-implementation of Ogre's vertex morph functionality.
	  The only case when this would be usefull is when an application creates geometry and thus morph animations real-time, and
	  it would like to save it e.g. as a part of a save-game (and of course later re-load it). However I think this rarely happens.

- Element "entity":
	- Add support for specifying new materials not only for the entity as a whole but for all sub-entities separately.
	
- Support parsing of hexadecimal and octal values everywhere where int, unsigned int, short or unsigned short is parsed. Would be
  especialy usefull for query and visibility masks.
  
- Support specifing color values as HSL values and as non-normalized RGB values.
- Support enabling a near plane to be a custom near plane (using Frustum::enableCustomNearClipPlane();).

- Parsing/serializing some kind of movable objects is currently not supported, so add parsing/serializing support for new elements:
	- "billboardChain"
		- "ribbonTrail"
	- "instancedGeometry::batchInstance" (does loading/serializing this element have any sense?)
	- "manualObject"
	- "simpleRenderable"
		- "instancedGeometry::geometryBucket" (does loading/serializing this element have any sense?)
		- "rectangle2D"
		- "wireBoundingBox"
	- "staticGeometry::Region": dotSceneOctree have to be revised and further developed, a new element with name
	  "staticGeometry::Region" might "appear" during this.
	

- Implement serializing (saving) from an Ogre scene manager to a .scene file any time.


- Pre-declare of resources for proper loadingtime forecast? I don't think so this will become implemented in the core of
  DotSceneInterface, it would require a pre-look in the .scene, outside the dotScene xml node processors, which would break the
  main phylosophy behind the whole dotScene project.
  
- Make doxygen config file. Pre-generate documentation.


In the long term
----------------
- Adding sub-project for pre-computing radiance transfer on scenes and storing PRT textures in octree nodes.
- A special static octree may be implemented instead of the currently used dynamic one. This will speed up visibility surface determination.
- Extending dotsceneformat and dotSceneLoader to be able to support many types of scene managers. E.g. element <BSP> would be added to the elements that can be contained by the element <scene>. Terrain scene manager support would be finished. dotSceneLoader would create the proper scene manager on the loading of the scene.
- Make the whole source compile under Macintosh, Linux, etc. Makefiles, testing and perhaps porting are required.
- Make the whole source compile with MSVC 7.1 and MSVC 9.0.


- POSSIBLE BUGS IN OGRE!!!
	- BillboardSet accurate facing BUG:
	  First enable accurate facing on a billboard set with a call to BillboardSet::useAccurateFacing(true).
	  Then create billboard which is using the set's default dimensions.
	  Then create billboard which is using its own dimensions (Billboard::setDimensions(width, height)).
	  The billboard which is using the set's default dimensions won't be displayed correctly.
	  The problem doesn't emerge if either only billboards using the set's default dimensions are used or only billboards using their
	  own dimensions are used, no matter if accurate facing is enabled or not.
	  - If this is a bug, then fix it.
	  - If this is not a bug, then always specify own dimensions for all billboards in a set, if even one billboard has own
	    dimensions specified and accurate facing is enabled.
	  