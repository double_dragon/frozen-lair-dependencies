/*
-----------------------------------------------------------------------------
Author:		Balazs Hajdics (wolverine_@freemail.hu), James Le Cuirot

Copyright (C) 2007 Balazs Hajdics

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.

-----------------------------------------------------------------------------
*/

/// \file XNPPrerequisites.h
/// It is important to include this file *before* tinyxml.h.
/// Copyright (C) 2007 Balazs Hajdics 


#ifndef __XNPPrerequisites_H__
#define __XNPPrerequisites_H__


#include <map>
#include <vector>
#include <set>
#include <string>

	// TODO: FIXTHIS:
	/// HUGE HACK!!! This is temporary until a common namespace becomes implemented with common-usage classes like Exception,
	/// LogManager, Log, SharedPtr, Singleton, etc.
		//#include "Ogre.h"
		//namespace Common = Ogre;
		#define EXCEPT OGRE_EXCEPT

		//#include "Common.h"
		//using namespace Common;

	// Temporarily, as including the whole Ogre.h would result ugly name collisions:
		//#include "OgreException.h"

	//-------------------

namespace XmlNodeProcessing {


	/// Since currently even XmlNodeProcessor itself relies on std::string, ask tinyxml to do it the same way.
	#define TIXML_USE_STL

//----------------------------------------------------------------------------
// Windows Settings

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	// If we're not including this from a client build, specify that the stuff
	// should get exported. Otherwise, import it.
#	if defined( XMLNODEPROCESSING_BUILDMODE_LIB )
	// Linux compilers don't have symbol import/export directives.
#   	define _XmlNodeProcessing_BuildMode
#   	define _XmlNodeProcessing_BuildModePrivate
#   else
#   	if defined( XMLNODEPROCESSING_BUILDMODE_DLL_EXPORT )
#       	define _XmlNodeProcessing_BuildMode __declspec( dllexport )
#   	else
#           if defined( __MINGW32__ )
#               define _XmlNodeProcessing_BuildMode
#           else
#       	    define _XmlNodeProcessing_BuildMode __declspec( dllimport )
#           endif
#   	endif
#   	define _XmlNodeProcessing_BuildModePrivate
#	endif
#endif // #if OGRE_PLATFORM == OGRE_PLATFORM_WIN32

#ifndef WIN32 
#define _XmlNodeProcessing_BuildMode
#define _XmlNodeProcessing_BuildModePrivate
#endif
	typedef std::string			String;
	typedef std::vector<String> StringVector;



	// ---------------------------------------------------- Pre-declare classes ----------------------------------------------------
	class XmlNodeProcessor;
	class CommonParamsOfXmlNodeProcessors;
	class XmlNodeProcessorTreeBuilder;
}


#endif
