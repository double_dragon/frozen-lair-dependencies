/*
-----------------------------------------------------------------------------
Author:		Tibor Tihon (tibortihon@gmail.com), Balazs Hajdics (wolverine_@freemail.hu), James Le Cuirot
Copyright (c) 2007 Tibor Tihon

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
-----------------------------------------------------------------------------
*/

///	@file XNPDemo.cpp
///	@brief Demo application for XmlNodeProcessor, a processor base class for TinyXML nodes.
///
///	@author Tibor Tihon

#include "XnpXmlNodeProcessor.h"

/// Test processor class that writes out the attributes of elements and makes notes about the text nodes, too.
class XmlTestProcessor : public XmlNodeProcessing::XmlNodeProcessor
{
public:
	virtual void* parseElementImpl(XmlNodeProcessing::XmlNodeProcessor* parent, TiXmlElement* currentNode)
	{
		printf("%s", currentNode->Value());
		TiXmlAttribute* attr = currentNode->FirstAttribute();
		while ( attr != NULL )
		{
			printf(" %s=\"%s\"", attr->Name(), attr->Value());
			attr = attr->Next();
		}
		printf("\n");
		return NULL;
	}

	virtual void parseElementEndImpl(XmlNodeProcessing::XmlNodeProcessor* parent, TiXmlElement* currentNode)
	{
		printf("/%s\n", currentNode->Value());
	}

	virtual void* parseTextImpl(XmlNodeProcessor* parent, TiXmlText* currentNode)
	{
		printf("TEXT: %s\n", currentNode->Value());
		return NULL;
	}
};

void test1()
{
	TiXmlDocument doc("test.scene");
	if (!doc.LoadFile())
		return;

	XmlTestProcessor* proc = new XmlTestProcessor();
	proc->addElementProcessor("environment", proc);
	proc->addElementProcessor("scene", proc);
	// if you comment this line, only environment and scene elements will be parsed
	proc->addElementProcessor("", proc);
	// if you uncomment this line, every text and comment nodes are also parsed
	// proc->addDefaultProcessor(proc);
	proc->parseElement( NULL, doc.FirstChild("scene")->ToElement() );
	delete proc;
}

void test2()
{
	TiXmlDocument doc("test.xml");
	if (!doc.LoadFile())
		return;

	XmlTestProcessor* proc = new XmlTestProcessor();
	proc->addDefaultProcessor(proc);
	proc->parseElement(NULL, doc.FirstChild("ToDo")->ToElement());
	delete proc;
}

/// These classes test if different processor classes can work together.
class XmlNodesProcessor : public XmlNodeProcessing::XmlNodeProcessor
{
public:
	virtual void* parseElementImpl(XmlNodeProcessing::XmlNodeProcessor* parent, TiXmlElement* currentNode)
	{
		printf("-=nodes=-\n");
		printf("%s", currentNode->Value());
		TiXmlAttribute* attr = currentNode->FirstAttribute();
		while ( attr != NULL )
		{
			printf(" %s=\"%s\"", attr->Name(), attr->Value());
			attr = attr->Next();
		}
		printf("\n");
		return NULL;
	}

	virtual void parseElementEndImpl(XmlNodeProcessing::XmlNodeProcessor* parent, TiXmlElement* currentNode)
	{
		printf("/%s\n", currentNode->Value());
	}
};

class XmlSceneProcessor : public XmlNodeProcessing::XmlNodeProcessor
{
protected:
	XmlNodesProcessor* m_pNodesProcessor;

public:
	XmlSceneProcessor()
	{
		addElementProcessor("nodes", m_pNodesProcessor = new XmlNodesProcessor() );
	}

	~XmlSceneProcessor()
	{
		removeElementProcessor("nodes");
		delete m_pNodesProcessor;
	}

	virtual void* parseElementImpl(XmlNodeProcessing::XmlNodeProcessor* parent, TiXmlElement* currentNode)
	{
		printf("-=scene=-\n");
		printf("%s", currentNode->Value());
		TiXmlAttribute* attr = currentNode->FirstAttribute();
		while ( attr != NULL )
		{
			printf(" %s=\"%s\"", attr->Name(), attr->Value());
			attr = attr->Next();
		}
		printf("\n");
		return NULL;
	}

	virtual void parseElementEndImpl(XmlNodeProcessing::XmlNodeProcessor* parent, TiXmlElement* currentNode)
	{
		printf("/%s\n", currentNode->Value());
	}
};

void test3()
{
	TiXmlDocument doc("test.scene");
	if (!doc.LoadFile())
		return;

	XmlSceneProcessor* proc = new XmlSceneProcessor();
	proc->parseElement(NULL, doc.FirstChild("scene")->ToElement());
	delete proc;
}

class XmlTestCommentProcessor : public XmlNodeProcessing::XmlNodeProcessor
{
public:
	virtual void* parseCommentImpl(XmlNodeProcessing::XmlNodeProcessor* parent, TiXmlComment* currentNode)
	{
		printf("Comment: %s\n", currentNode->Value());
		return (void*)currentNode->Value();
	}
};

class Listener : public XmlNodeProcessing::XmlNodeProcessor::ParseListener
{
	void preElementParse(XmlNodeProcessing::XmlNodeProcessor* processor, TiXmlElement* currentNode)
	{
		printf("Element--->\n");
	}

	void postElementParse(XmlNodeProcessing::XmlNodeProcessor* processor, TiXmlElement* currentNode)
	{
		printf("--->Element\n");
	}

	void preCommentParse(XmlNodeProcessing::XmlNodeProcessor* processor, TiXmlComment* currentNode)
	{
		printf("Comment--->\n");
	}

	void postCommentParse(XmlNodeProcessing::XmlNodeProcessor* processor, TiXmlComment* currentNode)
	{
		printf("--->Comment\n");
	}

	void preTextParse(XmlNodeProcessing::XmlNodeProcessor* processor, TiXmlText* currentNode)
	{
		printf("Text--->\n");
	}

	void postTextParse(XmlNodeProcessing::XmlNodeProcessor* processor, TiXmlText* currentNode)
	{
		printf("--->Text\n");
	}
};

void testParseBad()
{
	TiXmlDocument doc("test.scene");
	if (!doc.LoadFile())
		return;

	XmlTestProcessor* proc = new XmlTestProcessor();
	XmlTestCommentProcessor* procComment = new XmlTestCommentProcessor();
	proc->addDefaultProcessor(proc);
	proc->addCommentProcessor(procComment);
	Listener* lstnr = new Listener();
	proc->addParseListener(lstnr);
//	procComment->addParseListener(lstnr);
	proc->parseElement(NULL, doc.FirstChild("scene")->ToElement());
	delete lstnr;
	delete proc;
}

/// The next demo demonstrates serializing. The data to be serialized should
///	be also defined, though. That follows here. Please don't search for much
///	logic in the XML node structure of the example.
class Player
{
protected:
	std::string mName;
	int			mHeight;

public:
	Player(std::string name, int height) : mName( name ), mHeight( height )
	{
	}

	std::string name() const
	{
		return mName;
	}

	int height() const
	{
		return mHeight;
	}
};

class Club
{
protected:
	std::string mName;
	std::vector< Player* > mPlayers;
	std::string mManager;

public:
	Club(std::string name, std::string manager) : mName( name ), mManager( manager )
	{
	}

	~Club()
	{
		std::vector< Player* >::iterator i;
		i = mPlayers.begin();
		while (i != mPlayers.end())
		{
			delete *i;
			++i;
		}
	}

	void addNewPlayer(std::string playerName, int playerHeight)
	{
		mPlayers.push_back(new Player(playerName, playerHeight));
	}

	std::string name() const
	{
		return mName;
	}

	std::string manager() const
	{
		return mManager;
	}

	Player* player(size_t number) const
	{
		return mPlayers[number];
	}

	size_t numberOfPlayers() const
	{
		return mPlayers.size();
	}
};

/// Now the various processor classes.

class XmlHeightProcessor : public XmlNodeProcessing::XmlNodeProcessor
{
public:
	void* serializeElementImpl(bool& hasSiblingsToSerialize, TiXmlElement& currentNode, void* parentData, size_t counter)
	{
		Player* player = static_cast<Player*>(parentData);
		char buffer[12];
		_itoa_s(player->height(), buffer, 10);

		TiXmlText text(buffer);
		currentNode.InsertEndChild(text);
		return player;
	}
};

class XmlPlayerProcessor : public XmlNodeProcessing::XmlNodeProcessor
{
protected:
	XmlHeightProcessor*	mHeightProcessor;

public:
	XmlPlayerProcessor()
	{
		addElementProcessor("height", mHeightProcessor = new XmlHeightProcessor());
	}

	~XmlPlayerProcessor()
	{
		delete mHeightProcessor;
	}

	virtual bool hasNodesToSerialize(void* parentData)
	{
		return static_cast<Club*>(parentData)->numberOfPlayers() > 0;
	}

	void* serializeElementImpl(bool& hasSiblingsToSerialize, TiXmlElement& currentNode, void* parentData, size_t counter)
	{
		Club* club = static_cast<Club*>(parentData);
		Player* player = club->player(counter);
		currentNode.SetAttribute("name", player->name().c_str());

		hasSiblingsToSerialize = counter < club->numberOfPlayers() - 1;

		return player;
	}
};

class XmlManagerProcessor : public XmlNodeProcessing::XmlNodeProcessor
{
	void* serializeElementImpl(bool& hasSiblingsToSerialize, TiXmlElement& currentNode, void* parentData, size_t counter)
	{
		Club* club = static_cast<Club*>(parentData);
		TiXmlText text(club->manager().c_str());
		currentNode.InsertEndChild(text);
		
		hasSiblingsToSerialize = false;
		return club;
	}
};

class XmlCommentProcessor : public XmlNodeProcessing::XmlNodeProcessor
{
public:
	void* serializeCommentImpl(bool& hasSiblingsToSerialize, TiXmlComment& currentNode, void* parentData, size_t counter)
	{
		currentNode.SetValue("Good team!");
		return parentData;
	}
};

class XmlClubProcessor : public XmlNodeProcessing::XmlNodeProcessor
{
protected:
	XmlPlayerProcessor* mPlayerProcessor;
	XmlManagerProcessor* mManagerProcessor;
	XmlCommentProcessor* mCommentProcessor;

public:
	XmlClubProcessor()
	{
		addCommentProcessor(mCommentProcessor = new XmlCommentProcessor());
		addElementProcessor("manager", mManagerProcessor = new XmlManagerProcessor());
		addElementProcessor("player", mPlayerProcessor = new XmlPlayerProcessor());
	}

	~XmlClubProcessor()
	{
		delete mPlayerProcessor;
		delete mManagerProcessor;
		delete mCommentProcessor;
	}

	void* serializeElementImpl(bool& hasSiblingsToSerialize, TiXmlElement& currentNode, void* parentData, size_t counter)
	{
		currentNode.SetAttribute("name", static_cast<Club*>(parentData)->name().c_str());
		hasSiblingsToSerialize = false;
		return parentData;
	}
};

/// See if serialization works.
void test4()
{
	Club* club = new Club("AC Milan", "Carlo Ancelotti");
	club->addNewPlayer("Ronaldo", 177);
	club->addNewPlayer("Dida", 198);
	XmlClubProcessor* proc = new XmlClubProcessor();
	TiXmlDocument doc;
	proc->serializeRootElement("club", doc, club);
	doc.SaveFile("testSerialized.xml");
	delete proc;
	delete club;
}

int main(int argc, char* argv[])
{
// 	test1();
// 	test2();
// 	test3();
// 	test4();
	testParseBad();
	return 0;
}