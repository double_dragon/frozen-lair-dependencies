/*
-----------------------------------------------------------------------------
Author:		Balazs Hajdics (wolverine_@freemail.hu), James Le Cuirot

Copyright (C) 2007 Balazs Hajdics

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.

-----------------------------------------------------------------------------
*/

/// Copyright (C) 2007 Balazs Hajdics 

#include "XNPCommonParamsOfXmlNodeProcessors.h"
#include "XNPPrerequisites.h"

namespace XmlNodeProcessing {

		void CommonParamsOfXmlNodeProcessors::setParameter(const String& paramName, void* param)
		{
			ParameterMap::iterator it = mParameterMap.find(paramName);

			if ( it == mParameterMap.end() )
			{
				mParameterMap[paramName] = param;		// Add to parameters map.
				mParameterNames.push_back(paramName);	// Save parameter name.
			}
			else
			{
				it->second = param;
			}
		}

		void CommonParamsOfXmlNodeProcessors::removeParameter(const String& paramName)
		{
			mParameterMap.erase(paramName);
			ParameterNames::iterator it;
			for (it = mParameterNames.begin(); *it != paramName && it != mParameterNames.end(); it++);
			mParameterNames.erase(it);
		}

		void* CommonParamsOfXmlNodeProcessors::getParameter(const String& paramName) const
		{
			ParameterMap::const_iterator it = mParameterMap.find(paramName);

			if ( it == mParameterMap.end() )
			{
				// Paremeter not found.
				// TODO: design question: should we throw an exeption or just simply return with a null pointer.
				// For deciding see usage of classes sub-classes from XmlNodeProcessorTreeBuilder.
				// Currently it seems a better solution to just simply return a null pointer.

				//EXCEPT(	Exception::ERR_ITEM_NOT_FOUND,
				//		"Parameter \"" + paramName + "\" was not found!\n"
				//		"This is because the parameter was never set before using CommonParamsOfXmlNodeProcessors::setParameter()\n"
				//		"You must first set a parameter before trying to acquire it.\n",
				//		"dotSceneCommonParamsOfXmlNodeProcessors::getParameter()");

				return 0;
			}

			return it->second;
		}
}