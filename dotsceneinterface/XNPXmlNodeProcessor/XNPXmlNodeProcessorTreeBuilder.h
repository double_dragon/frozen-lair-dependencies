/*
-----------------------------------------------------------------------------
Author:		Balazs Hajdics (wolverine_@freemail.hu), James Le Cuirot

Copyright (C) 2007 Balazs Hajdics

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.

-----------------------------------------------------------------------------
*/

/// Copyright (C) 2007 Balazs Hajdics 


#ifndef __XNPXmlNodeProcessorTreeBuilder_H__
#define __XNPXmlNodeProcessorTreeBuilder_H__

#include "XNPPrerequisites.h"
#include "XNPCommonParamsOfXmlNodeProcessors.h"
#include "XNPXmlNodeProcessor.h"


namespace XmlNodeProcessing {

	/// @brief Abstract class defining interface for building a hierarchy from instances of derived classes of XmlNodeProcessor in
	/// order to process an xml with a given specification (.dtd file).
	///
	/// @note Notes to building xml node processing trees:
	///		- If more then one element type can contain the same element types, then register different instances of the same
	///		  XmlNodeProcessor subclasses to them. Otherwise an error could occur in the following case. Given xml elements named
	///		  "A", "B", "C". "A" can optionally contain "B" and "C", while "C" must contain "A". If the same parser instance for "A"
	///		  is used for all elements, then when parsing the element "A" which is contained by element "C", the mParsedNode member
	///		  variable will be overwritten with the new parsed value. However parseEndImpl() function of the top level "A" element 
	///		  will be called after all sub-elements in the top level "A" are parsed. This function may operate on member variable
	///		  mParsedNode, which may consist a wrong value.
	///		  
	class _XmlNodeProcessing_BuildMode XmlNodeProcessorTreeBuilder
	{
	public:
		typedef	std::vector<XmlNodeProcessor*>				ProcessorVector;

	protected:
		typedef std::map<std::string, ProcessorVector>			NamesToProcessorsMap;
		typedef std::map<XmlNodeProcessor*, ProcessorVector>	ChildrenToParentsMap;


	public:
		XmlNodeProcessorTreeBuilder(CommonParamsOfXmlNodeProcessors* commonParams):
			mCommonParamsOfXmlNodeProcessors(commonParams),
			mRoot(0)
		{

		}

		virtual		~XmlNodeProcessorTreeBuilder() {};

		/// \brief
		///
		/// @remarks Common parameters must be set *before* calling buildTree().
		/// @returns The root xml node processor capable of processing an xml with the given structure.
		virtual		XmlNodeProcessor*					buildTree()=0;
		virtual		void								destroyTree()=0;

		CommonParamsOfXmlNodeProcessors*	getCommonParamsOfXmlNodeProcessors() 
		{ 
			return mCommonParamsOfXmlNodeProcessors;
		}
		void								setCommonParamsOfXmlNodeProcessors(CommonParamsOfXmlNodeProcessors* commonParameters)
		{ 
			mCommonParamsOfXmlNodeProcessors = commonParameters;
		}

		virtual	void	setCommonParameter(const String& paramName, void* param)
		{ 
			mCommonParamsOfXmlNodeProcessors->setParameter(paramName, param);
		}
		virtual void*	getCommonParameter(const String& paramName)
		{
			return mCommonParamsOfXmlNodeProcessors->getParameter(paramName);
		}
		virtual void	removeCommonParameter(const String& paramName)
		{
			return mCommonParamsOfXmlNodeProcessors->removeParameter(paramName);
		}
		virtual const CommonParamsOfXmlNodeProcessors::ParameterNames&	getCurrentlySetCommonParameters() const
		{
			return mCommonParamsOfXmlNodeProcessors->getCurrentlySetParameters();
		}

		virtual ProcessorVector		getProcessorsByName(const String& name);
		virtual ProcessorVector		getParents(XmlNodeProcessor *pChild);
		virtual void				addParseListener(XmlNodeProcessor::ParseListener *pListener);
		virtual void				removeParseListener(XmlNodeProcessor::ParseListener *pListener);

		void						unregisterElementProcessors(const StringVector& tags);
		void						unregisterElementProcessor(const String& name);

	protected:
		void	registerElementProcessor(const String& name, XmlNodeProcessor* pParent, XmlNodeProcessor* pChild);

		void	registerRootProcessor(const String& name, XmlNodeProcessor* pRoot);

	protected:
		CommonParamsOfXmlNodeProcessors*			mCommonParamsOfXmlNodeProcessors;

		NamesToProcessorsMap						mNamesToProcessorsMap;
		ChildrenToParentsMap						mChildrenToParentsMap;
		ProcessorVector								mProcessors;
		XmlNodeProcessor*							mRoot;
	};
}



#endif