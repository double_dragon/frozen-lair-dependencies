/*
-----------------------------------------------------------------------------
Theory:		Balazs Hajdics, Tibor Tihon
Author:		Tibor Tihon (tibortihon@gmail.com), Balazs Hajdics (wolverine_@freemail.hu), James Le Cuirot, Ihor Tregubov

Copyright (C) 2007 Tibor Tihon, Balazs Hajdics

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
-----------------------------------------------------------------------------
*/

///	@file XNPXmlNodeProcessor.cpp
///	@brief Source for XmlNodeProcessor, a processor base class for TinyXML nodes.
///
///	@author Tibor Tihon, Balazs Hajdics


#include "XNPXmlNodeProcessor.h"
#include "XNPPrerequisites.h"
#include "XNPXmlNodeProcessor.h"
#include <algorithm>

/// XmlNodeProcessor is a general purpose xml processor class, so place it in its own namespace.
namespace XmlNodeProcessing {

	String XmlNodeProcessor::BLANK_STRING = "";


	const String&	XmlNodeProcessor::getParentRegisteredNameOfNodeProcessor(XmlNodeProcessor* parent, XmlNodeProcessor* child)
	{
		return parent->getElementProcessorName(child);
	}

	XmlNodeProcessor::XmlNodeProcessor():
		mTiXmlNode(0),
		mTextProcessor(0),
		mCommentProcessor(0),
		mDefaultProcessor(0),

		mTreeBuilder(0)
	{
	}

	void XmlNodeProcessor::removeFromProcessorVector(XmlNodeProcessor* processor)
	{
		XmlNodeProcessorVector::iterator node = mNodeProcessorVector.begin();
		while (node != mNodeProcessorVector.end() )
		{
			if ((*node).first == processor)
			{
				mNodeProcessorVector.erase(node);
				return;
			}
			++node;
		}
	}

	void XmlNodeProcessor::addElementProcessor(const std::string& elementName, XmlNodeProcessor* processor)
	{
		XmlNodeProcessorMap::iterator node = mElementProcessorMap.find(elementName);
		if (node != mElementProcessorMap.end())
			removeFromProcessorVector((*node).second);

		mElementProcessorMap[elementName] = processor;
		mNodeProcessorVector.push_back(XmlNodeProcessorVectorItem(processor, elementName));
	}
	XmlNodeProcessor* XmlNodeProcessor::getElementProcessor(const String& elementName)
	{
		XmlNodeProcessorMap::iterator it = mElementProcessorMap.find(elementName);
		if (it == mElementProcessorMap.end()) 
		{
			return 0;
			/*EXCEPT(
				Ogre::Exception::ERR_ITEM_NOT_FOUND,
				"XmlNodeProcessor element \"" + elementName + "\" does not exist.",
				"XmlNodeProcessor::getElementProcessor()");*/
		}else return it->second;
	}
	const String& XmlNodeProcessor::getElementProcessorName(XmlNodeProcessor*	processor)
	{
		XmlNodeProcessorMap::iterator it;
		// Now search for the element;
		for ( it =  mElementProcessorMap.begin(); (it->second != processor && it != mElementProcessorMap.end()); it++ );
		if ( it == mElementProcessorMap.end() )
		{
			// TODO: Element processor not found. Throw and exception or log something if you wish.

			return XmlNodeProcessor::BLANK_STRING;
		}else
			return it->first;
	}
	bool XmlNodeProcessor::removeElementProcessor(const std::string& elementName)
	{
		XmlNodeProcessorMap::iterator node = mElementProcessorMap.find(elementName);
		if (node == mElementProcessorMap.end())
			return false;

		removeFromProcessorVector((*node).second);
		mElementProcessorMap.erase(node);

		return true;
	}

	void XmlNodeProcessor::addCommentProcessor(XmlNodeProcessor* processor)
	{
		if (mCommentProcessor != NULL)
			removeFromProcessorVector(mCommentProcessor);

		mCommentProcessor = processor;
		mNodeProcessorVector.push_back(XmlNodeProcessorVectorItem(processor, ""));
	}

	bool XmlNodeProcessor::removeCommentProcessor()
	{
		if (mCommentProcessor == NULL)
			return false;

		removeFromProcessorVector(mCommentProcessor);
		mCommentProcessor = NULL;
		return true;
	}

	void XmlNodeProcessor::addTextProcessor(XmlNodeProcessor* processor)
	{
		if (mTextProcessor != NULL)
			removeFromProcessorVector(mTextProcessor);

		mTextProcessor = processor;
		mNodeProcessorVector.push_back(XmlNodeProcessorVectorItem(processor, ""));
	}

	bool XmlNodeProcessor::removeTextProcessor()
	{
		if (mTextProcessor == NULL)
			return false;

		removeFromProcessorVector(mTextProcessor);
		mTextProcessor = NULL;
		return true;
	}

	void XmlNodeProcessor::addDefaultProcessor(XmlNodeProcessor* processor)
	{
		if (mDefaultProcessor != NULL)
			removeFromProcessorVector(mDefaultProcessor);

		mDefaultProcessor = processor;
		mNodeProcessorVector.push_back(XmlNodeProcessorVectorItem(processor, ""));
	}

	bool XmlNodeProcessor::removeDefaultProcessor()
	{
		if (mDefaultProcessor == NULL)
			return false;

		removeFromProcessorVector(mDefaultProcessor);
		mDefaultProcessor = NULL;
		return true;
	}

	void XmlNodeProcessor::removeAllProcessors()
	{
		mNodeProcessorVector.clear();
		mElementProcessorMap.clear();
		mCommentProcessor = NULL;
		mTextProcessor = NULL;
		mDefaultProcessor = NULL;
	}

	void* XmlNodeProcessor::parseElement(XmlNodeProcessor* parent, TiXmlElement* currentNode)
	{
		mTiXmlNode = currentNode;
		assert(currentNode != NULL); /// NOTE: an exception might be thrown.

		mProcessedElementsMapStack.push(ProcessedNodesMap());
		mProcessedCommentsStack.push(ProcessedNodesVector());
		mProcessedTextsStack.push(ProcessedNodesVector());

		for (std::set<ParseListener*>::iterator l = mParseListeners.begin(); l != mParseListeners.end(); ++l)
			if (*l != NULL)
				(*l)->preElementParse(this, currentNode);

		void* parsedNode = parseElementImpl(parent, currentNode);
		mParsedNodes.push( parsedNode );

		XmlNodeProcessor* defaultElementProcessor = NULL;
		XmlNodeProcessorMap::iterator i = mElementProcessorMap.find("");
		if (i != mElementProcessorMap.end())
			defaultElementProcessor = (*i).second;

		TiXmlNode* child = currentNode->FirstChild();
		while (child != NULL)
		{
			switch (child->Type())
			{
			case TiXmlNode::ELEMENT:
				{
					TiXmlElement* element = child->ToElement();
					XmlNodeProcessorMap::iterator i = mElementProcessorMap.find(element->Value());
					if (i != mElementProcessorMap.end())
					{
						void* processResult = (*i).second->parseElement(this, element);
						mProcessedElementsMapStack.top()[element->Value()].push_back(processResult);
					}
					else if (defaultElementProcessor != NULL)
					{
						void* processResult = defaultElementProcessor->parseElement(this, element);
						mProcessedElementsMapStack.top()[element->Value()].push_back(processResult);
					}
					else if (mDefaultProcessor != NULL)
					{
						void* processResult = mDefaultProcessor->parseElement(this, element);
						mProcessedElementsMapStack.top()[element->Value()].push_back(processResult);
					}
				}
				break;
			case TiXmlNode::COMMENT:
				{
					if (mCommentProcessor != NULL)
					{
						void* processResult = mCommentProcessor->parseComment(this, child->ToComment());
						mProcessedCommentsStack.top().push_back(processResult);
					}
					else if (mDefaultProcessor != NULL)
					{
						void* processResult = mDefaultProcessor->parseComment(this, child->ToComment());
						mProcessedCommentsStack.top().push_back(processResult);
					}
				}
				break;
			case TiXmlNode::TEXT:
				{
					if (mTextProcessor != NULL)
					{
						void* processResult = mTextProcessor->parseText(this, child->ToText());
						mProcessedTextsStack.top().push_back(processResult);
					}
					else if (mDefaultProcessor != NULL)
					{
						void* processResult = mDefaultProcessor->parseText(this, child->ToText());
						mProcessedTextsStack.top().push_back(processResult);
					}
				}
				break;
			case TiXmlNode::DOCUMENT:
			case TiXmlNode::DECLARATION:
			case TiXmlNode::UNKNOWN:
				// TODO: could be implemented if needed
				break;
			}
			child = currentNode->IterateChildren(child);
		}

		parseElementEndImpl(parent, currentNode);

		for (std::set<ParseListener*>::iterator l = mParseListeners.begin(); l != mParseListeners.end(); ++l)
			if (*l != NULL)
				(*l)->postElementParse(this, currentNode);

		mParsedNodes.pop();
		mProcessedElementsMapStack.pop();
		mProcessedCommentsStack.pop();
		mProcessedTextsStack.pop();

		return parsedNode;
	}

	void* XmlNodeProcessor::parseComment(XmlNodeProcessor* parent, TiXmlComment* currentNode)
	{
		for (std::set<ParseListener*>::iterator l = mParseListeners.begin(); l != mParseListeners.end(); ++l)
			if (*l != NULL)
				(*l)->preCommentParse(this, currentNode);

		void* parsedComment = parseCommentImpl(parent, currentNode);
		mParsedNodes.push( parsedComment );

		for (std::set<ParseListener*>::iterator l = mParseListeners.begin(); l != mParseListeners.end(); ++l)
			if (*l != NULL)
				(*l)->postCommentParse(this, currentNode);

		mParsedNodes.pop();

		return parsedComment;
	}

	void* XmlNodeProcessor::parseText(XmlNodeProcessor* parent, TiXmlText* currentNode)
	{
		for (std::set<ParseListener*>::iterator l = mParseListeners.begin(); l != mParseListeners.end(); ++l)
			if (*l != NULL)
				(*l)->preTextParse(this, currentNode);
		
		void* parsedText = parseTextImpl(parent, currentNode);
		mParsedNodes.push( parseTextImpl(parent, currentNode) ); 

		for (std::set<ParseListener*>::iterator l = mParseListeners.begin(); l != mParseListeners.end(); ++l)
			if (*l != NULL)
				(*l)->postTextParse(this, currentNode);

		mParsedNodes.pop();

		return parsedText;
	}

	TiXmlNode* XmlNodeProcessor::getTiXmlNode()
	{
		return mTiXmlNode;
	}

	void* XmlNodeProcessor::getParsedNode()
	{
		return mParsedNodes.size() > 0 ? mParsedNodes.top() : NULL;
	}

	void XmlNodeProcessor::serializeChildren(const XmlNodeProcessorVectorItem& item, TiXmlElement& currentNode, void* pParentData)
	{
		XmlNodeProcessor* processor = item.first;
		if (processor == mTextProcessor)
		{
                    TiXmlText childText("");
                    if( item.first->serializeText( childText, pParentData ) )
                    {
                          currentNode.InsertEndChild(childText);
                    }
                    /*
			bool hasSiblingsToSerialize = processor->hasNodesToSerialize(currentData);
			while (hasSiblingsToSerialize)
			{
				hasSiblingsToSerialize = false;
				TiXmlText childText("");
				item.first->serializeText(hasSiblingsToSerialize, childText, currentData, counter++);
				currentNode.InsertEndChild(childText);
			}
                    */
		}
		else if (processor == mCommentProcessor)
		{
                     TiXmlComment childComment;
                     if( item.first->serializeComment( childComment, pParentData ) )
                     {
                          currentNode.InsertEndChild(childComment);
                     }
                     /*
			hasSiblingsToSerialize = false;
//				TiXmlComment childComment(""); // Old version uses new version of tiny xml.
			TiXmlComment childComment;
			item.first->serializeComment(hasSiblingsToSerialize, childComment, currentData, counter++);
			currentNode.InsertEndChild(childComment);
                     */
		}
		else
		{
                     item.first->serializeElement( item.second.c_str(), currentNode, pParentData );
                     /*
			bool hasSiblingsToSerialize = processor->hasNodesToSerialize(currentData);
			while (hasSiblingsToSerialize)
			{
				hasSiblingsToSerialize = false;
				TiXmlElement childElement(item.second.c_str());
				item.first->serializeElement(hasSiblingsToSerialize, childElement, currentData, counter++);
				currentNode.InsertEndChild(childElement);
			}
                    */
		}
	}

	bool XmlNodeProcessor::serializeElement( const char* strElementName, TiXmlElement& parentElement, void* pParentData )
	{
        bool bResult = false;

        size_t counter = 0;

        XmlNodeProcessor::ElementsDataList elementsDataList;
        if( serializeElementBeginImpl( parentElement.Value(), pParentData, &elementsDataList ) )
        {
            while( elementsDataList.size() )
            {
                TiXmlElement NewElement( strElementName );

                for (std::set<SerializeListener*>::iterator l = mSerializeListeners.begin(); l != mSerializeListeners.end(); ++l)
                    if (*l != NULL)
                        (*l)->preElementSerialize(this, NewElement);

                void* pElementData = elementsDataList.front();

                serializeElementImpl( NewElement, parentElement, pElementData, counter++);

                for (XmlNodeProcessorVector::iterator i = mNodeProcessorVector.begin(); i != mNodeProcessorVector.end(); ++i)
                    serializeChildren(*i, NewElement, pElementData);

                if (mDefaultProcessor != NULL)
                    serializeChildren( XmlNodeProcessorVectorItem(mDefaultProcessor, ""), NewElement, pElementData );

                serializeElementEndImpl( NewElement, pParentData, pElementData);

                //! End listeners
                for (std::set<SerializeListener*>::iterator l = mSerializeListeners.begin(); l != mSerializeListeners.end(); ++l)
                    if (*l != NULL)
                        (*l)->postElementSerialize(this, NewElement );

                elementsDataList.pop_front();
                
                parentElement.InsertEndChild( NewElement );
            }
            
        }
		return bResult;
	}

	void* XmlNodeProcessor::serializeComment( TiXmlComment& currentNode, void* parentData )
	{
		for (std::set<SerializeListener*>::iterator l = mSerializeListeners.begin(); l != mSerializeListeners.end(); ++l)
			if (*l != NULL)
				(*l)->preCommentSerialize(this, currentNode);

		void* currentData = serializeCommentImpl( currentNode, parentData );

		for (std::set<SerializeListener*>::iterator l = mSerializeListeners.begin(); l != mSerializeListeners.end(); ++l)
			if (*l != NULL)
				(*l)->postCommentSerialize(this, currentNode);

		return currentData;
	}

	void* XmlNodeProcessor::serializeText( TiXmlText& currentNode, void* parentData )
	{
		for (std::set<SerializeListener*>::iterator l = mSerializeListeners.begin(); l != mSerializeListeners.end(); ++l)
			if (*l != NULL)
				(*l)->preTextSerialize(this, currentNode);

		void* currentData = serializeTextImpl( currentNode, parentData );

		for (std::set<SerializeListener*>::iterator l = mSerializeListeners.begin(); l != mSerializeListeners.end(); ++l)
			if (*l != NULL)
				(*l)->postTextSerialize(this, currentNode);

		return currentData;
	}

	bool XmlNodeProcessor::serializeRootElement( std::string elementName, TiXmlDocument& document, void* data)
	{
        return serializeElement( elementName.c_str(), (TiXmlElement&)document, data );
	}

	void XmlNodeProcessor::addParseListener(ParseListener* l)
	{
		mParseListeners.insert(l);
	}

	void XmlNodeProcessor::removeParseListener(ParseListener* l)
	{
		mParseListeners.erase(l);
	}

	void XmlNodeProcessor::addSerializeListener(SerializeListener* l)
	{
		mSerializeListeners.insert(l);
	}

	void XmlNodeProcessor::removeSerializeListener(SerializeListener* l)
	{
		mSerializeListeners.erase(l);
	}
}
