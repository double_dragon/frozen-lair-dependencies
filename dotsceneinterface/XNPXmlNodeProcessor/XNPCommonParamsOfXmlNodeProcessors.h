/*
-----------------------------------------------------------------------------
Author:		Balazs Hajdics (wolverine_@freemail.hu), James Le Cuirot

Copyright (C) 2007 Balazs Hajdics

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.

-----------------------------------------------------------------------------
*/

#ifndef __XNPCommonParamsOfXmlNodeProcessors_H__
#define __XNPCommonParamsOfXmlNodeProcessors_H__

#include "XNPPrerequisites.h"

namespace XmlNodeProcessing {

	class _XmlNodeProcessing_BuildMode CommonParamsOfXmlNodeProcessors
	{
	public:
		typedef				std::map<String, void*>		ParameterMap;
		typedef				std::vector<String>			ParameterNames;

	protected:
		ParameterMap			mParameterMap;
		ParameterNames			mParameterNames;

	public:
		virtual void					setParameter(const String& paramName, void* param);
		virtual void					removeParameter(const String& paramName);
		/// Returns a pointer to the common parameter named by paramName.
		/// @Note If a parameter by name paramName wasn't set before a null pointer will be returned.
		/// Please set all parameters you want to use before calling XmlNodeProcessor::parseElement(). If a parameter will be
		/// created by one of the sub-element processors, then either set it to null in the common parameter list or ignore it at
		/// all.
		virtual void*					getParameter(const String& paramName) const;
		virtual const ParameterNames&	getCurrentlySetParameters() const
		{
			return mParameterNames;
		}
	};

}


#endif