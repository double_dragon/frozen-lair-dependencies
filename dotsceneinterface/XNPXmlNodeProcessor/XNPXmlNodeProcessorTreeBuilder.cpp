/*
-----------------------------------------------------------------------------
Theory:		Balazs Hajdics
Author:		David Vizi (tibortihon@gmail.com), Balazs Hajdics (wolverine_@freemail.hu), James Le Cuirot

Copyright (C) 2007 David Vizi, Balazs Hajdics

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
-----------------------------------------------------------------------------
*/

///	@file XNPXmlNodeProcessorTreeBuilder.cpp
///	@brief Source for XmlNodeProcessorTreeBuilder, ...
///
///	@author Balazs Hajdics, David Vizi


#include "XNPXmlNodeProcessor.h"
#include "XNPPrerequisites.h"
#include "XNPXmlNodeProcessorTreeBuilder.h"
#include <algorithm>

/// XmlNodeProcessorTreeBuilder relates to XmlNodeProcessor class, so place it into the namespace of XmlNodeProcessor.
namespace XmlNodeProcessing {


XmlNodeProcessorTreeBuilder::ProcessorVector XmlNodeProcessorTreeBuilder::getProcessorsByName(const String& name)
{
	return mNamesToProcessorsMap[name];
}

XmlNodeProcessorTreeBuilder::ProcessorVector XmlNodeProcessorTreeBuilder::getParents(XmlNodeProcessor *pChild)
{
	return mChildrenToParentsMap[pChild];
}

void XmlNodeProcessorTreeBuilder::addParseListener(XmlNodeProcessor::ParseListener *pListener)
{
	for (size_t i = 0; i < mProcessors.size(); ++i)
		mProcessors[i]->addParseListener(pListener);
}

void XmlNodeProcessorTreeBuilder::removeParseListener(XmlNodeProcessor::ParseListener *pListener)
{
	for (size_t i = 0; i < mProcessors.size(); ++i)
		mProcessors[i]->removeParseListener(pListener);
}


void XmlNodeProcessorTreeBuilder::registerElementProcessor(const String& name, XmlNodeProcessor *pParent, XmlNodeProcessor *pChild)
{
	XmlNodeProcessorTreeBuilder::ProcessorVector& namedVector = mNamesToProcessorsMap[name];
	if (std::find(namedVector.begin(), namedVector.end(), pChild) == namedVector.end())
		namedVector.push_back(pChild);
	mChildrenToParentsMap[pChild].push_back(pParent);
	pParent->addElementProcessor(name, pChild);

	// Add the child to the vector containing all processors.
	if (std::find(mProcessors.begin(), mProcessors.end(), pChild) == mProcessors.end())
		mProcessors.push_back(pChild);
}

void XmlNodeProcessorTreeBuilder::unregisterElementProcessor(const String& name)
{
	XmlNodeProcessorTreeBuilder::ProcessorVector& namedVector = mNamesToProcessorsMap[name];
	for (XmlNodeProcessorTreeBuilder::ProcessorVector::iterator it = namedVector.begin(); it != namedVector.end(); it++)
	{
		XmlNodeProcessorTreeBuilder::ProcessorVector& parents = mChildrenToParentsMap[*it];
		for (XmlNodeProcessorTreeBuilder::ProcessorVector::iterator parentIt = parents.begin(); parentIt != parents.end(); parentIt++)
		{
			(*parentIt)->removeElementProcessor(name);
		}
	}
	namedVector.clear();
}

void XmlNodeProcessorTreeBuilder::unregisterElementProcessors(const StringVector& tags)
{
	for (StringVector::const_iterator it = tags.begin(); it != tags.end(); it++)
	{
		unregisterElementProcessor(*it);
	}
}


void XmlNodeProcessorTreeBuilder::registerRootProcessor(const String& name, XmlNodeProcessor* pRoot)
{
	mRoot = pRoot;
	mNamesToProcessorsMap[name].push_back(pRoot);
	mProcessors.push_back(pRoot);
}

}