/*
-----------------------------------------------------------------------------
Theory:		Balazs Hajdics, Tibor Tihon
Author:		Tibor Tihon (tibortihon@gmail.com), Balazs Hajdics (wolverine_@freemail.hu), James Le Cuirot, Ihor Tregubov

Copyright (C) 2007 Tibor Tihon, Balazs Hajdics

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
-----------------------------------------------------------------------------
*/


///	@file XNPXmlNodeProcessor.h
///	@brief Header file for XmlNodeProcessor, a processor base class for TinyXML nodes.
/// Copyright (C) 2007 Tibor Tihon-Balazs Hajdics 
///
///	@author Tibor Tihon, Balazs Hajdics


#ifndef __XNPXmlNodeProcessor_H__
#define __XNPXmlNodeProcessor_H__

#include "XNPPrerequisites.h"

#include <map>
#include <vector>
#include <set>
#include <stack>
#include <string>
#include "tinyxml.h"
#include <list>

/// XmlNodeProcessor is a general purpose xml processor class, so place it in its own namespace.
namespace XmlNodeProcessing {

	///	@brief XmlNodeProcessor is a processor base class for TinyXML nodes.
	///
	///	@author Tibor Tihon, Balazs Hajdics
	///	@details
	///		It helps to parse from and serialize into TinyXML nodes.
	///		You have to inherit a class from it and implement one or more of the
	///		parseElementImpl(), parseCommentImpl(), parseTextImpl(),
	///		parseElementEndImpl(), serializeNodeImpl(), serializeElementEndImpl()
	///		member functions. They are not pure virtual functions but they do
	///		not do anything if not overridden.
	///
	///		One of its main advantages that it's very flexible. Any new tags can be
	///		processed easily without having to know how any of the other nodes are 
	///		processed. The only thing you have to do is to register an object for
	///		the processor of the new XML tag in the processor of the parent tag.
	class _XmlNodeProcessing_BuildMode XmlNodeProcessor
	{
		// -= Constructing & destructing =-

	public:
		XmlNodeProcessor();


		// -= Child processor handling =-

	public:
		typedef	std::map<std::string, XmlNodeProcessor*>	XmlNodeProcessorMap;
		typedef std::pair<XmlNodeProcessor*, std::string>	XmlNodeProcessorVectorItem;
		typedef	std::vector<XmlNodeProcessorVectorItem>		XmlNodeProcessorVector;

		typedef	std::vector<void*>							ProcessedNodesVector;
		typedef	std::stack<ProcessedNodesVector>			ProcessedNodesVectorStack;
		typedef std::map<std::string, ProcessedNodesVector>	ProcessedNodesMap;
		typedef	std::stack<ProcessedNodesMap>				ProcessedNodesMapStack;

		typedef	std::map<String, XmlNodeProcessor*>	XmlNodeProcessorMap;
		enum XmlAttributeDefaultDecl
		{
			XMLADD_IMPLIED,
			XMLADD_REQUIRED,
			XMLADD_DEFAULTVALUEPRESENT
		};

	protected:
		XmlNodeProcessorMap				mElementProcessorMap;
		XmlNodeProcessor*				mTextProcessor;
		XmlNodeProcessor*				mCommentProcessor;
		XmlNodeProcessor*				mDefaultProcessor;

		/// This vector keeps processor objects in their registration order.
		XmlNodeProcessorVector			mNodeProcessorVector;

		ProcessedNodesMapStack			mProcessedElementsMapStack;
		ProcessedNodesVectorStack		mProcessedCommentsStack;
		ProcessedNodesVectorStack		mProcessedTextsStack;

		/// Pointer to parent xml node processor tree builder.
		XmlNodeProcessorTreeBuilder*	mTreeBuilder;

	protected:
		///	\brief Helper function for removing a processor from the registered processors.
		///
		///	@note
		///		Maximum one instance gets removed, even if the given processor
		///		object is present more times in the vector.
		void removeFromProcessorVector(XmlNodeProcessor* processor);

	public:
		///	Registers a new XmlNodeProcessor for an element node with the specified element name.
		///	@note
		///		If a processor for the specified element name is already registered, it gets overriden.
		void addElementProcessor(const std::string& elementName, XmlNodeProcessor* processor);

		/// Returns the first element processor registered under the name passed in as parameter.
		XmlNodeProcessor*	getElementProcessor(const String& elementName);

		/// Returns the registered name of the element processor passed in as parameter.
		const String&		getElementProcessorName(XmlNodeProcessor*	processor);

		/// Unregisters a registered XmlNodeProcessor for an element node with the specified element name.
		bool removeElementProcessor(const std::string& elementName);

		/// Registers a new XmlNodeProcessor for all the comment nodes.
		void addCommentProcessor(XmlNodeProcessor* processor);

		/// Unregisters the XmlNodeProcessor for all the comment nodes.
		bool removeCommentProcessor();

		/// Registers a new XmlNodeProcessor for all the text nodes.
		void addTextProcessor(XmlNodeProcessor* processor);

		/// Unregisters the XmlNodeProcessor for all the text nodes.
		bool removeTextProcessor();

		/// Registers a new XmlNodeProcessor for all the nodes that don't have a registered processor for their kinds.
		void addDefaultProcessor(XmlNodeProcessor* processor);

		/// Unregisters the default XmlNodeProcessor.
		bool removeDefaultProcessor();

		/// Unregisters all the XmlNodeProcessor objects.
		void removeAllProcessors();


		// -= Parsing =-

	protected:
		/// Stack containing the pointers to parsed data.
		std::stack< void* >		mParsedNodes;

		/// Pointer to the Tiny XML node being parsed.
		TiXmlNode*	mTiXmlNode;

	public:
		///	Parses an element for which the processor object is registered. It does
		///	administration tasks, calls the redefineable parseElementImpl() function
		///	that makes the real parsing and calls the appropriate parsing function
		///	for the children XML nodes which a processor is registered for.
		///	@param
		///		parent 	The processor that called the function. In the most outside level
		///				(which is probably the only level where you have to call
		///				this function explicitly) it should be NULL.
		///		currentNode The node to be parsed.
		void* parseElement(XmlNodeProcessor* parent, TiXmlElement* currentNode);
		
	protected:
		///	An empty virtual function for parsing the element for which the processor
		///	object is registered for. It gets called back in the parseElement()
		///	function before any children nodes would be parsed. You could typically
		///	read the attributes of the XML element in this function of an inherited
		///	processor class. You might want deal with the children elements here but
		///	the architecture is such that it's recommended to leave that task for
		///	another processor class, registered appropriately.
		///	@param
		///		parent 	The processor that called the function. You can use it for getting
		///				the object the parent made, through its getParsedNode()
		///				function. In the most outside level its value is NULL.
		///				So if you override the function you can assume that NULL
		///				parent always means a most outside level element.
		///	@param
		///		currentNode The node to be parsed.
		///	@returns
		///		A pointer to the parsed data must be returned.
		virtual void* parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode) { return NULL; }

		///	An empty virtual function for parsing the end of the element for which the
		///	processor object is registered for. It gets called back in the
		///	parseElement() function after all the children nodes of the element have
		///	been parsed. You have to override it if there are finalizing tasks that
		///	couldn't be done before the children nodes have been parsed.
		///	@param
		///		parent 	The processor that called the function. You can use it for getting
		///				the object the parent made, through its getParsedNode()
		///				function. In the most outside level its value is NULL.
		///	@param
		///		currentNode The node to be parsed.
		virtual void parseElementEndImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode) {}

	public:
		///	Parses a comment node for which the processor object is registered. It does
		/// administration tasks and calls the redefineable parseCommentImpl()
		/// function that makes the real parsing.
		///	@param
		///		parent	The processor that called the function. In the most outside level
		/// 			(which is probably the only level where you have to call
		/// 			this function explicitly) it should be NULL.
		///	@param
		///		currentNode The node to be parsed.
		void* parseComment(XmlNodeProcessor* parent, TiXmlComment* currentNode);

	protected:
		///	An empty virtual function for parsing the comment node for which the processor
		///	object is registered for. It gets called back in the parseComment()
		///	function before any children nodes would be parsed.
		///	@param
		///		parent 	The processor that called the function. You can use it for getting
		///				the object the parent made, through its getParsedNode()
		///				function. In the most outside level its value is NULL.
		///	@param
		///		currentNode The node to be parsed.
		///	@returns
		///		A pointer to the parsed data should be returned.
		virtual void* parseCommentImpl(XmlNodeProcessor* parent, TiXmlComment* currentNode) { return NULL; }

	public:
		///	Parses a text node that the processor object is registered for. It does
		///	administration tasks and calls the redefineable parseTextImpl()
		///	function that makes the real parsing.
		///	@param
		///		parent 	The processor that called the function. In the most outside level
		/// 			(which is probably the only level where you have to call
		/// 			this function explicitly) it should be NULL.
		///	@param
		/// 	currentNode The node to be parsed.
		void* parseText(XmlNodeProcessor* parent, TiXmlText* currentNode);

	protected:
		///	An empty virtual function for parsing the text node for which the processor
		///	object is registered for. It gets called back in the parseText()
		///	function before any children nodes would be parsed.
		///	@param
		///		parent 	The processor that called the function. You can use it for getting
		///				the object the parent made, through its getParsedNode()
		///				function. In the most outside level its value is NULL.
		///	@param
		///		currentNode The node to be parsed.
		///	@returns
		///		A pointer to the parsed data should be returned.
		virtual void* parseTextImpl(XmlNodeProcessor* parent, TiXmlText* currentNode) { return NULL; }

	public:
		///	Returns the last parsed XML node.
		TiXmlNode* getTiXmlNode();

		///	Returns a pointer to the parsed data.
		void* getParsedNode();


	//!==================================Serialization================================
	protected:

		/// \brief Serializes children of current node. 
		///
		///	@param
		///		item
		///			Refrence on current element.
		///	@param
		///		parentElement
		///			Refrence on parent element.
		///	@param
		///		parentData
		///			Data of the parent.
		///	
		void serializeChildren(const XmlNodeProcessorVectorItem& item, TiXmlElement& parentElement, void* pParentData );

	public:
        /// \brief List of elements to serialize
        /// BeginImpl function enumerates all children and fills this list.
        typedef std::list<void*> ElementsDataList;

		/// \brief Serializes data to an XML element. 
		///	It calls the serializeElementBeginImpl() function.
		///	then serializeElementImpl() function for each element
		///	then serializeChildren() for children
		///	and finally serializeElementEndImpl to finalize and clear
		///
		///	@param
		///		strElementName
		///			Name of parent element
		///	@param
		///		parentElement
		///			Refrence to parent element. For reverse calls.
		///	@param
		///		parentData
		///			Data of the parent.
		///			
		bool serializeElement( const char* strElementName, TiXmlElement& parentElement, void* parentData );

	protected:
		/// \brief Serializes data to an XML element. 
		///
		///	@param
		///		currentNode
		///			New TiXmlElement was created to for data.
		///
		///	@param
		///		parentNode
		///			Refrence for parent TiXmlElement.
		///
		///	@param
		///		pElementData
		///			Pointer for current element data.
		///
		///	@param
		///		counter
		///			Tells how many times this function was called for the same kind of
		///			element.
		///			
		virtual void serializeElementImpl( TiXmlElement& currentNode, TiXmlElement& parentNode, void* pElementData, size_t counter) {}

        /// \brief Gets called back before element serializetion and forms data set for current element.
        ///	This enumerates all elements that are to be serialized
		///	@param
		///		strParentName
		///			Parent name. Used to choose the source of data.
		///	
        ///	@param
        ///		parentData
        ///			Data of the parent element. 
		///	@param
		///		elementsData
		///			List of siblings elements to be serialied. 
		///         If an elements data is created here, it must be deleted at serializeElementEndImpl 
        ///	
        ///	@returns If there is at least one element to serialize.
        virtual bool serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData ){ return false; }

		/// \brief Gets called back post all the children XML nodes have been created for the
		///		element.
		///
		///	@param
		///		currentNode
		///			Current node reftence. For data finalization.
		///	@param
		///		parentData
		///			Pointer to the data which was serialized for the element.
		///	@param
		///		currentData
		///			The system can't tell what part of an object should be serialized
		///			so the parent object is given to this function.
		virtual void serializeElementEndImpl(TiXmlElement& currentNode, void* parentData, void* currentData ){}

	public:
		/// \brief Serializes data to an XML comment.
		///
		///	@note See serializeElement() for more information.
		void* serializeComment( TiXmlComment& currentNode, void* parentData );

	protected:
		/// \brief Overrideable function to serialize data to an XML comment.
		///
		///	@note See serializeElementImpl() for more information.
		virtual void* serializeCommentImpl( TiXmlComment& currentNode, void* parentData ) { return NULL; }

	public:
		/// \brief Serializes data to an XML comment.
		///
		///	@note See serializeElement() for more information.
		void* serializeText( TiXmlText& currentNode, void* parentData );

	protected:
		/// \brief Overrideable function to serialize data to an XML text.
		///
		///	@note See serializeElementImpl() for more information.
		virtual void* serializeTextImpl( TiXmlText& currentNode, void* parentData ) { return NULL; }

	public:
		/// \brief The entry point for serializing a document.
		///
		///	@param
		///		strRootElementName
		///			Name of the root element. (It's needed because it is not registered
		///			to any processors.)
		///
		///	@param
		///		document
		///         Newly created document for saving.

		///	@param
		///		data
		///			Pointer to the data to be serialized.
		///	@note This function gets the pointer to the serializeable data, not
		///				to its parent!
		bool serializeRootElement( std::string strRootElementName, TiXmlDocument& document, void* data);


		// -= Event listeners =-

	public:
		/// \brief Listener which gets called back on node parsing events.
		///
		/// @author Tibor Tihon, Balazs Hajdics
		class ParseListener
		{
		public:
			ParseListener() {}
			virtual ~ParseListener() {}

			/// Called before an element node has been parsed.
			virtual void preElementParse(XmlNodeProcessor* processor, TiXmlElement* currentNode) {}

			/// Called after an element node has been parsed.
			virtual void postElementParse(XmlNodeProcessor* processor, TiXmlElement* currentNode) {}

			/// Called before a coment node has been parsed.
			virtual void preCommentParse(XmlNodeProcessor* processor, TiXmlComment* currentNode) {}

			/// Called after a comment node has been parsed.
			virtual void postCommentParse(XmlNodeProcessor* processor, TiXmlComment* currentNode) {}

			/// Called before a text node has been parsed.
			virtual void preTextParse(XmlNodeProcessor* processor, TiXmlText* currentNode) {}

			/// Called after a text node has been parsed.
			virtual void postTextParse(XmlNodeProcessor* processor, TiXmlText* currentNode) {}
		};

		/// \brief Listener which gets called back on node serializing events.
		/// 
		/// @author Tibor Tihon, Balazs Hajdics
		class SerializeListener
		{
		public:
			SerializeListener() {}
			virtual ~SerializeListener() {}

			/// Called before an element node has been serialized.
			virtual void preElementSerialize(XmlNodeProcessor* processor, TiXmlElement& currentNode) {}

			/// Called after an element node has been serialized.
			virtual void postElementSerialize(XmlNodeProcessor* processor, TiXmlElement& currentNode) {}

			/// Called before an comment node has been serialized.
			virtual void preCommentSerialize(XmlNodeProcessor* processor, TiXmlComment& currentNode) {}

			/// Called after an comment node has been serialized.
			virtual void postCommentSerialize(XmlNodeProcessor* processor, TiXmlComment& currentNode) {}

			/// Called before a text node has been serialized.
			virtual void preTextSerialize(XmlNodeProcessor* processor, TiXmlText& currentNode) {}

			/// Called after a text node has been serialized.
			virtual void postTextSerialize(XmlNodeProcessor* processor, TiXmlText& currentNode) {}
		};

		/// \brief Notify XmlNodeProcessor about the processortree builder which has created it.
		///
		/// @remarks Subclasses may redefine this function and query some of the common processing parameters from processor tree
		/// builder into member variables. However it is deprecated, try to query the required parameters at parsing/serializing
		/// time.
		virtual void		_notifyTreeBuilder(XmlNodeProcessorTreeBuilder* treeBuilder)
		{ 
			mTreeBuilder = treeBuilder;
		}

	public:
		/// \brief
		///
		/// @remarks For code reusability child node processors do not store pointers to their parents. This is because the same node
		/// processor can be registered to many node processors as a sub-node processor. It is also possible to register the same
		/// node processor (so the same code) for nodes (usually elements) with different names, but with the same contents.
		/// This helper static function is for determining the name as the child node processor was registered in its parent.
		/// This also increases modularity and code-reusability when giving error messages, since you don't have to "hardwire"
		/// the name of the node which you process into the code.
		/// @param parent Pointer to the parent node processor.
		/// @param child Pointer to the child node. The name of this node will be retrieved.
		static const String&	getParentRegisteredNameOfNodeProcessor(XmlNodeProcessor* parent, XmlNodeProcessor* child);

		static	String BLANK_STRING;

	protected:
		/// Set of registered ParseListener objects.
		std::set<ParseListener*>		mParseListeners;
		/// Set of registered SerializeListener objects.
		std::set<SerializeListener*>	mSerializeListeners;

	public:
		/// Registers a new ParseListener.
		void addParseListener(ParseListener* l);
		/// Unregisters an existing ParseListener.
		void removeParseListener(ParseListener* l);

		/// Registers a new SerializeListener.
		void addSerializeListener(SerializeListener* l);
		/// Unregisters an existing SerializeListener.
		void removeSerializeListener(SerializeListener* l);

	};
}

#endif
