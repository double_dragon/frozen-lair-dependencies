/*
-----------------------------------------------------------------------------
Original file:	???
New Author:		Balazs Hajdics (wolverine_@freemail.hu)

Copyright (c) 2007 Balazs Hajdics

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.

-----------------------------------------------------------------------------
*/

#include "dotSceneStableHeaders.h"
#include "dotSceneUtils.h"
#include "dotSceneInfo.h"

#include "tinyxml.h"

namespace Ogre
{
	//***************************************************************************************************
	namespace dsi
	{
		//***************************************************************************************************
		namespace utils
		{
			//***************************************************************************************************
			Ogre::String getFileTitle(const Ogre::String &fileName)
			{
				Ogre::String outFileTitle = "";
				Ogre::String::size_type pos = fileName.find_last_of("\\");
				Ogre::String::size_type nopos = Ogre::String::npos;
				Ogre::String::size_type len = fileName.length();
				if (pos != nopos)
					outFileTitle = fileName.substr(pos + 1, len - pos - 1);
				else
					outFileTitle = fileName;
				return outFileTitle;
			}

			//***************************************************************************************************
			bool isRenderWindow(Ogre::RenderTarget *pRenderTarget)
			{
				Ogre::RenderWindow* rw = dynamic_cast<Ogre::RenderWindow*> (pRenderTarget);
				if (rw) return true;
				return false;
			}

			//***************************************************************************************************
			bool isRenderTexture(Ogre::RenderTarget *pRenderTarget)
			{
				Ogre::RenderTexture* rt = dynamic_cast<Ogre::RenderTexture*> (pRenderTarget);
				if (rt) return true;
				return false;
			}

			//***************************************************************************************************
			bool isMultiRenderTarget(Ogre::RenderTarget *pRenderTarget)
			{
				Ogre::MultiRenderTarget* mrt = dynamic_cast<Ogre::MultiRenderTarget*> (pRenderTarget);
				if (mrt) return true;
				return false;
			}

			//***************************************************************************************************
			String getRelativeFilename(const String& currentDirectory, const String& absoluteFilename)
			{
				String cdNorm = StringUtil::normalizeFilePath(currentDirectory, OGRE_PLATFORM == OGRE_PLATFORM_WIN32);
				String afNorm = StringUtil::normalizeFilePath(absoluteFilename, OGRE_PLATFORM == OGRE_PLATFORM_WIN32);

				int cdNormLen = cdNorm.length();
				int afNormLen = afNorm.length();

				if ( cdNormLen == 0 || cdNormLen == 0 )
				{
					return BLANKSTRING;
				}

				// If the letters are different on Windows.
				if ( cdNorm[0] != afNorm[0] )
				{
					return absoluteFilename;
				}

				if ( '/' != cdNorm[cdNorm.length() - 1] )
				{
					cdNorm.append("/");
					++cdNormLen;
				}

				int i = 0;
				while ( i < cdNormLen && i < afNormLen && cdNorm[i] == afNorm[i] )
				{
					++i;
				}

				if ( ( i == afNormLen && cdNormLen == afNormLen ) || ( i == afNormLen && cdNormLen + 1 == afNormLen ) )
				{
					return String(".");
				}

				// Go back if the filename is the same as a the directory
				i = static_cast<int>( cdNorm.find_last_of( '/', i - 1 ) ) + 1;

				// Cut same prefixes
				String cdNormCut = cdNorm.substr(i);
				String afNormCut = afNorm.substr(i);				

				// Calculate number of "../"
				int slashCount = static_cast<int>( std::count( cdNormCut.begin(), cdNormCut.end(), '/' ) );

				// Append "../" prefixes
				for ( int j=0; j < slashCount; ++j )
				{
					afNormCut = "../" + afNormCut;
				}
				
				return afNormCut;
			}


			//***********************************************************************************************************
		} // namespace utils
		//*******************************************************************************************************
		namespace xml
		{
			//*******************************************************************************************************
			Ogre::ColourValue readColor(const TiXmlElement *clrElem)
			{
				if (!clrElem)
					return ColourValue::White;
				Real r,g,b,a;
				r = StringConverter::parseReal(clrElem->Attribute("r"));
				g = StringConverter::parseReal(clrElem->Attribute("g"));
				b = StringConverter::parseReal(clrElem->Attribute("b"));
				a = clrElem->Attribute("a") != NULL ? StringConverter::parseReal(clrElem->Attribute("a")) : 1;
				return ColourValue(r,g,b,a);
			}
			//*******************************************************************************************************
			Ogre::Vector3 readVector3(const TiXmlElement *vecElem)
			{
				if (!vecElem)
					return Vector3::ZERO;

				Vector3 position;
				position.x = StringConverter::parseReal(vecElem->Attribute("x"));
				position.y = StringConverter::parseReal(vecElem->Attribute("y"));
				position.z = StringConverter::parseReal(vecElem->Attribute("z"));
				return position;
			}
			//*******************************************************************************************************
			Ogre::Vector4 readVector4(const TiXmlElement *vecElem)
			{
				if (!vecElem)
					return Vector4::ZERO;

				Vector4 position;
				position.x = StringConverter::parseReal(vecElem->Attribute("x"));
				position.y = StringConverter::parseReal(vecElem->Attribute("y"));
				position.z = StringConverter::parseReal(vecElem->Attribute("z"));
				position.w = StringConverter::parseReal(vecElem->Attribute("w"));
				return position;
			}
			//*******************************************************************************************************
			Ogre::String readString(const TiXmlElement *pElem)
			{
				if (!pElem)
					return "";
				return pElem->Attribute("value");
			}
			//*******************************************************************************************************
			int readInt(const TiXmlElement *pElem)
			{
				if (!pElem)
					return -1;
				return Ogre::StringConverter::parseInt(pElem->Attribute("value"));
			}

			//***************************************************************************************************
			bool getAttribute(const TiXmlElement *pElem, const String &attribute, String &attributeValue)
			{
				const char *s = pElem->Attribute(attribute.c_str());
				if (!s)
					return false;
	
				attributeValue = String(s);
				return true;
			}
			//***************************************************************************************************
			bool getAttribute(const TiXmlElement *pElem, const String &attribute, bool &attributeValue)
			{
				String value;
				if (!getAttribute(pElem, attribute, value))
					return false;
	
				attributeValue = StringConverter::parseBool(value);
				return true;
			}
			//***************************************************************************************************
			bool getAttribute(const TiXmlElement *pElem, const String &attribute, Real &attributeValue)
			{
				String value;
				if (!getAttribute(pElem, attribute, value))
					return false;
	
				attributeValue = StringConverter::parseReal(value);
				return true;
			}
			//***************************************************************************************************
			bool getAttribute(const TiXmlElement *pElem, const String &attribute, int &attributeValue)
			{
				String value;
				if (!getAttribute(pElem, attribute, value))
					return false;
	
				attributeValue = StringConverter::parseInt(value);
				return true;
			}
			//***************************************************************************************************
			bool getAttribute(const TiXmlElement *pElem, const String &attribute, uint32 &attributeValue)
			{
				String value;
				if (!getAttribute(pElem, attribute, value))
					return false;
	
				attributeValue = StringConverter::parseUnsignedInt(value);
				return true;
			}



			//*******************************************************************************************************
			// NAMED WRITE METHODS
			//*******************************************************************************************************
			void writeNamedValue(TiXmlElement *pParentElem, const Ogre::String &name, const Ogre::String &value)
			{
				TiXmlElement *pElem = pParentElem->InsertEndChild(TiXmlElement(name))->ToElement();
				pElem->SetAttribute("value", value);
			}
			//*******************************************************************************************************
			void writeNamedValue(TiXmlElement *pParentElem, const Ogre::String &name, int value)
			{
				TiXmlElement *pElem = pParentElem->InsertEndChild(TiXmlElement(name))->ToElement();
				pElem->SetAttribute("value", Ogre::StringConverter::toString(value));
			}
			//*******************************************************************************************************
			void writeNamedValue(TiXmlElement *pParentElem, const Ogre::String &name, float value)
			{
				TiXmlElement *pElem = pParentElem->InsertEndChild(TiXmlElement(name))->ToElement();
				pElem->SetAttribute("value", Ogre::StringConverter::toString(value));
			}
			//*******************************************************************************************************
			void writeNamedValue(TiXmlElement *pParentElem, const Ogre::String &name, double value)
			{
				TiXmlElement *pElem = pParentElem->InsertEndChild(TiXmlElement(name))->ToElement();
				pElem->SetAttribute("value", Ogre::StringConverter::toString((Ogre::Real)value));
			}
			//*******************************************************************************************************
			void writeNamedValue(TiXmlElement *pParentElem, const Ogre::String &name, const Ogre::Matrix3 &value)
			{
				TiXmlElement *pElem = pParentElem->InsertEndChild(TiXmlElement(name))->ToElement();
				pElem->SetAttribute("11", Ogre::StringConverter::toString(value[0][0]));
				pElem->SetAttribute("12", Ogre::StringConverter::toString(value[0][1]));
				pElem->SetAttribute("13", Ogre::StringConverter::toString(value[0][2]));
				pElem->SetAttribute("21", Ogre::StringConverter::toString(value[1][0]));
				pElem->SetAttribute("22", Ogre::StringConverter::toString(value[1][1]));
				pElem->SetAttribute("23", Ogre::StringConverter::toString(value[1][2]));
				pElem->SetAttribute("31", Ogre::StringConverter::toString(value[2][0]));
				pElem->SetAttribute("32", Ogre::StringConverter::toString(value[2][1]));
				pElem->SetAttribute("33", Ogre::StringConverter::toString(value[2][2]));
			}
			//*******************************************************************************************************
			void writeNamedValue(TiXmlElement *pParentElem, const Ogre::String &name, const Ogre::Matrix4 &value)
			{
				TiXmlElement *pElem = pParentElem->InsertEndChild(TiXmlElement(name))->ToElement();
				pElem->SetAttribute("11", Ogre::StringConverter::toString(value[0][0]));
				pElem->SetAttribute("12", Ogre::StringConverter::toString(value[0][1]));
				pElem->SetAttribute("13", Ogre::StringConverter::toString(value[0][2]));
				pElem->SetAttribute("14", Ogre::StringConverter::toString(value[0][3]));
				pElem->SetAttribute("21", Ogre::StringConverter::toString(value[1][0]));
				pElem->SetAttribute("22", Ogre::StringConverter::toString(value[1][1]));
				pElem->SetAttribute("23", Ogre::StringConverter::toString(value[1][2]));
				pElem->SetAttribute("24", Ogre::StringConverter::toString(value[1][3]));
				pElem->SetAttribute("31", Ogre::StringConverter::toString(value[2][0]));
				pElem->SetAttribute("32", Ogre::StringConverter::toString(value[2][1]));
				pElem->SetAttribute("33", Ogre::StringConverter::toString(value[2][2]));
				pElem->SetAttribute("34", Ogre::StringConverter::toString(value[2][3]));
				pElem->SetAttribute("41", Ogre::StringConverter::toString(value[3][0]));
				pElem->SetAttribute("42", Ogre::StringConverter::toString(value[3][1]));
				pElem->SetAttribute("43", Ogre::StringConverter::toString(value[3][2]));
				pElem->SetAttribute("44", Ogre::StringConverter::toString(value[3][3]));
			}
			//*******************************************************************************************************
			void writeNamedValue(TiXmlElement *pParentElem, const Ogre::String &name, const Ogre::Vector3 &value)
			{
				TiXmlElement *pElem = pParentElem->InsertEndChild(TiXmlElement(name))->ToElement();
				pElem->SetAttribute("x", Ogre::StringConverter::toString(value.x));
				pElem->SetAttribute("y", Ogre::StringConverter::toString(value.y));
				pElem->SetAttribute("z", Ogre::StringConverter::toString(value.z));
			}
			//*******************************************************************************************************
			void writeNamedValue(TiXmlElement *pParentElem, const Ogre::String &name, const Ogre::Vector4 &value)
			{
				TiXmlElement *pElem = pParentElem->InsertEndChild(TiXmlElement(name))->ToElement();
				pElem->SetAttribute("x", Ogre::StringConverter::toString(value.x));
				pElem->SetAttribute("y", Ogre::StringConverter::toString(value.y));
				pElem->SetAttribute("z", Ogre::StringConverter::toString(value.z));
				pElem->SetAttribute("w", Ogre::StringConverter::toString(value.w));
			}
			//*******************************************************************************************************
			void writeNamedValue(TiXmlElement *pParentElem, const Ogre::String &name, const Ogre::ColourValue &value)
			{
				TiXmlElement *pElem = pParentElem->InsertEndChild(TiXmlElement(name))->ToElement();
				pElem->SetAttribute("r", Ogre::StringConverter::toString(value.r));
				pElem->SetAttribute("g", Ogre::StringConverter::toString(value.g));
				pElem->SetAttribute("b", Ogre::StringConverter::toString(value.b));
				pElem->SetAttribute("a", Ogre::StringConverter::toString(value.a));
			}
			//*******************************************************************************************************
			void writeNamedValue(TiXmlElement *pParentElem, const Ogre::String &name, const Ogre::Quaternion &value)
			{
				TiXmlElement *pElem = pParentElem->InsertEndChild(TiXmlElement(name))->ToElement();
				pElem->SetAttribute("qx", Ogre::StringConverter::toString(value.x));
				pElem->SetAttribute("qy", Ogre::StringConverter::toString(value.y));
				pElem->SetAttribute("qz", Ogre::StringConverter::toString(value.z));
				pElem->SetAttribute("qw", Ogre::StringConverter::toString(value.w));
			}
			//*******************************************************************************************************
			void writeNamedValue(TiXmlElement *pParentElem, const Ogre::String &name, bool value)
			{
				TiXmlElement *pElem = pParentElem->InsertEndChild(TiXmlElement(name))->ToElement();
				pElem->SetAttribute("value", Ogre::StringConverter::toString(value));
			}
		} // namespace xml
	}// namespace dsi
}// namespace Ogre
