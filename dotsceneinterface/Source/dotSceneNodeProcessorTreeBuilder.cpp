/*
-----------------------------------------------------------------------------
Author:	Balazs Hajdics

Copyright (c) 2007 Balazs Hajdics

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.

-----------------------------------------------------------------------------
*/

#include "dotSceneStableHeaders.h"

#include "XNPPrerequisites.h"
#include "XNPXmlNodeProcessorTreeBuilder.h"
#include "dotSceneNodeProcessorTreeBuilder.h"

#include "Ogre.h"

#include <algorithm>


namespace Ogre {
	namespace dsi {

	DotSceneNodeProcessorTreeBuilder::DotSceneNodeProcessorTreeBuilder(CommonParamsOfXmlNodeProcessors* commonParams):
		XmlNodeProcessorTreeBuilder(commonParams)
	{

	}

	DotSceneNodeProcessorTreeBuilder::~DotSceneNodeProcessorTreeBuilder()
	{
	}


	XmlNodeProcessor*	DotSceneNodeProcessorTreeBuilder::buildTree()
	{
		// ----------------------------------------- Allocate memory for XmlNodeProcessors -----------------------------------------
		mSceneElementProcessor								= new SceneElementProcessor;

		mSceneManagerOptionElementProcessor					= new SceneManagerOptionElementProcessor;

		mResourceGroupElementProcessor						= new ResourceGroupElementProcessor;
			mResourceLocationElementProcessor				= new ResourceLocationElementProcessor;
			mResourceDeclarationElementProcessor			= new ResourceDeclarationElementProcessor;
				mNameValuePairListElementProcessor			= new NameValuePairListElementProcessor;
					mNameValuePairElementProcessor			= new NameValuePairElementProcessor;

		mShadowSettingsElementProcessor						= new ShadowSettingsElementProcessor;
			mShadowTextureConfigElementProcessor			= new ShadowTextureConfigElementProcessor;

		mRenderTargetsElementProcessor						= new RenderTargetsElementProcessor;
		mRenderWindowElementProcessor						= new RenderWindowElementProcessor;

		mEnvironmentElementProcessor						= new EnvironmentElementProcessor;
			mFogElementProcessor							= new FogElementProcessor;
			mSkyBoxElementProcessor							= new SkyBoxElementProcessor;
			mSkyDomeElementProcessor						= new SkyDomeElementProcessor;
			mSkyPlaneElementProcessor						= new SkyPlaneElementProcessor;
			mColourAmbientElementProcessor					= new ColourAmbientElementProcessor;
			mColourBackgroundElementProcessor				= new ColourBackgroundElementProcessor;

		mTrackTargetElementProcessor						= new TrackTargetElementProcessor;
			mLocalDirectionVectorElementProcessor			= new LocalDirectionVectorElementProcessor;
			mOffsetElementProcessor							= new OffsetElementProcessor;

		mLookTargetElementProcessor							= new LookTargetElementProcessor;

		mLightElementProcessor								= new LightElementProcessor;
			mColourDiffuseElementProcessor					= new ColourDiffuseElementProcessor;
			mColourSpecularElementProcessor					= new ColourSpecularElementProcessor;
			mSpotLightRangeElementProcessor					= new SpotLightRangeElementProcessor;
			mLightAttenuationElementProcessor				= new LightAttenuationElementProcessor;
		mCameraElementProcessor								= new CameraElementProcessor;
			mClippingElementProcessor						= new ClippingElementProcessor;

		mNodesElementProcessor								= new NodesElementProcessor;
		mNodeElementProcessor								= new NodeElementProcessor;
		mNodeElementProcessor2								= new NodeElementProcessor;
			mUserDataElementProcessor						= new UserDataElementProcessor;
			mEntityElementProcessor							= new EntityElementProcessor;
				mMeshLODBiasElementProcessor				= new MeshLODBiasElementProcessor;
				mMaterialLODBiasElementProcessor			= new MaterialLODBiasElementProcessor;
			mParticleSystemElementProcessor					= new ParticleSystemElementProcessor;
			mBillboardSetElementProcessor					= new BillboardSetElementProcessor;
				mBillboardElementProcessor					= new BillboardElementProcessor;
				mCommonDirectionElementProcessor			= new CommonDirectionElementProcessor;
				mCommonUpVectorElementProcessor				= new CommonUpVectorElementProcessor;
				mTextureCoordsElementProcessor				= new TextureCoordsElementProcessor;
					mFloatRectElementProcessor				= new FloatRectElementProcessor;
				mTextureStacksAndSlicesElementProcessor 	= new TextureStacksAndSlicesElementProcessor;


		mViewportsElementProcessor							= new ViewportsElementProcessor;
			mViewportElementProcessor						= new ViewportElementProcessor;
				mCompositorInstancesElementProcessor		= new CompositorInstancesElementProcessor;
					mCompositorInstanceElementProcessor		= new CompositorInstanceElementProcessor;
//					mHDRCompositorInstanceElementProcessor	= new HDRCompositorInstanceElementProcessor;



		mAnimationsElementProcessor							= new AnimationsElementProcessor;
			mAnimationElementProcessor						= new AnimationElementProcessor;
				mNodeAnimationTrackElementProcessor			= new NodeAnimationTrackElementProcessor;
					mTransformKeyFrameElementProcessor		= new TransformKeyFrameElementProcessor;
				mVertexAnimationTrackElementProcessor		= new VertexAnimationTrackElementProcessor;
					mVertexPoseKeyFrameElementProcessor		= new VertexPoseKeyFrameElementProcessor;
						mPoseReferenceElementProcessor		= new PoseReferenceElementProcessor;
		mAnimationStatesElementProcessor					= new AnimationStatesElementProcessor;
			mAnimationStateElementProcessor					= new AnimationStateElementProcessor;


		mCommonMovableObjectParamsElementProcessor			= new CommonMovableObjectParamsElementProcessor;

		mPositionElementProcessor							= new PositionElementProcessor;
		mOrientationElementProcessor						= new OrientationElementProcessor;
			mQuaternionElementProcessor						= new QuaternionElementProcessor;
			mAngle_AxisElementProcessor						= new Angle_AxisElementProcessor;
			mAxisXYZElementProcessor						= new AxisXYZElementProcessor;
			mDirectionElementProcessor						= new DirectionElementProcessor;
		mScaleElementProcessor								= new ScaleElementProcessor;

#ifdef _USE_DOT_OCTREE
		mOctreeElementProcessor								= new OctreeElementProcessor;
#endif
		// -------------------------------------------------------------------------------------------------------------------------

		// --------------------------------------- Notify XmlNodeProcessors of tree builder ----------------------------------------
		mSceneElementProcessor->							_notifyTreeBuilder(this);

		mSceneManagerOptionElementProcessor->				_notifyTreeBuilder(this);

		mResourceGroupElementProcessor->					_notifyTreeBuilder(this);
			mResourceLocationElementProcessor->				_notifyTreeBuilder(this);
			mResourceDeclarationElementProcessor->			_notifyTreeBuilder(this);
				mNameValuePairListElementProcessor->		_notifyTreeBuilder(this);
					mNameValuePairElementProcessor->		_notifyTreeBuilder(this);

		mShadowSettingsElementProcessor->					_notifyTreeBuilder(this);
			mShadowTextureConfigElementProcessor->			_notifyTreeBuilder(this);

		mRenderTargetsElementProcessor->					_notifyTreeBuilder(this);
		mRenderWindowElementProcessor->						_notifyTreeBuilder(this);

		mCameraElementProcessor->							_notifyTreeBuilder(this);
			mClippingElementProcessor->						_notifyTreeBuilder(this);

		mViewportsElementProcessor->						_notifyTreeBuilder(this);
			mViewportElementProcessor->						_notifyTreeBuilder(this);
				mCompositorInstancesElementProcessor->		_notifyTreeBuilder(this);
					mCompositorInstanceElementProcessor->	_notifyTreeBuilder(this);
//					mHDRCompositorInstanceElementProcessor->_notifyTreeBuilder(this);

		mEnvironmentElementProcessor->						_notifyTreeBuilder(this);
			mFogElementProcessor->							_notifyTreeBuilder(this);
			mSkyBoxElementProcessor->						_notifyTreeBuilder(this);
			mSkyDomeElementProcessor->						_notifyTreeBuilder(this);
			mSkyPlaneElementProcessor->						_notifyTreeBuilder(this);
			mColourAmbientElementProcessor->				_notifyTreeBuilder(this);
			mColourBackgroundElementProcessor->				_notifyTreeBuilder(this);

		mTrackTargetElementProcessor->						_notifyTreeBuilder(this);
			mLocalDirectionVectorElementProcessor->			_notifyTreeBuilder(this);
			mOffsetElementProcessor->						_notifyTreeBuilder(this);

		mLookTargetElementProcessor->						_notifyTreeBuilder(this);

		mLightElementProcessor->							_notifyTreeBuilder(this);
			mColourDiffuseElementProcessor->				_notifyTreeBuilder(this);
			mColourSpecularElementProcessor->				_notifyTreeBuilder(this);
			mSpotLightRangeElementProcessor->				_notifyTreeBuilder(this);
			mLightAttenuationElementProcessor->				_notifyTreeBuilder(this);

		mNodesElementProcessor->							_notifyTreeBuilder(this);
		mNodeElementProcessor->								_notifyTreeBuilder(this);
		mNodeElementProcessor2->							_notifyTreeBuilder(this);
			mUserDataElementProcessor->						_notifyTreeBuilder(this);
			mEntityElementProcessor->						_notifyTreeBuilder(this);
				mMeshLODBiasElementProcessor->				_notifyTreeBuilder(this);
				mMaterialLODBiasElementProcessor->			_notifyTreeBuilder(this);
			mParticleSystemElementProcessor->				_notifyTreeBuilder(this);
			mBillboardSetElementProcessor->					_notifyTreeBuilder(this);
				mBillboardElementProcessor->				_notifyTreeBuilder(this);
				mCommonDirectionElementProcessor->			_notifyTreeBuilder(this);
				mCommonUpVectorElementProcessor->			_notifyTreeBuilder(this);
				mTextureCoordsElementProcessor->			_notifyTreeBuilder(this);
					mFloatRectElementProcessor->			_notifyTreeBuilder(this);
				mTextureStacksAndSlicesElementProcessor->	_notifyTreeBuilder(this);


		mAnimationsElementProcessor->						_notifyTreeBuilder(this);
			mAnimationElementProcessor->					_notifyTreeBuilder(this);
				mNodeAnimationTrackElementProcessor->		_notifyTreeBuilder(this);
					mTransformKeyFrameElementProcessor->	_notifyTreeBuilder(this);
				mVertexAnimationTrackElementProcessor->		_notifyTreeBuilder(this);
					mVertexPoseKeyFrameElementProcessor->	_notifyTreeBuilder(this);
						mPoseReferenceElementProcessor->	_notifyTreeBuilder(this);
		mAnimationStatesElementProcessor->					_notifyTreeBuilder(this);
			mAnimationStateElementProcessor->				_notifyTreeBuilder(this);


		mCommonMovableObjectParamsElementProcessor->		_notifyTreeBuilder(this);

		mPositionElementProcessor->							_notifyTreeBuilder(this);
		mOrientationElementProcessor->						_notifyTreeBuilder(this);
			mQuaternionElementProcessor->					_notifyTreeBuilder(this);
			mAngle_AxisElementProcessor->					_notifyTreeBuilder(this);
			mAxisXYZElementProcessor->						_notifyTreeBuilder(this);
			mDirectionElementProcessor->					_notifyTreeBuilder(this);
		mScaleElementProcessor->							_notifyTreeBuilder(this);

#ifdef _USE_DOT_OCTREE
		mOctreeElementProcessor->							_notifyTreeBuilder(this);
#endif
		// -------------------------------------------------------------------------------------------------------------------------



		// ---------------------------------------------- Register XmlNodeProcessors -----------------------------------------------

		// Register the root processor first.
			registerRootProcessor( "/root", mSceneElementProcessor );

		// Since orientation has sub-elements and it is used as a sub-element processor for many dotScene elements, add element
		// processors for it before registering sub-element processors for other processors.
		// <orientation>
			registerElementProcessor( "quaternion", mOrientationElementProcessor, mQuaternionElementProcessor );
			registerElementProcessor( "angle_axis", mOrientationElementProcessor, mAngle_AxisElementProcessor );
			registerElementProcessor( "axisXYZ", mOrientationElementProcessor, mAxisXYZElementProcessor );
			registerElementProcessor( "direction", mOrientationElementProcessor, mDirectionElementProcessor );
		// </orientation>

		// <scene>
			
			// <sceneManagerOption>
			registerElementProcessor( "sceneManagerOption", mSceneElementProcessor, mSceneManagerOptionElementProcessor );
			// </sceneManagerOption>

			// <resourceGroup>
			registerElementProcessor( "resourceGroup", mSceneElementProcessor, mResourceGroupElementProcessor );

				// <resourceLocation>
				registerElementProcessor( "resourceLocation", mResourceGroupElementProcessor, mResourceLocationElementProcessor );
				// </resourceLocation>

				// <resourceDeclaration>
				registerElementProcessor( "resourceDeclaration", mResourceGroupElementProcessor, mResourceDeclarationElementProcessor );
					
					// <NameValuePairList>
					registerElementProcessor( "NameValuePairList", mResourceDeclarationElementProcessor, mNameValuePairListElementProcessor );
						// <NameValuePair>
						registerElementProcessor( "NameValuePair", mNameValuePairListElementProcessor, mNameValuePairElementProcessor );
						// </NameValuePair>
					// </NameValuePairList>
				// </resourceDeclaration>

			// </resourceGroup>

			// <shadowSettings>
			registerElementProcessor( "shadowSettings", mSceneElementProcessor, mShadowSettingsElementProcessor );

				// <colourDiffuse>
				registerElementProcessor( "colourDiffuse", mShadowSettingsElementProcessor, mColourDiffuseElementProcessor );
				// </colourDiffuse>

				// <shadowTextureConfig>
				registerElementProcessor( "shadowTextureConfig", mShadowSettingsElementProcessor, mShadowTextureConfigElementProcessor );
				// </shadowTextureConfig>
			// </shadowSettings>
		
			// <renderTargets>
			registerElementProcessor( "renderTargets", mSceneElementProcessor, mRenderTargetsElementProcessor );

				// <renderWindow>
				registerElementProcessor( "renderWindow", mRenderTargetsElementProcessor, mRenderWindowElementProcessor );

				// </renderWindow>

				// <renderTexture>

				// </renderTexture>

				// <multiRenderTarget>

					// <renderTexture>
					// </renderTexture>
				// </multiRenderTarget>
			// </renderTargets>

			// <camera>
			registerElementProcessor( "camera", mSceneElementProcessor, mCameraElementProcessor );

					// <commonMovableObjectParams>
					registerElementProcessor( "commonMovableObjectParams", mCameraElementProcessor, mCommonMovableObjectParamsElementProcessor );
					// </commonMovableObjectParams>

					// <clipping>
					registerElementProcessor( "clipping", mCameraElementProcessor, mClippingElementProcessor );
					// </clipping>

					// <position>
					registerElementProcessor( "position", mCameraElementProcessor, mPositionElementProcessor );
					// </position>

					// <orientation>
					registerElementProcessor( "orientation", mCameraElementProcessor, mOrientationElementProcessor );
					// </orientation>

					// <trackTarget>
					registerElementProcessor( "trackTarget", mCameraElementProcessor, mTrackTargetElementProcessor );

						// <localDirectionVector>
						registerElementProcessor( "localDirectionVector", mTrackTargetElementProcessor, mLocalDirectionVectorElementProcessor );
						// </localDirectionVector>

						// <offset>
						registerElementProcessor( "offset", mTrackTargetElementProcessor, mOffsetElementProcessor );
						// </offset>
					// </trackTarget>					

					// <lookTarget>
					registerElementProcessor( "lookTarget", mCameraElementProcessor, mLookTargetElementProcessor );

						// <position>
						registerElementProcessor( "position", mLookTargetElementProcessor, mPositionElementProcessor );
						// </position>

						// <localDirectionVector>
						registerElementProcessor( "localDirectionVector", mLookTargetElementProcessor, mLocalDirectionVectorElementProcessor );
						// </localDirectionVector>
					// </lookTarget>					

			// </camera>

			// <viewports>
			registerElementProcessor( "viewports", mSceneElementProcessor, mViewportsElementProcessor );
				// <viewport>
				registerElementProcessor( "viewport", mViewportsElementProcessor, mViewportElementProcessor );
					
					// <compositorInstances>
					registerElementProcessor( "compositorInstances", mViewportElementProcessor, mCompositorInstancesElementProcessor );
						
						// <compositorInstance>
						registerElementProcessor( "compositorInstance", mCompositorInstancesElementProcessor, mCompositorInstanceElementProcessor );
						// </compositorInstance>

						// <HDRCompositorInstance>
//						registerElementProcessor( "HDRCompositorInstance", mCompositorInstancesElementProcessor, mHDRCompositorInstanceElementProcessor );
						// </HDRCompositorInstance>
					// </compositorInstances>

				// </viewport>
			// </viewports>

			// <environment>
			registerElementProcessor( "environment", mSceneElementProcessor, mEnvironmentElementProcessor );

				// <fog>
				registerElementProcessor( "fog", mEnvironmentElementProcessor, mFogElementProcessor );
				// </fog>

				// <skyBox>
				registerElementProcessor( "skyBox", mEnvironmentElementProcessor, mSkyBoxElementProcessor );
					// <orientation>
					registerElementProcessor( "orientation", mSkyBoxElementProcessor, mOrientationElementProcessor );
					// </orientation>
				// </skyBox>

				// <skyDome>
				registerElementProcessor( "skyDome", mEnvironmentElementProcessor, mSkyDomeElementProcessor );
					// <orientation>
					registerElementProcessor( "orientation", mSkyDomeElementProcessor, mOrientationElementProcessor );
					// </orientation>
				// </skyDome>

				// <skyPlane>
				registerElementProcessor( "skyPlane", mEnvironmentElementProcessor, mSkyPlaneElementProcessor );
				// </skyPlane>

				// <colourAmbient>
				registerElementProcessor( "colourAmbient", mEnvironmentElementProcessor, mColourAmbientElementProcessor );
				// </colourAmbient>

				// <colourBackground>
				registerElementProcessor( "colourBackground", mEnvironmentElementProcessor, mColourBackgroundElementProcessor );
				// </colourBackground>
			// </environment>


			// <light>
			registerElementProcessor( "light", mSceneElementProcessor, mLightElementProcessor );

					// <commonMovableObjectParams>
					registerElementProcessor( "commonMovableObjectParams", mLightElementProcessor, mCommonMovableObjectParamsElementProcessor );
					// </commonMovableObjectParams>

					// <position>
					registerElementProcessor( "position", mLightElementProcessor, mPositionElementProcessor );
					// </position>

					// <direction>
					registerElementProcessor( "direction", mLightElementProcessor, mDirectionElementProcessor );
					// </direction>

					// TODO: uncomment this if Ogre starts to support arbitrary light shapes. (E.g. used with cubic-attenution maps.)
					// <orientation>
//					registerElementProcessor( "orientation", mLightElementProcessor, mOrientationElementProcessor );
					// </orientation>

					// <colourDiffuse>
					registerElementProcessor( "colourDiffuse", mLightElementProcessor, mColourDiffuseElementProcessor );
					// </colourDiffuse>

					// <colourSpecular>
					registerElementProcessor( "colourSpecular", mLightElementProcessor, mColourSpecularElementProcessor );
					// </colourSpecular>

					// <spotLightRange>
					registerElementProcessor( "spotLightRange", mLightElementProcessor, mSpotLightRangeElementProcessor );
					// </spotLightRange>

					// <lightAttenuation>
					registerElementProcessor( "lightAttenuation", mLightElementProcessor, mLightAttenuationElementProcessor );
					// </lightAttenuation>
			// </light>

			// <nodes>
				registerElementProcessor( "nodes", mSceneElementProcessor, mNodesElementProcessor );
				
				// <position>
				registerElementProcessor( "position", mNodesElementProcessor, mPositionElementProcessor );
				// </position>

				// <orientation>
				registerElementProcessor( "orientation", mNodesElementProcessor, mOrientationElementProcessor );
				// </orientation>

				// <scale>
				registerElementProcessor( "scale", mNodesElementProcessor, mScaleElementProcessor );
				// </scale>

				// <light>
				registerElementProcessor( "light", mNodesElementProcessor, mLightElementProcessor );
				// </light>

				// <camera>
				registerElementProcessor( "camera", mNodesElementProcessor, mCameraElementProcessor );
				// </camera>

				// <node>
				registerElementProcessor( "node", mNodesElementProcessor, mNodeElementProcessor );

					// <user_data>
					registerElementProcessor("user_data", mNodeElementProcessor, mUserDataElementProcessor);

					// <position>
					registerElementProcessor( "position", mNodeElementProcessor, mPositionElementProcessor );
					// </position>

					// <orientation>
					registerElementProcessor( "orientation", mNodeElementProcessor, mOrientationElementProcessor );
					// </orientation>

					// <scale>
					registerElementProcessor( "scale", mNodeElementProcessor, mScaleElementProcessor );
					// </scale>

					// <trackTarget>
					registerElementProcessor( "trackTarget", mNodeElementProcessor, mTrackTargetElementProcessor );
					// </trackTarget>

					// <lookTarget>
					registerElementProcessor( "lookTarget", mNodeElementProcessor, mLookTargetElementProcessor );
					// </lookTarget>

					// <node>
					registerElementProcessor( "node", mNodeElementProcessor, mNodeElementProcessor );
					// </node>

//---------------------------------------------------------------------------------------------------------------------------------
					registerElementProcessor( "position", mNodeElementProcessor2, mPositionElementProcessor );
					registerElementProcessor( "orientation", mNodeElementProcessor2, mOrientationElementProcessor );
					registerElementProcessor( "scale", mNodeElementProcessor2, mScaleElementProcessor );
					registerElementProcessor( "trackTarget", mNodeElementProcessor2, mTrackTargetElementProcessor );
					registerElementProcessor( "lookTarget", mNodeElementProcessor2, mLookTargetElementProcessor );
					registerElementProcessor( "node", mNodeElementProcessor2, mNodeElementProcessor2 );
					registerElementProcessor( "light", mNodeElementProcessor2, mLightElementProcessor );
					registerElementProcessor( "camera", mNodeElementProcessor2, mCameraElementProcessor );
					registerElementProcessor( "particleSystem", mNodeElementProcessor2, mParticleSystemElementProcessor );
					registerElementProcessor( "billboardSet", mNodeElementProcessor2, mBillboardSetElementProcessor );
//---------------------------------------------------------------------------------------------------------------------------------

					// <entity>
					registerElementProcessor( "entity", mNodeElementProcessor, mEntityElementProcessor );
						// <commonMovableObjectParams>
						registerElementProcessor( "commonMovableObjectParams", mEntityElementProcessor, mCommonMovableObjectParamsElementProcessor );
						// </commonMovableObjectParams>

						// <animations>
						registerElementProcessor( "animations", mEntityElementProcessor, mAnimationsElementProcessor );
						
							// <animation>
							registerElementProcessor( "animation", mAnimationsElementProcessor, mAnimationElementProcessor );

								// <nodeAnimationTrack>
								registerElementProcessor( "nodeAnimationTrack", mAnimationElementProcessor, mNodeAnimationTrackElementProcessor );

									// <transformKeyFrame>
									registerElementProcessor( "transformKeyFrame", mNodeAnimationTrackElementProcessor, mTransformKeyFrameElementProcessor );

										// <translate>
										registerElementProcessor( "translation", mTransformKeyFrameElementProcessor, mPositionElementProcessor );
										// </translate>

										// <orientation>
										registerElementProcessor( "orientation", mTransformKeyFrameElementProcessor, mOrientationElementProcessor );
										// </orientation>

										// <scale>
										registerElementProcessor( "scale", mTransformKeyFrameElementProcessor, mScaleElementProcessor );
										// </scale>
									// </transformKeyFrame>
								// </nodeAnimationTrack>

								// <vertexAnimationTrack>				
								registerElementProcessor( "vertexAnimationTrack", mAnimationElementProcessor, mVertexAnimationTrackElementProcessor );

									// <vertexPoseKeyFrame>
									registerElementProcessor( "vertexPoseKeyFrame", mVertexAnimationTrackElementProcessor, mVertexPoseKeyFrameElementProcessor );

										// <poseReference>
										registerElementProcessor( "poseReference", mVertexPoseKeyFrameElementProcessor, mPoseReferenceElementProcessor );
										// </poseReference>
									// </vertexPoseKeyFrame>
								// </vertexAnimationTrack>
							// </animation>
						// </animations>

						// <animationStates>
						registerElementProcessor( "animationStates", mEntityElementProcessor, mAnimationStatesElementProcessor );
							// <animationState>
							registerElementProcessor( "animationState", mAnimationStatesElementProcessor, mAnimationStateElementProcessor );
							// </animationState>
						// </animationStates>

						// <meshLODBias>
						registerElementProcessor( "meshLODBias", mEntityElementProcessor, mMeshLODBiasElementProcessor );
						// </meshLODBias>

						// <materialLODBias>
						registerElementProcessor( "materialLODBias", mEntityElementProcessor, mMaterialLODBiasElementProcessor );
						// </materialLODBias>
					// </entity>

					// <light>
					registerElementProcessor( "light", mNodeElementProcessor, mLightElementProcessor );
					// </light>

					// <camera>
					registerElementProcessor( "camera", mNodeElementProcessor, mCameraElementProcessor );
					// </camera>

					// <particleSystem>
					registerElementProcessor( "particleSystem", mNodeElementProcessor, mParticleSystemElementProcessor );
						// <commonMovableObjectParams>
						registerElementProcessor( "commonMovableObjectParams", mParticleSystemElementProcessor, mCommonMovableObjectParamsElementProcessor );
						// </commonMovableObjectParams>
					// </particleSystem>

					// <billboardSet>
					registerElementProcessor( "billboardSet", mNodeElementProcessor, mBillboardSetElementProcessor );

						// <commonMovableObjectParams>
						registerElementProcessor( "commonMovableObjectParams", mBillboardSetElementProcessor, mCommonMovableObjectParamsElementProcessor );
						// </commonMovableObjectParams>

						// <billboard>
						registerElementProcessor( "billboard", mBillboardSetElementProcessor, mBillboardElementProcessor );

							// <position>
							registerElementProcessor( "position", mBillboardElementProcessor, mPositionElementProcessor );
							// </position>

							// <colourDiffuse>
							registerElementProcessor( "colourDiffuse", mBillboardElementProcessor, mColourDiffuseElementProcessor );
							// </colourDiffuse>

							// <textureCoords>
							registerElementProcessor( "textureCoords", mBillboardElementProcessor, mTextureCoordsElementProcessor );
							// </textureCoords>
						// </billboard>

						// <commonDirection>
						registerElementProcessor( "commonDirection", mBillboardSetElementProcessor, mCommonDirectionElementProcessor );
						// </commonDirection>

						// <commonUpVector>
						registerElementProcessor( "commonUpVector", mBillboardSetElementProcessor, mCommonUpVectorElementProcessor );
						// </commonUpVector>

						// <textureCoords>
						registerElementProcessor( "textureCoords", mBillboardSetElementProcessor, mTextureCoordsElementProcessor );
							// <floatRect>
							registerElementProcessor( "floatRect", mTextureCoordsElementProcessor, mFloatRectElementProcessor );
							// </floatRect>
						// </textureCoords>

						// <textureStacksAndSlices>
						registerElementProcessor( "textureStacksAndSlices", mBillboardSetElementProcessor, mTextureStacksAndSlicesElementProcessor );
						// </textureStacksAndSlices>
					// </billboardSet>
				// </node>
			// </nodes>

			// <animations>
			registerElementProcessor( "animations", mSceneElementProcessor, mAnimationsElementProcessor );
			// </animations>

			// <animationStates>
			registerElementProcessor( "animationStates", mSceneElementProcessor, mAnimationStatesElementProcessor );
			// </animationStates>

#ifdef _USE_DOT_OCTREE
			// <octree>
			registerElementProcessor( "octree", mSceneElementProcessor, mOctreeElementProcessor );
			// </octree>
#endif

		// </scene>

		// -------------------------------------------------------------------------------------------------------------------------

		return mSceneElementProcessor;
	}

	void DotSceneNodeProcessorTreeBuilder::destroyTree()
	{
		OGRE_DELETE(mRoot);
		mNamesToProcessorsMap.clear();
		mChildrenToParentsMap.clear();
		mProcessors.clear();
	}



	} // namespace dsi
} // namespace Ogre
