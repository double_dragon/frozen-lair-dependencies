/*
-----------------------------------------------------------------------------
Author:		Balazs Hajdics (wolverine_@freemail.hu), James Le Cuirot, Daniel Banky (ViveTech Ltd., daniel.banky@vivetech.com)

Copyright (c) 2007 Balazs Hajdics

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.

-----------------------------------------------------------------------------
*/


#include "dotSceneStableHeaders.h"

#include "dotScenePlatformUtils.h"
#include "Ogre.h"

using namespace Ogre;


#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#include <tchar.h>
#elif OGRE_PLATFORM == OGRE_PLATFORM_LINUX
#include <unistd.h>
#include <libgen.h>
#include <errno.h>
#include <string.h>
#elif OGRE_PLATFORM == OGRE_PLATFORM_APPLE

#endif

namespace Ogre
{

namespace dsi {

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
String	PlatformUtils::GetLastError_AsString()
{
	// Get error code.
	LPVOID lpMsgBuff;
	DWORD errorCode = GetLastError(); 

	// Convert it to string.
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL,
		errorCode,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR) &lpMsgBuff,
		0, NULL );

	return String((TCHAR*)lpMsgBuff);
}

#endif

String PlatformUtils::getCurrentDirectory()
{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32

	TCHAR*	currentDir;
	DWORD	currentDirLength;

	currentDirLength = GetCurrentDirectory(0, NULL);
	currentDir = new TCHAR[currentDirLength];
	if (!currentDir)
		OGRE_EXCEPT(Exception::ERR_INTERNAL_ERROR, "Out of memory.", "PlatformUtils::getCurrentDirectory");

	if ( GetCurrentDirectory(currentDirLength, currentDir) != (currentDirLength-sizeof(TCHAR)) )
	{
		OGRE_DELETE(currentDir);

		// Throw an exception.
		OGRE_EXCEPT(Exception::ERR_INTERNAL_ERROR,
					"WinAPI GetCurrentDirectory() failed, the returned error message is: " + PlatformUtils::GetLastError_AsString(),
					"PlatformUtils::getCurrentDirectory()");
	}

	String strCurrentDir(currentDir);
	OGRE_DELETE(currentDir);

	// Standardise path, replace all "\\" with "/", and place a "/" at the end of the path.
	strCurrentDir = StringUtil::standardisePath(strCurrentDir);

	return strCurrentDir;

#elif OGRE_PLATFORM == OGRE_PLATFORM_LINUX

	char* currentDir = getcwd(NULL, 0);
	
	if (!currentDir)
		OGRE_EXCEPT(Exception::ERR_INTERNAL_ERROR, strerror(errno), "PlatformUtils::getCurrentDirectory");
	
	String strCurrentDir(currentDir);
	OGRE_DELETE(currentDir);

	return strCurrentDir;
	
#elif OGRE_PLATFORM == OGRE_PLATFORM_APPLE

#endif
}


String	PlatformUtils::getCurrentExecutableFilePath()
{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32

	TCHAR* tcharExecutablePath = new TCHAR[MAX_PATH];
	if (!tcharExecutablePath)
		OGRE_EXCEPT(Exception::ERR_INTERNAL_ERROR, "Out of memory.", "PlatformUtils::getCurrentExecutableFilePath");

	if (!GetModuleFileName(NULL, tcharExecutablePath, MAX_PATH))
	{
		OGRE_DELETE(tcharExecutablePath);

		// Throw an exception.
		OGRE_EXCEPT(Exception::ERR_INTERNAL_ERROR,
					"WinAPI GetModuleFileName() failed, the returned error message is: " + PlatformUtils::GetLastError_AsString(),
					"PlatformUtils::getCurrentExecutableFilePath()");
	}

	String baseName, strExecutablePath;
	StringUtil::splitFilename(String(tcharExecutablePath), baseName, strExecutablePath);

	OGRE_DELETE(tcharExecutablePath);

	return strExecutablePath;

#elif OGRE_PLATFORM == OGRE_PLATFORM_LINUX
		char executablePath[PATH_MAX + 1];
		ssize_t length;
	
		length = readlink("/proc/self/exe", executablePath, PATH_MAX);
		if (length < 0)
			OGRE_EXCEPT(Exception::ERR_INTERNAL_ERROR, strerror(errno), "PlatformUtils::getCurrentExecutableFilePath");
		
		executablePath[length] = '\0';
		String strExecutablePath(dirname(executablePath));
	
		return strExecutablePath;
#elif OGRE_PLATFORM == OGRE_PLATFORM_APPLE

#endif
}


} // namespace dsi
} // namespace Ogre