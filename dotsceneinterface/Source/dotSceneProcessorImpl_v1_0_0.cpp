/*
-----------------------------------------------------------------------------
Original file:	???
New Author:		Balazs Hajdics (wolverine_@freemail.hu), James Le Cuirot, Ihor Tregubov, Daniel Banky (ViveTech Ltd., daniel.banky@vivetech.com)

Copyright (c) 2007 Balazs Hajdics

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.

-----------------------------------------------------------------------------
*/

#include "dotSceneStableHeaders.h"

#include "DotSceneProcessorImpl_v1_0_0.h"

#include "dotSceneXmlNodeProcessors.h"
#include "dotSceneNodeProcessorTreeBuilder.h"

//***************************************************************************************************
/// define those just in case
#define	PREFIX_NODE			//"dsiNode"
#define	PREFIX_ENT			//"dsiEntity"
#define	PREFIX_LIGHT		//"dsiLight"
#define	PREFIX_CAMERA		//"dsiCamera"
#define	PREFIX_BILLBOARDSET	//"dsiBillboardSet"
#define	PREFIX_BILLBOARD	//"dsiBillboard"

namespace Ogre
{
	//***************************************************************************************************
	namespace dsi
	{
		//***************************************************************************************************
		DotSceneProcessorImpl_v1_0_0::DotSceneProcessorImpl_v1_0_0():
			mRootXmlNodeProcessor(NULL),
			mDotSceneNodeProcessorTreeBuilder(NULL),
			mCommonParamsOfXmlNodeProcessors(NULL)
		{
			mpSceneManager	= NULL;
			mpWin			= NULL;
			mpRoot			= NULL;
			mDoMats			= false;
			mShadow			= false;
			mDoInfo			= false;
		}
		//***************************************************************************************************
		DotSceneProcessorImpl_v1_0_0::~DotSceneProcessorImpl_v1_0_0()
		{
			shutdown();
		}
		//***************************************************************************************************

		//***************************************************************************************************
		const String& DotSceneProcessorImpl_v1_0_0::getVersion()
		{
			return getStVersion();
		}
		//***************************************************************************************************

		//***************************************************************************************************
		void DotSceneProcessorImpl_v1_0_0::initialize()
		{
			// Create common params of xml node processors and build the xml node processor tree.
			mCommonParamsOfXmlNodeProcessors = new CommonParamsOfXmlNodeProcessors;

			mDotSceneNodeProcessorTreeBuilder	= new DotSceneNodeProcessorTreeBuilder(mCommonParamsOfXmlNodeProcessors);
			mRootXmlNodeProcessor				= mDotSceneNodeProcessorTreeBuilder->buildTree();
		}
		//***************************************************************************************************

		//***************************************************************************************************
		void DotSceneProcessorImpl_v1_0_0::load(
			TiXmlDocument*					pXMLDoc,
			SceneManager*					pSceneManager,
			RenderWindow*					pRWin,
			const String&					groupName,
			const String&					strNamePrefix,
			const StringVector&				tagsToSkip,
			SceneNode*						pRootNode,
			bool							doMaterials,
			bool							forceShadowBuffers,
			DotSceneInfo**					ppDotSceneInfo
			)
		{
			// ------------------------------------------------ Copy parameters to member variables ------------------------------------------------
			assert(pXMLDoc); assert(pSceneManager); assert(pRWin);
			mpXMLDoc		= pXMLDoc;
			mpSceneManager	= pSceneManager; // This is a must.
			mpWin			= pRWin;		 // This is a must.

			// Do the root node.
			if (pRootNode)
				mpRoot = pRootNode;
			else
			{
				mpRoot = mpSceneManager->getRootSceneNode();
				if (!mpRoot)
					OGRE_EXCEPT(Exception::ERR_INTERNAL_ERROR,
								"Something went wrong while creating the child of the SceneRoot node.\nWEIRD", "DotSceneProcessorImpl_v1_0_0::load");
			}
			mDoMats		= doMaterials;
			mShadow		= forceShadowBuffers;
			mDoInfo		= ppDotSceneInfo != 0;
			mGroupName	= groupName;
			mTagsToSkip = tagsToSkip;
			
			for (StringVector::const_iterator it= mTagsToSkip.begin(); it != mTagsToSkip.end(); ++it)
			{
				mDotSceneNodeProcessorTreeBuilder->unregisterElementProcessor(*it);
			}

			// ------------------------------------------------------- Set common parameters -------------------------------------------------------
			mCommonParamsOfXmlNodeProcessors->setParameter( "DotSceneProcessor", this );
			mCommonParamsOfXmlNodeProcessors->setParameter("SceneManager", pSceneManager);
			mCommonParamsOfXmlNodeProcessors->setParameter("RootSceneNode", pRootNode);

			//			mCommonParamsOfXmlNodeProcessors->setParameter("ResourceGroup", &ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME);
			mCommonParamsOfXmlNodeProcessors->setParameter("ResourceGroup", 0);
			//			mCommonParamsOfXmlNodeProcessors->setParameter("ResourceGroup", new String("UncreatedTestResourceGroup"));
			//			ResourceGroupManager::getSingleton().createResourceGroup("CreatedTestResourceGroup");
			//			mCommonParamsOfXmlNodeProcessors->setParameter("ResourceGroup", new String("CreatedTestResourceGroup"));
			//			mCommonParamsOfXmlNodeProcessors->setParameter("ResourceGroup", new String(""));

			mCommonParamsOfXmlNodeProcessors->setParameter("RenderWindow", pRWin);
			//mCommonParamsOfXmlNodeProcessors->setParameter("NamePrefix", new String("Test_"));
			mCommonParamsOfXmlNodeProcessors->setParameter("NamePrefix", new String(strNamePrefix.c_str()));
			
			if ( ppDotSceneInfo )
				mCommonParamsOfXmlNodeProcessors->setParameter("DotSceneInfo", *ppDotSceneInfo);
			else // If ppDotSceneInfo is set to 0, then do not give information about the loaded scene at all.
				mCommonParamsOfXmlNodeProcessors->setParameter("DotSceneInfo", 0);


			// ----------------------------------------------------------- Load the scene ----------------------------------------------------------
			mRootXmlNodeProcessor->parseElement(0, mpXMLDoc->RootElement());

			// --- New testing version.
			// TESTING PART!!!
			// Test if resource declarations are created for resources loaded indirectly due to calls of e.g.
			// SceneManager::createEntity().
				//ResourceGroupManager::ResourceDeclarationList resourceDeclarationList = 
				//	ResourceGroupManager::getSingleton().getResourceDeclarationList(*((String*)mCommonParamsOfXmlNodeProcessors->getParameter("ResourceGroup")));
	
				//LogManager::getSingleton().logMessage("Resources declarations after loading the dotScene file: \n", LML_NORMAL, false);

				//for (	ResourceGroupManager::ResourceDeclarationList::iterator it = resourceDeclarationList.begin();
				//		it != resourceDeclarationList.end(); it++ )
				//{
				//	LogManager::getSingleton().logMessage(
				//		"Resource name: \"" + it->resourceName + "\"\n"
				//		"Resource type: \"" + it->resourceType + "\"\n",
				//		LML_NORMAL, false);
				//}
			// END OF TESTING PART!!!

			StringVector optionKeys;
			bool success = mpSceneManager->getOptionKeys(optionKeys);
			SceneManager::AnimationIterator itAnimation = mpSceneManager->getAnimationIterator();
			while ( itAnimation.hasMoreElements() )
			{
				Animation* animation = itAnimation.getNext();
				String animationName = animation->getName();
			}

			// ---
		}
		//***************************************************************************************************

        void DotSceneProcessorImpl_v1_0_0::save(
            TiXmlDocument*				pXMLDoc,
            SceneManager*				pSceneManager,
            RenderWindow*				pRWin,
			const StringVector&			tagsToSkip,
            DotSceneInfo**				ppDotSceneInfo
            )
        {
            assert(pXMLDoc); assert(pSceneManager); assert(pRWin);
            mpXMLDoc		= pXMLDoc;
            mpSceneManager	= pSceneManager; // This is a must.
            mpWin			= pRWin;		 // This is a must.
			mTagsToSkip = tagsToSkip;

			for (StringVector::const_iterator it= mTagsToSkip.begin(); it != mTagsToSkip.end(); ++it)
			{
				mDotSceneNodeProcessorTreeBuilder->unregisterElementProcessor(*it);
			}

            //mDoMats		= doMaterials;
            //mShadow		= forceShadowBuffers;
            //mDoInfo		= ppDotSceneInfo != 0;
            //mGroupName	= groupName;

			mCommonParamsOfXmlNodeProcessors->setParameter( "DotSceneProcessor", this );
            mCommonParamsOfXmlNodeProcessors->setParameter( "SceneManager", pSceneManager );
            mCommonParamsOfXmlNodeProcessors->setParameter( "RootSceneNode", pSceneManager->getRootSceneNode());
            mCommonParamsOfXmlNodeProcessors->setParameter( "ResourceGroup", 0);
            mCommonParamsOfXmlNodeProcessors->setParameter( "RenderWindow", pRWin);
            mCommonParamsOfXmlNodeProcessors->setParameter( "NamePrefix", "");

            if ( ppDotSceneInfo )
                mCommonParamsOfXmlNodeProcessors->setParameter("DotSceneInfo", *ppDotSceneInfo);
            else // If ppDotSceneInfo is set to 0, then do not give information about the loaded scene at all.
                mCommonParamsOfXmlNodeProcessors->setParameter("DotSceneInfo", 0);

            TiXmlDocument serializepXMLDoc(pXMLDoc->Value());

            mRootXmlNodeProcessor->serializeRootElement( "scene", serializepXMLDoc, (void*)pSceneManager );

            serializepXMLDoc.SaveFile();
        }
		//***************************************************************************************************

		void DotSceneProcessorImpl_v1_0_0::shutdown()
		{
			if (mRootXmlNodeProcessor) delete mRootXmlNodeProcessor;
			if (mDotSceneNodeProcessorTreeBuilder) delete mDotSceneNodeProcessorTreeBuilder;
			if (mCommonParamsOfXmlNodeProcessors) delete mCommonParamsOfXmlNodeProcessors;

			mRootXmlNodeProcessor = NULL;
			mDotSceneNodeProcessorTreeBuilder = NULL;
			mCommonParamsOfXmlNodeProcessors = NULL;
		}
		//***************************************************************************************************

		//***************************************************************************************************
		const CommonParamsOfXmlNodeProcessors*
		DotSceneProcessorImpl_v1_0_0::getCommonParams() const
		{
			return mCommonParamsOfXmlNodeProcessors;
		}
		//***************************************************************************************************
		CommonParamsOfXmlNodeProcessors*
		DotSceneProcessorImpl_v1_0_0::getCommonParams()
		{
			return mCommonParamsOfXmlNodeProcessors;
		}
		//***************************************************************************************************
		const DotSceneNodeProcessorTreeBuilder*
		DotSceneProcessorImpl_v1_0_0::getTreeBuilder() const
		{
			return mDotSceneNodeProcessorTreeBuilder;
		}
		//***************************************************************************************************
		DotSceneNodeProcessorTreeBuilder*
			DotSceneProcessorImpl_v1_0_0::getTreeBuilder()
		{
			return mDotSceneNodeProcessorTreeBuilder;
		}
		//***************************************************************************************************
		const StringVector&
			DotSceneProcessorImpl_v1_0_0::getTagsToSkip() const 
		{
			return mTagsToSkip;
		}
		//***************************************************************************************************
	}// namespace dsi
	//*******************************************************************************************************
}// namespace Ogre
//*******************************************************************************************************
