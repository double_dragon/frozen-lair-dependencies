/*
-----------------------------------------------------------------------------
Original file:	???
New Author:		Balazs Hajdics (wolverine_@freemail.hu), James Le Cuirot, Ihor Tregubov, Daniel Banky (ViveTech Ltd., daniel.banky@vivetech.com)

Copyright (c) 2007 Balazs Hajdics

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.

-----------------------------------------------------------------------------
*/

#include "dotSceneStableHeaders.h"

#include "DotSceneProcessor.h"
#include "dotSceneResourceManager.h"
#include "DotSceneProcessorImpl_v1_0_0.h"

#include "tinyxml.h"

namespace Ogre
{
	//***************************************************************************************************
	template<> dsi::DotSceneProcessor *Singleton<dsi::DotSceneProcessor>::msSingleton = 0;
	const String dsi::DotSceneProcessor::DEFAULT = "DEFAULT";
	//***************************************************************************************************
	namespace dsi
	{
	//***************************************************************************************************
	DotSceneProcessor::DotSceneProcessor() :
		mpCurrentProcessor( NULL ),
		mpXMLDoc( NULL ),
		mpDefaultProcessor( NULL )
	{
		// I register here all DotSceneProcessor implementations. Maybe it's not the best place, but I suppose
		// it should work even without breaking the interface after compiling...
		// ie. after adding a new implementation and adding it here the client app should not require
		// to be recompiled against the new DLL library. At least I hope so.
		// ***************************************************************************
		// IT IS VERY IMPORTANT TO INSERT THE NEWEST AVAILABLE IMPLEMENTATION LAST !!!
		// ***************************************************************************
		DotSceneProcessorImpl_v1_0_0* dotSceneProcessorImpl_v1_0_0 = new dsi::DotSceneProcessorImpl_v1_0_0();
		mpDefaultProcessor = dotSceneProcessorImpl_v1_0_0;
		mDotSceneProcessorImplementations.insert(DotSceneProcessorImplMap::value_type(DEFAULT, dotSceneProcessorImpl_v1_0_0));
		mDotSceneProcessorImplementations.insert(DotSceneProcessorImplMap::value_type(dotSceneProcessorImpl_v1_0_0->getVersion(), dotSceneProcessorImpl_v1_0_0));
	}
	//***************************************************************************************************
	DotSceneProcessor::~DotSceneProcessor()
	{
		// Delete DEFAULT processor
		DotSceneProcessorImplMap::iterator it = mDotSceneProcessorImplementations.find( DEFAULT );
		if ( it != mDotSceneProcessorImplementations.end() ) delete it->second;

		// Clean up all.
		mDotSceneProcessorImplementations.clear();
		// TODO: store DotSceneInfo instance of the last loaded scene or all of the loaded scenes and clear it on destruction?
	}
	//***************************************************************************************************
	void DotSceneProcessor::initialize(const String& version)
	{
		// We must at least have one loader registered, so check this first.
		if (mDotSceneProcessorImplementations.empty()) 
			OGRE_EXCEPT(Exception::ERR_ITEM_NOT_FOUND, 
						"There is no registered .scene loader implementation available at all. Recompile the source and call"
						"mDotSceneProcessorImplementations.insert() with your .scene loader implementation.",
						"DotSceneProcessor::load"); 
		
		// Initialize all DotScene processor implementations.
		for (DotSceneProcessorImplMap::iterator i = mDotSceneProcessorImplementations.begin(); i != mDotSceneProcessorImplementations.end(); i++)
			if (i->second->getVersion() != DEFAULT)
				i->second->initialize();
	}

	//***************************************************************************************************
	void DotSceneProcessor::load(
		const Ogre::String&				fileName, 
		SceneManager*					pSceneManager, 
		RenderWindow*					pRWin, 
		const String&					groupName,
		const String&					strNamePrefix,
		const StringVector&					tagsToSkip,
		SceneNode*						pRootNode,
		bool							doMaterials,
		bool							forceShadowBuffers,
		DotSceneInfo**					ppDotSceneInfo
		)
	{ 
		// Create DotSceneInfo instance.
		if ( ppDotSceneInfo )
		{
			// If *ppDotSceneInfo is null, then user hasn't pre-created DotSceneInfo (this usually happens when he don't want us to
			// upgrade an already existing structure), so create a new DotSceneInfo instance.
			if ( !(*ppDotSceneInfo) ) *ppDotSceneInfo = new DotSceneInfo;
		}

		// Load the .scene.
		mpXMLDoc = new TiXmlDocument(fileName);

		if (!mpXMLDoc->LoadFile()) 
		{ 
			mpXMLDoc->ClearError(); 

			// Try to look for the file in the resource paths... 
			DataStreamPtr pStream = ResourceGroupManager::getSingleton().openResource(fileName, groupName); 
			if(pStream->size()) 
			{
				String s = pStream->getAsString() + "\0"; 
				mpXMLDoc->Parse(s.c_str()); 

				// Check for errors .
				if (mpXMLDoc->Error()) 
				{ 
					String errDesc = mpXMLDoc->ErrorDesc(); 
					delete mpXMLDoc; 
					OGRE_EXCEPT(Exception::ERR_INVALIDPARAMS,
								"An error occured while loading the .scene file named \"" + fileName + "\" :\n" + errDesc,
								"DotSceneProcessor::load"); 
				}
			}
			else
				// Since ResourceGroupManager::openResource() throws an exception if the file can not be opened, or the resource
				// group not found, this should be an invalid file (e.g. a file with 0 size).
				OGRE_EXCEPT(Exception::ERR_INVALIDPARAMS,
							"An error occured while loading the .scene file named \"" + fileName + "\":\n "
							"The opened file has the size 0?!",
							"DotSceneProcessor::load"); 
		} 
		// say what we're doing to the interested public 
		this->updateProgressListeners("loading \"" + fileName + "\""); 
		TiXmlElement *pElem = NULL;
		TiXmlElement *pRootElem = mpXMLDoc->RootElement(); 
		// Check version first, fail if it's not what we want.
        String ver = BLANKSTRING;
        ver = pRootElem->Attribute("formatVersion") ? pRootElem->Attribute("formatVersion") : BLANKSTRING;
		// Find a suitable loader implementation.
        if (ver == BLANKSTRING)
		{ 
			// Not found default to the newest version available.
			if ( ppDotSceneInfo && *ppDotSceneInfo )
				(*ppDotSceneInfo)->logLoadWarning(".scene file does not have a \"formatVersion\" attribute, will default/try with the newest registered dotSceneImpl..."); 

			mpCurrentProcessor = mDotSceneProcessorImplementations.begin()->second;
		}
		else
		{ 
			// Try to find exact match first.
			DotSceneProcessorImplMap::iterator it = mDotSceneProcessorImplementations.find(ver); 
			if (it == mDotSceneProcessorImplementations.end()) 
			{ 
				// Not found default to the newest version available.
				if ( ppDotSceneInfo && *ppDotSceneInfo )
					(*ppDotSceneInfo)->logLoadWarning(".scene file does not have a \"formatVersion\" attribute, will default/try with the newest registered dotSceneImpl..."); 

				mpCurrentProcessor = mDotSceneProcessorImplementations.begin()->second;
			}
			else // Use the loaderImpl to load the scene.
				mpCurrentProcessor =	it->second;
		}

		mpCurrentProcessor->load(mpXMLDoc, pSceneManager, pRWin, groupName, strNamePrefix, tagsToSkip, pRootNode, doMaterials, forceShadowBuffers, ppDotSceneInfo);
		delete mpXMLDoc; 
		mpXMLDoc = NULL;
	}
    //***************************************************************************************************
	void DotSceneProcessor::load(
		const Ogre::String&				fileName, 
		SceneManager*					pSceneManager, 
		RenderWindow*					pRWin, 
		const String&					groupName,
		const String&					strNamePrefix,
		SceneNode*						pRootNode,
		bool							doMaterials,
		bool							forceShadowBuffers,
		DotSceneInfo**					ppDotSceneInfo
		)
	{ 
		this->load(fileName, pSceneManager, pRWin, groupName, strNamePrefix, StringVector(),
			pRootNode, doMaterials, forceShadowBuffers, ppDotSceneInfo);
	}
	//***************************************************************************************************
    void DotSceneProcessor::save(		
        const String&					fileName, 
        SceneManager*					pSceneManager, 
        RenderWindow*					pRWin, 
		const StringVector&					tagsToSkip,
        DotSceneInfo**					ppDotSceneInfo,
		const Ogre::String&				version)
    {
        if ( ppDotSceneInfo )
        {
            if ( !(*ppDotSceneInfo) ) 
            {
                *ppDotSceneInfo = new DotSceneInfo;
            }
        }

        mpXMLDoc = new TiXmlDocument(fileName);

        this->updateProgressListeners("saving \"" + fileName + "\"");

        TiXmlElement *pElem = NULL;
        TiXmlElement *pRootElem = mpXMLDoc->RootElement(); 

		String ver = DEFAULT;

		DotSceneProcessorImplMap::iterator it = mDotSceneProcessorImplementations.find(version);

		if ( it != mDotSceneProcessorImplementations.end() )
		{
			ver = it->first;
			mpCurrentProcessor = it->second;
		}
		else
		{
			mpCurrentProcessor = mpDefaultProcessor;
			if ( ppDotSceneInfo && *ppDotSceneInfo )
				(*ppDotSceneInfo)->logLoadWarning("dotSceneProcessorImplementation version "+ version +" is not registred.");
		}
			
        mpCurrentProcessor->save(mpXMLDoc, pSceneManager, pRWin, tagsToSkip, ppDotSceneInfo);

        delete mpXMLDoc; 
        mpXMLDoc = NULL;
    }
	//***************************************************************************************************
    void DotSceneProcessor::save(		
        const String&					fileName, 
        SceneManager*					pSceneManager, 
        RenderWindow*					pRWin, 
        DotSceneInfo**					ppDotSceneInfo,
		const Ogre::String&				version )
    {
		this->save(fileName, pSceneManager, pRWin, StringVector(), ppDotSceneInfo, version);
	}
	//***************************************************************************************************
	void DotSceneProcessor::shutdown()
	{
		// Initialize all DotScene processor implementations.
		for (DotSceneProcessorImplMap::iterator i = mDotSceneProcessorImplementations.begin(); i != mDotSceneProcessorImplementations.end(); i++)
			if (i->first != DEFAULT)
				i->second->shutdown();

		mpCurrentProcessor = NULL;
		mpXMLDoc = NULL;
	}
	//***************************************************************************************************
	void DotSceneProcessor::addProgressListener(progressListener *pNewPL)
	{
		DotSceneProcessorImplMap::iterator it = mDotSceneProcessorImplementations.begin();
		for (; it != mDotSceneProcessorImplementations.end(); ++it)
			it->second->addProgressListener(pNewPL);
	}
	//***************************************************************************************************
	DotSceneProcessorImpl* DotSceneProcessor::getImplementation(const String& version)
	{
		DotSceneProcessorImplMap::const_iterator it = mDotSceneProcessorImplementations.find( version );
		if ( it != mDotSceneProcessorImplementations.end() )
			return it->second;
		else
			OGRE_EXCEPT(Exception::ERR_INVALIDPARAMS,
			"An error occured while getting dotSceneProcessorImplementation:\n "
			"Version not found: " + version + "\n",
			"DotSceneProcessor::getImplementation"); 
	}
	//***************************************************************************************************
	void DotSceneProcessor::addImplementation(DotSceneProcessorImpl* impl)
	{
		DotSceneProcessorImplMap::const_iterator it = mDotSceneProcessorImplementations.find( impl->getVersion() );
		if ( it == mDotSceneProcessorImplementations.end() )
			mDotSceneProcessorImplementations.insert(DotSceneProcessorImplMap::value_type(impl->getVersion(), impl));
		else
			OGRE_EXCEPT(Exception::ERR_INVALIDPARAMS,
			"An error occured while adding dotSceneProcessorImplementation:\n "
			"Version already exists: " + impl->getVersion() + "\n",
			"DotSceneProcessor::addImplementation"); 
	}
	//***************************************************************************************************
	}// namespace dsi
	//*******************************************************************************************************
}// namespace Ogre
//*******************************************************************************************************
