/*
-----------------------------------------------------------------------------
Original file:	???
New Author:		Balazs Hajdics (wolverine_@freemail.hu), James Le Cuirot, Ihor Tregubov, Daniel Banky (ViveTech Ltd., daniel.banky@vivetech.com)

Copyright (c) 2007 Balazs Hajdics

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.

-----------------------------------------------------------------------------
*/

#include "dotSceneStableHeaders.h"

#include "DotSceneInfo.h"

namespace Ogre
{
//****************************************************************************************************
namespace dsi
{
	void DotSceneInfo::clear(bool clearRootSceneNodeAndSceneManager)
	{
		if ( clearRootSceneNodeAndSceneManager )
		{
			mRootSceneNode	= 0;
			mSceneManager	= 0;
		}
		mLoadedSceneNodes.clear();

//		mLoadedHDRCompositorInstancesMap.clear();


		mLoadedMovableObjects.clear();
		mLoadedStaticMovableObjects.clear();
		mLoadedNonStaticMovableObjects.clear();

		mLoadedEntities.clear();
		mLoadedNonStaticEntities.clear();
		mLoadedStaticEntities.clear();

		mLoadedLights.clear();
		mLoadedNonStaticLights.clear();
		mLoadedStaticLights.clear();

		mLoadedCameras.clear();
		mLoadedNonStaticCameras.clear();
		mLoadedStaticCameras.clear();

		mLoadedParticleSystems.clear();
		mLoadedNonStaticParticleSystems.clear();
		mLoadedStaticParticleSystems.clear();

		mLoadedBillboardSets.clear();
		mLoadedNonStaticBillboardSets.clear();
		mLoadedStaticBillboardSets.clear();

		mLoadedAnimationStates.clear();
		mLoadedSceneManagerAnimationStates.clear();
		mLoadedEntityAnimationStates.clear();

		mLoadedAnimations.clear();
		mLoadedSceneManagerAnimations.clear();
		mLoadedEntityAnimations.clear();

		mFogData.clear();
		mSkyBoxes.clear();
		mSkyDomes.clear();
		mSkyPlanes.clear();

		mAmbientLightColour			= ColourValue::Black;
		mHasAmbientLightColourSet	= false;

		mBackgroundColour			= ColourValue::Black;
		mHasBackgroundColourSet		= false;

		mLoadLogs.clear();
		mLoadInfoLogs.clear();
		mLoadWarningLogs.clear();
		mLoadErrorLogs.clear();
	}

	void	DotSceneInfo::DestroyDotScene(bool removeSceneNodes)
	{
		assert(mSceneManager && "DotSceneInfo instances must consists a pointer to a valid SceneManager instance!");


		// --------------------------------- Destroy HDR Compositor instances' listeners ----------------------------------
//		for (	LoadedHDRCompositorInstancesMap::iterator it = mLoadedHDRCompositorInstancesMap.begin();
//				it != mLoadedHDRCompositorInstancesMap.end(); it++ )
//		{
//			OGRE_DELETE(it->second);
//		}


		// ------------------------------------------- Destroy animation states -------------------------------------------
		for (	SceneManagerAnimationStatesMultiMap::iterator it = mLoadedSceneManagerAnimationStates.begin();
				it != mLoadedSceneManagerAnimationStates.end(); it++ )
		{
			it->first->destroyAnimationState(it->second->getAnimationName());
		}

		for (	EntityAnimationStatesMultiMap::iterator it = mLoadedEntityAnimationStates.begin();
				it != mLoadedEntityAnimationStates.end(); it++ )
		{
			it->first->getAllAnimationStates()->removeAnimationState(it->second->getAnimationName());
		}

		// ---------------------------------------------- Destroy animations -----------------------------------------------
		for (	SceneManagerAnimationsMultiMap::iterator it = mLoadedSceneManagerAnimations.begin();
				it != mLoadedSceneManagerAnimations.end(); it++ )
		{
			it->first->destroyAnimation(it->second->getName());
		}

		for (	EntityAnimationsMultiMap::iterator it = mLoadedEntityAnimations.begin();
				it != mLoadedEntityAnimations.end(); it++ )
		{
			if ( it->first->getMesh()->hasAnimation(it->second->getName()) )
				it->first->getMesh()->removeAnimation(it->second->getName());
			else if ( it->first->getSkeleton()->hasAnimation(it->second->getName()) )
				it->first->getSkeleton()->removeAnimation(it->second->getName());
		}


		// ----------------------------------------- Destroy all movable objects ------------------------------------------
		for (	LoadedMovableObjectList::iterator it = mLoadedMovableObjects.begin();
				it != mLoadedMovableObjects.end(); it++ )
		{
			// First detach object from its parent node, to avoid access violation.
			SceneNode* parentSceneNode;
			if ( (parentSceneNode = (*it)->getParentSceneNode()) ) parentSceneNode->detachObject((*it));

			// Now destroy the give movable object.
			if ( (*it)->getMovableType() != "Camera" )
			{
				mSceneManager->destroyMovableObject((*it)->getName(), (*it)->getMovableType());
			}else
			{// Cameras in Ogre 1.4.x although movable object instances, they don't behave like movable objects, e.g. they don't
			 // have a factory class which creates/destroys them so they have to be destroyed separately.
				mSceneManager->destroyCamera((Camera*)(*it));
			}
		}

		// ------------------------------------------ Destroy loaded scene nodes -------------------------------------------
		// This is done last.
		if ( removeSceneNodes )
		{
			if ( mRootSceneNode )
			{
				mRootSceneNode->removeAndDestroyAllChildren();

				try
				{
					mSceneManager->destroySceneNode(mRootSceneNode->getName());
				}catch(Ogre::Exception& ex)
				{// If the given root node is not only the root node of the .scene file, but the entire scene, it can not be destroyed.
				 // Root scene node can not be destroyed. So an exception will be thrown. Ignore it silently.
					(void)ex;
				}

				mRootSceneNode = 0;
			}
		}


		// ------------------------------------------ Clear DotSceneInfo instance ------------------------------------------
		clear(false);
	}


	/// scene graph static obj's container
	DotSceneInfo::tMapMovableEx	DotSceneInfo::mMOStatic;
	/// scene graph dynamic obj's container
	DotSceneInfo::tMapMovableEx	DotSceneInfo::mMODynamic;
	/// scene graph all obj's container
	DotSceneInfo::tMapMovableEx	DotSceneInfo::mMOAll;
	/// scene graph target nodes container
	DotSceneInfo::tMapSceneNode	DotSceneInfo::mSNTargets;
	/// num tris in current scene graph
	size_t						DotSceneInfo::mNumTris;
	/// up to date flag
	bool						DotSceneInfo::mIsUpToDateWithScene	= false;
	//**************************************************************************************************
	//void DotSceneInfo::_addMovable(Ogre::MovableObject *pMO, Ogre::Vector3 &pos, Ogre::Quaternion &rot, Ogre::Vector3 &scale, bool isStatic)
	//{
	//	if (isStatic)
	//		mMOStatic.insert(tMapMovableEx::value_type(pMO->getName(), new types::MovableObjectEx(pMO, pos, rot, scale, isStatic)));
	//	else
	//		mMODynamic.insert(tMapMovableEx::value_type(pMO->getName(), new types::MovableObjectEx(pMO, pos, rot, scale, isStatic)));
	//	mMOAll.insert(tMapMovableEx::value_type(pMO->getName(), new types::MovableObjectEx(pMO, pos, rot, scale, isStatic)));
	//	if (pMO->getMovableType() == "Entity")
	//	{
	//		Ogre::MeshPtr pMesh = ((Ogre::Entity *)pMO)->getMesh();
	//		if (pMesh->sharedVertexData)
	//			mNumTris += pMesh->sharedVertexData->vertexCount;
	//		else
	//		{
	//			for (int n = 0; n < pMesh->getNumSubMeshes(); ++n)
	//				mNumTris += pMesh->getSubMesh(n)->vertexData->vertexCount;
	//		}
	//	}
	//}
	////**************************************************************************************************
	//void DotSceneInfo::_addTargetSceneNode(Ogre::SceneNode *pSN)
	//{
	//	mSNTargets.insert(tMapSceneNode::value_type(pSN->getName(), pSN));
	//}

	//**************************************************************************************************
	void DotSceneInfo::logLoadInfo(const Ogre::String &text)
	{	
		mLoadLogs.push_back("dsi::INFO->" + text + "\n");
		mLoadInfoLogs.push_back("dsi::INFO->" + text + "\n");

		Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_TRIVIAL, String("dsi::INFO->" + text + "\n"));
	}
	//**************************************************************************************************
	void DotSceneInfo::logLoadWarning(const Ogre::String &text)
	{
		mLoadLogs.push_back("dsi::WARNING->" + text + "\n");
		mLoadWarningLogs.push_back("dsi::INFO->" + text + "\n");

		Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_NORMAL, String("dsi::WARNING->" + text + "\n"));
	}
	//**************************************************************************************************
	void DotSceneInfo::logLoadError(const Ogre::String &text)
	{
		mLoadLogs.push_back("dsi::ERROR->" + text + "\n");
		mLoadErrorLogs.push_back("dsi::INFO->" + text + "\n");

		Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_CRITICAL, String("dsi::ERROR->" + text + "\n"));
	}


	// ------------------------------------------------------ Scene ------------------------------------------------------
	void DotSceneInfo::setSceneAuthor( const String& sAuthor )
	{
		msSceneAuthor = sAuthor;
	}
	String DotSceneInfo::getSceneAuthor()
	{
		return msSceneAuthor;
	}
	void DotSceneInfo::setSceneID( const String& sID )
	{
		msSceneID = sID;
	}
	String DotSceneInfo::getSceneID()
	{
		return msSceneID;
	}
	void DotSceneInfo::setOrientationSaveType( int orientationSaveType )
	{
		mOrientationSaveType = orientationSaveType;
	}
	int DotSceneInfo::getOrientationSaveType()
	{
		return mOrientationSaveType;
	}
	// ------------------------------------------------------ Scene ------------------------------------------------------
	
	
	// ------------------------------------------------------ LoadedElements ------------------------------------------------------
	DotSceneInfo::ConstLoadedLightIterator DotSceneInfo::getStaticLightsIterator() const
	{
		return ConstLoadedLightIterator(
			mLoadedStaticLights.begin(), mLoadedStaticLights.end());
	}

	DotSceneInfo::ConstLoadedCameraIterator DotSceneInfo::getStaticCamerasIterator()const
	{
		return ConstLoadedCameraIterator(
			mLoadedStaticCameras.begin(), mLoadedStaticCameras.end());
	}

	DotSceneInfo::ConstLoadedEntityIterator DotSceneInfo::getStaticEntitiesIterator()const
	{
		return ConstLoadedEntityIterator(
			mLoadedStaticEntities.begin(), mLoadedStaticEntities.end());
	}

	DotSceneInfo::ConstLoadedParticleSystemIterator DotSceneInfo::getStaticParticleSystemsIterator()const
	{
		return ConstLoadedParticleSystemIterator(
			mLoadedStaticParticleSystems.begin(), mLoadedStaticParticleSystems.end());
	}

	DotSceneInfo::ConstLoadedBillboardSetIterator DotSceneInfo::getStaticBillboardSetsIterator()const
	{
		return ConstLoadedBillboardSetIterator(
			mLoadedStaticBillboardSets.begin(), mLoadedStaticBillboardSets.end());
	}
	// ------------------------------------------------------ LoadedElements ------------------------------------------------------

}// namespace dsi
}// namespace Ogre
