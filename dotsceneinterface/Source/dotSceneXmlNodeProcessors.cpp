/*
-----------------------------------------------------------------------------
Author:		Balazs Hajdics (wolverine_@freemail.hu), James Le Cuirot, Ihor Tregubov, Daniel Banky (ViveTech Ltd., daniel.banky@vivetech.com)

Copyright (c) 2007 Balazs Hajdics

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.

-----------------------------------------------------------------------------
*/

// --------------------------------------------------- DotScene specific includes --------------------------------------------------
#include "dotSceneStableHeaders.h"

#include "dotSceneXmlNodeProcessors.h"
#include "dotScenePlatformUtils.h"

#include "dotSceneInfo.h"
#include "dotSceneUtils.h"


// --------------------------------------------------- XmlNodeProcessor includes ---------------------------------------------------
#include "XNPPrerequisites.h"
#include "XNPXmlNodeProcessor.h"
#include "XNPXmlNodeProcessorTreeBuilder.h"


// -------------------------------------------------------- Tinyxml includes -------------------------------------------------------
#include "tinyxml.h"


// --------------------------------------------------------- Ogre includes ---------------------------------------------------------
#include "Ogre.h"
#include <OgreInstancedGeometry.h>

// -------------------------------------------------------- HDRLib includes --------------------------------------------------------
//#include "HDRCompositor.h"


namespace Ogre {

// ----------------------------- The following functions should be placed into the class StringConverter -----------------------------

//	HDRCompositor::STARTYPE		StringStarTypeToEnumStarType(const String& strStarType)
//	{
//		typedef std::map<String, HDRCompositor::STARTYPE> StringStarTypeToEnumStarType;
//		static bool starTypeMapInited = false;

//		static StringStarTypeToEnumStarType mStringStarTypeToEnumStarTypeMap;

//		// --------------------- Create string type frame buffer type to enumerated type frame buffer type map ---------------------
//		if ( !starTypeMapInited )
//		{
//			mStringStarTypeToEnumStarTypeMap["none"]			= HDRCompositor::ST_NONE;
//			mStringStarTypeToEnumStarTypeMap["plus"]			= HDRCompositor::ST_PLUS;
//			mStringStarTypeToEnumStarTypeMap["cross"]			= HDRCompositor::ST_CROSS;
//			mStringStarTypeToEnumStarTypeMap["plusCross"]		= HDRCompositor::ST_PLUSCROSS;
//			starTypeMapInited = true;
//		}

//		// ------------------ Now convert frame buffer type given as string to frame buffer type given as an enum ------------------
//		HDRCompositor::STARTYPE eStarType;
//		StringStarTypeToEnumStarType::iterator it =
//			mStringStarTypeToEnumStarTypeMap.find(strStarType);
//		if ( it == mStringStarTypeToEnumStarTypeMap.end() )
//		{// Frame buffer type not found.
//			// Concatenate valid values together for logging.
//			it = mStringStarTypeToEnumStarTypeMap.begin();
//			String strValidValues = "\"" + it->first + "\"";
//			it++;
//			for ( ; it != mStringStarTypeToEnumStarTypeMap.end(); it++ )
//				strValidValues += ", \"" + it->first + "\"";

//			OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND,
//						"\"" + strStarType + "\" is not a valid star type.\n"
//						"Valid values are: " + strValidValues + ".",
//						"::StringStarTypeToEnumStarType()");
//		}else
//		{// Frame buffer type found.
//			eStarType = it->second;
//		}

//		return eStarType;
//	}

//	HDRCompositor::GLARETYPE	StringGlareTypeToEnumGlareType(const String& strGlareType)
//	{
//		typedef std::map<String, HDRCompositor::GLARETYPE> StringGlareTypeToEnumGlareType;
//		static bool glareTypeMapInited = false;

//		static StringGlareTypeToEnumGlareType mStringGlareTypeToEnumGlareTypeMap;

//		// --------------------- Create string type frame buffer type to enumerated type frame buffer type map ---------------------
//		if ( !glareTypeMapInited )
//		{
//			mStringGlareTypeToEnumGlareTypeMap["none"]			= HDRCompositor::GT_NONE;
//			mStringGlareTypeToEnumGlareTypeMap["blur"]			= HDRCompositor::GT_BLUR;
//			glareTypeMapInited = true;
//		}

//		// ------------------ Now convert frame buffer type given as string to frame buffer type given as an enum ------------------
//		HDRCompositor::GLARETYPE eGlareType;
//		StringGlareTypeToEnumGlareType::iterator it =
//			mStringGlareTypeToEnumGlareTypeMap.find(strGlareType);
//		if ( it == mStringGlareTypeToEnumGlareTypeMap.end() )
//		{// Frame buffer type not found.
//			// Concatenate valid values together for logging.
//			it = mStringGlareTypeToEnumGlareTypeMap.begin();
//			String strValidValues = "\"" + it->first + "\"";
//			it++;
//			for ( ; it != mStringGlareTypeToEnumGlareTypeMap.end(); it++ )
//				strValidValues += ", \"" + it->first + "\"";

//			OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND,
//						"\"" + strGlareType + "\" is not a valid glare type.\n"
//						"Valid values are: " + strValidValues + ".",
//						"::StringGlareTypeToEnumGlareType()");
//		}else
//		{// Frame buffer type found.
//			eGlareType = it->second;
//		}

//		return eGlareType;
//	}


//	HDRCompositor::TONEMAPPER	StringToneMapperToEnumToneMapper(const String& strToneMapper)
//	{
//		typedef std::map<String, HDRCompositor::TONEMAPPER> StringToneMapperToEnumToneMapper;
//		static bool toneMapperMapInited = false;

//		static StringToneMapperToEnumToneMapper mStringToneMapperToEnumToneMapperMap;

//		// --------------------- Create string type frame buffer type to enumerated type frame buffer type map ---------------------
//		if ( !toneMapperMapInited )
//		{
//			mStringToneMapperToEnumToneMapperMap["none"]				= HDRCompositor::TM_NONE;
//			mStringToneMapperToEnumToneMapperMap["linear"]				= HDRCompositor::TM_LINEAR;
//			mStringToneMapperToEnumToneMapperMap["reinhards"]			= HDRCompositor::TM_REINHARDS;
//			mStringToneMapperToEnumToneMapperMap["modifiedReinhards"]	= HDRCompositor::TM_REINHARDSMOD;
//			mStringToneMapperToEnumToneMapperMap["logaritmic"]			= HDRCompositor::TM_LOG;
//			mStringToneMapperToEnumToneMapperMap["adaptLogaritmic"]		= HDRCompositor::TM_ADAPTLOG;
//			mStringToneMapperToEnumToneMapperMap["reinhardLocal"]		= HDRCompositor::TM_REINHARDLOCAL;
//			mStringToneMapperToEnumToneMapperMap["count"]				= HDRCompositor::TM_COUNT;
//			toneMapperMapInited = true;
//		}

//		// ------------------ Now convert frame buffer type given as string to frame buffer type given as an enum ------------------
//		HDRCompositor::TONEMAPPER eToneMapper;
//		StringToneMapperToEnumToneMapper::iterator it =
//			mStringToneMapperToEnumToneMapperMap.find(strToneMapper);
//		if ( it == mStringToneMapperToEnumToneMapperMap.end() )
//		{// Frame buffer type not found.
//			// Concatenate valid values together for logging.
//			it = mStringToneMapperToEnumToneMapperMap.begin();
//			String strValidValues = "\"" + it->first + "\"";
//			it++;
//			for ( ; it != mStringToneMapperToEnumToneMapperMap.end(); it++ )
//				strValidValues += ", \"" + it->first + "\"";

//			OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND,
//						"\"" + strToneMapper + "\" is not a valid tone mapper type.\n"
//						"Valid values are: " + strValidValues + ".",
//						"::StringToneMapperToEnumToneMapper()");
//		}else
//		{// Frame buffer type found.
//			eToneMapper = it->second;
//		}

//		return eToneMapper;
//	}
	

	Ogre::FrameBufferType StringFrameBufferTypeToEnumFrameBufferType(const String& strFrameBufferType)
	{
		typedef std::map<String, FrameBufferType> StringFrameBufferTypeToEnumFrameBufferType;
		static bool frameBufferTypeMapInited = false;

		static StringFrameBufferTypeToEnumFrameBufferType mStringFrameBufferTypeToEnumFrameBufferTypeMap;

		// --------------------- Create string type frame buffer type to enumerated type frame buffer type map ---------------------
		if ( !frameBufferTypeMapInited )
		{
			mStringFrameBufferTypeToEnumFrameBufferTypeMap["colour"]		= Ogre::FBT_COLOUR;
			mStringFrameBufferTypeToEnumFrameBufferTypeMap["depth"]		= Ogre::FBT_DEPTH;
			mStringFrameBufferTypeToEnumFrameBufferTypeMap["stencil"]	= Ogre::FBT_STENCIL;
			frameBufferTypeMapInited = true;
		}

		// ------------------ Now convert frame buffer type given as string to frame buffer type given as an enum ------------------
		Ogre::FrameBufferType eFrameBufferType;
		StringFrameBufferTypeToEnumFrameBufferType::iterator it = 
			mStringFrameBufferTypeToEnumFrameBufferTypeMap.find(strFrameBufferType);
		if ( it == mStringFrameBufferTypeToEnumFrameBufferTypeMap.end() )
		{// Frame buffer type not found.
			// Concatenate valid values together for logging.
			it = mStringFrameBufferTypeToEnumFrameBufferTypeMap.begin();
			String strValidValues = "\"" + it->first + "\"";
			it++;
			for ( ; it != mStringFrameBufferTypeToEnumFrameBufferTypeMap.end(); it++ )
				strValidValues += ", \"" + it->first + "\"";

			OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND,
						"\"" + strFrameBufferType + "\" is not a valid frame buffer type.\n"
						"Valid values are: " + strValidValues + ".",
						"::StringTargetModeToEnumTargetMode()");
		}else
		{// Frame buffer type found.
			eFrameBufferType = it->second;
		}

		return eFrameBufferType;
	}

	Ogre::VertexAnimationTrack::TargetMode StringTargetModeToEnumTargetMode(const String& strTargetMode)
	{
		typedef std::map<String, Ogre::VertexAnimationTrack::TargetMode> StringTargetModeToEnumTargetModeMap;
		static bool targetModeMapInited = false;

		static StringTargetModeToEnumTargetModeMap mStringTargetModeToEnumTargetModeMap;

		// ----------------------------- Create string type angle unitto enumerated type angle unit map ------------------------------
		if ( !targetModeMapInited )
		{
			mStringTargetModeToEnumTargetModeMap["software"]	= Ogre::VertexAnimationTrack::TM_SOFTWARE;
			mStringTargetModeToEnumTargetModeMap["hardware"]	= Ogre::VertexAnimationTrack::TM_HARDWARE;
			targetModeMapInited = true;
		}

		// -------------------------- Now convert angle unit given as string to angle unit given as an enum --------------------------
		Ogre::VertexAnimationTrack::TargetMode eTargetMode;
		StringTargetModeToEnumTargetModeMap::iterator it = mStringTargetModeToEnumTargetModeMap.find(strTargetMode);
		if ( it == mStringTargetModeToEnumTargetModeMap.end() )
		{// Target mode not found.
			// Concatenate valid values together for logging.
			it = mStringTargetModeToEnumTargetModeMap.begin();
			String strValidValues = "\"" + it->first + "\"";
			it++;
			for ( ; it != mStringTargetModeToEnumTargetModeMap.end(); it++ )
				strValidValues += ", \"" + it->first + "\"";

			OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND,
						"\"" + strTargetMode + "\" is not a valid vertex animation track target mode.\n"
						"Valid values are: " + strValidValues + ".",
						"::StringTargetModeToEnumTargetMode()");
		}else
		{// Target mode found.
			eTargetMode = it->second;
		}

		return eTargetMode;
	}

	Ogre::VertexAnimationType StringVertexAnimationTypeToEnumVertexAnimationType(const String& strVertexAnimationType)
	{
		typedef std::map<String, Ogre::VertexAnimationType> StringVertexAnimationTypeToEnumVertexAnimationTypeMap;
		static bool vertexAnimationTypeMapInited = false;

		static StringVertexAnimationTypeToEnumVertexAnimationTypeMap mStringVertexAnimationTypeToEnumVertexAnimationTypeMap;

		// ------------------- Create string type vertex animation type to enumerated type vertex animation type -------------------
		if ( !vertexAnimationTypeMapInited )
		{
			mStringVertexAnimationTypeToEnumVertexAnimationTypeMap["morph"]	= Ogre::VAT_MORPH;
			mStringVertexAnimationTypeToEnumVertexAnimationTypeMap["pose"]	= Ogre::VAT_POSE;
			vertexAnimationTypeMapInited = true;
		}

		// -------------- Now convert vertex animation type given as string to vertex animation type given as an enum --------------
		Ogre::VertexAnimationType eVertexAnimationType;
		StringVertexAnimationTypeToEnumVertexAnimationTypeMap::iterator it =
			mStringVertexAnimationTypeToEnumVertexAnimationTypeMap.find(strVertexAnimationType);
		if ( it == mStringVertexAnimationTypeToEnumVertexAnimationTypeMap.end() )
		{// Vertex animation type not found.
			// Concatenate valid values together for logging.
			it = mStringVertexAnimationTypeToEnumVertexAnimationTypeMap.begin();
			String strValidValues = "\"" + it->first + "\"";
			it++;
			for ( ; it != mStringVertexAnimationTypeToEnumVertexAnimationTypeMap.end(); it++ )
				strValidValues += ", \"" + it->first + "\"";

			OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND,
						"\"" + strVertexAnimationType + "\" is not a valid angle unit.\n"
						"Valid values are: " + strValidValues + ".",
						"::StringVertexAnimationTypeToEnumVertexAnimationType()");
		}else
		{// Vertex animation type found.
			eVertexAnimationType = it->second;
		}

		return eVertexAnimationType;
	}

	Ogre::Math::AngleUnit StringAngleUnitToEnumAngleUnit(const String& strAngleUnit)
	{
		typedef std::map<String, Ogre::Math::AngleUnit> StringAngleUnitToEnumAngleUnitMap;
		static bool angleUnitMapInited = false;

		static StringAngleUnitToEnumAngleUnitMap mStringAngleUnitToEnumAngleUnitMap;

		// ---------------------------- Create string type angle unit to enumerated type angle unit map ----------------------------
		if ( !angleUnitMapInited )
		{
			mStringAngleUnitToEnumAngleUnitMap["degree"]	= Math::AU_DEGREE;
			mStringAngleUnitToEnumAngleUnitMap["radian"]	= Math::AU_RADIAN;
			angleUnitMapInited = true;
		}

		// ------------------------- Now convert angle unit given as string to angle unit given as an enum -------------------------
		Math::AngleUnit eAngleUnit;
		StringAngleUnitToEnumAngleUnitMap::iterator it = mStringAngleUnitToEnumAngleUnitMap.find(strAngleUnit);
		if ( it == mStringAngleUnitToEnumAngleUnitMap.end() )
		{// Angle unit not found.
			// Concatenate valid values together for logging.
			it = mStringAngleUnitToEnumAngleUnitMap.begin();
			String strValidValues = "\"" + it->first + "\"";
			it++;
			for ( ; it != mStringAngleUnitToEnumAngleUnitMap.end(); it++ )
				strValidValues += ", \"" + it->first + "\"";

			OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND,
						"\"" + strAngleUnit + "\" is not a valid angle unit.\n"
						"Valid values are: " + strValidValues + ".",
						"::StringAngleUnitToEnumAngleUnit()");
		}else
		{// Angle unit found.
			eAngleUnit = it->second;
		}

		return eAngleUnit;
	}

	Ogre::Node::TransformSpace StringTransformSpaceToEnumTransformSpace(const String& transformSpace, const String& elementName, const String& filename, dsi::DotSceneInfo* dotSceneInfo)
	{
		Node::TransformSpace eTransformSpace;
		if		( transformSpace == "local" )	eTransformSpace = SceneNode::TS_LOCAL;
		else if ( transformSpace == "parent" )	eTransformSpace = SceneNode::TS_PARENT;
		else if ( transformSpace == "world" )	eTransformSpace = SceneNode::TS_WORLD;
		else
		{
			if (dotSceneInfo) dotSceneInfo->logLoadError(
				"Attribute \"relativeTo\" of element \"" + elementName + "\" in file \"" + filename + "\" has an invalid value: \"" + transformSpace + "\"!\n"
				"Valid values are \"local\", \"parent\", \"world\". \"relativeTo\" attribute will now default to \"local\".\n"
				"See the dotsceneformat.dtd for more information.\n");
			eTransformSpace = Node::TS_LOCAL;
		}

		return eTransformSpace;
	}

	Ogre::PixelFormat StringPixelFormatToEnumPixelFormat(const String& strPixelFormat)
	{
		InitPixelFormatMap();
		// -------------------------- Now convert pixel format given as string to pixel format given as an enum ---------------------------
		Ogre::PixelFormat ePixelFormat;
		StringPixelFormatToEnumPixelFormatMap::iterator it = g_StringPixelFormatToEnumPixelFormatMap.find(strPixelFormat);
		if ( it == g_StringPixelFormatToEnumPixelFormatMap.end() )
		{// Pixel format not found.
			// Concatenate valid values together for logging.
			it = g_StringPixelFormatToEnumPixelFormatMap.begin();
			String strValidValues = "\"" + it->first + "\"";
			it++;
			for ( ; it != g_StringPixelFormatToEnumPixelFormatMap.end(); it++ )
				strValidValues += ", \"" + it->first + "\"";

			OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND,
						"\"" + strPixelFormat + "\" is not a valid pixel format.\n"
						"Valid values are: " + strValidValues + ".",
						"::StringPixelFormatToEnumPixelFormat()");
		}else
		{// Pixel format found.
			ePixelFormat = it->second;
		}

		return ePixelFormat;
	}


	String EnumPixelFormatToStringPixelFormat( Ogre::PixelFormat pFormat )
	{	
		InitPixelFormatMap();

		String strPixelFormat = "";

		StringPixelFormatToEnumPixelFormatMap::iterator itPixelFormatMap = g_StringPixelFormatToEnumPixelFormatMap.begin();
		StringPixelFormatToEnumPixelFormatMap::iterator itPixelFormatMapEnd = g_StringPixelFormatToEnumPixelFormatMap.end();

		for( ; itPixelFormatMap != itPixelFormatMapEnd; ++ itPixelFormatMap )
		{
			if( itPixelFormatMap->second == pFormat )
			{
				strPixelFormat = itPixelFormatMap->first;
				return strPixelFormat;
			}
		}

		OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND,
			"Found not a valid pixel format.\n",
			"::EnumPixelFormatToStringPixelFormat()" );
		return strPixelFormat;
	}

	void InitPixelFormatMap() 
	{
		// ----------------------------- Create string type pixel format to enumerated type pixel format map -----------------------------
		if ( !g_pixelFormatMapInited )
		{
			g_StringPixelFormatToEnumPixelFormatMap["PF_UNKNOWN"]		= PF_UNKNOWN;
			g_StringPixelFormatToEnumPixelFormatMap["PF_L8"]			= PF_L8;
			g_StringPixelFormatToEnumPixelFormatMap["PF_BYTE_L"]		= PF_BYTE_L;
			g_StringPixelFormatToEnumPixelFormatMap["PF_L16"]			= PF_L16;
			g_StringPixelFormatToEnumPixelFormatMap["PF_SHORT_L"]		= PF_SHORT_L;
			g_StringPixelFormatToEnumPixelFormatMap["PF_A8"]			= PF_A8;
			g_StringPixelFormatToEnumPixelFormatMap["PF_BYTE_A"]		= PF_BYTE_A;
			g_StringPixelFormatToEnumPixelFormatMap["PF_A4L4"]			= PF_A4L4;
			g_StringPixelFormatToEnumPixelFormatMap["PF_BYTE_LA"]		= PF_BYTE_LA;
			g_StringPixelFormatToEnumPixelFormatMap["PF_R5G6B5"]		= PF_R5G6B5;
			g_StringPixelFormatToEnumPixelFormatMap["PF_B5G6R5"]		= PF_B5G6R5;
			g_StringPixelFormatToEnumPixelFormatMap["PF_R3G3B2"]		= PF_R3G3B2;
			g_StringPixelFormatToEnumPixelFormatMap["PF_A4R4G4B4"]		= PF_A4R4G4B4;
			g_StringPixelFormatToEnumPixelFormatMap["PF_A1R5G5B5"]		= PF_A1R5G5B5;
			g_StringPixelFormatToEnumPixelFormatMap["PF_R8G8B8"]		= PF_R8G8B8;
			g_StringPixelFormatToEnumPixelFormatMap["PF_B8G8R8"]		= PF_B8G8R8;
			g_StringPixelFormatToEnumPixelFormatMap["PF_A8R8G8B8"]		= PF_A8R8G8B8;
			g_StringPixelFormatToEnumPixelFormatMap["PF_A8B8G8R8"]		= PF_A8B8G8R8;
			g_StringPixelFormatToEnumPixelFormatMap["PF_B8G8R8A8"]		= PF_B8G8R8A8;
			g_StringPixelFormatToEnumPixelFormatMap["PF_R8G8B8A8"]		= PF_R8G8B8A8;
			g_StringPixelFormatToEnumPixelFormatMap["PF_X8R8G8B8"]		= PF_X8R8G8B8;
			g_StringPixelFormatToEnumPixelFormatMap["PF_X8B8G8R8"]		= PF_X8B8G8R8;
			g_StringPixelFormatToEnumPixelFormatMap["PF_BYTE_RGB"]		= PF_BYTE_RGB;
			g_StringPixelFormatToEnumPixelFormatMap["PF_BYTE_BGR"]		= PF_BYTE_BGR;
			g_StringPixelFormatToEnumPixelFormatMap["PF_BYTE_BGRA"]		= PF_BYTE_BGRA;
			g_StringPixelFormatToEnumPixelFormatMap["PF_BYTE_RGBA"]		= PF_BYTE_RGBA;
			g_StringPixelFormatToEnumPixelFormatMap["PF_A2R10G10B10"]	= PF_A2R10G10B10;
			g_StringPixelFormatToEnumPixelFormatMap["PF_A2B10G10R10"]	= PF_A2B10G10R10;
			g_StringPixelFormatToEnumPixelFormatMap["PF_DXT1"]			= PF_DXT1;
			g_StringPixelFormatToEnumPixelFormatMap["PF_DXT2"]			= PF_DXT2;
			g_StringPixelFormatToEnumPixelFormatMap["PF_DXT3"]			= PF_DXT3;
			g_StringPixelFormatToEnumPixelFormatMap["PF_DXT4"]			= PF_DXT4;
			g_StringPixelFormatToEnumPixelFormatMap["PF_DXT5"]			= PF_DXT5;
			g_StringPixelFormatToEnumPixelFormatMap["PF_FLOAT16_R"]		= PF_FLOAT16_R;
			g_StringPixelFormatToEnumPixelFormatMap["PF_FLOAT16_RGB"]	= PF_FLOAT16_RGB;
			g_StringPixelFormatToEnumPixelFormatMap["PF_FLOAT16_RGBA"]	= PF_FLOAT16_RGBA;
			g_StringPixelFormatToEnumPixelFormatMap["PF_FLOAT32_R"]		= PF_FLOAT32_R;
			g_StringPixelFormatToEnumPixelFormatMap["PF_FLOAT32_RGB"]	= PF_FLOAT32_RGB;
			g_StringPixelFormatToEnumPixelFormatMap["PF_FLOAT32_RGBA"]	= PF_FLOAT32_RGBA;
			g_StringPixelFormatToEnumPixelFormatMap["PF_FLOAT16_GR"]	= PF_FLOAT16_GR;
			g_StringPixelFormatToEnumPixelFormatMap["PF_FLOAT32_GR"]	= PF_FLOAT32_GR;
			g_StringPixelFormatToEnumPixelFormatMap["PF_DEPTH"]			= PF_DEPTH;
			g_StringPixelFormatToEnumPixelFormatMap["PF_SHORT_RGBA"]	= PF_SHORT_RGBA;
			g_StringPixelFormatToEnumPixelFormatMap["PF_SHORT_GR"]		= PF_SHORT_GR;
			g_StringPixelFormatToEnumPixelFormatMap["PF_SHORT_RGB"]		= PF_SHORT_RGB;
			g_StringPixelFormatToEnumPixelFormatMap["PF_COUNT"]			= PF_COUNT;
			g_pixelFormatMapInited = true;
		}
	}

	Animation::InterpolationMode StringInterpolationModeToEnumInterpolationMode(const String& strInterpolationMode)
	{
		typedef std::map<String, Ogre::Animation::InterpolationMode> StringInterpolationModeToEnumInterpolationModeMap;
		static bool interpolationModeMapInited = false;

		static StringInterpolationModeToEnumInterpolationModeMap mStringInterpolationModeToEnumInterpolationModeMap;

		// --------------------- Create string type interpolation mode to enumerated type interpolation mode map ---------------------
		if ( !interpolationModeMapInited )
		{
			mStringInterpolationModeToEnumInterpolationModeMap["linear"]		= Animation::IM_LINEAR;
			mStringInterpolationModeToEnumInterpolationModeMap["spline"]		= Animation::IM_SPLINE;
			interpolationModeMapInited = true;
		}

		// ------------------ Now convert interpolation mode given as string to interpolation mode given as an enum ------------------
		Animation::InterpolationMode eInterpolationMode;
		StringInterpolationModeToEnumInterpolationModeMap::iterator it =
			mStringInterpolationModeToEnumInterpolationModeMap.find(strInterpolationMode);
		if ( it == mStringInterpolationModeToEnumInterpolationModeMap.end() )
		{// Interpolation mode not found.
			// Concatenate valid values together for logging.
			it = mStringInterpolationModeToEnumInterpolationModeMap.begin();
			String strValidValues = "\"" + it->first + "\"";
			it++;
			for ( ; it != mStringInterpolationModeToEnumInterpolationModeMap.end(); it++ )
				strValidValues += ", \"" + it->first + "\"";


			OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND,
						"\"" + strInterpolationMode + "\" is not a valid interpolation mode.\n"
						"Valid values are: " + strValidValues + ".",
						"::StringInterpolationModeToEnumInterpolationMode()");
		}else
		{// Interpolation mode found.
			eInterpolationMode = it->second;
		}

		return eInterpolationMode;
	}

	Animation::RotationInterpolationMode StringRInterpolationModeToEnumRInterpolationMode(const String& strRInterpolationMode)
	{
		typedef std::map<String, Ogre::Animation::RotationInterpolationMode> StringRInterpolationModeToEnumRInterpolationModeMap;
		static bool rotationalInterpolationModeMapInited = false;

		static StringRInterpolationModeToEnumRInterpolationModeMap mStringRInterpolationModeToEnumRInterpolationModeMap;

		// ----------------- Create string type rotation interpolation mode to enumerated type interpolation mode map -----------------
		if ( !rotationalInterpolationModeMapInited )
		{
			mStringRInterpolationModeToEnumRInterpolationModeMap["linear"]		= Animation::RIM_LINEAR;
			mStringRInterpolationModeToEnumRInterpolationModeMap["spherical"]	= Animation::RIM_SPHERICAL;
			rotationalInterpolationModeMapInited = true;
		}

		// -------------- Now convert rotation interpolation mode given as string to interpolation mode given as an enum --------------
		Animation::RotationInterpolationMode eRInterpolationMode;
		StringRInterpolationModeToEnumRInterpolationModeMap::iterator it =
			mStringRInterpolationModeToEnumRInterpolationModeMap.find(strRInterpolationMode);
		if ( it == mStringRInterpolationModeToEnumRInterpolationModeMap.end() )
		{// Rotation interpolation mode not found.
			// Concatenate valid values together for logging.
			it = mStringRInterpolationModeToEnumRInterpolationModeMap.begin();
			String strValidValues = "\"" + it->first + "\"";
			it++;
			for ( ; it != mStringRInterpolationModeToEnumRInterpolationModeMap.end(); it++ )
				strValidValues += ", \"" + it->first + "\"";

			OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND,
						"\"" + strRInterpolationMode + "\" is not a valid rotation interpolation mode.\n"
						"Valid values are: " + strValidValues + ".",
						"::StringRInterpolationModeToEnumRInterpolationMode()");
		}else
		{// Rotation interpolation mode found.
			eRInterpolationMode = it->second;
		}

		return eRInterpolationMode;
	}
// ----------------------------------- End of functions to be placed into the StringConverter class ----------------------------------


void*	ParseStringBasedOnType(const String& value, const String& type, const String& elementName, const String& filename, SceneManager* sceneManager, const String& namePrefix, dsi::DotSceneInfo* dotSceneInfo)
{
	void*	parsedData;

	if	( type == "bool" )
	{
		parsedData = new bool;
		*((bool*)parsedData) = StringConverter::parseBool(value);
	}
	else if	( type == "real" )
	{
		parsedData = new Real;
		*((Real*)parsedData) = StringConverter::parseReal(value);
	}else if	( type == "int" )
	{
		parsedData = new int;
		*((int*)parsedData) = StringConverter::parseInt(value);
	}
	else if	( type == "unsigned_int" )
	{
		parsedData = new unsigned int;
		*((unsigned int*)parsedData) = StringConverter::parseUnsignedInt(value);
	}
	else if	( type == "short" )
	{
		parsedData = new short;
		*((short*)parsedData) = StringConverter::parseInt(value); // TODO: support for parsing type "short" directly.
	}
	else if	( type == "unsigned_short" )
	{
		parsedData = new unsigned short;
		*((unsigned short*)parsedData) = StringConverter::parseUnsignedInt(value); // TODO: support for parsing type "short" directly.
	}
	else if	( type == "long" )
	{
		parsedData = new long;
		*((long*)parsedData) = StringConverter::parseLong(value);
	}
	else if	( type == "unsigned_long" )
	{
		parsedData = new unsigned long;
		*((unsigned long*)parsedData) = StringConverter::parseUnsignedLong(value);
	}
	else if	( type == "size_t" )
	{
		parsedData = new size_t;
		*((size_t*)parsedData) = StringConverter::parseUnsignedInt(value); // TODO: support for parsing type "size_t" directly.
	}
	else if	( type == "string" )
	{
		parsedData = (void*)&value;
	}
	else if	( type == "vector3" )
	{
		parsedData = new Vector3;
		*((Vector3*)parsedData) = StringConverter::parseVector3(value);
	}
	else if	( type == "matrix3" )
	{
		parsedData = new Matrix3;
		*((Matrix3*)parsedData) = StringConverter::parseMatrix3(value);
	}
	else if	( type == "matrix4" )
	{
		parsedData = new Matrix4;
		*((Matrix4*)parsedData) = StringConverter::parseMatrix4(value);
	}
	else if	( type == "quaternion" )
	{
		parsedData = new Quaternion;
		*((Quaternion*)parsedData) = StringConverter::parseQuaternion(value);
	}
	else if	( type == "colourvalue" )
	{
		parsedData = new ColourValue;
		*((ColourValue*)parsedData) = StringConverter::parseColourValue(value);
	}
	else if	( type == "movableObject" )
	{
		StringVector movableName_Type = StringUtil::split(value);
		if ( movableName_Type.size() != 2 )
		{
			if (dotSceneInfo) dotSceneInfo->logLoadError(
				"Attribute \"value\" of element \"" + elementName + "\" has an invalid value: \"" + value + "\"!\n"
				"Check the .scene file.");
		}else
		{
			if ( namePrefix != "" )	movableName_Type[0] = namePrefix + movableName_Type[0];

			parsedData = (void*)sceneManager->getMovableObject(movableName_Type[0], movableName_Type[1]);
		}
	}
	else if	( type == "camera" )
	{
		String cameraPrefixedName;
		if ( namePrefix != "" ) cameraPrefixedName = namePrefix + value;

		parsedData = (void*)sceneManager->getCamera(cameraPrefixedName);
	}
	else if	( type == "node" )
	{
		String nodePrefixedName;
		if ( namePrefix != "" ) nodePrefixedName = namePrefix + value;

		parsedData = (void*)sceneManager->getSceneNode(nodePrefixedName);
	}
	else
	{
		if (dotSceneInfo) dotSceneInfo->logLoadError(
			"Attribute \"type\" of element \"" + elementName + "\" in file \"" + filename + "\" has an invalid value: \"" + type + "\"!\n"
			"Valid values are \"bool\", \"real\", \"int\", \"unsigned_int\", \"short\", \"unsigned_short\", \"long\", \"unsigned_long\", "
			"\"size_t\", \"string\", \"vector3\", \"matrix3\", \"matrix4\", \"quaternion\", \"colourvalue\", \"movableObject\", \"camera\", \"node\".\n"
			"The pointer to the converted data returned from StringTransformSpaceToEnumTransformSpace() will be a null pointer.\n"
			"See the dotsceneformat.dtd for more information.\n");
		parsedData = 0;
	}

	return parsedData;
}


String	SerializeValueBasedOnType( void* pValue, const String& type, const String& elementName, const String& filename, dsi::DotSceneInfo* dotSceneInfo)
{
	String sValue;

	if	( type == "bool" )
	{
		bool bValue = (*(bool*)pValue);
		sValue = StringConverter::toString( bValue );
	}
	else if	( type == "real" )
	{
		sValue = StringConverter::toString( (*(Real*)pValue) );
	}
	else if	( type == "int" )
	{
		sValue = StringConverter::toString( (*(int*)pValue) );
	}
	else if	( type == "unsigned_int" )
	{
		sValue = StringConverter::toString( (*(uint*)pValue) );
	}
	else if	( type == "short" )
	{
		sValue = StringConverter::toString( (*(short*)pValue) );
	}
	else if	( type == "unsigned_short" )
	{
		sValue = StringConverter::toString( (*(ushort*)pValue) );
	}
	else if	( type == "long" )
	{
		sValue = StringConverter::toString( (*(long*)pValue) );
	}
	else if	( type == "unsigned_long" )
	{
		sValue = StringConverter::toString( (*(ulong*)pValue) );
	}
	else if	( type == "size_t" )
	{
		sValue = StringConverter::toString( (*(size_t*)pValue) );
	}
	else if	( type == "string" )
	{
		sValue = ((String*)pValue)->c_str();
	}
	else if	( type == "vector3" )
	{
		sValue = StringConverter::toString( (*(Vector3*)pValue) );
	}
	else if	( type == "matrix3" )
	{
		sValue = StringConverter::toString( (*(Matrix3*)pValue) );
	}
	else if	( type == "matrix4" )
	{
		sValue = StringConverter::toString( (*(Matrix4*)pValue) );
	}
	else if	( type == "quaternion" )
	{
		sValue = StringConverter::toString( (*(Quaternion*)pValue) );
	}
	else if	( type == "colourvalue" )
	{
		sValue = StringConverter::toString( (*(ColourValue*)pValue) );
	}
	//! \TODO What should we do with object pointers
	else if	( type == "movableObject" )
	{

	}
	else if	( type == "camera" )
	{
	}
	else if	( type == "node" )
	{

	}
	else
	{
		if (dotSceneInfo) dotSceneInfo->logLoadError(
			"Attribute \"type\" of element \"" + elementName + "\" in file \"" + filename + "\" has an invalid value: \"" + type + "\"!\n"
			"Valid values are \"bool\", \"real\", \"int\", \"unsigned_int\", \"short\", \"unsigned_short\", \"long\", \"unsigned_long\", "
			"\"size_t\", \"string\", \"vector3\", \"matrix3\", \"matrix4\", \"quaternion\", \"colourvalue\", \"movableObject\", \"camera\", \"node\".\n"
			"The pointer to the converted data returned from StringTransformSpaceToEnumTransformSpace() will be a null pointer.\n"
			"See the dotsceneformat.dtd for more information.\n");
	}
	return sValue;
}

void	DotSceneXmlNodeProcessor::logAttributeNotFoundError(XmlAttributeDefaultDecl xmlAttributeDefaultDecl, const String& attributeName, const String& elementName, const String& filename, const String& defaultValue)
{
		switch ( xmlAttributeDefaultDecl )
		{
		case XmlNodeProcessor::XMLADD_REQUIRED:
			if (mDotSceneInfo) mDotSceneInfo->logLoadError(
				"Attribute \"" + attributeName + "\" of element \"" + elementName + "\" in file \"" + filename + "\" is missing!\n"
				"This is not according to the dotScene format standard, since this attribute is required! "
				"Attribute \"" + attributeName + "\" will default to \"" + defaultValue + "\".\n"
				"See the dotsceneformat.dtd for more information.\n"
				"Parsing will continue, however the loaded scene may function unexpectedly."
			);
			break;
		case XmlNodeProcessor::XMLADD_IMPLIED:
			if (mDotSceneInfo) mDotSceneInfo->logLoadInfo(
				"Attribute \"" + attributeName + "\" of element \"" + elementName + "\" in file \"" + filename + "\" is missing!\n"
				"This isn't a violation of the dotScene format standard, since this attribute is implied. "
				"Attribute \"" + attributeName + "\" will default to \"" + defaultValue + "\".\n"
				"See the dotsceneformat.dtd for more information."
			);
			break;
		case XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT:
			if (mDotSceneInfo) mDotSceneInfo->logLoadInfo(
				"Attribute \"" + attributeName + "\" of element \"" + elementName + "\" in file \"" + filename + "\" is missing!\n"
				"This isn't a violation of the dotScene format standard, since this attribute has a default value. "
				"Attribute \"" + attributeName + "\" will default to \"" + defaultValue + "\".\n"
				"See the dotsceneformat.dtd for more information."
			);
			break;
		}
}

template<typename AttributeType> 
AttributeType	DotSceneXmlNodeProcessor::parseAttribute(const String& attributeName, XmlNodeProcessor* parent, TiXmlElement* currentNode, XmlAttributeDefaultDecl xmlAttributeDefaultDecl, const AttributeType& defaultValue)
{
	AttributeType parsedAttribute;
	if (!dsi::xml::getAttribute(currentNode, attributeName, parsedAttribute) )
	{
		logAttributeNotFoundError(xmlAttributeDefaultDecl, attributeName, mElementName, mFilename, StringConverter::toString(defaultValue));
		parsedAttribute = defaultValue;
	}else
	{
		// TODO: the attribute is present, we have parsed it, log something if you wish.
	}

	return parsedAttribute;
}


template<> _DotSceneInterfaceExport
String	DotSceneXmlNodeProcessor::parseAttribute(const String& attributeName, XmlNodeProcessor* parent, TiXmlElement* currentNode, XmlAttributeDefaultDecl xmlAttributeDefaultDecl, const String& defaultValue)
{
	String parsedAttribute;
	if (!dsi::xml::getAttribute(currentNode, attributeName, parsedAttribute) )
	{
		logAttributeNotFoundError(xmlAttributeDefaultDecl, attributeName, mElementName, mFilename, defaultValue);
		parsedAttribute = defaultValue;
	}else
	{
		// TODO: the attribute is present, we have parsed it, log something if you wish.
	}

	return parsedAttribute;
}

template<> _DotSceneInterfaceExport
bool	DotSceneXmlNodeProcessor::parseAttribute(const String& attributeName, XmlNodeProcessor* parent, TiXmlElement* currentNode, XmlAttributeDefaultDecl xmlAttributeDefaultDecl, const bool& defaultValue)
{
	bool parsedAttribute;
	if (!dsi::xml::getAttribute(currentNode, attributeName, parsedAttribute) )
	{
		logAttributeNotFoundError(xmlAttributeDefaultDecl, attributeName, mElementName, mFilename, StringConverter::toString(defaultValue));
		parsedAttribute = defaultValue;
	}else
	{
		// TODO: the attribute is present, we have parsed it, log something if you wish.
	}

	return parsedAttribute;
}

template<> _DotSceneInterfaceExport
	int	DotSceneXmlNodeProcessor::parseAttribute(const String& attributeName, XmlNodeProcessor* parent, TiXmlElement* currentNode, XmlAttributeDefaultDecl xmlAttributeDefaultDecl, const int& defaultValue)
{
	int parsedAttribute;
	if (!dsi::xml::getAttribute(currentNode, attributeName, parsedAttribute) )
	{
		logAttributeNotFoundError(xmlAttributeDefaultDecl, attributeName, mElementName, mFilename, StringConverter::toString(defaultValue));
		parsedAttribute = defaultValue;
	}else
	{
		// TODO: the attribute is present, we have parsed it, log something if you wish.
	}

	return parsedAttribute;
}

void	DotSceneXmlNodeProcessor::parseAttributeAndSaveAsNameValuePair(const String& attributeName, XmlNodeProcessor* parent, TiXmlElement* currentNode, XmlAttributeDefaultDecl xmlAttributeDefaultDecl, NameValuePairList& nameValuePairList, const String& defaultValue)
{
	String value;
	if (dsi::xml::getAttribute(currentNode, attributeName, value))
	{// Attribute is present, save it to the name-value pair list.
		nameValuePairList.insert(Ogre::NameValuePairList::value_type(attributeName, value));
	}else
	{// Attribute is not present log an error, and do not save anything to the name-value pair list.
		logAttributeNotFoundError(xmlAttributeDefaultDecl, attributeName, mElementName, mFilename, defaultValue);
		if (xmlAttributeDefaultDecl == XMLADD_DEFAULTVALUEPRESENT)
		{// Attribute is not present, however it has a default value defined in the dotScene format specification (dotScene.dtd).
		 // Default value specified in the parameter defaultValue is only applied if the attribute has a default value in the
		 // xml specification, otherwise it is ignored.
			nameValuePairList.insert(Ogre::NameValuePairList::value_type(attributeName, defaultValue));			
		}
	}
}

void DotSceneXmlNodeProcessor::parseAttribute_id(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	// TODO: If you know some kind of general implementation for "id" attribute parsing, then feel free to implement it.
	// I believe, that attribute "id" will be either parsed by sub-classes of this class or by listeners.
	// However parse it anyway and log it.
	String id;
	if ( dsi::xml::getAttribute(currentNode, "id", id) )
		if (mDotSceneInfo)
		{
			mDotSceneInfo->logLoadInfo("Attribute \"id\" of the element \"" + mElementName + "\" in the file \"" + mFilename + "\" is \"" + id + "\".");
			mDotSceneInfo->setSceneID( id );
		}
}


String	DotSceneXmlNodeProcessor::getNodeNameAutoNameIfNeeded(TiXmlElement *tiXmlElement)
{
	return tiXmlElement->Attribute(XmlNodeProcessing::String("name")) == NULL ? nextAutoName() : tiXmlElement->Attribute("name");
}

unsigned int DotSceneXmlNodeProcessor::msAutoNameNumber = 0;
String	DotSceneXmlNodeProcessor::nextAutoName()
{
	return String("dotSceneLoaderAutoName" + StringConverter::toString(msAutoNameNumber++));
}

String DotSceneXmlNodeProcessor::getNodeName_AutoNamedAndPrefixedIfNeeded(TiXmlElement* currentNode)
{
	String name;
	name = getNodeNameAutoNameIfNeeded(currentNode);
	String* prefix;
	if ( (prefix = ((String*)mTreeBuilder->getCommonParameter("NamePrefix"))) != 0 ) name = *prefix + name;

	return name;
}



//***********************************************************************************************************************************
//****************************************************** ColourElementProcessor *****************************************************
//***********************************************************************************************************************************

void*	ColourElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	mColour.r = parseAttribute("r", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)0.0);
	mColour.g = parseAttribute("g", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)0.0);
	mColour.b = parseAttribute("b", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)0.0);
	mColour.a = parseAttribute("a", parent, currentNode, XmlNodeProcessor::XMLADD_IMPLIED,	(Real)1.0);

	return &mColour; // This is not really needed.
}

//***********************************************************************************************************************************
//************************************************** End of ColourElementProcessor **************************************************
//***********************************************************************************************************************************





//***********************************************************************************************************************************
//****************************************************** SceneElementProcessor ******************************************************
//***********************************************************************************************************************************

void SceneElementProcessor::parseAttribute_formatVersion(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	// Check version first, fail if it's not what we want.
	String ver = BLANKSTRING;
	if (dsi::xml::getAttribute(currentNode, "formatVersion", ver))
	{// "formatVersion" attribute is present, see if it consists the correct values.
		if (ver != "1.0.0")
			if (mDotSceneInfo) mDotSceneInfo->logLoadError(
				"Attribute \"formatVersion\" of element \"" + mElementName + "\" in file \"" + 
				mFilename + "\" is not 1.0.0.\n"
				"The version supported by this .scene loader is 1.0.0, will try to parse anyway, however compatibility issues may\n"
				"arrise during loading.\n"
				"See dotsceneformat.dtd for more information.");
	}
	else
	{// No "formatVersion" attribute.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"Element \"" + mElementName + "\" in file \"" + mFilename + "\" does not have a "
			"\"formatVersion\" attribute;\n"
			"\"formatVersion\" is required according to the dotsceneformat specification.\n"
			"See dotsceneformat.dtd for more information.\n"
			"\"formatVersion\" now defaults to 1.0.0...");
	}
}

void SceneElementProcessor::parseAttribute_minOgreVersion(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	String ver;
	if (dsi::xml::getAttribute(currentNode, "minOgreVersion", ver ))
	{// See if required Ogre version is less or equal than the Ogre version this exe compiled against.
		StringVector versions = StringUtil::split(ver, ".", 3);
		if ( versions.size() == 3 )
		{// Version was specified correctly.
			int majorVerMin			= StringConverter::parseInt(versions[0]),
				minorVerMin			= StringConverter::parseInt(versions[1]),
				patchVerMin			= StringConverter::parseInt(versions[2]);
			int OgreVerMin			= ((majorVerMin << 16) | (minorVerMin << 8) | patchVerMin);
			int relevantOgreVerMin	= ((majorVerMin << 16) | (minorVerMin << 8));
			
			if ( OgreVerMin > OGRE_VERSION )
			{// Minimum Ogre version required to load the dotScene file is greater then the Ogre version this loader is compiled against.
				int relevantOgreVer		= ((OGRE_VERSION_MAJOR << 16) | (OGRE_VERSION_MINOR << 8));
				if ( relevantOgreVerMin > relevantOgreVer )
				{// There is relevant difference between Ogre versions (different Ogre distributions).
					OGRE_EXCEPT(
						Ogre::Exception::ERR_NOT_IMPLEMENTED,
						"The Ogre version required by the dotScene file \"" + mFilename + "\" is higher then "
						"the Ogre version this exe is compiled against.\n"
						"Required Ogre version: " + ver + "\n"
						"Ogre version this loader is compiled against: " +
						StringConverter::toString(OGRE_VERSION_MAJOR) + "." + 
						StringConverter::toString(OGRE_VERSION_MINOR) + "." +
						StringConverter::toString(OGRE_VERSION_PATCH) + "\n"
						"Unable to load dotScene file. Exiting.",
						"SceneElementProcessor::parseElementImpl()" );
				}else
				{// There is no relevant difference between Ogre versions (only patch).
					if (mDotSceneInfo) mDotSceneInfo->logLoadWarning(
						"The Ogre version required by the dotScene file \"" + mFilename + "\" is higher then "
						"the Ogre version this exe is compiled against.\n"
						"However there is only difference in the patch version.\n"
						"Required Ogre version: " + ver + "\n"
						"Ogre version this loader is compiled against: " +
						StringConverter::toString(OGRE_VERSION_MAJOR) + "." + 
						StringConverter::toString(OGRE_VERSION_MINOR) + "." +
						StringConverter::toString(OGRE_VERSION_PATCH) + "\n"
						"Parsing of the dotScene file will continue, however the loaded scene may function unexpected due to previously"
						"unsforseen Ogre version compatibility issues.\n");
				}
			}
		}else
		{// Version is in incorrect format.
			if (mDotSceneInfo) mDotSceneInfo->logLoadError(
				"Attribute \"minOgreVersion\" of element \"" + mElementName + "\" in file \"" + 
				mFilename + "\" has an illegal format.\n"
				"The format should be <major version>.<minor version>.<patch version>.\n"
				"See dotsceneformat.dtd for more information.");
		}
	}else
	{// No "minOgreVersion" attribute, it is not required, but it is recommended so log a warning anyway.
		if (mDotSceneInfo) mDotSceneInfo->logLoadWarning(
			"Element \"" + mElementName + "\" in file \"" + mFilename + "\" does not have a "
			"\"minOgreVersion\" attribute; \"minOgreVersion\" is optional according to the dotsceneformat specification, however it "
			"is recommended. "
			"Parsing of the dotScene file will continue, however the loaded scene may function unexpectedly due to previously "
			"unsforseen Ogre version compatibility issues that my arise. "
			"See dotsceneformat.dtd for more information. ");
	}
}

void SceneElementProcessor::parseAttribute_sceneManager(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	SceneManager* sceneManager = (SceneManager*)mTreeBuilder->getCommonParameter("SceneManager");

	String sceneMgrFactoryName;
	if ( dsi::xml::getAttribute(currentNode, "sceneManagerType", sceneMgrFactoryName) )
	{// Attribute "sceneManager" is present in the element.
		// Check if a scene manager was specified at construction time.
		if ( sceneManager == 0 )
		{// No scene manager was created and passed as parameter at construction time, but there is a scene manager type specified in
		 // the .scene file. Create scene manager.
			String	sceneMgrName = BLANKSTRING;
			if ( !dsi::xml::getAttribute(currentNode, "sceneManagerName", sceneMgrName) )
			{// No scene manager name was specified. Drop a warning, Ogre will auto name the new scene manager instance.
				if (mDotSceneInfo) mDotSceneInfo->logLoadWarning(
					"Attribute \"sceneManagerName\" in element \"" + mElementName + "\" is missing, while attribute \"sceneManagerType\" is specified.\n"
					"This is not illegal, but scene managers must have a name, Ogre will generate a name automatically." );

				if (mDotSceneInfo) mDotSceneInfo->logLoadInfo(
					"Creating a scene manager of type \"" + sceneMgrFactoryName + "\".");
			}else
			{
				if (mDotSceneInfo) mDotSceneInfo->logLoadInfo(
					"Creating a scene manager of type \"" + sceneMgrFactoryName + "\" with name \"" + sceneMgrName + "\".");
			}

			sceneManager = Root::getSingleton().createSceneManager(sceneMgrFactoryName, sceneMgrName);
			mTreeBuilder->setCommonParameter("SceneManager", sceneManager);
		}else
		{// Scene manager was created before and there is one present in the file too. Check if their type is match.
			if ( sceneManager->getTypeName() == sceneMgrFactoryName )
			{// Scene manager type specified in file equals with the one created before.
				// Log some info.
				if (mDotSceneInfo) mDotSceneInfo->logLoadInfo(
					"Attribute \"sceneManagerType\" of element \"" + mElementName + "\" in dotScene file \"" +
					mFilename + "\" is \"" + sceneMgrFactoryName + "\".\n"
					"However a scene manager with the same type was created before and passed to the loader routines as the destination\n"
					"scene manager. dotScene loader won't create any scene managers.");
			}else
			{// Scene manager type specified in file does not equal with the one created before.
				if (mDotSceneInfo) mDotSceneInfo->logLoadError(
					"Scene manager type specified in the attribute \"sceneManagerType\" of element \"" + mElementName + "\" "
					"in file \"" + mFilename + "\" is \"" + sceneMgrFactoryName + "\".\n"
					"However a scene manager which type is \"" + sceneManager->getTypeName() + "\" was created before and specified "
					"for the dotScene loader routines.\n"
					"The two types does not match.\n"
					"Loader will continue parsing, however it is possible that either the scene specified in the file can not be\n"
					"loaded totally into the given scene manager, or the loaded scene won't function as expected. Check the dotScene\n"
					"file.");
			}
		}
	}else
	{// Attribute "sceneManager" is not present in the element.
	
		// Check if a scene manager was specified at construction time.
		if ( sceneManager == 0 )
		{// No scene manager was created before and no one specified in the dotScene file. Throw an Ogre::Exception.
			OGRE_EXCEPT(
				Ogre::Exception::ERR_ITEM_NOT_FOUND,
				"Scene manager was neither created before calling dotScene loader, nor it was specified in the dotScene file \"" +
				mFilename + "\".\n"
				"Missing attribute \"sceneManagerType\" and (optionally) attribute \"sceneManagerName\" of element \"" + mElementName +
				"\".\n"
				"See the dotsceneformat.dtd for more information.",
				"SceneElementProcessor::parseAttribute_sceneManager");
		}else
		{// Scene manager was created before and no one specified in the dotScene file. We are going to use the one passed as
		 // parameter at construction time.
			if (mDotSceneInfo) mDotSceneInfo->logLoadInfo(
				"Attribute \"sceneManagerType\" of element \"" + mElementName + "\" in dotScene file \"" + mFilename + "\" is missing.\n"
				"However a scene manager was created before and specified for the dotScene loader routines with the type \"" +
				sceneManager->getTypeName() + "\".\n"
				"dotScene loader won't create any scene managers.");
		}
	}

	// Finally parsing "showBoundingBoxes"
	if ( sceneManager )
	{
		bool showBoundingBoxes = parseAttribute("showBoundingBoxes", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)false);
		sceneManager->showBoundingBoxes( showBoundingBoxes );
	}
}

void SceneElementProcessor::parseAttribute_author(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	String author;
	if ( dsi::xml::getAttribute(currentNode, "author", author) )
		if (mDotSceneInfo)
		{
			mDotSceneInfo->logLoadInfo("The author of the file \"" + mFilename + "\" is \"" + author + "\".");
			mDotSceneInfo->setSceneAuthor( author );
		}
}


void* SceneElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// -------------------------------------------------- "formatVersion" attribute --------------------------------------------------
	parseAttribute_formatVersion(parent, currentNode);
	// -------------------------------------------------------------------------------------------------------------------------------

	// ------------------------------------------------- "minOgreVersion" attribute --------------------------------------------------
	parseAttribute_minOgreVersion(parent, currentNode);
	// -------------------------------------------------------------------------------------------------------------------------------

	// -------------------------------------------------- "sceneManager" attribute ---------------------------------------------------
	parseAttribute_sceneManager(parent, currentNode);
	// -------------------------------------------------------------------------------------------------------------------------------

	// ------------------------------------------------------- "id" attribute --------------------------------------------------------
	parseAttribute_id(parent, currentNode);
	// -------------------------------------------------------------------------------------------------------------------------------

	// ----------------------------------------------------- "author" attribute ------------------------------------------------------
	parseAttribute_author(parent, currentNode);
	// -------------------------------------------------------------------------------------------------------------------------------


	SceneManager* sceneManager = (SceneManager*)mTreeBuilder->getCommonParameter("SceneManager");


	// --------------------------------- Report created/used scene manager to DotSceneInfo instance --------------------------------
	if (mDotSceneInfo)	mDotSceneInfo->reportSceneManager(sceneManager);

	return sceneManager;
}

void SceneElementProcessor::parseElementEndImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	// -------------------------------------------- Process track targets for scene nodes --------------------------------------------
	TrackTargetDataOfSceneNodes*	trackTargetDataOfSceneNodes;
	if ( (trackTargetDataOfSceneNodes = 
			(TrackTargetDataOfSceneNodes*)mTreeBuilder->getCommonParameter(CommonParName_TrackTargetDataOfSceneNodes)) )
	{
		for (	TrackTargetDataOfSceneNodes::iterator itTrackTargetDataOfSceneNodes = trackTargetDataOfSceneNodes->begin();
				itTrackTargetDataOfSceneNodes != trackTargetDataOfSceneNodes->end();
				itTrackTargetDataOfSceneNodes++ )
		{
			SceneNode*	sceneNode = 0;
			try
			{
				String	sceneNodeName(""); // Concatenated scene node name. Concatenation is needed if global scene node prefixes are used.
				String*	namePrefix;
				if ( namePrefix = ((String*)mTreeBuilder->getCommonParameter("NamePrefix")) ) sceneNodeName = *namePrefix;
				sceneNodeName += itTrackTargetDataOfSceneNodes->second.mTargetSceneNodeName;

				sceneNode = ((SceneManager*)mTreeBuilder->getCommonParameter("SceneManager"))->getSceneNode(sceneNodeName);
			}catch (Ogre::Exception& ex)
			{
				if (mDotSceneInfo) mDotSceneInfo->logLoadError(
					"Target scene node \"" + itTrackTargetDataOfSceneNodes->second.mTargetSceneNodeName + "\" for scene node \"" +
					itTrackTargetDataOfSceneNodes->first->getName() + "\" not found!\n"
					"The exception description returned from the search is: " + ex.getFullDescription() + "\n"
					"Autotracking won't be enabled on this scene node. Check the .scene file.");
			}
			if ( sceneNode )
				itTrackTargetDataOfSceneNodes->first->setAutoTracking(
					true, sceneNode, 
					itTrackTargetDataOfSceneNodes->second.mLocalDirectionVector,
					itTrackTargetDataOfSceneNodes->second.mOffset);
		}

		// TODO: output track target data to somewhere?

		// Destroy track target data, since we don't need it anymore.
		OGRE_DELETE(trackTargetDataOfSceneNodes);
		mTreeBuilder->removeCommonParameter(CommonParName_TrackTargetDataOfSceneNodes);
	}

	// ---------------------------------------------- Process track targets for cameras ----------------------------------------------
	TrackTargetDataOfCameras*	trackTargetDataOfCameras;
	if ( (trackTargetDataOfCameras = 
			(TrackTargetDataOfCameras*)mTreeBuilder->getCommonParameter(CommonParName_TrackTargetDataOfCameras)) )
	{
		for (	TrackTargetDataOfCameras::iterator itTrackTargetDataOfCameras = trackTargetDataOfCameras->begin();
				itTrackTargetDataOfCameras != trackTargetDataOfCameras->end();
				itTrackTargetDataOfCameras++ )
		{
			SceneNode*	sceneNode = 0;
			try
			{
				String	sceneNodeName(""); // Concatenated scene node name. Concatenation is needed if global scene node prefixes are used.
				String*	namePrefix;
				if ( namePrefix = ((String*)mTreeBuilder->getCommonParameter("NamePrefix")) ) sceneNodeName = *namePrefix;
				sceneNodeName += itTrackTargetDataOfCameras->second.mTargetSceneNodeName;

				sceneNode = ((SceneManager*)mTreeBuilder->getCommonParameter("SceneManager"))->getSceneNode(sceneNodeName);
			}catch (Ogre::Exception& ex)
			{
				if (mDotSceneInfo) mDotSceneInfo->logLoadError(
					"Target scene node \"" + itTrackTargetDataOfCameras->second.mTargetSceneNodeName + "\" for camera \"" +
					itTrackTargetDataOfCameras->first->getName() + "\" not found!\n"
					"The exception description returned from the search is: " + ex.getFullDescription() + "\n"
					"Autotracking won't be enabled on this camera. Check the .scene file.");
			}
			if ( sceneNode )
				itTrackTargetDataOfCameras->first->setAutoTracking(true, sceneNode, itTrackTargetDataOfCameras->second.mOffset);
		}

		// TODO: output track target data to somewhere?

		// Destroy track target data, since we don't need it anymore.
		OGRE_DELETE(trackTargetDataOfCameras);
		mTreeBuilder->removeCommonParameter(CommonParName_TrackTargetDataOfCameras);
	}



	// -------------------------------------------- Process look targets for scene nodes --------------------------------------------
	LookTargetDataOfSceneNodes*	lookTargetDataOfSceneNodes;
	if ( (lookTargetDataOfSceneNodes = 
			(LookTargetDataOfSceneNodes*)mTreeBuilder->getCommonParameter(CommonParName_LookTargetDataOfSceneNodes)) )
	{
		for (	LookTargetDataOfSceneNodes::iterator itLookTargetDataOfSceneNodes = lookTargetDataOfSceneNodes->begin();
				itLookTargetDataOfSceneNodes != lookTargetDataOfSceneNodes->end();
				itLookTargetDataOfSceneNodes++ )
		{
			Vector3 lookAtPosition(Vector3::ZERO);
			if ( itLookTargetDataOfSceneNodes->second.mLookTargetSceneNodeName != "" )
			{// Look target scene node name is not empty, search for look target scene node.
				SceneNode*	sceneNode = 0;
				String	sceneNodeName(""); // Concatenated scene node name. Concatenation is needed if global scene node prefixes are used.
				String*	namePrefix;
				if ( namePrefix = ((String*)mTreeBuilder->getCommonParameter("NamePrefix")) ) sceneNodeName = *namePrefix;
				sceneNodeName += itLookTargetDataOfSceneNodes->second.mLookTargetSceneNodeName;

				try
				{
					sceneNode = ((SceneManager*)mTreeBuilder->getCommonParameter("SceneManager"))->getSceneNode(sceneNodeName);
				}catch (Ogre::Exception& ex)
				{
					if (mDotSceneInfo) mDotSceneInfo->logLoadError(
						"Look target scene node \"" + itLookTargetDataOfSceneNodes->second.mLookTargetSceneNodeName + "\" for scene node \"" +
						itLookTargetDataOfSceneNodes->first->getName() + "\" not found!\n"
						"The exception description returned from the search is: " + ex.getFullDescription() + "\n"
						"Scene node won't look at the given scene node. Check the .scene file.");
				}
				if ( sceneNode ) 
				{
					lookAtPosition = sceneNode->_getDerivedPosition();

					// Attribute "relativeTo" specifies which space the lookat offset (the vector stored in child element "position" of element
					// "lookTarget") was given in. However scene node position is in world space so it may need retransformation.
					switch ( itLookTargetDataOfSceneNodes->second.mRelativeTo )
					{
					case Ogre::Node::TS_LOCAL:
						{// Scene node pos in world space, look at offset in parent space. Transform look at offset before adding it to
						 // scene node position.
							Ogre::Matrix4 fullInverseTransformOfSceneNode;
							fullInverseTransformOfSceneNode.makeInverseTransform(
								itLookTargetDataOfSceneNodes->first->_getDerivedPosition(),
								itLookTargetDataOfSceneNodes->first->_getDerivedScale(), // Cameras always use unit length uniform scaling.
								itLookTargetDataOfSceneNodes->first->_getDerivedOrientation());

							// Transform target scene node position to looking scene node's local space.
							// Transform offset to world space and add it scene node's position to get the final look at point.
							lookAtPosition = fullInverseTransformOfSceneNode * lookAtPosition;
						}break;
					case Ogre::Node::TS_PARENT:
						{// Scene node pos in world space, look at offset in parent space. Transform scene node pos before adding it to
						 // look at offset.
							if ( itLookTargetDataOfSceneNodes->first->getParent() )
							{
								Ogre::Matrix4 fullInverseTransformOfParentOfSceneNode;
								fullInverseTransformOfParentOfSceneNode.makeInverseTransform(
									itLookTargetDataOfSceneNodes->first->getParent()->_getDerivedPosition(),
									itLookTargetDataOfSceneNodes->first->getParent()->_getDerivedScale(),
									itLookTargetDataOfSceneNodes->first->getParent()->_getDerivedOrientation());

								// Transform target scene node position to looking scene node's parent space.
								lookAtPosition = fullInverseTransformOfParentOfSceneNode * lookAtPosition;
							}else
							{// Scene node is not attached to a scene node, while it has a look target with an offset in "parent" space.
							 // Log an error.
								// TODO: FIXTHIS: HUGE HACK, SceneElementProcessor shouldn't use the names of other element processors
								// in error messages, to keep redundancy and dependencies between element processors as low as
								// possible.
								if (mDotSceneInfo) mDotSceneInfo->logLoadError(
									"Scene node \"" + itLookTargetDataOfSceneNodes->first->getName() + "\" isn't attached to a scene node, but it "
									"has a \"lookTarget\" element with an offset specified in \"parent\" space.\n"
									"Offset specified in child element \"position\" of element \"lookTarget\" will be treated as if it were given in world space.\n"
									"Also if this scene node have to look at another scene node, its coordinates will be given in world space.\n");
							}
						}break;
					case Ogre::Node::TS_WORLD:
						{// Scene node pos in world space, look at offset in world space, there is nothing to do.
						}break;
					}
				}// if ( sceneNode )
			} // if ( itLookTargetDataOfSceneNodes->second.mLookTargetSceneNodeName != "" )

			// Since we transform scene node's position to the correct space, just add it to the look at position.
			lookAtPosition += itLookTargetDataOfSceneNodes->second.mLookPosition;
			itLookTargetDataOfSceneNodes->first->lookAt(lookAtPosition,
														itLookTargetDataOfSceneNodes->second.mRelativeTo,
														itLookTargetDataOfSceneNodes->second.mLocalDirectionVector);
		}

		// TODO: output look target data to somewhere?

		// Destroy look target data, since we don't need it anymore.
		OGRE_DELETE(lookTargetDataOfSceneNodes);
		mTreeBuilder->removeCommonParameter(CommonParName_LookTargetDataOfSceneNodes);
	}

	// ---------------------------------------------- Process look targets for cameras ----------------------------------------------
	LookTargetDataOfCameras*	lookTargetDataOfCameras;
	if ( (lookTargetDataOfCameras = 
			(LookTargetDataOfCameras*)mTreeBuilder->getCommonParameter(CommonParName_LookTargetDataOfCameras)) )
	{
		for (	LookTargetDataOfCameras::iterator itLookTargetDataOfCameras = lookTargetDataOfCameras->begin();
				itLookTargetDataOfCameras != lookTargetDataOfCameras->end();
				itLookTargetDataOfCameras++ )
		{
			Vector3 lookAtPosition(Vector3::ZERO);
			if ( itLookTargetDataOfCameras->second.mLookTargetSceneNodeName != "" )
			{// Look target scene node name is not empty, search for look target scene node.
				SceneNode*	sceneNode = 0;
				String	sceneNodeName(""); // Concatenated scene node name. Concatenation is needed if global scene node prefixes are used.
				String*	namePrefix;
				if ( namePrefix = ((String*)mTreeBuilder->getCommonParameter("NamePrefix")) ) sceneNodeName = *namePrefix;
				sceneNodeName += itLookTargetDataOfCameras->second.mLookTargetSceneNodeName;

				try
				{
					sceneNode = ((SceneManager*)mTreeBuilder->getCommonParameter("SceneManager"))->getSceneNode(sceneNodeName);
				}catch (Ogre::Exception& ex)
				{
					if (mDotSceneInfo) mDotSceneInfo->logLoadError(
						"Look target scene node \"" + itLookTargetDataOfCameras->second.mLookTargetSceneNodeName + "\" for camera \"" +
						itLookTargetDataOfCameras->first->getName() + "\" not found!\n"
						"The exception description returned from the search is: " + ex.getFullDescription() + "\n"
						"Camera won't look at the given scene node. Check the .scene file.");
				}

				if ( sceneNode ) lookAtPosition = sceneNode->_getDerivedPosition();
			}

			// Attribute "relativeTo" specifies which space the lookat offset (the vector stored in child element "position" of element
			// "lookTarget") was given in.
			switch ( itLookTargetDataOfCameras->second.mRelativeTo )
			{
			case Ogre::Node::TS_LOCAL:
				{// Scene node pos in world space, look at offset in parent space. Transform look at offset before adding it to
				 // scene node position.
					Ogre::Matrix4 fullTransformOfCamera;
					fullTransformOfCamera.makeTransform(
						itLookTargetDataOfCameras->first->getDerivedPosition(),
						Vector3(1,1,1), // Cameras always use unit length uniform scaling.
						itLookTargetDataOfCameras->first->getDerivedOrientation());

					// Transform offset to world space and add it scene node's position to get the final look at point.
					lookAtPosition += fullTransformOfCamera * itLookTargetDataOfCameras->second.mLookPosition;
				}break;
			case Ogre::Node::TS_PARENT:
				{// Scene node pos in world space, look at offset in parent space. Transform look at offset before adding it to
				 // scene node position.
					if ( itLookTargetDataOfCameras->first->getParentNode() )
					{
						Ogre::Matrix4 fullTransformOfParentOfCamera;
						fullTransformOfParentOfCamera.makeTransform(
							itLookTargetDataOfCameras->first->getParentNode()->_getDerivedPosition(),
							itLookTargetDataOfCameras->first->getParentNode()->_getDerivedScale(),
							itLookTargetDataOfCameras->first->getParentNode()->_getDerivedOrientation());

						// Transform offset to world space and add it scene node's position to get the final look at point.
						lookAtPosition += fullTransformOfParentOfCamera * itLookTargetDataOfCameras->second.mLookPosition;
					}else
					{// Camera is not attached to a node, while it has a look target with an offset in "parent" space.
					 // Log an error.
						// TODO: FIXTHIS: HUGE HACK, SceneElementProcessor shouldn't use the names of other element processors in
						// error messages, to keep redundancy and dependencies between element processors as low as possible.
						if (mDotSceneInfo) mDotSceneInfo->logLoadError(
							"Camera \"" + itLookTargetDataOfCameras->first->getName() + "\" isn't attached to a scene node, but it "
							"has a \"lookTarget\" element with an offset specified in \"parent\" space.\n"
							"Offset specified in child element \"position\" of element \"lookTarget\" will be treated as if it were given in world space.\n");
						lookAtPosition += itLookTargetDataOfCameras->second.mLookPosition;
					}
				}break;
			case Ogre::Node::TS_WORLD:
				{// Scene node pos in world space, look at offset in world space, just add them together.
					lookAtPosition += itLookTargetDataOfCameras->second.mLookPosition;
				}break;
			}
			itLookTargetDataOfCameras->first->lookAt(lookAtPosition);
		}

		// TODO: output look target data to somewhere?

		// Destroy look target data, since we don't need it anymore.
		OGRE_DELETE(lookTargetDataOfCameras);
		mTreeBuilder->removeCommonParameter(CommonParName_LookTargetDataOfCameras);
	}
}

//***********************************************************************************************************************************
//*************************************************** End of SceneElementProcessor **************************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//*********************************************** SceneManagerOptionsElementProcessor ***********************************************
//***********************************************************************************************************************************

void*	SceneManagerOptionElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// Parse attribute "name". Stores option key name.
	String name = parseAttribute("name", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (String)BLANKSTRING);

	// Parse attribute "type". Stores option value type, required for parsing option value.
	String type = parseAttribute("type", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (String)BLANKSTRING);

	// Parse attribute "type". Stores option value type, required for parsing option value.
	String value = parseAttribute("value", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (String)BLANKSTRING);

	if ( type != "" )
	{
		void* parsedData = 0;
		bool result = false;

		try
		{
			SceneManager* sceneManager = (SceneManager*)mTreeBuilder->getCommonParameter("SceneManager"); 
			String* namePrefix = (String*)mTreeBuilder->getCommonParameter("NamePrefix");
			if	( type == "bool" )
			{
					bool parsedData = StringConverter::parseBool(value);
					result = sceneManager->setOption(name, &parsedData);
			}
			else if	( type == "real" )
			{
				Real parsedData = StringConverter::parseReal(value);
				result = sceneManager->setOption(name, &parsedData);
			}
			else if	( type == "int" )
			{
				int parsedData = StringConverter::parseInt(value);
				result = sceneManager->setOption(name, &parsedData);
			}
			else if	( type == "unsigned_int" )
			{
				unsigned int parsedData = StringConverter::parseUnsignedInt(value);
				result = sceneManager->setOption(name, &parsedData);
			}
			else if	( type == "short" )
			{
				short parsedData = StringConverter::parseInt(value); // TODO: support for parsing type "short" directly.
				result = sceneManager->setOption(name, &parsedData);
			}
			else if	( type == "unsigned_short" )
			{
				unsigned short parsedData = StringConverter::parseUnsignedInt(value); // TODO: support for parsing type "short" directly.
				result = sceneManager->setOption(name, &parsedData);
			}
			else if	( type == "long" )
			{
				long parsedData = StringConverter::parseLong(value);
				result = sceneManager->setOption(name, &parsedData);
			}
			else if	( type == "unsigned_long" )
			{
				unsigned long parsedData = StringConverter::parseUnsignedLong(value);
				result = sceneManager->setOption(name, &parsedData);
			}
			else if	( type == "size_t" )
			{
				size_t parsedData = StringConverter::parseUnsignedInt(value); // TODO: support for parsing type "size_t" directly.
				result = sceneManager->setOption(name, &parsedData);
			}
			else if	( type == "string" )
			{
				result = sceneManager->setOption(name, &value);
			}
			else if	( type == "vector3" )
			{
				Vector3 parsedData = StringConverter::parseVector3(value);
				result = sceneManager->setOption(name, &parsedData);
			}
			else if	( type == "matrix3" )
			{
				Matrix3 parsedData = StringConverter::parseMatrix3(value);
				result = sceneManager->setOption(name, &parsedData);
			}
			else if	( type == "matrix4" )
			{
				Matrix4 parsedData = StringConverter::parseMatrix4(value);
				result = sceneManager->setOption(name, &parsedData);
			}	
			else if	( type == "quaternion" )
			{
				Quaternion parsedData = StringConverter::parseQuaternion(value);
				result = sceneManager->setOption(name, &parsedData);
			}
			else if	( type == "colourvalue" )
			{
				ColourValue parsedData = StringConverter::parseColourValue(value);
				result = sceneManager->setOption(name, &parsedData);
			}
			else if	( type == "movableObject" )
			{
				StringVector movableName_Type = StringUtil::split(value);
				
				if ( movableName_Type.size() != 2 )
				{
					if (mDotSceneInfo) mDotSceneInfo->logLoadError(
						"Attribute \"value\" of element \"" + mElementName + "\" has an invalid value: \"" + value + "\"!\n"
						"Check the .scene file.");
				}
				else
				{
					if (namePrefix) movableName_Type[0] = *namePrefix + movableName_Type[0];
						result = sceneManager->setOption(name, sceneManager->getMovableObject(movableName_Type[0], movableName_Type[1]));
				}
			}
			else if	( type == "camera" )
			{
				String cameraPrefixedName = namePrefix ? *namePrefix + value : value;
				result = sceneManager->setOption(name, sceneManager->getCamera(cameraPrefixedName));
			}
			else if	( type == "node" )
			{
				String nodePrefixedName = namePrefix ? *namePrefix + value : value;
				result = sceneManager->setOption(name, sceneManager->getSceneNode(nodePrefixedName));
			}
			else
			{
				if (mDotSceneInfo) mDotSceneInfo->logLoadError(
						"Attribute \"type\" of element \"" + mElementName + "\" in file \"" + mFilename + "\" has an invalid value: \"" + type + "\"!\n"
						"Valid values are \"bool\", \"real\", \"int\", \"unsigned_int\", \"short\", \"unsigned_short\", \"long\", \"unsigned_long\", "
						"\"size_t\", \"string\", \"vector3\", \"matrix3\", \"matrix4\", \"quaternion\", \"colourvalue\", \"movableObject\", \"camera\", \"node\".\n"
						"The pointer to the converted data returned from StringTransformSpaceToEnumTransformSpace() will be a null pointer.\n"
						"See the dotsceneformat.dtd for more information.\n");
				result = true;
			} 
		}catch (Ogre::Exception& ex)
		{
			if (mDotSceneInfo) mDotSceneInfo->logLoadError(
				"SceneManagerOptionElementProcessor::parseElementImpl(): An exception occured while calling ParseStringBasedOnType(): \n" +
				ex.getFullDescription() + "\n"
				"If attribute \"type\" has the value \"camera\", \"light\", \"entity\", or anything similar which indicates that the\n"
				"attribute \"value\" stores the name (as a string) of some kind of Ogre related object, which needs to be resolved to a\n"
				"pointer to that object, then define the given \"" + String(currentNode->Value()) + "\" element *after* all other sibling\n"
				"xml elements in the \"scene\" parent element. Otherwise the name of the object wouldn't get resolved to a pointer to the\n"
				"object, because the object does not exist and an exception will be thrown.");
		}

		if (!result && mDotSceneInfo) mDotSceneInfo->logLoadError(
				"Can not set option \"" + name + "\" to \"" + value + "\" on scene manager!"); 
	}
	else
	{// No type specified. Log an error.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"No type specified for scene node option. Option won't be set on SceneManager. Check the .scene file.");
	}

	// TODO: design question: what should we return with? There is no exact parsed data.
	return 0;
}

//***********************************************************************************************************************************
//******************************************** End of SceneManagerOptionsElementProcessor *******************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************** ShadowSettingsElementProcessor *************************************************
//***********************************************************************************************************************************


Ogre::ShadowTechnique ShadowSettingsElementProcessor::parseAttribute_shadowTechnique(const String& attributeName, XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	String strShadowTechnique = 
		parseAttribute(attributeName, parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, String("none"));
	Ogre::ShadowTechnique shadowTechnique;

	if		( strShadowTechnique == "none"							) shadowTechnique = Ogre::SHADOWTYPE_NONE;
	else if ( strShadowTechnique == "stencilModulative"				) shadowTechnique = Ogre::SHADOWTYPE_STENCIL_MODULATIVE;
	else if ( strShadowTechnique == "stencilAdditive"				) shadowTechnique = Ogre::SHADOWTYPE_STENCIL_ADDITIVE;
	else if ( strShadowTechnique == "textureModulative"				) shadowTechnique = Ogre::SHADOWTYPE_TEXTURE_MODULATIVE;
	else if ( strShadowTechnique == "textureAdditive"				) shadowTechnique = Ogre::SHADOWTYPE_TEXTURE_ADDITIVE;
	else if ( strShadowTechnique == "textureModulativeIntegrated"	) shadowTechnique = Ogre::SHADOWTYPE_TEXTURE_MODULATIVE_INTEGRATED;
	else if ( strShadowTechnique == "textureAdditiveIntegrated"		) shadowTechnique = Ogre::SHADOWTYPE_TEXTURE_ADDITIVE_INTEGRATED;
	else
	{// Invalid value for attribute.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"Attribute \"" + attributeName + "\" of element \"" + String(currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value: \"" + strShadowTechnique + "\"!\n"
			"Valid values are \"none\", \"stencilModulative\", \"stencilAdditive\", \"textureModulative\", \"textureAdditive\", \"textureAdditive\", \"textureModulativeIntegrated\" and \"textureAdditiveIntegrated\".\n"
			"\""+ attributeName + "\" attribute will now default to \"none\".\n"
			"See the dotsceneformat.dtd for more information.\n");
		shadowTechnique = Ogre::SHADOWTYPE_NONE;
	}
	return shadowTechnique;
}

void* ShadowSettingsElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// -------------------------------------------------- Common shadow attributes ---------------------------------------------------
	// Get a copy of the pointer to the scene manager for faster accessing.
	SceneManager* sceneManager = (SceneManager*)mTreeBuilder->getCommonParameter("SceneManager");

	// Parse attribute "shadowTechnique", storing the shadow technique to be used.
	Ogre::ShadowTechnique shadowTechnique = parseAttribute_shadowTechnique("shadowTechnique", parent, currentNode);
	sceneManager->setShadowTechnique(shadowTechnique);

	// Parse attribute "showDebugShadows". Whether or not show the debug shadows.
	bool showDebugShadows = parseAttribute("showDebugShadows", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)false);
	sceneManager->setShowDebugShadows(showDebugShadows);

	// Parse attribute "shadowFarDistance".
	Real shadowFarDistance = parseAttribute("shadowFarDistance", parent, currentNode, XmlNodeProcessor::XMLADD_IMPLIED, (Real)0);
	sceneManager->setShadowFarDistance(shadowFarDistance);


	// ---------------------------------------- Attributes specific for stencil based shadows ----------------------------------------

	// Parse attribute "shadowDirectionalLightExtrusionDistance".
	Real shadowDirectionalLightExtrusionDistance =
		parseAttribute(	"shadowDirectionalLightExtrusionDistance",
						parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)10000);
	sceneManager->setShadowDirectionalLightExtrusionDistance(shadowDirectionalLightExtrusionDistance);

	// Parse attribute "shadowIndexBufferSize".
	size_t shadowIndexBufferSize =
		parseAttribute(	"shadowIndexBufferSize",
		parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (unsigned int)51200);
	sceneManager->setShadowIndexBufferSize(shadowIndexBufferSize);

	// Parse attribute "shadowUseInfiniteFarPlane".
	bool shadowUseInfiniteFarPlane =
		parseAttribute(	"shadowUseInfiniteFarPlane", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)true);
	sceneManager->setShadowUseInfiniteFarPlane(shadowUseInfiniteFarPlane);

	// ---------------------------------------- Attributes specific for texture based shadows ----------------------------------------

	// Parse attribute "shadowTextureCount".
	size_t shadowTextureCount =
		parseAttribute(	"shadowTextureCount", parent, currentNode, XmlNodeProcessor::XMLADD_IMPLIED, (unsigned int)0);
	sceneManager->setShadowTextureCount(shadowTextureCount);

	// Parse attribute "shadowTextureSize".
	unsigned short shadowTextureSize =
		parseAttribute(	"shadowTextureSize", parent, currentNode, XmlNodeProcessor::XMLADD_IMPLIED, (unsigned int)0);
	if ( shadowTextureSize ) sceneManager->setShadowTextureSize(shadowTextureSize);

	// Parse attribute "shadowTexturePixelFormat".
	String strShadowTexturePixelFormat =
		parseAttribute(	"shadowTexturePixelFormat", parent, currentNode, XmlNodeProcessor::XMLADD_IMPLIED, (String)BLANKSTRING);
	PixelFormat eShadowTexturePixelFormat;
	try
	{
		eShadowTexturePixelFormat = StringPixelFormatToEnumPixelFormat(strShadowTexturePixelFormat);
	}catch (Ogre::Exception& ex)
	{
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"Attribute \"shadowTexturePixelFormat\" of element \"" + mElementName + "\" in file \"" + mFilename + "\" has an invalid "
			"value: \"" + strShadowTexturePixelFormat + "\"! An exception occured while trying to convert it to an enumerated type pixel format: "
			+ ex.getFullDescription());
		eShadowTexturePixelFormat = PF_UNKNOWN;
	}
	if ( eShadowTexturePixelFormat != PF_UNKNOWN ) sceneManager->setShadowTexturePixelFormat(eShadowTexturePixelFormat);

	// Parse attribute "shadowDirLightTextureOffset".
	Real shadowDirLightTextureOffset =
		parseAttribute(	"shadowDirLightTextureOffset", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)0.6);
	sceneManager->setShadowDirLightTextureOffset(shadowDirLightTextureOffset);

	// Parse attribute "shadowTextureFadeStart".
	Real shadowTextureFadeStart =
		parseAttribute(	"shadowTextureFadeStart", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)0.7);
	sceneManager->setShadowTextureFadeStart(shadowTextureFadeStart);

	// Parse attribute "shadowTextureFadeEnd".
	Real shadowTextureFadeEnd =
		parseAttribute(	"shadowTextureFadeEnd", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)0.9);
	sceneManager->setShadowTextureFadeEnd(shadowTextureFadeEnd);

	// Parse attribute "shadowTextureSelfShadow".
	bool shadowTextureSelfShadow =
		parseAttribute(	"shadowTextureSelfShadow", parent, currentNode, XmlNodeProcessor::XMLADD_IMPLIED, (bool)false);
	sceneManager->setShadowTextureSelfShadow(shadowTextureSelfShadow);

	// Parse attribute "shadowTextureCasterMaterial".
	String shadowTextureCasterMaterial =
		parseAttribute(	"shadowTextureCasterMaterial", parent, currentNode, XmlNodeProcessor::XMLADD_IMPLIED, (String)BLANKSTRING);
	sceneManager->setShadowTextureCasterMaterial(shadowTextureCasterMaterial);

	// Parse attribute "shadowTextureReceiverMaterial".
	String shadowTextureReceiverMaterial =
		parseAttribute(	"shadowTextureReceiverMaterial", parent, currentNode, XmlNodeProcessor::XMLADD_IMPLIED, (String)BLANKSTRING);
	sceneManager->setShadowTextureReceiverMaterial(shadowTextureReceiverMaterial);

	// Parse attribute "shadowCasterRenderBackFaces".
	bool shadowCasterRenderBackFaces =
		parseAttribute(	"shadowCasterRenderBackFaces", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)true);
	sceneManager->setShadowCasterRenderBackFaces(shadowCasterRenderBackFaces);

	//sceneManager->setShadowCameraSetup(ShadowCameraSetupPtr(new DefaultShadowCameraSetup()));


	// TODO: design question: what should we return with? There is no exact parsed data.
	return 0;
}

//***********************************************************************************************************************************
//*********************************************** End of ShadowSettingsElementProcessor *********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//*********************************************** ShadowTextureConfigElementProcessor ***********************************************
//***********************************************************************************************************************************

void* ShadowTextureConfigElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// Parse attribute "shadowIndex".
	size_t shadowIndex = parseAttribute("shadowIndex", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (unsigned int)0);

	// Parse attribute "width".
	mShadowTextureConfig.width = parseAttribute("width", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (unsigned int)0);

	// Parse attribute "height".
	mShadowTextureConfig.height = parseAttribute("height", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (unsigned int)0);

	// Parse attribute "pixelFormat".
	String strPixelFormat = parseAttribute("pixelFormat", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (String)BLANKSTRING);
	try
	{
		mShadowTextureConfig.format = StringPixelFormatToEnumPixelFormat(strPixelFormat);
	}catch (Ogre::Exception& ex)
	{
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"Attribute \"pixelFormat\" of element \"" + mElementName + "\" in file \"" + mFilename + "\" has an invalid "
			"value: \"" + strPixelFormat + "\"! An exception occured while trying to convert it to an enumerated type pixel format: "
			+ ex.getFullDescription());
		mShadowTextureConfig.format = PF_UNKNOWN;
	}

	try
	{
		// Now set the shadow texture config.
		((SceneManager*)mTreeBuilder->getCommonParameter("SceneManager"))->setShadowTextureConfig(shadowIndex, mShadowTextureConfig);
	}catch(Ogre::Exception& ex)
	{
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"Unable to set shadow texture config with SceneManager::setShadowTextureConfig()! Exception occured: " +
			ex.getFullDescription());
	}

	// TODO: shadow texture config doesn't consist shadowIndex, so the returned data is not totally equal with the parsed data.
	// Try to find some kind of fix for this.
	return &mShadowTextureConfig;
}

//***********************************************************************************************************************************
//******************************************* End of ShadowTextureConfigElementProcessor ********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************** ResourceGroupElementProcessor **************************************************
//***********************************************************************************************************************************

void* ResourceGroupElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// Parse attribute "name".
	mResourceGroupName = parseAttribute("name", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (String)BLANKSTRING);

	// Check if user has set a resource group to use, other then the one specified in the dotScene file.
	void* resourceGroupCommonPar; // This is just to avoid quering common parameter twice, since it requires a search in a map.
	String strResourceGroupCommonPar = "";
	if ( (resourceGroupCommonPar = mTreeBuilder->getCommonParameter("ResourceGroup")) != 0 )
	{
		if ( (strResourceGroupCommonPar = *((String*)resourceGroupCommonPar)) != "" )
		{
			if ( strResourceGroupCommonPar == ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME )
			{
				if ( mResourceGroupName != "" )
				{// User specified common param "ResourceGroup" to be autodetect and the dotScene file consist a valid resource
				 // group name. Use that resource group name, since resources can not be loaded into the autodetect group.
					if (mDotSceneInfo) mDotSceneInfo->logLoadWarning(
						"Common parameter \"ResourceGroup\" is equal with ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME,\n"
						"while the .scene file consist a valid resource group name: \"" + mResourceGroupName + "\".\n"
						"However resource locations can not be loaded into and resource declarations can not be created in the autodetect group.\n"
						"The resource group specified in the .scene file will be created and resource locations, resource declarations will be set on it.\n");
				}else
				{// User specified common param "ResourceGroup" to be autodetect and the dotScene file doesn't consist a valid
				 // resource group name. However resources can not be loaded into the autodetect group, so throw an exception.
					OGRE_EXCEPT(
						Exception::ERR_INVALIDPARAMS,
						"Common parameter \"ResourceGroup\" is equal with ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME,\n"
						"while the .scene file \"" + mFilename + "\" does not consist a valid resource group name. However resource locations can not be\n"
						"loaded into and resource declarations can not be created in the autodetect group.",
						"ResourceGroupElementProcessor::parseElementImpl");
				}
			}else
			{
				if (mDotSceneInfo) mDotSceneInfo->logLoadWarning(
					"Common parameter \"ResourceGroup\" has been specified for the dotScene loader, while the scene file \"" + mFilename +
					"\" has the element \"" + String(currentNode->Value()) + "\" with attribute \"name\"=\"" + mResourceGroupName + "\".\n"
					"The resource group specified in the dotScene file won't be created, instead all resource locations will be added and resources will\n"
					"be loaded into the resource group specified as common parameter."
					"CAUTION!!! The resource group \"" + strResourceGroupCommonPar + "\" will be cleared in order to be able to initialise it again after all resource locations are parsed and all resource declarations are made.\n"
					"All data loaded from the resource group \"" + strResourceGroupCommonPar + "\" and all resource declarations in this group will be lost!");
				mResourceGroupName = strResourceGroupCommonPar;
			}
		}
	}

	if ( mResourceGroupName != "" )
	{
		try
		{
			Ogre::ResourceGroupManager::getSingleton().createResourceGroup(mResourceGroupName);
		}catch (Ogre::Exception& ex)
		{// By the time of writing 1.0.0 version of dotScene loader, Ogre only throws exceptions during resource group creation if
		 // the given resource group already exist. Log an error message about it anyway and then ignore it.
			if ( ex.getNumber() == Ogre::Exception::ERR_DUPLICATE_ITEM )
			{// ex.typeName == "ItemIdentityException"
				if (mDotSceneInfo) mDotSceneInfo->logLoadError(
					"Unable to create resource group: \"" + mResourceGroupName + "\"! A resource group by that name already exist.\n"
					"Exception description: " + ex.getFullDescription() + "\n"
					"CAUTION!!! The resource group \"" + mResourceGroupName + "\" will be cleared in order to be able to initialise it again after all resource locations are parsed and all resource declarations are made.\n"
					"All data loaded from the resource group \"" + mResourceGroupName + "\" and all resource declarations in this group will be lost!");
			}else
			{
				if (mDotSceneInfo) mDotSceneInfo->logLoadError("Unable to create resource group: \"" + mResourceGroupName + "\"! Exception occured: " + ex.getFullDescription());

				// TODO: design question: should we rethrow exception? Maybe we should, because exceptions other then ItemIdentityExceptions may cause the loader
				// the fail later anyway.
			}
		}
	}else
	{// If resource group can not be obtained neither from common parameters nor from the dotScene file, then log an error and use
	 // the default resource group.
		mResourceGroupName = ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME;
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"No name specified for resource group, resource locations will be added to the default resource group!\n"
			"CAUTION!!! The default resource group will be cleared in order to be able to initialise it again after all resource locations are parsed and all resource declarations are made.\n"
			"All data loaded from the default resource group (\"" + mResourceGroupName + "\") and all resource declarations in the default resource group will be lost!");
	}

	// If the resource group is comming from file and not from a common parameter, then set the common parameter "ResourceGroup",
	// so other element parsers can use it later.
	if ( resourceGroupCommonPar == 0 || strResourceGroupCommonPar == "" ) 
		mTreeBuilder->setCommonParameter("ResourceGroup", &mResourceGroupName);
	ResourceGroupManager::getSingleton().clearResourceGroup(mResourceGroupName);

	return &mResourceGroupName;
}

void ResourceGroupElementProcessor::parseElementEndImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	// Initialize previously created resource group (parse scripts found at resource locations and create pre-declared resources).
	ResourceGroupManager::getSingleton().initialiseResourceGroup(mResourceGroupName);
	// Load resource group.
	ResourceGroupManager::getSingleton().loadResourceGroup(mResourceGroupName);
}

//***********************************************************************************************************************************
//********************************************* End of ResourceGroupElementProcessor ************************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************ ResourceLocationElementProcessor *************************************************
//***********************************************************************************************************************************

// These lines were copied from OgreFileSystem.cpp. It is licensed under LGPL.
// Copyright (c) 2000-2006 Torus Knot Software Ltd
// Also see acknowledgements in Readme.html
	static bool is_absolute_path(const char* path)
	{
	#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
		if (isalpha(uchar(path[0])) && path[1] == ':')
			return true;
	#endif
		return path[0] == '/' || path[0] == '\\';
	}
// End of copied lines.


ResourceLocationElementProcessor::RelativeTo
	ResourceLocationElementProcessor::parseAttribute_relativeTo(
		const String& attributeName, XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	String strRelativeTo = 
		parseAttribute(attributeName, parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, String("dotSceneFileLocation"));
	ResourceLocationElementProcessor::RelativeTo relativeTo;

	if		( strRelativeTo == "dotSceneFileLocation" )	relativeTo = ResourceLocationElementProcessor::RT_DOT_SCENE_FILE_LOCATION;
	else if ( strRelativeTo == "exeFileLocation"	  )	relativeTo = ResourceLocationElementProcessor::RT_EXE_FILE_LOCATION;
	else if ( strRelativeTo == "currentWorkingDir"	  )	relativeTo = ResourceLocationElementProcessor::RT_CURRENT_WORKING_DIRECTORY;
	else if ( strRelativeTo == "absolute"			  )	relativeTo = ResourceLocationElementProcessor::RT_ABSOLUTE;
	else
	{// Invalid value for attribute.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"Attribute \"" + attributeName + "\" of element \"" + String(currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value: \"" + strRelativeTo + "\"!\n"
			"Valid values are \"dotSceneFileLocation\", \"exeFileLocation\", \"currentWorkingDir\" and \"absolute\".\n"
			"\""+ attributeName + "\" attribute will now default to \"dotSceneFileLocation\".\n"
			"See the dotsceneformat.dtd for more information.\n");
		relativeTo = ResourceLocationElementProcessor::RT_DOT_SCENE_FILE_LOCATION;
	}
	return relativeTo;
}

void* ResourceLocationElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	String		resourceLocationName = parseAttribute("name", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, BLANKSTRING);
	String		resourceLocationType = parseAttribute("type", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, BLANKSTRING);
	bool		recursive			 = parseAttribute("recursive", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)true);
	RelativeTo	relativeTo			 = parseAttribute_relativeTo("relativeTo", parent, currentNode);


//	String	resourceGroup		 = *((String*)mTreeBuilder->getCommonParameter("ResourceGroup"));
	// Use this line instead of the preceding one to ensure that all resource locations are declared in the correct resource groups
	// even if element "scene" has more then one child element "resourceGroup".
	String	resourceGroup		 = *((String*)parent->getParsedNode());


	// Check for path validity.
	if ( is_absolute_path(resourceLocationName.c_str()) && relativeTo != ResourceLocationElementProcessor::RT_ABSOLUTE )
	{
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"Attribute \"relativeTo\" of element \"" + String(currentNode->Value()) + "\" in file \"" + mFilename + "\" hasn't got "
			"the value \"absolute\", while the path specified in attribute \"name\" is an absolute path!\n"
			"The value of attribute \"name\" will be treated as if \"absolute\" were specified for attribute \"relativeTo\".\n"
			"See the dotsceneformat.dtd for more information.\n");
		relativeTo = ResourceLocationElementProcessor::RT_ABSOLUTE;
	}

	// Concatenate resource location path with the path specified in attribte "relativeTo".
	String strConcatenatedResourceLocation;
	switch ( relativeTo )
	{
	case ResourceLocationElementProcessor::RT_DOT_SCENE_FILE_LOCATION:
		{
			String sceneFilePathAndName = currentNode->GetDocument()->Value();
			String sceneFilename, sceneFilePath;
			StringUtil::splitFilename(sceneFilePathAndName, sceneFilename, sceneFilePath);
			if ( sceneFilePath == "" || !is_absolute_path(sceneFilePath.c_str()) )
			{// Either XML document name didn't consist a fully qualified path name, just a filename, or the path returned by
			 // the TiXMLDocument is not an absolute path, so we have to concatenate it with the current working directory.
				String currentDir = dsi::PlatformUtils::getCurrentDirectory();
				
				if ( sceneFilePath.substr(0, 2) == "./" ) sceneFilePath = sceneFilePath.substr(2);
				sceneFilePath = currentDir + sceneFilePath;
			}

			strConcatenatedResourceLocation = sceneFilePath + resourceLocationName;
		}break;
	case ResourceLocationElementProcessor::RT_EXE_FILE_LOCATION:
		{
			// Get current working directory.
			String strExeDir = dsi::PlatformUtils::getCurrentExecutableFilePath();

			// The returned path is standardized, so just simply use it.
			strConcatenatedResourceLocation = strExeDir + resourceLocationName;
		}break;
	case ResourceLocationElementProcessor::RT_CURRENT_WORKING_DIRECTORY:
		{
			// Get current working directory.
			String strCurrentDir = dsi::PlatformUtils::getCurrentDirectory();

			// The returned path is standardized, so just simply use it.
			strConcatenatedResourceLocation = strCurrentDir + resourceLocationName;
		}break;
	case ResourceLocationElementProcessor::RT_ABSOLUTE: // Resource location is absolute, do not touch it.
		{
			strConcatenatedResourceLocation = resourceLocationName;
		}break;
	}

	// Add resource location.
	ResourceGroupManager::getSingleton().addResourceLocation(
		strConcatenatedResourceLocation, resourceLocationType, resourceGroup, recursive);

	// TODO: design question: what should we return with? There is no exact parsed data.
	return 0;
}

//***********************************************************************************************************************************
//********************************************* End of ResourceLocationElementProcessor *********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//*********************************************** ResourceDeclarationElementProcessor ***********************************************
//***********************************************************************************************************************************

void* ResourceDeclarationElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	mResourceDeclarationName = parseAttribute("name",			parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, BLANKSTRING);
	mResourceType			 = parseAttribute("resourceType",	parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, BLANKSTRING);

	// Do nothing else, since we have to wait for child element processor to parse optional name-value pair lists.
	// Resource declaration is done in parseElementEndImpl();

	// TODO: design question: what should we return with? There is no exact parsed data.
	return 0;
}

void ResourceDeclarationElementProcessor::parseElementEndImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	ResourceGroupManager::getSingleton().declareResource(
		mResourceDeclarationName, mResourceType, *((String*)parent->getParsedNode()), mNameValuePairList);
}


//***********************************************************************************************************************************
//******************************************** End of ResourceDeclarationElementProcessor *******************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************ NameValuePairListElementProcessor ************************************************
//***********************************************************************************************************************************

void* NameValuePairListElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// Return with the currently empty name-value pair list, so child element processors can populate it.
	return &mNameValuePairList;
}

void NameValuePairListElementProcessor::parseElementEndImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	((NameValuePairListReceiverElementProcessor*)parent)->setNameValuePairList(mNameValuePairList);
}

//***********************************************************************************************************************************
//********************************************* End of NameValuePairListElementProcessor ********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************** NameValuePairElementProcessor **************************************************
//***********************************************************************************************************************************

void* NameValuePairElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	mNameValuePair.first	= parseAttribute("name",	parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, BLANKSTRING);
	mNameValuePair.second	= parseAttribute("value",	parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, BLANKSTRING);

	((Ogre::NameValuePairList*)parent->getParsedNode())->
		insert(Ogre::NameValuePairList::value_type(mNameValuePair.first, mNameValuePair.second));

	return &mNameValuePair;
}

//***********************************************************************************************************************************
//*********************************************** End of NameValuePairElementProcessor **********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************** RenderTargetsElementProcessor **************************************************
//***********************************************************************************************************************************

void* RenderTargetsElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// TODO: should "RenderTargetsElementProcessor" element processor parse anything at all?

	// TODO: design question: what should we return with?
	return 0;
}

//***********************************************************************************************************************************
//*********************************************** End of RenderTargetsElementProcessor **********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//*************************************************** RenderWindowElementProcessor **************************************************
//***********************************************************************************************************************************

void* RenderWindowElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// ----------------------------------- Parse "normal" attributes, which will be used directly ----------------------------------
	// Prefixing is not needed, we don't want to recreate all of the RenderWindows in case we load the same scene multiple times
	// into different nodes.
	mRenderWindowData.mName			= getNodeNameAutoNameIfNeeded(currentNode);
	mRenderWindowData.mWidth		= 
		parseAttribute("width",			parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED,	(unsigned int)0.0);
	mRenderWindowData.mHeight		=
		parseAttribute("height",		parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED,	(unsigned int)0.0);

	mRenderWindowData.mFullscreen	= 
		parseAttribute("fullscreen",	parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(bool)false);

	mRenderWindowData.mPriority		= 
		parseAttribute("priority",		parent, currentNode, XmlNodeProcessor::XMLADD_IMPLIED, (unsigned int)0);

	mRenderWindowData.mActive		= 
		parseAttribute("active",		parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)true);
	mRenderWindowData.mAutoUpdated	= 
		parseAttribute("autoUpdated",	parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)true);
	mRenderWindowData.mPrimary		=
		parseAttribute("primary",		parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)false);
	
	mRenderWindowData.mVisible		=
		parseAttribute("visible",		parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)true);


	// ------------------------------ Parse attributes which have to be converted to name-value pairs ------------------------------
	// Ogre expects a varying number of parameters through name-value pairs for RenderWindow creation.
	// However, name-value pairs would be stored as separate child elements of xml element "renderWindow". This would make it
	// impossible to create a RenderWindow based only on its xml element attributes. This would be against of the phylosophy of
	// XmlNodeProcessor, which says that every xml element has to has all the attributes necessary to create an application object
	// representing it, and child elements should only tweak or extend the given xml element.
	// So the work around for this issue is to store these name-value pairs in the dot scene file as normal xml element attributes,
	// and convert them to string name-value pairs during parsing. This is what is done in the next few lines.
	Ogre::NameValuePairList		miscParams;

	parseAttributeAndSaveAsNameValuePair("title",				parent, currentNode, XMLADD_IMPLIED, miscParams);
	parseAttributeAndSaveAsNameValuePair("colourDepth",			parent, currentNode, XMLADD_IMPLIED, miscParams);
	parseAttributeAndSaveAsNameValuePair("left",				parent, currentNode, XMLADD_IMPLIED, miscParams);
	parseAttributeAndSaveAsNameValuePair("top",					parent, currentNode, XMLADD_IMPLIED, miscParams);
	parseAttributeAndSaveAsNameValuePair("depthBuffer",			parent, currentNode, XMLADD_DEFAULTVALUEPRESENT, miscParams, "true");
	parseAttributeAndSaveAsNameValuePair("FSAA",				parent, currentNode, XMLADD_DEFAULTVALUEPRESENT, miscParams, "0");
	parseAttributeAndSaveAsNameValuePair("displayFrequency",	parent, currentNode, XMLADD_IMPLIED, miscParams);
	parseAttributeAndSaveAsNameValuePair("vsync",				parent, currentNode, XMLADD_DEFAULTVALUEPRESENT, miscParams, "false");
	parseAttributeAndSaveAsNameValuePair("border",				parent, currentNode, XMLADD_DEFAULTVALUEPRESENT, miscParams, "resize");
	parseAttributeAndSaveAsNameValuePair("outerDimensions",		parent, currentNode, XMLADD_DEFAULTVALUEPRESENT, miscParams, "false");

	// ----------------------------------------------------------------------------------------------------------------------------

	try

	{
		bool primaryWindowAlreadyExists = false;
		if (mRenderWindowData.mPrimary)
		{// The RenderWindow declared in the .xml file is the primary render window.
		 // If a primary RenderWindow already exists, then modify its properties. If no window was created before, then sipmly 
		 // create the window, it will be the primary render window anyway.
			
			if ( (mRenderWindow = Root::getSingleton().getAutoCreatedWindow()) && mRenderWindow->isPrimary() )
			{// Ogre has auto created a window for us previously. This window must be the primary window.
				primaryWindowAlreadyExists  = true;
			}else
			{// Ogre hasn't auto created a primary RenderWindow, however it is still possible that someone has done it before, so
			 // search through all of the RenderWindows.
				RenderTarget*	renderTarget;
				Ogre::RenderSystem::RenderTargetIterator it = Root::getSingleton().getRenderSystem()->getRenderTargetIterator();
				while (it.hasMoreElements())
				{
					renderTarget = it.getNext();
					if (renderTarget->isPrimary())
					{// We have found the previously manually created primary RenderWindow.
						mRenderWindow				= (RenderWindow*)renderTarget; // TODO: is this typecast safe in every case?
						primaryWindowAlreadyExists	= true;
					}
				}
			}

			if (primaryWindowAlreadyExists)
			{// Primary window was created before, try to tweak its parameters.
				if (mDotSceneInfo) 
					mDotSceneInfo->logLoadWarning("A primary render window with name \"" + mRenderWindow->getName() +
												  "\" already exists, however a render window with name \"" + mRenderWindowData.mName + 
												  "\" marked as primary is also present in the file \"" + mFilename + "\". "
												  "Properties of the current primary render window will be overwritten with "
												  "the properties parsed from the scene file.");

				// First change general properties.
				if (mRenderWindow->isFullScreen() != mRenderWindowData.mFullscreen)
					mRenderWindow->setFullscreen(mRenderWindowData.mFullscreen, mRenderWindowData.mWidth, mRenderWindowData.mHeight);
				else
					mRenderWindow->resize(mRenderWindowData.mWidth, mRenderWindowData.mHeight);

				NameValuePairList::iterator itLeft	= miscParams.find("left");
				NameValuePairList::iterator itTop	= miscParams.find("top");
				if (itLeft != miscParams.end() && itTop != miscParams.end())
					mRenderWindow->reposition(StringConverter::parseInt(itLeft->second), StringConverter::parseInt(itTop->second));


				// Then change properties given as name-value pairs and only changeable through RenderSystem:setConfigOption() calls.
				// The following lines aren't working. It seems that these properties aren't changeable after the primary
				// RenderWindow has been created.
				RenderSystem*	renderSystem = Root::getSingleton().getRenderSystem();
				if (renderSystem)
				{
					String renderSystemName = renderSystem->getName();
					NameValuePairList::iterator it;
					if ( (it = miscParams.find("vsync")) != miscParams.end() ) renderSystem->setConfigOption("VSync", it->second);
					if ( (it = miscParams.find("FSAA")) != miscParams.end() ) 
					{
						if (renderSystemName == "Direct3D9 Rendering Subsystem")
							renderSystem->setConfigOption("Anti aliasing", it->second);
						else if (renderSystemName == "OpenGL Rendering Subsystem")
							renderSystem->setConfigOption("FSAA", it->second);
					}
					if ( (it = miscParams.find("displayFrequency")) != miscParams.end() && 
						 (renderSystemName == "OpenGL Rendering Subsystem") ) // Display frequency is only changeable under OpenGL.
						renderSystem->setConfigOption("Display Frequency", it->second);
				}
			}
		}

		if (!mRenderWindowData.mPrimary ||(mRenderWindowData.mPrimary && !primaryWindowAlreadyExists))
			mRenderWindow = Root::getSingleton().createRenderWindow(
				mRenderWindowData.mName, mRenderWindowData.mWidth, mRenderWindowData.mHeight, mRenderWindowData.mFullscreen,
				&miscParams);
	}catch(Ogre::Exception& ex)
	{
		if (mDotSceneInfo) 
			mDotSceneInfo->logLoadError("Unable to create render window \"" + mRenderWindowData.mName + 
										"\"! Exception occured: " + ex.getFullDescription());
		mRenderWindow = 0;
	}

	if (mRenderWindow)
	{
		mRenderWindow->setActive(mRenderWindowData.mActive);
		mRenderWindow->setAutoUpdated(mRenderWindowData.mAutoUpdated);
		mRenderWindow->setVisible(mRenderWindowData.mVisible);
		mRenderWindow->setPriority(mRenderWindowData.mPriority);
	}

	return mRenderWindow;
}

//***********************************************************************************************************************************
//*********************************************** End of RenderWindowElementProcessor ***********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//**************************************************** ViewportsElementProcessor ****************************************************
//***********************************************************************************************************************************

void* ViewportsElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// TODO: should "ViewportsElementProcessor" element processor parse anything at all?


	return parent->getParsedNode();
}

//***********************************************************************************************************************************
//************************************************* End of ViewportsElementProcessor ************************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//***************************************************** ViewportElementProcessor ****************************************************
//***********************************************************************************************************************************

void* ViewportElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// ---------------------------------------------------- Parse viewport data ----------------------------------------------------
	// ------------------- Parse render target name.
	// IMPORTANT! Render target names are never prefixed. See readme.txt for more information.
	mViewportData.mRenderTargetName = 
		parseAttribute("renderTargetName", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED,	(String)"");
	// Search for render target.
	if ( (mViewportData.mRenderTarget = Root::getSingleton().getRenderTarget(mViewportData.mRenderTargetName)) == 0 )
	{// Render target not found, log an error and return.
		if (mDotSceneInfo) 
			mDotSceneInfo->logLoadError("Error processing element \"" + mElementName + "\": render target with name \"" +
										mViewportData.mRenderTargetName + "\" does not exists! Can not create viewport. "
										"Check the .scene file.");
		mViewportData.mRenderTarget = 0; 
		mViewport = 0;
		return mViewport;
	}

	// ------------------- Parse camera name.
	mViewportData.mCameraName	= 
		parseAttribute("cameraName", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED,	(String)"");
	// Now prefix camera name.
	String* prefix;
	if ( (prefix = ((String*)mTreeBuilder->getCommonParameter("NamePrefix"))) != 0 ) 
		mViewportData.mCameraName = *prefix + mViewportData.mCameraName;
	// Search for camera.
	SceneManager*	sceneManager;
	if ( (sceneManager = (SceneManager*)mTreeBuilder->getCommonParameter("SceneManager")) != 0)
	{
		try
		{
			mViewportData.mCamera = sceneManager->getCamera(mViewportData.mCameraName);
		}catch(Ogre::Exception& ex)
		{
			if (mDotSceneInfo) 
				mDotSceneInfo->logLoadError("Error creating viewport: unable to retrieve camera \"" +
											mViewportData.mCameraName + "\"! Exception occured: " + ex.getFullDescription());
			mViewportData.mCamera = 0;
			mViewport = 0;
			return mViewport;
		}
	}else
	{// No common parameter "SceneManager". Without a scene manager we can't search the camera corresponding to the
	 // viewport. Log an error.

		if (mDotSceneInfo) 
			mDotSceneInfo->logLoadError("Common parameter \"SceneManager\" is null! The camera corresponding to the viewport can "
										"not be retrieved without its owning SceneManager instance. Viewport won't be created!");
		mViewport = 0;
		return mViewport;
	}

	// ------------------- 
	mViewportData.mChangeCameraAspectRatio = 
		parseAttribute("changeCameraAspectRatio",	parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)true);


	mViewportData.mLeft	 = parseAttribute("left",	parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)0.0);
	mViewportData.mTop	 = parseAttribute("top",	parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)0.0);
	mViewportData.mWidth = parseAttribute("width",	parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)1.0);
	mViewportData.mHeight= parseAttribute("height",	parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)1.0);
	mViewportData.mZOrder= parseAttribute("zOrder",	parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (int)0);

	// ------------------- Parse "clearEveryFrame" and "clearBuffers".
	mViewportData.mClearEveryFrame =
		parseAttribute("clearEveryFrame",	parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)true);

	String clearBuffers =
		parseAttribute("clearBuffers",		parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (String)"colour|depth");
	mViewportData.mClearBuffers = 0;
	StringVector	clearBuffersSplit = StringUtil::split(clearBuffers, "| ", 0);
	for (StringVector::iterator it = clearBuffersSplit.begin(); it != clearBuffersSplit.end(); it++)
	{
		// Do not try to convert empty string to enumerated frame buffer type, because it means no buffer to clear.
		if ( *it != "" )
		{
			try
			{
				mViewportData.mClearBuffers |= StringFrameBufferTypeToEnumFrameBufferType(*it);
			}catch (Ogre::Exception& ex)
			{
				if (mDotSceneInfo) mDotSceneInfo->logLoadError(
					"Attribute \"clearBuffers\" of element \"" + mElementName + "\" in file \"" + mFilename + "\" has an invalid "
					"sub-value: \"" + *it + "\"! This sub-value will be ingored, however other sub-values will be used."
					"An exception occured while trying to convert it to an enumerated type frame buffer type: "
					+ ex.getFullDescription());
			}
		}
	}

	// -------------------

	mViewportData.mMaterialSchemeName = 
		parseAttribute("materialSchemeName",	parent, currentNode, XmlNodeProcessor::XMLADD_IMPLIED, (String)"");

	mViewportData.mOverlaysEnabled	= 
		parseAttribute("overlaysEnabled", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)true);
	mViewportData.mSkiesEnabled		=
		parseAttribute("skiesEnabled", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)true);
	mViewportData.mShadowsEnabled	=
		parseAttribute("shadowsEnabled", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)true);

	mViewportData.mVisibilityMask	=
		parseAttribute("visibilityMask", parent, currentNode, XmlNodeProcessor::XMLADD_IMPLIED, (unsigned int)0xFFFFFFFF);
	mViewportData.mRenderQueueInvocationSequenceName = 
		parseAttribute("renderQueueInvocationSequenceName", parent, currentNode, XmlNodeProcessor::XMLADD_IMPLIED, (String)"");

	//
	// Search for a viewport with the same ZOrder as the one we are trying to create. If a viewport with the same ZOrder already
	// exists, then it will be modified, instead of creation.
	bool viewportWithTheSameZOrderAlreadyExists = false;
	unsigned short i; // i will be used after the for loop too, so declare it local to the function and not the for loop.
	for ( i = 0; i < mViewportData.mRenderTarget->getNumViewports() && !viewportWithTheSameZOrderAlreadyExists; i++ )
	{
		mViewport = mViewportData.mRenderTarget->getViewport(i);
		if (mViewport->getZOrder() == mViewportData.mZOrder) viewportWithTheSameZOrderAlreadyExists = true;
	}
	
	if (viewportWithTheSameZOrderAlreadyExists)
	{// The given RenderTarget already has a viewport at the same ZOrder as the viewport declared in the .scene file has.
		if (mViewport->getCamera() != mViewportData.mCamera)
		{// The camera used to render to the viewport at the given ZOrder is different than the camera specified
		 // in the .scene file. Log a warning and overwrite current camera with the one specified in the .scene file.
			if (mDotSceneInfo) 
				mDotSceneInfo->logLoadWarning(
				"The camera \"" + mViewportData.mCameraName + "\" used to render to the viewport with index " + 
				StringConverter::toString(i) + " at the given ZOrder " + StringConverter::toString(mViewport->getZOrder()) +
				" of RenderTarget \"" + mViewportData.mRenderTargetName + "\" is different than the camera specified in the .scene "
				"file for rendering . The current camera of the viewport will be overwritten!");
			mViewport->setCamera(mViewportData.mCamera);
		}
		mViewport->setDimensions(mViewportData.mLeft, mViewportData.mTop, mViewportData.mWidth, mViewportData.mHeight);

	}else
	{// The RenderTarget hasn't got a viewport at the declared ZOrder yet. Create it!
		// ---------------------------------------------------- Create viewport ----------------------------------------------------
		try
		{
			mViewport = mViewportData.mRenderTarget->addViewport(mViewportData.mCamera, mViewportData.mZOrder, 
																 mViewportData.mLeft, mViewportData.mTop, 
																 mViewportData.mWidth, mViewportData.mHeight);
		}catch(Ogre::Exception& ex)
		{
			if (mDotSceneInfo) 
				mDotSceneInfo->logLoadError("Error creating viewport: RenderTarget::addViewport() failed! "
											"Exception occured: " + ex.getFullDescription());
			mViewport = 0;
		}
	}

	// --------------------------------------------------- Set viewport settigns ---------------------------------------------------
	if (mViewport)
	{
		try
		{
			mViewport->setClearEveryFrame(mViewportData.mClearEveryFrame, mViewportData.mClearBuffers);
			mViewport->setMaterialScheme(mViewportData.mMaterialSchemeName);
			mViewport->setOverlaysEnabled(mViewportData.mOverlaysEnabled);
			mViewport->setSkiesEnabled(mViewportData.mSkiesEnabled);
			mViewport->setShadowsEnabled(mViewportData.mShadowsEnabled);
			mViewport->setVisibilityMask(mViewportData.mVisibilityMask);
			mViewport->setRenderQueueInvocationSequenceName(mViewportData.mRenderQueueInvocationSequenceName);

			if (mViewportData.mChangeCameraAspectRatio)
				mViewportData.mCamera->setAspectRatio( Real(mViewport->getActualWidth()) / Real(mViewport->getActualHeight()));
		}catch(Ogre::Exception& ex)
		{
			// In Ogre 1.4.5, only Viewport::setRenderQueueInvocationSequenceName() is capable of throwing an exception. It throws
			// it indirectly through a call to Root::getRenderQueueInvocationSequence(). getRenderQueueInvocationSequence() throws
			// an exception if the render queue invocation sequence with the given name can not be found.

			if (mDotSceneInfo) 
				mDotSceneInfo->logLoadError("Error tweaking the paremeters of viewport! "
											"Exception occured: " + ex.getFullDescription());
		}
	}

	return mViewport;
}

//***********************************************************************************************************************************
//************************************************* End of ViewportElementProcessor *************************************************
//***********************************************************************************************************************************



//***********************************************************************************************************************************
//*********************************************** CompositorInstancesElementProcessor ***********************************************
//***********************************************************************************************************************************

void* CompositorInstancesElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	return parent->getParsedNode();
}

//***********************************************************************************************************************************
//******************************************* End of CompositorInstancesElementProcessor ********************************************
//***********************************************************************************************************************************

//***********************************************************************************************************************************
//*********************************************** CompositorInstanceElementProcessor ************************************************
//***********************************************************************************************************************************

void CompositorInstanceElementProcessor::parseCompositorData(XmlNodeProcessor* parent, TiXmlElement* currentNode, CompositorInstanceData& compositorInstanceData)
{
	compositorInstanceData.mCompositorName = 
		parseAttribute("compositorName",parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED,				(String)"");
	compositorInstanceData.mPosition = 
		parseAttribute("position",		parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(int)-1);
	compositorInstanceData.mEnabled = 
		parseAttribute("enabled",		parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(bool)false);
}

void* CompositorInstanceElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	Viewport*	viewport;
	if ( (viewport = (Ogre::Viewport*)parent->getParsedNode()) != 0 )
	{// Parent viewport creation was successful.
		parseCompositorData(parent, currentNode, mCompositorInstanceData);

		// If fails addCompositor() doesn't throw an exception, instead it returns with a null pointer.
		mCompositorInstance = CompositorManager::getSingleton().addCompositor(
										viewport, mCompositorInstanceData.mCompositorName, mCompositorInstanceData.mPosition);

		if (mCompositorInstance)
		{
			CompositorManager::getSingleton().setCompositorEnabled(
				viewport, mCompositorInstanceData.mCompositorName, mCompositorInstanceData.mEnabled);
		}else
		{
			if (mDotSceneInfo) 
				mDotSceneInfo->logLoadError("Can not create compositor instance. CompositorManager::addCompositor() failed.");
		}
	}else
		mCompositorInstance = 0;		

	return mCompositorInstance;
}

//***********************************************************************************************************************************
//******************************************** End of CompositorInstanceElementProcessor ********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//********************************************** HDRCompositorInstanceElementProcessor **********************************************
//***********************************************************************************************************************************


//void*	HDRCompositorInstanceElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
//{
//	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

//	Viewport*	viewport;
//	if ( (viewport = (Ogre::Viewport*)parent->getParsedNode()) != 0 )
//	{// Parent viewport creation was successful.

//		// TODO: what if HDRCompositorInstance is used for compositing a rendering which is done to a RenderTexture instance?
//		// Wouldn't this type cast of viewport->getTarget() to RenderWindow* will cause problems?
//		mHDRCompositorInstance = new HDRCompositor((RenderWindow*)viewport->getTarget(), viewport->getCamera());

//		bool enabled = parseAttribute("enabled", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(bool)false);

//		// ------------ Attribute "toneMapper"
//		String strToneMapper =
//			parseAttribute("toneMapper", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(String)"none");
//		HDRCompositor::TONEMAPPER eToneMapper;
//		try
//		{
//			eToneMapper = StringToneMapperToEnumToneMapper(strToneMapper);
//		}catch (Ogre::Exception& ex)
//		{
//			if (mDotSceneInfo) mDotSceneInfo->logLoadError(
//				"Attribute \"toneMapper\" of element \"" + mElementName + "\" in file \"" + mFilename + "\" has an invalid "
//				"value: \"" + strToneMapper + "\"! An exception occured while trying to convert it to an enumerated type frame buffer type: "
//				+ ex.getFullDescription());
//			eToneMapper = HDRCompositor::TM_NONE;
//		}
//		// ------------



//		// ------------ Attribute "glareType"
//		String strGlareType =
//			parseAttribute("glareType", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(String)"none");
//		HDRCompositor::GLARETYPE	eGlareType;
//		try
//		{
//			eGlareType = StringGlareTypeToEnumGlareType(strGlareType);
//		}catch (Ogre::Exception& ex)
//		{
//			if (mDotSceneInfo) mDotSceneInfo->logLoadError(
//				"Attribute \"glareType\" of element \"" + mElementName + "\" in file \"" + mFilename + "\" has an invalid "
//				"value: \"" + strGlareType + "\"! An exception occured while trying to convert it to an enumerated type glare type: "
//				+ ex.getFullDescription());
//			eGlareType = HDRCompositor::GT_NONE;
//		}
//		// ------------
//		Real glareStrength =
//			parseAttribute("glareStrength", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(Real)1.0);
//		int glarePasses	=
//			parseAttribute("glarePasses", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(Real)1.0);



//		// ------------ Attribute "starType"
//		String strStarType =
//			parseAttribute("starType", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(String)"none");
//		HDRCompositor::STARTYPE	eStarType;
//		try
//		{
//			eStarType = StringStarTypeToEnumStarType(strStarType);
//		}catch (Ogre::Exception& ex)
//		{
//			if (mDotSceneInfo) mDotSceneInfo->logLoadError(
//				"Attribute \"starType\" of element \"" + mElementName + "\" in file \"" + mFilename + "\" has an invalid "
//				"value: \"" + strStarType + "\"! An exception occured while trying to convert it to an enumerated type star type: "
//				+ ex.getFullDescription());
//			eStarType = HDRCompositor::ST_NONE;
//		}
//		// ------------
//		Real starStrength =
//			parseAttribute("starStrength", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(Real)1.0);
//		int starPasses	=
//			parseAttribute("starPasses", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(Real)1.0);


//		bool autoKeying	=
//			parseAttribute("autoKeying", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(bool)false);
//		Real key	=
//			parseAttribute("key", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)0.2);

//		bool luminanceAdaptation	=
//			parseAttribute("luminanceAdaptation", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(bool)false);
//		Real adaptationScale	=
//			parseAttribute("adaptationScale", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)1.0);

//		Real localE	=
//			parseAttribute("localE", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)0.05);
//		Real localPhi	=
//			parseAttribute("localPhi", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)8.0);


//		// ------------------------------- Now set properties on the created hdr compositor instance -------------------------------
//		mHDRCompositorInstance->SetToneMapper(eToneMapper);

//		mHDRCompositorInstance->SetGlareType(eGlareType);
//		mHDRCompositorInstance->SetGlareStrength(glareStrength);
//		mHDRCompositorInstance->SetGlarePasses(glarePasses);

//		mHDRCompositorInstance->SetStarType(eStarType);
//		mHDRCompositorInstance->SetStarStrength(starStrength);
//		mHDRCompositorInstance->SetStarPasses(starPasses);

//		mHDRCompositorInstance->SetAutoKeying(autoKeying);
//		mHDRCompositorInstance->SetKey(key);

//		mHDRCompositorInstance->SetLumAdaptation(luminanceAdaptation);
//		mHDRCompositorInstance->SetAdaptationScale(adaptationScale);

//		mHDRCompositorInstance->SetLocalE(localE);
//		mHDRCompositorInstance->SetLocalPhi(localPhi);

//		mHDRCompositorInstance->Enable(enabled);
//		// ------------------------------- Report the created HDR compositor instance to dotSceneInfo ------------------------------
//		if (mDotSceneInfo) mDotSceneInfo->reportLoadedHDRCompositorInstance(viewport, mHDRCompositorInstance);

//	}else
//		mHDRCompositorInstance = 0;

//	return mHDRCompositorInstance;
//}


//***********************************************************************************************************************************
//****************************************** End of HDRCompositorInstanceElementProcessor *******************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//*************************************************** EnvironmentElementProcessor ***************************************************
//***********************************************************************************************************************************

void* EnvironmentElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	return parent->getParsedNode();
}

//***********************************************************************************************************************************
//********************************************** End of EnvironmentElementProcessor *************************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//******************************************************* FogElementProcessor *******************************************************
//***********************************************************************************************************************************

void* FogElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	SceneManager* sceneManager = (SceneManager*)mTreeBuilder->getCommonParameter("SceneManager");
	
	// Parse fog mode.
	String	fogMode = parseAttribute("mode", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, String("none"));
	if ( fogMode == "none" )		mFogData.mFogMode = Ogre::FOG_NONE;
	else if ( fogMode == "exp" )	mFogData.mFogMode = Ogre::FOG_EXP;
	else if ( fogMode == "exp2" )	mFogData.mFogMode = Ogre::FOG_EXP2;
	else if ( fogMode == "linear" ) mFogData.mFogMode = Ogre::FOG_LINEAR;
	else
	{
		// Invalid value for attribute "mode".
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"Attribute \"mode\" of element \"" + String(currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value: \"" + fogMode + "\"!\n"
			"Valid values are \"none\", \"exp\", \"exp2\", \"linear\". \"mode\" attribute will now default to \"none\".\n"
			"See the dotsceneformat.dtd for more information.\n");
		mFogData.mFogMode = Ogre::FOG_NONE;
	}

	// Parse colour.
	mFogData.mColour.r = parseAttribute("colourR",	parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED,	(Real)0.0);
	mFogData.mColour.g = parseAttribute("colourG",	parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED,	(Real)0.0);
	mFogData.mColour.b = parseAttribute("colourB",	parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED,	(Real)0.0);
	mFogData.mColour.a = 1.0;

	sceneManager->setFog(
		mFogData.mFogMode,
		mFogData.mColour,
		(mFogData.mExpDensity	= 
			parseAttribute("expDensity",	parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(Real)0.001)),
		(mFogData.mLinearStart	=
			parseAttribute("linearStart",	parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(Real)0.0)),
		(mFogData.mLinearEnd		=
			parseAttribute("linearEnd",		parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(Real)1.0))
		);

	// Now report parsed fog data to DotSceneInfo instance if there is any.
	if (mDotSceneInfo)	mDotSceneInfo->reportLoadedFogData(mFogData);

	return &mFogData;
}

//***********************************************************************************************************************************
//*************************************************** End of FogElementProcessor ****************************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//***************************************************** SkyBoxElementProcessor *****************************************************
//***********************************************************************************************************************************

void* SkyBoxElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	SceneManager* sceneManager = (SceneManager*)mTreeBuilder->getCommonParameter("SceneManager");

	// Parse attribute "name".
	mSkyBoxData.mName = getNodeName_AutoNamedAndPrefixedIfNeeded(currentNode);
	
	mSkyBoxData.mEnabled = 
		parseAttribute("enabled",		parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	true);

	mSkyBoxData.mMaterialName = 
		parseAttribute("materialName",	parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED,				BLANKSTRING);

	mSkyBoxData.mDistance = 
		parseAttribute("distance",		parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(Real)5000.0);

	mSkyBoxData.mDrawFirst = 
		parseAttribute("drawFirst",		parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	true);

	// Orientation will be set by sub-element processor OrientationElementProcessor.
	// Currently only set sky box if it is enabled since the current version of Ogre can only handle one sky box instance per
	// scene.
	if ( mSkyBoxData.mEnabled )
	{
		LogManager::getSingletonPtr()->getDefaultLog()->logMessage(mSkyBoxData.mMaterialName);
		MaterialPtr m = MaterialManager::getSingleton().getByName(mSkyBoxData.mMaterialName);

		sceneManager->setSkyBox(
			mSkyBoxData.mEnabled, mSkyBoxData.mMaterialName, mSkyBoxData.mDistance,	mSkyBoxData.mDrawFirst,
			Quaternion::IDENTITY, // Orientation will be parsed by sub-element processor "OrientationElementProcessor", now set it to identity.
			!mTreeBuilder->getCommonParameter("ResourceGroup") ?
				ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME : 
				*((String*)mTreeBuilder->getCommonParameter("ResourceGroup") )
			);
	}

	// This is to make SkyBoxElementProcessor independent of EnvironmentElementProcessor.
	return sceneManager->getSkyBoxNode();
}

void SkyBoxElementProcessor::parseElementEndImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	SceneManager* sceneManager	= (SceneManager*)mTreeBuilder->getCommonParameter("SceneManager");

	if ( sceneManager->getSkyBoxNode() )
		mSkyBoxData.mOrientation	= sceneManager->getSkyBoxNode()->getOrientation();
	else
		mSkyBoxData.mOrientation	= Quaternion::IDENTITY;

	// Now report parsed sky box data to DotSceneInfo instance if there is any.
	if (mDotSceneInfo)	mDotSceneInfo->reportLoadedSkyBox(mSkyBoxData);
}


//***********************************************************************************************************************************
//************************************************ End of SkyBoxElementProcessor ****************************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//**************************************************** SkyDomeElementProcessor *****************************************************
//***********************************************************************************************************************************

void* SkyDomeElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	SceneManager* sceneManager = (SceneManager*)mTreeBuilder->getCommonParameter("SceneManager");

	// Parse attribute "name".
	mSkyDomeData.mName = getNodeName_AutoNamedAndPrefixedIfNeeded(currentNode);

	mSkyDomeData.mEnabled = 
		parseAttribute("enabled",			parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	true);
	mSkyDomeData.mMaterialName = 
		parseAttribute("materialName",		parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED,				BLANKSTRING);
	mSkyDomeData.mCurvature = 
		parseAttribute("curvature",			parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(Real)10.0);
	mSkyDomeData.mTiling = 
		parseAttribute("tiling",			parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(Real)8.0);
	mSkyDomeData.mDistance = 
		parseAttribute("distance",			parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(Real)4000.0);
	mSkyDomeData.mDrawFirst = 
		parseAttribute("drawFirst",			parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	true);
	mSkyDomeData.mXSegments = 
		parseAttribute("xsegments",			parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(int)16);
	mSkyDomeData.mYSegments = 
		parseAttribute("ysegments",			parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(int)16);
	mSkyDomeData.mYSegmentsKeep = 
		parseAttribute("ysegments_keep",	parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(int)-1);

	// Orientation will be set by sub-element processor OrientationElementProcessor.
	// Currently only set sky dome if it is enabled since the current version of Ogre can only handle one sky dome instance per
	// scene.
	if ( mSkyDomeData.mEnabled )
	{
		sceneManager->setSkyDome(
			mSkyDomeData.mEnabled, mSkyDomeData.mMaterialName, mSkyDomeData.mCurvature, mSkyDomeData.mTiling, mSkyDomeData.mDistance,
			mSkyDomeData.mDrawFirst, 
			Quaternion::IDENTITY, // Orientation will be parsed by sub-element processor "OrientationElementProcessor", now set it to identity.
			mSkyDomeData.mXSegments, mSkyDomeData.mYSegments, mSkyDomeData.mYSegmentsKeep,
			!mTreeBuilder->getCommonParameter("ResourceGroup") ?
				ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME : 
				*((String*)mTreeBuilder->getCommonParameter("ResourceGroup") )
			);
	}

	// This is to make SkyDomeElementProcessor independent of EnvironmentElementProcessor.
	return sceneManager->getSkyDomeNode();
}

void SkyDomeElementProcessor::parseElementEndImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	SceneManager* sceneManager	= (SceneManager*)mTreeBuilder->getCommonParameter("SceneManager");
	if ( sceneManager->getSkyDomeNode() )
		mSkyDomeData.mOrientation	= sceneManager->getSkyDomeNode()->getOrientation();
	else
		mSkyDomeData.mOrientation	= Quaternion::IDENTITY;

	// Now report parsed sky dome data to DotSceneInfo instance if there is any.
	if (mDotSceneInfo)	mDotSceneInfo->reportLoadedSkyDome(mSkyDomeData);
}


//***********************************************************************************************************************************
//************************************************* End of SkyDomeElementProcessor **************************************************
//***********************************************************************************************************************************



//***********************************************************************************************************************************
//**************************************************** SkyPlaneElementProcessor *****************************************************
//***********************************************************************************************************************************

void* SkyPlaneElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	SceneManager* sceneManager = (SceneManager*)mTreeBuilder->getCommonParameter("SceneManager");

	// Parse attribute "name".
	mSkyPlaneData.mName = getNodeName_AutoNamedAndPrefixedIfNeeded(currentNode);

	mSkyPlaneData.mEnabled = 
		parseAttribute("enabled",			parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, true);
	mSkyPlaneData.mMaterialName = 
		parseAttribute("materialName",		parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED,				BLANKSTRING);
	mSkyPlaneData.mScale = 
		parseAttribute("scale",				parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(Real)1000.0);
	mSkyPlaneData.mTiling = 
		parseAttribute("tiling",			parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(Real)10.0);
	mSkyPlaneData.mDrawFirst = 
		parseAttribute("drawFirst",			parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	true);
	mSkyPlaneData.mBow = 
		parseAttribute("bow",				parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(Real)0.0);
	mSkyPlaneData.mXSegments = 
		parseAttribute("xsegments",			parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(int)1);
	mSkyPlaneData.mYSegments = 
		parseAttribute("ysegments",			parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT,	(int)1);

	mSkyPlaneData.mPlane.normal.x =
		parseAttribute("planeX", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)0.0);
	mSkyPlaneData.mPlane.normal.y =
		parseAttribute("planeY", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)-1.0);
	mSkyPlaneData.mPlane.normal.z =
		parseAttribute("planeZ", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)0.0);
	mSkyPlaneData.mPlane.d =
		parseAttribute("planeD", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)5000.0);

	// Currently only set sky plane if it is enabled since the current version of Ogre can only handle one sky dome instance per
	// scene.
	if ( mSkyPlaneData.mEnabled )
	{
		sceneManager->setSkyPlane(
			mSkyPlaneData.mEnabled,	mSkyPlaneData.mPlane, mSkyPlaneData.mMaterialName, mSkyPlaneData.mScale, mSkyPlaneData.mTiling,
			mSkyPlaneData.mDrawFirst, mSkyPlaneData.mBow,
			mSkyPlaneData.mXSegments, mSkyPlaneData.mYSegments,
			!mTreeBuilder->getCommonParameter("ResourceGroup") ?
				ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME : 
				*((String*)mTreeBuilder->getCommonParameter("ResourceGroup") )
			);
	}

	// Now report parsed sky plane data to DotSceneInfo instance if there is any.
	if (mDotSceneInfo)	mDotSceneInfo->reportLoadedSkyPlane(mSkyPlaneData);

	// This is to make SkyPlaneElementProcessor independent of EnvironmentElementProcessor.
	return sceneManager->getSkyPlaneNode();
}

//***********************************************************************************************************************************
//************************************************ End of SkyPlaneElementProcessor **************************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************** ColourAmbientElementProcessor **************************************************
//***********************************************************************************************************************************

void*	ColourAmbientElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	ColourElementProcessor::parseElementImpl(parent, currentNode);

	((SceneManager*)mTreeBuilder->getCommonParameter("SceneManager"))->setAmbientLight(mColour);

	// Report ambient light colour to DotSceneInfo instance.
	if (mDotSceneInfo)	mDotSceneInfo->reportAmbientLightColour(mColour);


	return &mColour;
}

//***********************************************************************************************************************************
//*********************************************** End of ColourAmbientElementProcessor **********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************ ColourBackgroundElementProcessor *************************************************
//***********************************************************************************************************************************

void*	ColourBackgroundElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	ColourElementProcessor::parseElementImpl(parent, currentNode);

	RenderWindow* renderWindow = (RenderWindow*)mTreeBuilder->getCommonParameter("RenderWindow");
	if ( renderWindow )
	{
		for (int n = 0; n < renderWindow->getNumViewports(); n++)
			renderWindow->getViewport(n)->setBackgroundColour(mColour);
	}else
	{
		// TODO: RenderWindow has not been created before. Do something if you wish.
		// Temporary throw an Ogre::Exception.
		OGRE_EXCEPT(Ogre::Exception::ERR_INVALIDPARAMS, 
					"Common parameter \"RenderWindow\" in null. Currently this is unsupported.",
					"ColourBackgroundElementProcessor::parseElementImpl()");
	}

	// Report ambient light colour to DotSceneInfo instance.
	if (mDotSceneInfo)	mDotSceneInfo->reportBackgroundColour(mColour);


	return &mColour;
}

//***********************************************************************************************************************************
//********************************************* End of ColourBackgroundElementProcessor *********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//**************************************************** BaseNodeElementProcessor *****************************************************
//***********************************************************************************************************************************

void	BaseNodeElementProcessor::setVisibility(SceneNode* sceneNode, XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	bool visible = 
		parseAttribute("visible", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)true);
	bool cascadeVisibility = 
		parseAttribute("cascadeVisibility", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)true);
	sceneNode->setVisible(visible, cascadeVisibility);
}

//***********************************************************************************************************************************
//************************************************* End of BaseNodeElementProcessor *************************************************
//***********************************************************************************************************************************



//***********************************************************************************************************************************
//****************************************************** NodesElementProcessor ******************************************************
//***********************************************************************************************************************************

void* NodesElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// Element "node" does not have any attributes to parse.

	// Just create root scene node if it hasn't been supplied by the application.
	SceneNode*	rootSceneNode = (SceneNode*)mTreeBuilder->getCommonParameter("RootSceneNode");
	if (!rootSceneNode)
	{
		rootSceneNode = ((SceneManager*)parent->getParsedNode())->getRootSceneNode();

		if (!rootSceneNode)
			OGRE_EXCEPT(
				Ogre::Exception::ERR_INTERNAL_ERROR,
				"Something went wrong while creating the root scene node.\n"
				"Scene manager name: \"" + ((SceneManager*)parent->getParsedNode())->getTypeName() + "\".",
				"NodesElementProcessor::parseElementImpl()");

		setVisibility(rootSceneNode, parent, currentNode);

		mTreeBuilder->setCommonParameter("RootSceneNode", rootSceneNode);

		// Report created root scene node to DotSceneInfo instance.
		if (mDotSceneInfo)	mDotSceneInfo->reportLoadedRootSceneNode(rootSceneNode);
	}

	return rootSceneNode;
}

//***********************************************************************************************************************************
//************************************************** End of NodesElementProcessor ***************************************************
//***********************************************************************************************************************************



//***********************************************************************************************************************************
//**************************************************** PositionElementProcessor *****************************************************
//***********************************************************************************************************************************

void* PositionElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// Parse position.
	mParsedPosition.x	= parseAttribute("x", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, 0.0f);
	mParsedPosition.y	= parseAttribute("y", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, 0.0f);
	mParsedPosition.z	= parseAttribute("z", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, 0.0f);

	// Set position on parent.

	// TODO: FIXTHIS: UGLY HACK!!! Ogre Lights and Cameras have a "special ability": although they are not Node objects and they are
	// not inherited from the Node class, their positions, orientations, scaling can be manipulated as if they were inherited from
	// the Node class. They simply reimplement the same functionality. However this causes an issue when you try to generalise
	// position/orientation/scaling setting. This was the base reason why the following lines were implemented.
	Node*					parentNode;
	Light*					parentLight;
	Camera*					parentCamera;
	Billboard*				parentBillboard;
	LookTargetData*			parentLookTargetData;
	TransformKeyFrame*		parentTransformKeyFrame;
	bool			parentDoesNotExist = false;
	String			parentXMLNodeName(currentNode->Parent()->Value());
	if		( parentXMLNodeName == "node" || parentXMLNodeName == "nodes" )
	{
		if ( !(parentNode = (Node*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else parentNode->setPosition(mParsedPosition);
	}else if ( parentXMLNodeName == "light" )
	{
		if ( !(parentLight = (Light*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else parentLight->setPosition(mParsedPosition);
	}else if ( parentXMLNodeName == "camera" )
	{
		if ( !(parentCamera = (Camera*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else parentCamera->setPosition(mParsedPosition);
	}else if ( parentXMLNodeName == "billboard" )
	{
		if ( !(parentBillboard = (Billboard*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else parentBillboard->setPosition(mParsedPosition);
	}else if ( parentXMLNodeName == "lookTarget" )
	{
		if ( !(parentLookTargetData = (LookTargetData*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else parentLookTargetData->mLookPosition = mParsedPosition;
	}else if ( parentXMLNodeName == "transformKeyFrame" )
	{
		if ( !(parentTransformKeyFrame = (TransformKeyFrame*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else parentTransformKeyFrame->setTranslate(mParsedPosition);
	}
	

	if ( parentDoesNotExist )
	{
		// An error occured during parent node creation.
		// TODO: An error occured during parent node creation, can't set properties for parent. Log an error message. Search for
		// parent name.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"PositionElementProcessor::parseElementImpl(): Can not set position on parent xml node.");
	}

	return &mParsedPosition;
}

//***********************************************************************************************************************************
//************************************************ End of PositionElementProcessor **************************************************
//***********************************************************************************************************************************





//***********************************************************************************************************************************
//************************************************** OrientationElementProcessor ****************************************************
//***********************************************************************************************************************************

void	OrientationElementProcessor::setOrientationOnParent(XmlNodeProcessor* parent, TiXmlElement* currentNode, const String& functionNameToReportIfError)
{
	Node*				parentNode;
	Light*				parentLight;
	Camera*				parentCamera;
	TransformKeyFrame*	parentTransformKeyFrame;
	bool				parentDoesNotExist = false;
	String				parentParentsXMLNodeName(currentNode->Parent()->Parent()->Value());
	if ( parentParentsXMLNodeName == "light" )
	{
		if ( !(parentLight = (Light*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else
		{// Error orientation can not be set on lights.
			if (mDotSceneInfo) mDotSceneInfo->logLoadError(
				"Element \"light\" contains sub-element \"orientation\". This is not according to the dotScene format standard,\n"
				"since element \"light\" can only contain element \"direction\"."
				"See the dotsceneformat.dtd for more information.\n");
			// TODO: uncomment this if Ogre starts to support arbitrary light shapes. (E.g. used with cubic-attenution maps.)
			//parentLight->setOrientation(mParsedQuaternion);
		}
	}else if (	parentParentsXMLNodeName == "node" || parentParentsXMLNodeName == "nodes" ||
				parentParentsXMLNodeName == "skyDome" || parentParentsXMLNodeName == "skyBox" || parentParentsXMLNodeName == "skyPlane" )
	{
		if ( !(parentNode = (Node*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else parentNode->setOrientation(mParsedQuaternion);
	}else if ( parentParentsXMLNodeName == "camera" )
	{
		if ( !(parentCamera = (Camera*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else parentCamera->setOrientation(mParsedQuaternion);
	}else if ( parentParentsXMLNodeName == "transformKeyFrame" )
	{
		if ( !(parentTransformKeyFrame = (TransformKeyFrame*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else parentTransformKeyFrame->setRotation(mParsedQuaternion);
	}

	if ( parentDoesNotExist )
	{
		// An error occured during parent node creation.
		// TODO: An error occured during parent node creation, can't set properties for parent. Log an error message. Search for
		// parent name.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			functionNameToReportIfError + ": Can not set orientation on parent object, parent object does not exist.");
	}
}

void* OrientationElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	return parent->getParsedNode();
}

//***********************************************************************************************************************************
//********************************************** End of OrientationElementProcessor *************************************************
//***********************************************************************************************************************************




//***********************************************************************************************************************************
//*************************************************** QuaternionElementProcessor ****************************************************
//***********************************************************************************************************************************

void* QuaternionElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// Parse quaternion.
	mParsedQuaternion.x	= parseAttribute("x", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, 0.0f);
	mParsedQuaternion.y	= parseAttribute("y", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, 0.0f);
	mParsedQuaternion.z	= parseAttribute("z", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, 0.0f);
	mParsedQuaternion.w	= parseAttribute("w", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, 0.0f);

	// Set orientation on parent.
	setOrientationOnParent(parent, currentNode, "QuaternionElementProcessor::parseElementImpl()");


	return &mParsedQuaternion;
}

//***********************************************************************************************************************************
//********************************************** End of OrientationElementProcessor *************************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//***************************************************** AxisXYZElementProcessor *****************************************************
//***********************************************************************************************************************************

void* AxisXYZElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// Parse all of the 3 axes.
	Vector3	xAxis, yAxis, zAxis;
	xAxis.x		= parseAttribute("xAxisX", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, 0.0f);
	xAxis.y		= parseAttribute("xAxisY", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, 0.0f);
	xAxis.z		= parseAttribute("xAxisZ", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, 0.0f);
								  
	yAxis.x		= parseAttribute("yAxisX", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, 0.0f);
	yAxis.y		= parseAttribute("yAxisY", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, 0.0f);
	yAxis.z		= parseAttribute("yAxisZ", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, 0.0f);
								  
	zAxis.x		= parseAttribute("zAxisX", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, 0.0f);
	zAxis.y		= parseAttribute("zAxisY", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, 0.0f);
	zAxis.z		= parseAttribute("zAxisZ", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, 0.0f);

	mParsedQuaternion.FromAxes(xAxis, yAxis, zAxis);

	// Set orientation on parent.
	setOrientationOnParent(parent, currentNode, "AxisXYZElementProcessor::parseElementImpl()");

	return &mParsedQuaternion;
}

//***********************************************************************************************************************************
//************************************************ End of AxisXYZElementProcessor ***************************************************
//***********************************************************************************************************************************



//***********************************************************************************************************************************
//*************************************************** Angle_AxisElementProcessor ****************************************************
//***********************************************************************************************************************************

void* Angle_AxisElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// Parse rotation angle.
	Real	angle		= parseAttribute("angle", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, 0.0f);

	// Parse attribute "angleUnit".
	String	strAngleUnit= parseAttribute("angleUnit", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (String)"degree");
	Math::AngleUnit eAngleUnit;
	try
	{
		eAngleUnit = StringAngleUnitToEnumAngleUnit(strAngleUnit);
	}catch (Ogre::Exception& ex)
	{
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"Attribute \"angleUnit\" of element \"" + mElementName + "\" in file \"" + mFilename + "\" has an invalid "
			"value: \"" + strAngleUnit + "\"! An exception occured while trying to convert it to an enumerated type angle value: "
			+ ex.getFullDescription());
		eAngleUnit = Math::AU_DEGREE;
	}

	// Parse rotation axis.
	Vector3	axis;
	axis.x				= parseAttribute("x", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, 0.0f);
	axis.y				= parseAttribute("y", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, 0.0f);
	axis.z				= parseAttribute("z", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, 0.0f);

	// Now create quaterion based on the angle and axis.
	if		( eAngleUnit == Math::AU_DEGREE ) mParsedQuaternion.FromAngleAxis(Degree(angle), axis);
	else if ( eAngleUnit == Math::AU_RADIAN ) mParsedQuaternion.FromAngleAxis(Radian(angle), axis);

	// Set orientation on parent.
	setOrientationOnParent(parent, currentNode, "Angle_AxisElementProcessor::parseElementImpl()");

	return &mParsedQuaternion;
}

//***********************************************************************************************************************************
//********************************************** End of Angle_AxisElementProcessor **************************************************
//***********************************************************************************************************************************



//***********************************************************************************************************************************
//**************************************************** DirectionElementProcessor ****************************************************
//***********************************************************************************************************************************

void DirectionElementProcessor::setDirectionOnParent(XmlNodeProcessor* parent, TiXmlElement* currentNode, const String& functionNameToReportIfError)
{
	SceneNode*	parentNode;
	Light*		parentLight;
	Camera*		parentCamera;
	bool		parentDoesNotExist = false;

	// It is important to check against lights first.
	if ( String(currentNode->Parent()->Value()) == "light" )
	{
		if ( !(parentLight = (Light*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else
		{
			parentLight->setDirection(mDirection);

			// TODO: FIXTHIS: give some real value to parsed quaternion.
			mParsedQuaternion = Quaternion::IDENTITY;
		}
	}else if ( String(currentNode->Parent()->Parent()->Value()) == "node" )
	{
		if ( !(parentNode = (SceneNode*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else 
		{
			parentNode->setDirection(mDirection, mTransformSpace, mLocalDir);
			mParsedQuaternion = parentNode->getOrientation();
		}
	}else if ( String(currentNode->Parent()->Parent()->Value()) == "camera" )
	{
		if ( !(parentCamera = (Camera*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else
		{
			parentCamera->setDirection(mDirection);
			mParsedQuaternion = parentCamera->getOrientation();
		}
	}

	if ( parentDoesNotExist )
	{
		// An error occured during parent node creation.
		// TODO: An error occured during parent node creation, can't set properties for parent. Log an error message. Search for
		// parent name.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			functionNameToReportIfError +": Can not set orientation on parent node.");
	}
}

void* DirectionElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// Parse rotation angle and axis.
	mDirection.x		= parseAttribute("x", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, 0.0f);
	mDirection.y		= parseAttribute("y", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, 0.0f);
	mDirection.z		= parseAttribute("z", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, 0.0f);

	String relativeTo;
	relativeTo		= parseAttribute("relativeTo",	parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, String("local"));
	mLocalDir.x		= parseAttribute("localDirX",	parent, currentNode, XmlNodeProcessor::XMLADD_IMPLIED,	0.0f);
	mLocalDir.y		= parseAttribute("localDirY",	parent, currentNode, XmlNodeProcessor::XMLADD_IMPLIED,	0.0f);
	mLocalDir.z		= parseAttribute("localDirZ",	parent, currentNode, XmlNodeProcessor::XMLADD_IMPLIED, -1.0f);


	mTransformSpace = StringTransformSpaceToEnumTransformSpace(relativeTo, String(currentNode->Value()), mFilename, mDotSceneInfo);

	// Set direction on parent.
	setDirectionOnParent(parent, currentNode, "DirectionElementProcessor::parseElementImpl()");

	return &mParsedQuaternion;
}

//***********************************************************************************************************************************
//*********************************************** End of DirectionElementProcessor **************************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//****************************************************** ScaleElementProcessor ******************************************************
//***********************************************************************************************************************************

void*	ScaleElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	mScale.x = parseAttribute("x", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)1.0);
	mScale.y = parseAttribute("y", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)1.0);
	mScale.z = parseAttribute("z", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)1.0);

	// Set scaling on parent.
	Node*				parentNode;
	TransformKeyFrame*	parentTransformKeyFrame;
	bool				parentDoesNotExist = false;
	String				parentXMLNodeName(currentNode->Parent()->Value());
	if			( parentXMLNodeName == "node" || parentXMLNodeName == "nodes" )
	{
		if ( !(parentNode = (Node*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else parentNode->setScale(mScale);
	}else if	( parentXMLNodeName == "transformKeyFrame" )
	{
		if ( !(parentTransformKeyFrame = (TransformKeyFrame*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else parentTransformKeyFrame->setScale(mScale);
	}

	if ( parentDoesNotExist )
	{
		// An error occured during parent node creation.
		// TODO: An error occured during parent node creation, can't set properties for parent. Log an error message. Search for
		// parent name.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"ScaleElementProcessor::parseElementImpl(): Can not set scale on parent object, parent object does not exist.");
	}

	return &mScale;
}

//***********************************************************************************************************************************
//************************************************** End of ScaleElementProcessor **************************************************
//***********************************************************************************************************************************


String CommonParName_TrackTargetDataOfSceneNodes= "TrackTargetDataOfSceneNodes";
String CommonParName_TrackTargetDataOfCameras	= "TrackTargetDataOfCameras";


//***********************************************************************************************************************************
//*************************************************** TrackTargetElementProcessor ***************************************************
//***********************************************************************************************************************************

void*	TrackTargetElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	mTrackTargetData.mTargetSceneNodeName =
		parseAttribute("nodeName", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (String)BLANKSTRING);

	return &mTrackTargetData;
}

void	TrackTargetElementProcessor::parseElementEndImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	TrackTargetDataOfSceneNodes*	trackTargetDataOfSceneNodes = 0;
	TrackTargetDataOfCameras*		trackTargetDataOfCameras = 0;

	SceneNode*	parentSceneNode;
	Camera*		parentCamera;
	bool		parentDoesNotExist = false;
	String		parentXMLNodeName(currentNode->Parent()->Value());
	if ( parentXMLNodeName == "camera" )
	{
		if ( !(parentCamera = (Camera*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else
		{
			// If parent is a camera then search for common parameter CommonParName_TrackTargetDataOfCameras and create it if it
			// doesn't exist.
			if ( (trackTargetDataOfCameras = 
					(TrackTargetDataOfCameras*)mTreeBuilder->getCommonParameter(CommonParName_TrackTargetDataOfCameras)) == 0 )
			{
				trackTargetDataOfCameras = new TrackTargetDataOfCameras;
				mTreeBuilder->setCommonParameter(CommonParName_TrackTargetDataOfCameras, trackTargetDataOfCameras);
			}
			// Now insert new track target data to the map.
			trackTargetDataOfCameras->insert(TrackTargetDataOfCameras::value_type(parentCamera, mTrackTargetData));
		}
	}if ( parentXMLNodeName == "node" )
	{
		if ( !(parentSceneNode = (SceneNode*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else
		{
			// If parent is a camera then search for common parameter CommonParName_TrackTargetDataOfSceneNodes and create it if it
			// doesn't exist.
			if ( (trackTargetDataOfSceneNodes = 
					(TrackTargetDataOfSceneNodes*)mTreeBuilder->getCommonParameter(CommonParName_TrackTargetDataOfSceneNodes)) == 0 )
			{
				trackTargetDataOfSceneNodes = new TrackTargetDataOfSceneNodes;
				mTreeBuilder->setCommonParameter(CommonParName_TrackTargetDataOfSceneNodes, trackTargetDataOfSceneNodes);
			}
			// Now insert new track target data to the map.
			trackTargetDataOfSceneNodes->insert(TrackTargetDataOfSceneNodes::value_type(parentSceneNode, mTrackTargetData));
		}
	}

	if ( parentDoesNotExist )
	{
		// An error occured during parent node creation.
		// TODO: An error occured during parent node creation, can't set properties for parent. Log an error message. Search for
		// parent name.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"TrackTargetElementProcessor::parseElementEndImpl(): Can not query parent node, auto tracking won't be set.");
	}
}

//***********************************************************************************************************************************
//************************************************ End of TrackTargetElementProcessor ***********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//*********************************************** LocalDirectionVectorElementProcessor **********************************************
//***********************************************************************************************************************************

void* LocalDirectionVectorElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	mLocalDirectionVector.x = parseAttribute("x", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)0.0);
	mLocalDirectionVector.y = parseAttribute("y", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)0.0);
	mLocalDirectionVector.z = parseAttribute("z", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)-1.0);

	// TODO: FIXTHIS: HUGE HACK!!! Separate versions of LocalDirectionVectorElementProcessor should be implemented for xml element
	// "trackTarget" and xml element "lookTarget", to keep dependencies between element processors as low as possible, and to make
	// element processing compatible with the element processing theory described in the documentation of class
	// XmlNodeProcessorTreeBuilder.
	String				parentXMLNodeName(currentNode->Parent()->Value());
	bool				parentDoesNotExist = false;
	TrackTargetData*	parentTrackTargetData = 0;
	LookTargetData*		parentLookTargetData = 0;
	if		( parentXMLNodeName == "trackTarget" )
	{
		if ( !(parentTrackTargetData = (TrackTargetData*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else parentTrackTargetData->mLocalDirectionVector = mLocalDirectionVector;
	}else if ( parentXMLNodeName == "lookTarget" )
	{
		if ( !(parentLookTargetData = (LookTargetData*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else parentLookTargetData->mLocalDirectionVector = mLocalDirectionVector;
	}

	if ( parentDoesNotExist )
	{
		// An error occured during parent node creation.
		// TODO: An error occured during parent node creation, can't set properties for parent. Log an error message. Search for
		// parent name.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"LocalDirectionVectorElementProcessor::parseElementImpl(): Can not set local direction vector on parent xml node's track target data, parent xml node's track target data does not exist.");
	}

	return &mLocalDirectionVector;
}

//***********************************************************************************************************************************
//******************************************* End of LocalDirectionVectorElementProcessor *******************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//***************************************************** OffsetElementProcessor *****************************************************
//***********************************************************************************************************************************

void* OffsetElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	mOffset.x = parseAttribute("x", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)0.0);
	mOffset.y = parseAttribute("y", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)0.0);
	mOffset.z = parseAttribute("z", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)0.0);

	TrackTargetData* parentTrackTargetData;
	if ( parentTrackTargetData = (TrackTargetData*)parent->getParsedNode() ) 
		parentTrackTargetData->mOffset = mOffset;
	else
	{
		// An error occured during parent node creation.
		// TODO: An error occured during parent node creation, can't set properties for parent. Log an error message. Search for
		// parent name.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"OffsetElementProcessor::parseElementImpl(): Can not set offset vector on parent xml node's track target data, parent xml node's track target data does not exist.");
	}

	return &mOffset;
}


//***********************************************************************************************************************************
//************************************************** End of OffsetElementProcessor **************************************************
//***********************************************************************************************************************************


String CommonParName_LookTargetDataOfSceneNodes	= "LookTargetDataOfSceneNodes";
String CommonParName_LookTargetDataOfCameras	= "LookTargetDataOfCameras";


//***********************************************************************************************************************************
//**************************************************** LookTargetElementProcessor ***************************************************
//***********************************************************************************************************************************

void*	LookTargetElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// Parse attribute "nodeName".
	mLookTargetData.mLookTargetSceneNodeName =
		parseAttribute("nodeName", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (String)BLANKSTRING);

	// Parse attribute "relativeTo".
	String relativeTo = 
		parseAttribute("relativeTo", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (String)"world");
	mLookTargetData.mRelativeTo = 
		StringTransformSpaceToEnumTransformSpace(relativeTo, String(currentNode->Value()), mFilename, mDotSceneInfo);

	return &mLookTargetData;
}

void	LookTargetElementProcessor::parseElementEndImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	LookTargetDataOfSceneNodes*	lookTargetDataOfSceneNodes = 0;
	LookTargetDataOfCameras*	lookTargetDataOfCameras	= 0;

	SceneNode*	parentSceneNode;
	Camera*		parentCamera;
	bool		parentDoesNotExist = false;
	String		parentXMLNodeName(currentNode->Parent()->Value());
	if ( parentXMLNodeName == "camera" )
	{
		if ( !(parentCamera = (Camera*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else
		{
			// If parent is a camera then search for common parameter CommonParName_LookTargetDataOfSceneNodes and create it if it
			// doesn't exist.
			if ( (lookTargetDataOfCameras = 
					(LookTargetDataOfCameras*)mTreeBuilder->getCommonParameter(CommonParName_LookTargetDataOfCameras)) == 0 )
			{
				lookTargetDataOfCameras = new LookTargetDataOfCameras;
				mTreeBuilder->setCommonParameter(CommonParName_LookTargetDataOfCameras, lookTargetDataOfCameras);
			}
			// Now insert new track target data to the map.
			lookTargetDataOfCameras->insert(LookTargetDataOfCameras::value_type(parentCamera, mLookTargetData));
		}
	}if ( parentXMLNodeName == "node" )
	{
		if ( !(parentSceneNode = (SceneNode*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else
		{
			// If parent is a camera then search for common parameter CommonParName_LookTargetDataOfCameras and create it if it
			// doesn't exist.
			if ( (lookTargetDataOfSceneNodes = 
					(LookTargetDataOfSceneNodes*)mTreeBuilder->getCommonParameter(CommonParName_LookTargetDataOfSceneNodes)) == 0 )
			{
				lookTargetDataOfSceneNodes = new LookTargetDataOfSceneNodes;
				mTreeBuilder->setCommonParameter(CommonParName_LookTargetDataOfSceneNodes, lookTargetDataOfSceneNodes);
			}
			// Now insert new track target data to the map.
			lookTargetDataOfSceneNodes->insert(LookTargetDataOfSceneNodes::value_type(parentSceneNode, mLookTargetData));
		}
	}

	if ( parentDoesNotExist )
	{
		// An error occured during parent node creation.
		// TODO: An error occured during parent node creation, can't set properties for parent. Log an error message. Search for
		// parent name.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"LookTargetElementProcessor::parseElementEndImpl(): Can not query parent node, auto tracking won't be set.");
	}
}



//***********************************************************************************************************************************
//****************************************************** NodeElementProcessor *******************************************************
//***********************************************************************************************************************************

void*	NodeElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	String nodeName = getNodeName_AutoNamedAndPrefixedIfNeeded(currentNode);

	bool isTarget = parseAttribute("isTarget", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)false);

	// TODO: do something with attribute "isTarget". Perhaps notify DotSceneInfo.


	parseAttribute_id(parent, currentNode);


	// Set scaling on parent.
	SceneNode* parentSceneNode = 0;
	SceneNode* currentSceneNode = 0;
	if ( parentSceneNode = ((SceneNode*)parent->getParsedNode()) ) currentSceneNode = parentSceneNode->createChildSceneNode(nodeName);
	else
	{
		// An error occured during parent node creation.
		// TODO: An error occured during parent node creation, can't set properties for parent. Log an error message. Search for
		// parent name.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"NodeElementProcessor::parseElementImpl(): Can not create child scene node using parent node, parent node does not exist.");
	}

	if ( currentSceneNode )
	{
		setVisibility(currentSceneNode, parent, currentNode);

		// Report created scene node to DotSceneInfo instance.
		if (mDotSceneInfo)	mDotSceneInfo->reportLoadedSceneNode(currentSceneNode);
	}

	return currentSceneNode;
}

//***********************************************************************************************************************************
//************************************************** End of NodeElementProcessor ***************************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//********************************************* CommonMovableObjectParamsElementProcessor *******************************************
//***********************************************************************************************************************************

void	CommonMovableObjectParamsElementProcessor::getDefaultQueryAndVisibilityFlagsForMovable(MovableObject* movableObject, uint32& defaultQueryFlags, uint32& defaultVisibilityFlags)
{
		// TODO: update this if more movable object are added to Ogre.
		if		( movableObject->getMovableType() == BillboardChainFactory::FACTORY_TYPE_NAME )
		{
			defaultQueryFlags		=	BillboardChain::getDefaultQueryFlags();
			defaultVisibilityFlags	=	BillboardChain::getDefaultVisibilityFlags();
		}
		else if	( movableObject->getMovableType() == BillboardSetFactory::FACTORY_TYPE_NAME )
		{
			defaultQueryFlags		=	BillboardSet::getDefaultQueryFlags();
			defaultVisibilityFlags	=	BillboardSet::getDefaultVisibilityFlags();
		}
		else if	( movableObject->getMovableType() == EntityFactory::FACTORY_TYPE_NAME )
		{
			defaultQueryFlags		=	Entity::getDefaultQueryFlags();
			defaultVisibilityFlags	=	Entity::getDefaultVisibilityFlags();
		}
		// TODO: replace the following line to the one currently commented out, when the new Ogre 1.6.x arrives.
		else if	( movableObject->getMovableType() == "Camera" )
//		else if	( movableObject->getMovableType() == CameraFactory::FACTORY_TYPE_NAME )
		{
			defaultQueryFlags		=	Camera::getDefaultQueryFlags();
			defaultVisibilityFlags	=	Camera::getDefaultVisibilityFlags();
		}
		// TODO: replace the following line to the one currently commented out, when Ogre will consist
		// InstancedGeometry::BatchInstanceFactory.
		else if	( movableObject->getMovableType() == "InstancedGeometry" )
//		else if	( movableObject->getMovableType() == InstancedGeometry::BatchInstanceFactory::FACTORY_TYPE_NAME )
		{
			defaultQueryFlags		=	InstancedGeometry::BatchInstance::getDefaultQueryFlags();
			defaultVisibilityFlags	=	InstancedGeometry::BatchInstance::getDefaultVisibilityFlags();
		}
		else if	( movableObject->getMovableType() == LightFactory::FACTORY_TYPE_NAME )
		{
			defaultQueryFlags		=	Light::getDefaultQueryFlags();
			defaultVisibilityFlags	=	Light::getDefaultVisibilityFlags();
		}
		else if	( movableObject->getMovableType() == ManualObjectFactory::FACTORY_TYPE_NAME )
		{
			defaultQueryFlags		=	ManualObject::getDefaultQueryFlags();
			defaultVisibilityFlags	=	ManualObject::getDefaultVisibilityFlags();
		}
		// TODO: replace the following line to the one currently commented out, when Ogre will consist MovablePlaneFactory.
		else if	( movableObject->getMovableType() == "MovablePlane" )
//		else if	( movableObject->getMovableType() == MovablePlaneFactory::FACTORY_TYPE_NAME )
		{
			defaultQueryFlags		=	MovablePlane::getDefaultQueryFlags();
			defaultVisibilityFlags	=	MovablePlane::getDefaultVisibilityFlags();
		}
		else if	( movableObject->getMovableType() == ParticleSystemFactory::FACTORY_TYPE_NAME)
		{
			defaultQueryFlags		=	ParticleSystem::getDefaultQueryFlags();
			defaultVisibilityFlags	=	ParticleSystem::getDefaultVisibilityFlags();
		}
		// TODO: replace the following line to the one currently commented out, when Ogre will consist SimpleRenderableFactory.
		else if	( movableObject->getMovableType() == "SimpleRenderable" )
//		else if	( movableObject->getMovableType() == SimpleRenderableFactory::FACTORY_TYPE_NAME )
		{
			defaultQueryFlags		=	SimpleRenderable::getDefaultQueryFlags();
			defaultVisibilityFlags	=	SimpleRenderable::getDefaultVisibilityFlags();
		}
		// TODO: replace the following line to the one currently commented out, when Ogre will consist
		// StaticGeometry::RegionFactory.
		else if	( movableObject->getMovableType() == "StaticGeometry" )
//		else if	( movableObject->getMovableType() == StaticGeometry::RegionFactory::FACTORY_TYPE_NAME )
		{
			defaultQueryFlags		=	StaticGeometry::Region::getDefaultQueryFlags();
			defaultVisibilityFlags	=	StaticGeometry::Region::getDefaultVisibilityFlags();
		}
		else
		{
			defaultQueryFlags		=	MovableObject::getDefaultQueryFlags();
			defaultVisibilityFlags	=	MovableObject::getDefaultVisibilityFlags();
		}
}

void*	CommonMovableObjectParamsElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	MovableObject* movableObject;

	movableObject = (MovableObject*)parent->getParsedNode() ? (MovableObject*)parent->getParsedNode() : 0;

	if (movableObject)
	{
		// Parse attribute "visible".
		bool visible =
				parseAttribute("visible", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, true);
		movableObject->setVisible(visible);

		// Parse attribute "castShadows".
		bool castShadows;
		castShadows = parseAttribute("castShadows", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, true);
		movableObject->setCastShadows(castShadows);

		// Parse attribute "renderingDistance".
		Real renderingDistance = 
			parseAttribute("renderingDistance", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)0.0);
		// Now set the distance at which the object is no longer rendered.
		movableObject->setRenderingDistance(renderingDistance);

		// Now select default query and visibility flags based on movable type.
		uint32 defaultQueryFlags, defaultVisibilityFlags;
		getDefaultQueryAndVisibilityFlagsForMovable(movableObject, defaultQueryFlags, defaultVisibilityFlags);

		// Parse attribute "queryFlags".
		uint32 queryFlags = 
			parseAttribute("queryFlags", parent, currentNode, XmlNodeProcessor::XMLADD_IMPLIED, defaultQueryFlags);
		movableObject->setQueryFlags(queryFlags);

		// Parse attribute "visibilityFlags".
		uint32 visibilityFlags = 
			parseAttribute("visibilityFlags", parent, currentNode, XmlNodeProcessor::XMLADD_IMPLIED, defaultVisibilityFlags);
		movableObject->setVisibilityFlags(visibilityFlags);

		// Parse attribute "static".
		bool isStatic;
		isStatic = parseAttribute("static", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, false);

		// Notify parent XmlNodeProcessor about its processed xml element begin static or not. See comments before
		// MovableObjectElementProcessor for details.
		((MovableObjectElementProcessor*)parent)->setStatic(isStatic);
	}
	return movableObject;
}

//***********************************************************************************************************************************
//***************************************** End of CommonMovableObjectParamsElementProcessor ****************************************
//***********************************************************************************************************************************




//***********************************************************************************************************************************
//****************************************************** EntityElementProcessor *****************************************************
//***********************************************************************************************************************************

HardwareBuffer::Usage EntityElementProcessor::parseAttribute_Usage(const String& attributeName, XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	String strBufferUsage = 
		parseAttribute(attributeName, parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, String("staticWriteOnly"));
	HardwareBuffer::Usage	bufferUsage;

	if		( strBufferUsage == "static" )						bufferUsage = HardwareBuffer::HBU_STATIC;
	else if ( strBufferUsage == "dynamic" )						bufferUsage = HardwareBuffer::HBU_DYNAMIC;
	else if ( strBufferUsage == "writeOnly"	)					bufferUsage = HardwareBuffer::HBU_WRITE_ONLY;
	else if ( strBufferUsage == "discardable"	)				bufferUsage = HardwareBuffer::HBU_DISCARDABLE;
	else if ( strBufferUsage == "staticWriteOnly")				bufferUsage = HardwareBuffer::HBU_STATIC_WRITE_ONLY;
	else if ( strBufferUsage == "dynamicWriteOnly")				bufferUsage = HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY;
	else if ( strBufferUsage == "dynamicWriteOnlyDiscardable")	bufferUsage = HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY_DISCARDABLE;
	else
	{// Invalid value for attribute.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"Attribute \"" + attributeName + "\" of element \"" + mElementName + "\" in file \"" + mFilename + "\" has an invalid value: \"" + strBufferUsage + "\"!\n"
			"Valid values are \"static\", \"dynamic\", \"writeOnly\", \"discardable\", \"staticWriteOnly\", \"dynamicWriteOnly\", \"dynamicWriteOnlyDiscardable\".\n"
			"\""+ attributeName + "\" attribute will now default to \"staticWriteOnly\".\n"
			"See the dotsceneformat.dtd for more information.\n");
		bufferUsage = HardwareBuffer::HBU_STATIC_WRITE_ONLY;
	}
	return bufferUsage;
}

void* EntityElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	Entity* entity = 0;
	bool entityExists = false;

	// Parse entity name.
	String entityName = getNodeName_AutoNamedAndPrefixedIfNeeded(currentNode);

	// Parse meshfile name.
	String meshName = parseAttribute("meshFile", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, BLANKSTRING);
	if ( meshName == BLANKSTRING )
	{
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"Attribute \"meshName\" of element \"" + String(currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value: \"" + meshName + "\"!\n"
			"Mesh names must be valid file names!\n"
			"Loading will continue, however the entity based on this mesh will not be created.\n"
			"See the dotsceneformat.dtd for more information.\n");
		return 0;
	}

	// Parse vertex buffer usage.
	HardwareBuffer::Usage	vertexBufferUsage, indexBufferUsage;
	vertexBufferUsage	= parseAttribute_Usage("vertexBufferUsage", parent, currentNode);
	indexBufferUsage	= parseAttribute_Usage("indexBufferUsage",	parent, currentNode);

	// Parse data about whether or not to use shadow buffer.
	bool vertexBufferUseShadow = 
		mTreeBuilder->getCommonParameter("VertexBufferUseShadow") ?
		// If common parameter "VertexBufferUseShadow" is present, use that instead. Shadow vertex buffer usage specified in
		// the .scene file will be ignored.
		*((bool*)mTreeBuilder->getCommonParameter("VertexBufferUseShadow")) :
	// If no "VertexBufferUseShadow" common parameter then get it from the .scene file.
	parseAttribute("vertexBufferUseShadow", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, false);

	bool indexBufferUseShadow = 
		mTreeBuilder->getCommonParameter("IndexBufferUseShadow") ?
		// If common parameter "IndexBufferUseShadow" is present, use that instead. Shadow vertex buffer usage specified in
		// the .scene file will be ignored.
		*((bool*)mTreeBuilder->getCommonParameter("IndexBufferUseShadow")) :
	// If no "IndexBufferUseShadow" common parameter then get it from the .scene file.
	parseAttribute("indexBufferUseShadow", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, false);

	try
	{
		MeshManager::getSingleton().load(
			meshName,															// Mesh file name.
			!mTreeBuilder->getCommonParameter("ResourceGroup") ?				// Resource group name name.
			ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME : 
		*((String*)mTreeBuilder->getCommonParameter("ResourceGroup")),
			vertexBufferUsage, 													// Vertex buffer usage.
			indexBufferUsage,													// Index buffer usage.
			vertexBufferUseShadow,												// Use shadow buffer for vertex buffer?
			indexBufferUseShadow												// Use shadow buffer for index buffer?
			);
	}
	catch (Ogre::Exception& ex)
	{
		if (mDotSceneInfo) mDotSceneInfo->logLoadError("Unable to load mesh file: \"" + meshName + "\"! Exception occured: " + ex.getFullDescription());
	}


	try
	{
		entity = ((SceneManager*)mTreeBuilder->getCommonParameter("SceneManager"))->getEntity(entityName);
		entityExists = true;
	}
	catch (Ogre::Exception& ex)
	{
		if (mDotSceneInfo) mDotSceneInfo->logLoadError("Unable to get entity \"" + entityName + ". Exception occured: " + ex.getFullDescription());
	}

	if ( ! entityExists )
	{
		try
		{
			entity = ((SceneManager*)mTreeBuilder->getCommonParameter("SceneManager"))->createEntity(entityName, meshName);
		}
		catch (Ogre::Exception& ex)
		{
			if (mDotSceneInfo) mDotSceneInfo->logLoadError("Unable to create entity \"" + entityName + "\" based on mesh file: \"" + meshName + "\"!  Exception occured: " + ex.getFullDescription());
			entity = 0;
		}
	}

	if ( entity )
	{
		// If attribute "materialName" is present then change the default material of the entity originally coming from the mesh.
		String materialName;
		if ( dsi::xml::getAttribute(currentNode, "materialName", materialName) ) entity->setMaterialName(materialName);

		// Parse attribute "displaySkeleton".
		bool displaySkeleton =
			parseAttribute("displaySkeleton", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)false);
		entity->setDisplaySkeleton(displaySkeleton);		

		// Parse attribute "polygonModeOverrideable".
		bool polygonModeOverrideable =
			parseAttribute("polygonModeOverrideable", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)true);
		entity->setPolygonModeOverrideable(polygonModeOverrideable);

		// Parse attribute "softwareAnimationRequests" and "softwareAnimationRequestsNormalsAlso".
		int softwareAnimationRequests =
			parseAttribute("softwareAnimationRequests", parent, currentNode, XmlNodeProcessor::XMLADD_IMPLIED, (int)0);
		int softwareAnimationRequestsNormalsAlso =
			parseAttribute("softwareAnimationRequestsNormalsAlso", parent, currentNode, XmlNodeProcessor::XMLADD_IMPLIED, (int)0);

		// Ogre 1.4.x does not support animating only normals in software, so if "softwareAnimationRequests" is 0, then ignore
		// "softwareAnimationRequestsNormalsAlso".
		if ( softwareAnimationRequestsNormalsAlso > softwareAnimationRequests )
		{
			if (mDotSceneInfo) mDotSceneInfo->logLoadError(
				"Attribute \"softwareAnimationRequestsNormalsAlso\" has a greater value " 
				"(" + StringConverter::toString(softwareAnimationRequestsNormalsAlso) + ") then attribute \"softwareAnimationRequests\" "
				"(" + StringConverter::toString(softwareAnimationRequests) + "), which is unsupported by Ogre. "
				"Entity::addSoftwareAnimationRequest() will be called only " +
				StringConverter::toString(softwareAnimationRequests) + " times.");

			softwareAnimationRequestsNormalsAlso = softwareAnimationRequests;
		}
		if ( softwareAnimationRequests > 0 )
		{
			// First add software animation requests with normal animation requests too.
			for ( int i = 0; i < softwareAnimationRequestsNormalsAlso; i++ )
			{
				entity->addSoftwareAnimationRequest(true);
			}

			// Then add software animation requests for vertex animation only.
			for ( int i = softwareAnimationRequestsNormalsAlso; i < softwareAnimationRequests; i++ )
			{
				entity->addSoftwareAnimationRequest(false);
			}
		}

		// Attach entity to parent scene node.
		if ( ! entityExists )
			((SceneNode*)parent->getParsedNode())->attachObject((MovableObject*)entity);
	}

	return entity;
}

void EntityElementProcessor::parseElementEndImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	// Now report entity to DotSceneInfo instance (if parsing was successfull).
	if ( getParsedNode() )
	{
		if (mDotSceneInfo)	mDotSceneInfo->reportLoadedEntity((Entity*)getParsedNode(), mStatic);
	}
}

//***********************************************************************************************************************************
//************************************************** End of EntityElementProcessor **************************************************
//***********************************************************************************************************************************

//***********************************************************************************************************************************
//***************************************************** LODBiasElementProcessor *****************************************************
//***********************************************************************************************************************************

void* LODBiasElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	m_LODBiasData.m_rFactor	= 
		parseAttribute("factor", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)1);
	m_LODBiasData.m_usMaxDetailIndex =
		parseAttribute("maxDetailIndex", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (unsigned int)0);
	m_LODBiasData.m_usMinDetailIndex =
		parseAttribute("minDetailIndex", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (unsigned int)99);

	// Check for data validation.
	if ( m_LODBiasData.m_usMinDetailIndex < m_LODBiasData.m_usMaxDetailIndex )
	{
		m_LODBiasData.m_usMaxDetailIndex = m_LODBiasData.m_usMinDetailIndex;
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"Attribute \"minDetailIndex\" is lesser than \"maxDetailIndex\". They will be treated as if they were equal. "
			"Check the .scene file.");
	}


	return &m_LODBiasData;
}

//***********************************************************************************************************************************
//************************************************** End of LODBiasElementProcessor *************************************************
//***********************************************************************************************************************************

//***********************************************************************************************************************************
//*************************************************** MeshLODBiasElementProcessor ***************************************************
//***********************************************************************************************************************************

void* MeshLODBiasElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	// First parse data.
	LODBiasElementProcessor::parseElementImpl(parent, currentNode);

	// Then set it as a mesh lod bias.
	Entity* parentEntity;
	if ( (parentEntity = (Entity*)parent->getParsedNode()) )
	{
		// Check for data validation.
		if ( m_LODBiasData.m_usMaxDetailIndex+1 > parentEntity->getMesh()->getNumLodLevels() )
		{
			m_LODBiasData.m_usMaxDetailIndex = parentEntity->getMesh()->getNumLodLevels()-1; // Indexes are zero based.
			if (mDotSceneInfo) mDotSceneInfo->logLoadError(
				"Attribute \"maxDetailIndex\" is greater than the maximum number of available lods. It will be truncated. "
				"Check the .scene file.");
		}
		parentEntity->setMeshLodBias(m_LODBiasData.m_rFactor, m_LODBiasData.m_usMaxDetailIndex, m_LODBiasData.m_usMinDetailIndex);
	}
	else
	{// Parent node hasn't been created.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"MeshLODBiasElementProcessor::parseElementImpl(): Can not set mesh LOD bias on parent entity. Parent entity does not exist.");
	}

	return &m_LODBiasData;
}

//***********************************************************************************************************************************
//************************************************ End of MeshLODBiasElementProcessor ***********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************* MaterialLODBiasElementProcessor *************************************************
//***********************************************************************************************************************************

void* MaterialLODBiasElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	// First parse data.
	LODBiasElementProcessor::parseElementImpl(parent, currentNode);

	// Then set it as a material lod bias.
	Entity* parentEntity;
	if ( (parentEntity = (Entity*)parent->getParsedNode()) )
	{
		// Check for data validation.
		//if ( m_LODBiasData.m_usMaxDetailIndex > parentEntity->getSubEntity(0)->getMaterial() )
		//{
		//	m_LODBiasData.m_usMaxDetailIndex = parentEntity->getMesh()->getNumLodLevels();
		//	if (mDotSceneInfo) mDotSceneInfo->logLoadError(
		//		"Attribute \"maxDetailIndex\" is greater than the maximum number of available lods. It will be truncated. "
		//		"Check the .scene file.");
		//}

		parentEntity->setMaterialLodBias(m_LODBiasData.m_rFactor, m_LODBiasData.m_usMaxDetailIndex, m_LODBiasData.m_usMinDetailIndex);
	}
	else
	{// Parent node hasn't been created.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"MaterialLODBiasElementProcessor::parseElementImpl(): Can not set material LOD bias on parent entity. Parent entity does not exist.");
	}

	return &m_LODBiasData;
}

//***********************************************************************************************************************************
//********************************************** End of MaterialLODBiasElementProcessor *********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//****************************************************** LightElementProcessor ******************************************************
//***********************************************************************************************************************************

Light::LightTypes LightElementProcessor::parseAttribute_type(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	String strType = parseAttribute("type", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, String("point"));
	Light::LightTypes lightType;
	if		( strType == "point" )
	{
		lightType = Light::LT_POINT;
	}
	else if ( strType == "directional" )
	{
		lightType = Light::LT_DIRECTIONAL;
	}
	else if ( strType == "spotLight" )
	{
		lightType = Light::LT_SPOTLIGHT;
	}
	else if ( strType == "radPoint" )
	{
		lightType = Light::LT_POINT;
		if (mDotSceneInfo) mDotSceneInfo->logLoadError("Currently light type \"radPoint\" is not supported! Light type will default to \"point\".");
	}
	else 
	{// Invalid value for attribute "type".
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"Attribute \"type\" of element \"" + String(currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value: \"" + strType + "\"!\n"
			"Valid values are \"point\", \"directional\", \"spotLight\", \"radPoint\" (currently this is unsupported).\n"
			"\"type\" attribute will now default to \"point\".\n"
			"See the dotsceneformat.dtd for more information.\n");
		lightType = Light::LT_POINT;
	}

	return lightType;
}

void* LightElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	String name = getNodeName_AutoNamedAndPrefixedIfNeeded(currentNode);

	SceneManager* sceneManager = ((SceneManager*)mTreeBuilder->getCommonParameter("SceneManager"));

	Light* light = sceneManager->createLight(name);

	parseAttribute_id(parent, currentNode);

	Light::LightTypes lightType = parseAttribute_type(parent, currentNode);
	light->setType(lightType);

	Real powerScale	= parseAttribute("powerScale", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)1.0);
	light->setPowerScale(powerScale);
	
	// Now attach light to parent node.
	SceneNode* parentSceneNode;
	if ( String(currentNode->Parent()->Value()) != "scene" )
	{// Try to attach only if light wasn't created at the scene level. In that case it should be attached to any nodes, since
	 // cameras contain positions/orientations for themselves.
		if ( (parentSceneNode = (SceneNode*)parent->getParsedNode()) ) parentSceneNode->attachObject(light);
		else
		{// Parent node hasn't been created.
			if (mDotSceneInfo) mDotSceneInfo->logLoadError(
				"CameraElementProcessor::parseElementImpl(): Can not attach camera to parent scene node. Parent scene node does not exist.");
		}
	}

	return light;
}

void LightElementProcessor::parseElementEndImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	// Now report light to DotSceneInfo instance (if parsing was successfull).
	if ( getParsedNode() )
	{
		if (mDotSceneInfo)	mDotSceneInfo->reportLoadedLight((Light*)getParsedNode(), mStatic);
	}
}

//***********************************************************************************************************************************
//*************************************************** End of LightElementProcessor **************************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************** ColourDiffuseElementProcessor **************************************************
//***********************************************************************************************************************************

void* ColourDiffuseElementProcessor::parseElementImpl(XmlNodeProcessing::XmlNodeProcessor *parent, TiXmlElement *currentNode)
{
	ColourElementProcessor::parseElementImpl(parent, currentNode);

	// TODO: FIXTHIS: HUGE HACK!!! Separate versions of ColourDiffuseElementProcessor should be implemented for xml element "light",
	// "billboard" and "shadowSettings", to keep dependencies between element processors as low as possible, and to make element
	// processing compatible with the element processing theory described in the documentation of class XmlNodeProcessorTreeBuilder.
	Light*		parentLight;
	Billboard*	parentBillboard;
	bool		parentDoesNotExist = false;
	String		parentXMLNodeName(currentNode->Parent()->Value());
	if ( parentXMLNodeName == "light" )
	{
		if ( !(parentLight = (Light*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else parentLight->setDiffuseColour(mColour);
	}else if ( parentXMLNodeName == "billboard" )
	{
		if ( !(parentBillboard = (Billboard*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else parentBillboard->setColour(mColour);
	}else if ( parentXMLNodeName == "shadowSettings" )
	{
		((SceneManager*)mTreeBuilder->getCommonParameter("SceneManager"))->setShadowColour(mColour);
	}

	if ( parentDoesNotExist )
	{
		// An error occured during parent node creation.
		// TODO: An error occured during parent node creation, can't set properties for parent. Log an error message. Search for
		// parent name.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"ColourDiffuseElementProcessor::parseElementImpl(): Can not set colour on parent node.");
	}

	return &mColour;
}

//***********************************************************************************************************************************
//*********************************************** End of ColourDiffuseElementProcessor **********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************* ColourSpecularElementProcessor **************************************************
//***********************************************************************************************************************************

void* ColourSpecularElementProcessor::parseElementImpl(XmlNodeProcessing::XmlNodeProcessor *parent, TiXmlElement *currentNode)
{
	ColourElementProcessor::parseElementImpl(parent, currentNode);

	Light* parentLight;
	if ( (parentLight = (Light*)parent->getParsedNode()) ) parentLight->setSpecularColour(mColour);
	else
	{
		// An error occured during parent node creation.
		// TODO: An error occured during parent node creation, can't set properties for parent. Log an error message. Search for
		// parent name.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"ColourSpecularElementProcessor::parseElementImpl(): Can not set colour on parent node, parent light does not exist.");
	}

	return &mColour;
}

//***********************************************************************************************************************************
//********************************************** End of ColourSpecularElementProcessor **********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************* SpotLightRangeElementProcessor **************************************************
//***********************************************************************************************************************************

void* SpotLightRangeElementProcessor::parseElementImpl(XmlNodeProcessing::XmlNodeProcessor *parent, TiXmlElement *currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	Real inner		= parseAttribute("inner",	parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)0);
	Real outer		= parseAttribute("outer",	parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)0);
	Real falloff	= parseAttribute("falloff", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)1);

	Light* parentLight;
	if ( (parentLight = (Light*)parent->getParsedNode()) )
	{
		try
		{
			parentLight->setSpotlightRange(Ogre::Angle(inner), Ogre::Angle(outer), falloff);
		}catch ( Ogre::Exception& ex)
		{
			if (mDotSceneInfo) mDotSceneInfo->logLoadError(
				"SpotLightRangeElementProcessor::parseElementImpl(): an Ogre::Exception occured during the execution of "
				"Light::setSpotLightRange(): " + ex.getFullDescription() );
		}
	}
	else
	{
		// An error occured during parent node creation.
		// TODO: An error occured during parent node creation, can't set properties for parent. Log an error message. Search for
		// parent name.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"SpotLightRangeElementProcessor::parseElementImpl(): Can not set spot light range properties on parent light; parent light does not exist.");
	}

	// TODO: design question: what should we return with?
//	return 0;
	return parent->getParsedNode();
}

//***********************************************************************************************************************************
//********************************************** End of SpotLightRangeElementProcessor **********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************* LightAttenuationElementProcessor ************************************************
//***********************************************************************************************************************************

void* LightAttenuationElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	Real range		= parseAttribute("range",		parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)1000);
	Real constant	= parseAttribute("constant",	parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)1);
	Real linear		= parseAttribute("linear",		parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)0);
	Real quadratic	= parseAttribute("quadratic",	parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)0);

	Light* parentLight;
	if ( (parentLight = (Light*)parent->getParsedNode()) )
		parentLight->setAttenuation(range, constant, linear, quadratic);
	else
	{
		// An error occured during parent node creation.
		// TODO: An error occured during parent node creation, can't set properties for parent. Log an error message. Search for
		// parent name.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"LightAttenuationRangeElementProcessor::parseElementImpl(): Can not set attenuation properties on parent light; parent light does not exist.");
	}

	// Design question: what should we return with?
//	return 0;
	return parent->getParsedNode();
}


//***********************************************************************************************************************************
//********************************************* End of LightAttenuationElementProcessor *********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//***************************************************** CameraElementProcessor ******************************************************
//***********************************************************************************************************************************

ProjectionType CameraElementProcessor::parseAttribute_projectionType(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	String strProjectionType = 
		parseAttribute("projectionType", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, String("perspective"));

	ProjectionType projectionType;
	if		( strProjectionType == "perspective" )
	{
		projectionType = PT_PERSPECTIVE;
	}
	else if ( strProjectionType == "orthographic" )
	{
		projectionType = PT_ORTHOGRAPHIC;
	}
	else 
	{// Invalid value for attribute "projectionType".
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"Attribute \"projectionType\" of element \"" + String(currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value: \"" + strProjectionType + "\"!\n"
			"Valid values are \"perspective\" and \"orthographic\".\n"
			"\"projectionType\" attribute will now default to \"perspective\".\n"
			"See the dotsceneformat.dtd for more information.\n");
		projectionType = PT_PERSPECTIVE;
	}

	return projectionType;
}

PolygonMode CameraElementProcessor::parseAttribute_polygonMode(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	String strPolygonMode = 
		parseAttribute("polygonMode", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, String("solid"));

	PolygonMode polygonMode;
	if ( strPolygonMode == "solid" )
	{
		polygonMode = PM_SOLID;
	}
	else if ( strPolygonMode == "points" )
	{
		polygonMode = PM_POINTS;
	}
	else if ( strPolygonMode == "wireframe" )
	{
		polygonMode = PM_WIREFRAME;
	}
	else 
	{// Invalid value for attribute "polygonMode".
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"Attribute \"polygonMode\" of element \"" + String(currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value: \"" + strPolygonMode + "\"!\n"
			"Valid values are \"points\", \"wireframe\" and \"solid\".\n"
			"\"polygonMode\" attribute will now default to \"solid\".\n"
			"See the dotsceneformat.dtd for more information.\n");
		polygonMode = PM_SOLID;
	}

	return polygonMode;
}

void* CameraElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// Parse attribute "name".
	String name = getNodeName_AutoNamedAndPrefixedIfNeeded(currentNode);

	SceneManager* sceneManager = ((SceneManager*)mTreeBuilder->getCommonParameter("SceneManager"));
	Camera* camera = sceneManager->createCamera(name);

	// Parse attribute "id".
	parseAttribute_id(parent, currentNode);

	// Parse attribute "FOVy".
	Real FOVy		 = parseAttribute("FOVy", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)45);
	camera->setFOVy(Ogre::Angle(FOVy));

	// Parse attribute "aspectRatio".
	Real aspectRatio = parseAttribute("aspectRatio", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)1.3333333);
	camera->setAspectRatio(aspectRatio);

	// Parse attribute "projectionType".
	ProjectionType projectionType =	parseAttribute_projectionType(parent, currentNode);
	camera->setProjectionType(projectionType);

	// Parse attribute "polygonMode".
	PolygonMode	polygonMode = parseAttribute_polygonMode(parent, currentNode);
	camera->setPolygonMode(polygonMode);

	// Parse attribute "useRenderingDistance".
	bool useRenderingDistance =
		parseAttribute("useRenderingDistance", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)true);
	camera->setUseRenderingDistance(useRenderingDistance);

	// Parse attribute "lodBiasFactor".
	Real lodBias = 
		parseAttribute("lodBiasFactor", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)1.0);
	camera->setLodBias(lodBias);


	// Now attach camera to parent node.
	SceneNode* parentSceneNode;
	if ( String(currentNode->Parent()->Value()) != "scene" )
	{// Try to attach only if camera wasn't created at the scene level. In that case it should be attached to any nodes, since
	 // cameras contain positions/orientations for themselves.
		if ( (parentSceneNode = (SceneNode*)parent->getParsedNode()) ) parentSceneNode->attachObject(camera);
		else
		{// Parent node hasn't been created.
			if (mDotSceneInfo) mDotSceneInfo->logLoadError(
				"CameraElementProcessor::parseElementImpl(): Can not attach camera to parent scene node. Parent scene node does not exist.");
		}
	}

	return camera;
}

void CameraElementProcessor::parseElementEndImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	// Now report camera to DotSceneInfo instance (if parsing was successfull).
	if ( getParsedNode() )
	{
		if (mDotSceneInfo)	mDotSceneInfo->reportLoadedCamera((Camera*)getParsedNode(), mStatic);
	}
}

//***********************************************************************************************************************************
//************************************************** End of CameraElementProcessor **************************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//**************************************************** ClippingElementProcessor *****************************************************
//***********************************************************************************************************************************

void* ClippingElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// Parse attributes.
	Real nearPlaneDist	 = parseAttribute("nearPlaneDist", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)100.0);
	Real farPlaneDist	 = parseAttribute("farPlaneDist",  parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)100000.0);

	// Now set near and far clipping planes on camera.
	Camera* parentCamera;
	if ( (parentCamera = (Camera*)parent->getParsedNode()) )
	{
		parentCamera->setNearClipDistance(nearPlaneDist);
		parentCamera->setFarClipDistance(farPlaneDist);
	}
	else
	{// Parent node hasn't been created.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"ClippingElementProcessor::parseElementImpl(): Can not set near and far clipping planes on parent camera. Parent camera does not exist.");
	}


	// Design question: what should we return with?
//	return 0;
	return parent->getParsedNode();
}

//***********************************************************************************************************************************
//************************************************* End of ClippingElementProcessor *************************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************* ParticleSystemElementProcessor **************************************************
//***********************************************************************************************************************************

void* ParticleSystemElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// Parse attribute "name".
	String name = getNodeName_AutoNamedAndPrefixedIfNeeded(currentNode);

	// Parse attribute "id".
	parseAttribute_id(parent, currentNode);

	// Parse attribute "templateName".
	String templateName = parseAttribute("templateName",  parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, BLANKSTRING);

	// Try to create particle system.
	ParticleSystem* particleSystem = 0;
	try
	{
		particleSystem = 
			((SceneManager*)mTreeBuilder->getCommonParameter("SceneManager"))->createParticleSystem(name, templateName);
	}catch (Ogre::Exception& ex)
	{
		if (mDotSceneInfo) mDotSceneInfo->logLoadError("Unable to create particle system \"" + name + "\" from template: \"" + templateName + "\"! Exception occured: " + ex.getFullDescription());
		particleSystem = 0;
	}

	if ( particleSystem )
	{

		// If attribute "materialName" is present then change the default material of the particle system originally coming from
		// the particle system's template.
		String materialName;
		if ( dsi::xml::getAttribute(currentNode, "materialName", materialName) ) particleSystem->setMaterialName(materialName);

		// Now attach particle system to parent node.
		SceneNode* parentSceneNode;
		if ( (parentSceneNode = (SceneNode*)parent->getParsedNode()) ) parentSceneNode->attachObject(particleSystem);
		else
		{// Parent node hasn't been created.
			if (mDotSceneInfo) mDotSceneInfo->logLoadError(
				"ParticleSystemElementProcessor::parseElementImpl(): Can not attach particle system to parent scene node. Parent scene node does not exist.");
		}
	}

	return particleSystem;
}

void ParticleSystemElementProcessor::parseElementEndImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	// Now report particle system to DotSceneInfo instance (if parsing was successfull).
	if ( getParsedNode() )
	{
		if (mDotSceneInfo)	mDotSceneInfo->reportLoadedParticleSystem((ParticleSystem*)getParsedNode(), mStatic);
	}
}


//***********************************************************************************************************************************
//********************************************** End of ParticleSystemElementProcessor **********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************** BillboardSetElementProcessor ***************************************************
//***********************************************************************************************************************************

BillboardType	BillboardSetElementProcessor::parseAttribute_billboardType(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	String strBillboardType = 
		parseAttribute("billboardType", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, String("point"));

	BillboardType billboardType;
	if ( strBillboardType == "point" )
	{
		billboardType = BBT_POINT;
	}
	else if ( strBillboardType == "orientedCommon" )
	{
		billboardType = BBT_ORIENTED_COMMON;
	}
	else if ( strBillboardType == "orientedSelf" )
	{
		billboardType = BBT_ORIENTED_SELF;
	}
	else if ( strBillboardType == "perpendicularCommon" )
	{
		billboardType = BBT_PERPENDICULAR_COMMON;
	}
	else if ( strBillboardType == "perpendicularSelf" )
	{
		billboardType = BBT_PERPENDICULAR_SELF;
	}
	else 
	{// Invalid value for attribute "billboardType".
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"Attribute \"billboardType\" of element \"" + String(currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value: \"" + strBillboardType + "\"!\n"
			"Valid values are \"point\", \"orientedCommon\", \"orientedSelf\", \"perpendicularCommon\" and \"perpendicularSelf\".\n"
			"\"billboardType\" attribute will now default to \"point\".\n"
			"See the dotsceneformat.dtd for more information.\n");
		billboardType = BBT_POINT;
	}

	return billboardType;
}

BillboardOrigin	BillboardSetElementProcessor::parseAttribute_billboardOrigin(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	String strBillboardOrigin = 
		parseAttribute("billboardOrigin", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, String("center"));

	BillboardOrigin billboardOrigin;
	if ( strBillboardOrigin == "topLeft" )
	{
		billboardOrigin = BBO_TOP_LEFT;
	}
	else if ( strBillboardOrigin == "topCenter" )
	{
		billboardOrigin = BBO_TOP_CENTER;
	}
	else if ( strBillboardOrigin == "topRight" )
	{
		billboardOrigin = BBO_TOP_RIGHT;
	}
	else if ( strBillboardOrigin == "centerLeft" )
	{
		billboardOrigin = BBO_CENTER_LEFT;
	}
	else if ( strBillboardOrigin == "center" )
	{
		billboardOrigin = BBO_CENTER;
	}
	else if ( strBillboardOrigin == "centerRight" )
	{
		billboardOrigin = BBO_CENTER_RIGHT;
	}
	else if ( strBillboardOrigin == "bottomLeft" )
	{
		billboardOrigin = BBO_BOTTOM_LEFT;
	}
	else if ( strBillboardOrigin == "bottomCenter" )
	{
		billboardOrigin = BBO_BOTTOM_CENTER;
	}
	else if ( strBillboardOrigin == "bottomRight" )
	{
		billboardOrigin = BBO_BOTTOM_RIGHT;
	}
	else 
	{// Invalid value for attribute "billboardOrigin".
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"Attribute \"billboardOrigin\" of element \"" + String(currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value: \"" + strBillboardOrigin + "\"!\n"
			"Valid values are \"topLef\", \"topCenter\", \"topRight\", \"left\", \"center\", \"right\", \"bottomLeft\", \"bottomCenter\" and \"bottomRight\".\n"
			"\"billboardOrigin\" attribute will now default to \"center\".\n"
			"See the dotsceneformat.dtd for more information.\n");
		billboardOrigin = BBO_CENTER;
	}

	return billboardOrigin;
}

BillboardRotationType	BillboardSetElementProcessor::parseAttribute_billboardRotationType(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	String strBillboardRotationType = 
		parseAttribute("billboardRotationType", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, String("texcoord"));

	BillboardRotationType billboardRotationType;
	if ( strBillboardRotationType == "texcoord" )
	{
		billboardRotationType = BBR_TEXCOORD;
	}
	else if ( strBillboardRotationType == "vertex" )
	{
		billboardRotationType = BBR_VERTEX;
	}
	else 
	{// Invalid value for attribute "billboardType".
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"Attribute \"billboardRotationType\" of element \"" + String(currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value: \"" + strBillboardRotationType + "\"!\n"
			"Valid values are \"texcoord\" and \"vertex\".\n"
			"\"billboardRotationType\" attribute will now default to \"texcoord\".\n"
			"See the dotsceneformat.dtd for more information.\n");
		billboardRotationType = BBR_TEXCOORD;
	}

	return billboardRotationType;
}

void*	BillboardSetElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// Parse attribute "name".
	String name = getNodeName_AutoNamedAndPrefixedIfNeeded(currentNode);

	// Parse attribute "id".
	parseAttribute_id(parent, currentNode);

	// Parse attribute "poolSize".
	unsigned int poolSize =
		parseAttribute("poolSize", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (unsigned int)20);

	// Try to create billboard set.
	BillboardSet*	billboardSet = 0;
	try
	{
		billboardSet = 
			((SceneManager*)mTreeBuilder->getCommonParameter("SceneManager"))->createBillboardSet(name, poolSize);
	}catch (Ogre::Exception& ex)
	{
		if (mDotSceneInfo) mDotSceneInfo->logLoadError("Unable to create billboard set \"" + name + "\" with pool size: \"" + StringConverter::toString(poolSize) + "\"! Exception occured: " + ex.getFullDescription());
		billboardSet = 0;
	}

	if ( billboardSet )
	{
		// Parse attribute "autoextend".
		bool autoextend =
			parseAttribute("autoextend", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)true);
		billboardSet->setAutoextend(autoextend);

		// Parse attribute "materialName".
		String materialName =
			parseAttribute("materialName", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (String)BLANKSTRING);
		billboardSet->setMaterialName(materialName);

		// Parse attribute "defaultWidth".
		Real defaultWidth =
			parseAttribute("defaultWidth", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)10);

		// Parse attribute "defaultHeight".
		Real defaultHeight =
			parseAttribute("defaultHeight", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)10);

		// Set default dimensions.
		billboardSet->setDefaultDimensions(defaultWidth, defaultHeight);


		// Parse attribute "billboardType".
		BillboardType billboardType = parseAttribute_billboardType(parent, currentNode);
		billboardSet->setBillboardType(billboardType);

		// Parse attribute "billboardOrigin".
		BillboardOrigin billboardOrigin = parseAttribute_billboardOrigin(parent, currentNode);
		billboardSet->setBillboardOrigin(billboardOrigin);

		// Parse attribute "billboardRotationType".
		BillboardRotationType billboardRotationType = parseAttribute_billboardRotationType(parent, currentNode);
		billboardSet->setBillboardRotationType(billboardRotationType);


		// Parse attribute "sortingEnabled".
		bool sortingEnabled =
			parseAttribute("sortingEnabled", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)false);
		billboardSet->setSortingEnabled(sortingEnabled);

		// Parse attribute "cullIndividually".
		bool cullIndividually =
			parseAttribute("cullIndividually", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)false);
		billboardSet->setCullIndividually(cullIndividually);

		// Parse attribute "accurateFacing".
		bool accurateFacing =
			parseAttribute("accurateFacing", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)false);
		billboardSet->setUseAccurateFacing(accurateFacing);

		// Parse attribute "billboardsInWorldSpace".
		bool billboardsInWorldSpace =
			parseAttribute("billboardsInWorldSpace", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)false);
		billboardSet->setBillboardsInWorldSpace(billboardsInWorldSpace);

		// Parse attribute "pointRenderingEnabled".
		bool pointRenderingEnabled =
			parseAttribute("pointRenderingEnabled", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)false);
		billboardSet->setPointRenderingEnabled(pointRenderingEnabled);

		// Now attach billboard set to parent node.
		SceneNode* parentSceneNode;
		if ( (parentSceneNode = (SceneNode*)parent->getParsedNode()) ) parentSceneNode->attachObject(billboardSet);
		else
		{// Parent node hasn't been created.
			if (mDotSceneInfo) mDotSceneInfo->logLoadError(
				"BillboardSetElementProcessor::parseElementImpl(): Can not attach billboard set to parent scene node. Parent scene node does not exist.");
		}
	}

	return billboardSet;
}

void	BillboardSetElementProcessor::parseElementEndImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	BillboardSet* billboardSet; 
	if ( billboardSet = (BillboardSet*)getParsedNode() )
	{
		// This is extremelly important!!!
		// After adding all the billboards to the billboard set we need to recalculate the billboard set's bounds, otherwise it
		// might become culled against the frustum when it shouldn't.
		billboardSet->_updateBounds();


		// Now report billboard set to DotSceneInfo instance (if parsing was successfull).
		if ( getParsedNode() )
		{
			if (mDotSceneInfo)	mDotSceneInfo->reportLoadedBillboardSet((BillboardSet*)getParsedNode(), mStatic);
		}
	}
}

//***********************************************************************************************************************************
//*********************************************** End of BillboardSetElementProcessor ***********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************* CommonDirectionElementProcessor *************************************************
//***********************************************************************************************************************************

void*	CommonDirectionElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	mCommonDirection.x = parseAttribute("x", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)0.0);
	mCommonDirection.y = parseAttribute("y", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)1.0);
	mCommonDirection.z = parseAttribute("z", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)0.0);

	// Now set common direction on parent billboard set.
	BillboardSet* billboardSet;
	if ( (billboardSet = (BillboardSet*)parent->getParsedNode()) ) billboardSet->setCommonDirection(mCommonDirection);
	else
	{// Parent billboard set hasn't been created.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"CommonDirectionElementProcessor::parseElementImpl(): Can not set common direction on parent billboard set. Parent billboard set does not exist.");
	}

	return &mCommonDirection;
}

//***********************************************************************************************************************************
//********************************************** End of CommonDirectionElementProcessor *********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************* CommonUpVectorElementProcessor **************************************************
//***********************************************************************************************************************************

void*	CommonUpVectorElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	mCommonUpVector.x = parseAttribute("x", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)0.0);
	mCommonUpVector.y = parseAttribute("y", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)0.0);
	mCommonUpVector.z = parseAttribute("z", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)1.0);

	// Now set common up vector on parent billboard set.
	BillboardSet* billboardSet;
	if ( (billboardSet = (BillboardSet*)parent->getParsedNode()) ) billboardSet->setCommonUpVector(mCommonUpVector);
	else
	{// Parent billboard set hasn't been created.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"CommonUpVectorElementProcessor::parseElementImpl(): Can not set common up vector on parent billboard set. Parent billboard set does not exist.");
	}

	return &mCommonUpVector;
}

//***********************************************************************************************************************************
//********************************************** End of CommonUpVectorElementProcessor **********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************** TextureCoordsElementProcessor **************************************************
//***********************************************************************************************************************************

void TextureCoordsElementProcessor::parseElementEndImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// Now set texture coordinates on parent billboard/billboard set.
	BillboardSet*	parentBillboardSet;
	Billboard*		parentBillboard;
	bool			parentDoesNotExist = false;
	String			errorMsg;
	String			parentXMLNodeName(currentNode->Parent()->Value()); // This is for optimization to avoid converting char* to String many times.
	if		( parentXMLNodeName == "billboard" )
	{
		if ( !(parentBillboard = (Billboard*)parent->getParsedNode()) )
		{
			parentDoesNotExist = true;
			errorMsg = "TextureCoordsElementProcessor::parseElementImpl(): Can not set texture coordinates on parent billboard. Parent billboard does not exist.";
		}
		else
		{
			parentBillboard->setTexcoordRect(mTextureCoords.back());
		}
	}else if ( parentXMLNodeName == "billboardSet" )
	{
		if ( !(parentBillboardSet = (BillboardSet*)parent->getParsedNode()) )
		{
			parentDoesNotExist = true;
			errorMsg = "TextureCoordsElementProcessor::parseElementImpl(): Can not set texture coordinates on parent billboard set. Parent billboard set does not exist.";
		}
		else 
		{
			parentBillboardSet->setTextureCoords(&mTextureCoords.front(), (uint16)mTextureCoords.size());
		}
	}

	if ( parentDoesNotExist )
	{
		// An error occured during parent node creation.
		// TODO: An error occured during parent node creation, can't set properties for parent. Log an error message. Search for
		// parent name.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(errorMsg);
	}

	// This is very important!!!
	// Without clearing the internal list of texture coords we would mess up the next billboard set's texture coords, since
	// TextureCoordsElementProcessor is reused across instances of "billboardSet" elements and instances of "billboard" elements.
	mTextureCoords.clear();
}

//***********************************************************************************************************************************
//*********************************************** End of TextureCoordsElementProcessor **********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//**************************************************** FloatRectElementProcessor ****************************************************
//***********************************************************************************************************************************

void* FloatRectElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	mFloatRect.left		= parseAttribute("left",	parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)0.0);
	mFloatRect.top		= parseAttribute("top",		parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)0.0);
	mFloatRect.right	= parseAttribute("right",	parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)1.0);
	mFloatRect.bottom	= parseAttribute("bottom",	parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)1.0);

	// Now set parsed texture coordinate on parent TextureCoordsElementProcessor.
	((TextureCoordsElementProcessor*)parent)->addTextureCoord(mFloatRect);

	return &mFloatRect;
}

//***********************************************************************************************************************************
//************************************************* End of FloatRectElementProcessor ************************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//********************************************** TextureStacksAndSlicesElementProcessor *********************************************
//***********************************************************************************************************************************

void* TextureStacksAndSlicesElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	mStacks	= parseAttribute("stacks",	parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (unsigned int)0);
	mSlices	= parseAttribute("slices",	parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (unsigned int)0);

	// Now set texture coordinates on parent billboard set.
	BillboardSet* billboardSet;
	if ( (billboardSet = (BillboardSet*)parent->getParsedNode()) )
	{
		billboardSet->setTextureStacksAndSlices(mStacks, mSlices);
	}
	else
	{// Parent billboard set hasn't been created.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"TextureStacksAndSlicesElementProcessor::parseElementImpl(): Can not set texture stacks and slices on parent billboard set. Parent billboard set does not exist.");
	}

	// TODO: design question: what should we return with? There is no exact parsed data.
	return 0;
}

//***********************************************************************************************************************************
//******************************************* End of TextureStacksAndSlicesElementProcessor *****************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//**************************************************** BillboardElementProcessor ****************************************************
//***********************************************************************************************************************************

void* BillboardElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// Create billboard.
	Billboard*		billboard = 0;
	BillboardSet*	billboardSet = 0;
	if ( (billboardSet = (BillboardSet*)parent->getParsedNode()) )
	{
		try
		{
			if ( !(billboard = billboardSet->createBillboard(Vector3::ZERO)) )
				if (mDotSceneInfo) mDotSceneInfo->logLoadError("Unable to create billboard with billboard set \"" + billboardSet->getName() + "\"! Billboard pool exhausted!" );
		}catch(Ogre::Exception& ex)
		{
			if (mDotSceneInfo) mDotSceneInfo->logLoadError("Unable to create billboard with billboard set \"" + billboardSet->getName() + "\"! Exception occured: " + ex.getFullDescription());
			billboard = 0;
		}
	}
	else
	{// Parent billboard set hasn't been created.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"BillboardElementProcessor::parseElementImpl(): Can not create billboard with parent billboard set. Parent billboard set does not exist.");
	}

	if ( billboard )
	{
		// Parse attribute "id".
		parseAttribute_id(parent, currentNode);

		// Parse attribute "rotation".
		Real rotation		= parseAttribute("rotation", parent, currentNode, XmlNodeProcessor::XMLADD_IMPLIED, (Real)0.0);
		billboard->setRotation(Ogre::Angle(rotation));

		// Parse attribute "width", "height".
		Real width, height;
		bool hasOwnWidth = false, hasOwnHeight = false;
		hasOwnWidth		= dsi::xml::getAttribute(currentNode, "width", width);
		hasOwnHeight	= dsi::xml::getAttribute(currentNode, "width", height);
		if ( hasOwnWidth && hasOwnHeight ) billboard->setDimensions(width, height);
		else if (hasOwnWidth || hasOwnHeight )
		{// Only one dimension is present. This is illegal.
			if ( hasOwnWidth )
				if (mDotSceneInfo) mDotSceneInfo->logLoadError(
					"Attribute \"width\" is present in element \"" + mElementName + "\" while attribute \"height\" is not!\n"
					"Attribute \"width\" and attribute \"height\" must be present simultaneously in element \"billboard\"!\n"
					"Please read the Readme.txt file located in the directory of the project dotSceneLoader for more information.\n");
			else
				if (mDotSceneInfo) mDotSceneInfo->logLoadError(
					"Attribute \"height\" is present in element \"" + mElementName + "\" while attribute \"width\" is not!\n"
					"Attribute \"width\" and attribute \"height\" must be present simultaneously in element \"billboard\"!\n"
					"Please read the Readme.txt file located in the directory of the project dotSceneLoader for more information.\n");
		}

		// Parse attribute "texcoodIndex".
		uint16 texcoordIndex	= parseAttribute("texcoordIndex", parent, currentNode, XmlNodeProcessor::XMLADD_IMPLIED, (unsigned int)0);
		billboard->setTexcoordIndex(texcoordIndex);
	}

	return billboard;
}

//***********************************************************************************************************************************
//************************************************ End of BillboardElementProcessor *************************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//**************************************************** AnimationsElementProcessor ***************************************************
//***********************************************************************************************************************************

void* AnimationsElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// TODO: should animationStates element processor parse anything at all?

	// This is required, since AnimationElementProcessor instances are going to create animations using parent entity's mesh,
	// skeleton or the parent scene manager.
	return parent->getParsedNode();
}

//***********************************************************************************************************************************
//************************************************ End of AnimationsElementProcessor ************************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//**************************************************** AnimationElementProcessor ****************************************************
//***********************************************************************************************************************************

Animation::InterpolationMode AnimationElementProcessor::parseAttribute_interpolationMode(const String& attributeName, XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	String strInterpolationMode = 
		parseAttribute(attributeName, parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (String)"linear");
	Animation::InterpolationMode eInterpolationMode;
	try
	{
		eInterpolationMode = StringInterpolationModeToEnumInterpolationMode(strInterpolationMode);
	}catch (Ogre::Exception& ex)
	{
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"Attribute \"" + attributeName + "\" of element \"" + mElementName + "\" in file \"" + mFilename + "\" has an invalid "
			"value: \"" + strInterpolationMode + "\"! An exception occured while trying to convert it to an enumerated type interpolation mode: "
			+ ex.getFullDescription());
		eInterpolationMode = Animation::IM_LINEAR;
	}

	return eInterpolationMode;
}

Animation::RotationInterpolationMode AnimationElementProcessor::parseAttribute_rotationInterpolationMode(const String& attributeName, XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	String strRInterpolationMode = 
		parseAttribute(attributeName, parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (String)"linear");
	Animation::RotationInterpolationMode eRInterpolationMode;
	try
	{
		eRInterpolationMode = StringRInterpolationModeToEnumRInterpolationMode(strRInterpolationMode);
	}catch (Ogre::Exception& ex)
	{
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"Attribute \"" + attributeName + "\" of element \"" + mElementName + "\" in file \"" + mFilename + "\" has an invalid "
			"value: \"" + strRInterpolationMode + "\"! An exception occured while trying to convert it to an enumerated type interpolation mode: "
			+ ex.getFullDescription());
		eRInterpolationMode = Animation::RIM_LINEAR;
	}

	return eRInterpolationMode;
}

void*	AnimationElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// NOTE: Attribute "name" is not parsed here because we have to prefix it if the current xml element is a child element of the
	// xml element "scene", but we don't have to prefix it if the current xml element is a child element of the xml element "entity".
	String name;

	// Parse attribute "length".
	Real length =
		parseAttribute("length", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)0);

	// Parse attribute "type".
	String type =
		parseAttribute("type", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (String)BLANKSTRING);


	// TODO: FIXTHIS: HUGE HACK!!! Separate versions of AnimationElementProcessor should be implemented for xml element "entity" and
	// xml element "scene", to keep dependencies between element processors as low as possible, and to make element processing
	// compatible with the element processing theory described in the documentation of class XmlNodeProcessorTreeBuilder.
	String				parentParentsXMLNodeName(currentNode->Parent()->Parent()->Value());
	bool				parentDoesNotExist = false;
	Entity*				parentEntity = 0;
	SceneManager*		parentSceneManager = 0;
	mParentEntity			= 0;
	mParentSkeletonInstance = 0;
	mParentMesh				= 0;
	mAnimation				= 0;
	if		( parentParentsXMLNodeName == "scene" )
	{
		if ( !(parentSceneManager = (SceneManager*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else 
		{
			if ( type != "node" )
			{
				String nameWithoutPrefix;
				dsi::xml::getAttribute(currentNode, "name", nameWithoutPrefix);
				if (mDotSceneInfo) mDotSceneInfo->logLoadWarning(
					"Animation \"" + nameWithoutPrefix + "\" is defined as a child xml element of xml element \"scene\", however it has a type \""
					+ type + "\".\n"
					"Animations defined for the scene, will be created by the scene manager and must have the type \"node\""
					", since they are always node animations.");
			}

			try
			{
				// Parse attribute "name".
				name = getNodeName_AutoNamedAndPrefixedIfNeeded(currentNode);
				mAnimation = parentSceneManager->createAnimation(name, length);

				// Report created animation to DotSceneInfo instance.
				if (mDotSceneInfo)	mDotSceneInfo->reportLoadedAnimation(parentSceneManager, mAnimation);
			}catch (Ogre::Exception& ex)
			{
				if (mDotSceneInfo) mDotSceneInfo->logLoadError(
					"Unable to create animation \"" + name + "\" with length " + StringConverter::toString(length) + 
					"! Exception occured: " + ex.getFullDescription());
				mAnimation = 0;
			}
		}
	}else if ( parentParentsXMLNodeName == "entity" )
	{
		if ( !(parentEntity = (Entity*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else 
		{
			try
			{
				// Parse attribute "name".
				name = parseAttribute("name", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (String)BLANKSTRING);

				// Store parent entity.
				mParentEntity			= parentEntity;
				// Get parent entity's skeleton instance.
				mParentSkeletonInstance = parentEntity->getSkeleton();
				// Get the mesh which parent entity is based on.
				mParentMesh				= parentEntity->getMesh().getPointer();

				if (type == "node")
				{// Create animation using parent entity's skeleton instance.
					mAnimation				= mParentSkeletonInstance->createAnimation(name, length);
					// Update animation state set of the parent entity to reflect the changes of the new animation set in the skeleton
					// instance of the entity.
					mParentSkeletonInstance->_refreshAnimationState(parentEntity->getAllAnimationStates());					
				}else if (type == "numeric")
				{
					OGRE_EXCEPT(Ogre::Exception::ERR_NOT_IMPLEMENTED,
								"Currently animations with numeric animation tracks are not implemented.",
								"AnimationElementProcessor::parseElementImpl");
				}else if (type == "mesh")
				{// Create animation using parent entity's mesh.
					mAnimation				= parentEntity->getMesh()->createAnimation(name, length);
					// Update animation state set of the parent entity to reflect the changes of the new animation set in the mesh
					// instance of the entity.
					parentEntity->getMesh()->_refreshAnimationState(parentEntity->getAllAnimationStates());
				}else
				{
					if (mDotSceneInfo) mDotSceneInfo->logLoadError(
						"Attribute \"type\" of element \"" + mElementName + "\" in file \"" + mFilename + "\" has an invalid value: \"" + type + "\"!\n"
						"Valid values are \"node\", \"numeric\", \"mesh\".\n"
						"Animation with name \"" + name + "\" wasn't created.\n"
						"See the dotsceneformat.dtd for more information.\n");
					mAnimation = 0;
				}

				// Report created animation to DotSceneInfo instance.
				if (mDotSceneInfo)	mDotSceneInfo->reportLoadedAnimation(parentEntity, mAnimation);
			}catch (Ogre::Exception& ex)
			{
				if (mDotSceneInfo) mDotSceneInfo->logLoadError(
					"Unable to create animation \"" + name + "\" with length " + StringConverter::toString(length) + 
					"! Exception occured: " + ex.getFullDescription());
				mAnimation = 0;
			}
		}
	}
	if ( parentDoesNotExist )
	{
		// An error occured during parent node creation.
		// TODO: An error occured during parent node creation, can't set properties for parent. Log an error message. Search for
		// parent name.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"AnimationElementProcessor::parseElementImpl(): Can not create/get animation with parent xml node's parsed data, parent xml node's parsed data does not exist.");
		mAnimation = 0;
	}

	if ( mAnimation )
	{
		// Parse attribute "rotationInterpolationMode".
		mAnimation->setInterpolationMode(
			this->parseAttribute_interpolationMode("interpolationMode", parent, currentNode));

		// Parse attribute "rotationInterpolationMode".
		mAnimation->setRotationInterpolationMode(
			this->parseAttribute_rotationInterpolationMode("rotationInterpolationMode", parent, currentNode));
	}

	return mAnimation;
}

void AnimationElementProcessor::parseElementEndImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	// These lines are for debugging purposes only.
	// TODO: DEBUG PART. Saving skeleton of entity after extending it with new animations from the .scene.
		//Ogre::SkeletonSerializer skeletonSerializer;
		//if ( mParentSkeletonInstance )
		//	skeletonSerializer.exportSkeleton(	mParentSkeletonInstance,
		//										mFilename + "_" + mParentSkeletonInstance->getName(),
		//										SkeletonSerializer::ENDIAN_NATIVE);
	// END OF DEBUG PART.
}

//***********************************************************************************************************************************
//************************************************* End of AnimationElementProcessor ************************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************ NodeAnimationTrackElementProcessor ***********************************************
//***********************************************************************************************************************************

void* NodeAnimationTrackElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	Animation* parentAnimation = 0;
	if ( (parentAnimation = (Animation*)parent->getParsedNode()) )
	{
		// Parse attribute "handle".
		unsigned short handle =
			parseAttribute("handle", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (unsigned int)0);

		// Parse attribute "associatedNodeName".
		String associatedNodeName =
			parseAttribute("associatedNodeName", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (String)BLANKSTRING);


		// ---------------------------------------------------- Create node track ----------------------------------------------------
		// NOTE: this is a much structurated way to decide what was the parent xml element of the xml element "animation", than
		// checking for the xml element names literally.
		SkeletonInstance* parentSkeletonInstance;
		if ( (parentSkeletonInstance = ((AnimationElementProcessor*)parent)->getParentSkeletonInstance()) != 0 )
		{// Parent is an entity with a given skeleton instance.
			Node* associatedBone;
			try
			{
				associatedBone = parentSkeletonInstance->getBone(associatedNodeName);
			}catch (Ogre::Exception& ex)
			{
				if (mDotSceneInfo) mDotSceneInfo->logLoadError(
					"Unable to retrieve bone \"" + associatedNodeName + "\" from skeleton instance \"" + parentSkeletonInstance->getName() + 
					//"\" of entity \"" + parentEntityName + 
					"\""
					"! Exception occured: " + ex.getFullDescription());
				return 0;
			}

			try
			{
				mNodeTrack = parentAnimation->createNodeTrack(handle, associatedBone);
			}catch (Ogre::Exception& ex)
			{
				if (mDotSceneInfo) mDotSceneInfo->logLoadError(
					"Unable to create node animation track with handle " + StringConverter::toString(handle) + 
					" and associate it with bone \"" + associatedNodeName + "\""
					"! Exception occured: " + ex.getFullDescription());
				return 0;
			}
		}else
		{// Parent is the scene itself.
			// Prefix associated node name *only* if the created node animation track's parent is the scene. This will allow loading
			// the same .scene file multiple times to the the same scene manager, using different root nodes for the scene, without
			// name collisions.
			String* prefix;
			if ( (prefix = ((String*)mTreeBuilder->getCommonParameter("NamePrefix"))) != 0 ) associatedNodeName = *prefix + associatedNodeName;

			// Now search for the node with the given name.
			mNodeTrack = parentAnimation->createNodeTrack(
				handle, ((SceneManager*)mTreeBuilder->getCommonParameter("SceneManager"))->getSceneNode(associatedNodeName));
		}
		//----------------------------------------------------------------------------------------------------------------------------

		// Parse attribute "useShortestRotationPath".
		bool useShortestRotationPath =
			parseAttribute("useShortestRotationPath", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)true);

		mNodeTrack->setUseShortestRotationPath(useShortestRotationPath);
	}else
	{
		// An error occured during parent node creation.
		// TODO: An error occured during parent node creation, can't set properties for parent. Log an error message. Search for
		// parent name.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"NodeAnimationTrackElementProcessor::parseElementImpl(): Can not create node animation track using parent animation, parent animation does not exist.");
		mNodeTrack = 0;
	}

	return mNodeTrack;
}

//***********************************************************************************************************************************
//******************************************** End of NodeAnimationTrackElementProcessor ********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************ TransformKeyFrameElementProcessor ************************************************
//***********************************************************************************************************************************

void* TransformKeyFrameElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	NodeAnimationTrack* parentNodeTrack = 0;
	if ( (parentNodeTrack = (NodeAnimationTrack*)parent->getParsedNode()) )
	{
		// Parse attribute "timePosition".
		Real timePosition =
			parseAttribute("timePosition", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)0);

		// TODO: The following lines were written according to the Ogre TransformKeyFrame and NodeAnimationTrack documentation,
		// however they don't work, the zeroth keyframe isn't always pre-created, so they are commented out.
		// -------
			//// If timePositon == 0, then don't create key frame, just retrieve it, since the keyframe at the zeroth position always
			//// exists.
			//if ( !timePosition )	mTransformKeyFrame = (TransformKeyFrame*)parentNodeTrack->getKeyFrame(0);
			//else					mTransformKeyFrame = parentNodeTrack->createNodeKeyFrame(timePosition);
		// -------

		// try-catch block not needed, keyframe creation won't throw an exception anyway.
		mTransformKeyFrame = parentNodeTrack->createNodeKeyFrame(timePosition);
	}else
	{
		// An error occured during parent node creation.
		// TODO: An error occured during parent node creation, can't set properties for parent. Log an error message. Search for
		// parent name.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"TransformKeyFrameElementProcessor::parseElementImpl(): Can not create transform keyframe using parent node animation track, parent node animation track does not exist.");
		mTransformKeyFrame = 0;
	}

	return mTransformKeyFrame;
}

//***********************************************************************************************************************************
//********************************************* End of TransformKeyFrameElementProcessor ********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//********************************************** VertexAnimationTrackElementProcessor ***********************************************
//***********************************************************************************************************************************

void* VertexAnimationTrackElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	Animation* parentAnimation = 0;
	if ( (parentAnimation = (Animation*)parent->getParsedNode()) )
	{
		// Attribute "handle" defines the handle for the vertex animation track and the mesh shared data/submesh index.
		unsigned short handle =
			parseAttribute("handle", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (unsigned int)0);
		VertexData* vertexData =
			((AnimationElementProcessor*)parent)->getParentEntity()->getMesh()->getSubMesh(handle)->vertexData;

		// Parse attribute "vertexAnimationType".
		String strVertexAnimationType =
			parseAttribute("vertexAnimationType", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (String)BLANKSTRING);

		// Converte string vertex animation type to enumerated vertex animation type.
		Ogre::VertexAnimationType eVertexAnimationType;
		try
		{
			eVertexAnimationType = StringVertexAnimationTypeToEnumVertexAnimationType(strVertexAnimationType);
		}catch (Ogre::Exception& ex)
		{
			if (mDotSceneInfo) mDotSceneInfo->logLoadError(
				"Attribute \"vertexAnimationType\" of element \"" + mElementName + "\" in file \"" + mFilename + "\" has an invalid "
				"value: \"" + strVertexAnimationType + "\"! An exception occured while trying to convert it to an enumerated type vertex animation type: "
				+ ex.getFullDescription());
			eVertexAnimationType = Ogre::VAT_NONE;
		}

		// Now create vertex animation track.
		mVertexTrack = parentAnimation->createVertexTrack(handle, vertexData, eVertexAnimationType);
		 
		// Parse attribute "targetMode". We will use it to get the vertex data which animation will be applied to.
		String strTargetMode =
			parseAttribute("targetMode", parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (String)"software");

		// Convert string target mode to enumerated target mode.
		Ogre::VertexAnimationTrack::TargetMode eTargetMode;
		try
		{
			eTargetMode = StringTargetModeToEnumTargetMode(strTargetMode);
		}catch (Ogre::Exception& ex)
		{
			if (mDotSceneInfo) mDotSceneInfo->logLoadError(
				"Attribute \"targetMode\" of element \"" + mElementName + "\" in file \"" + mFilename + "\" has an invalid "
				"value: \"" + strTargetMode + "\"! An exception occured while trying to convert it to an enumerated type vertex animation type: "
				+ ex.getFullDescription());
			eTargetMode = Ogre::VertexAnimationTrack::TM_SOFTWARE;
		}
		// Set target mode on vertex animation track.
		mVertexTrack->setTargetMode(eTargetMode);
	}else
	{
		// An error occured during parent node creation.
		// TODO: An error occured during parent node creation, can't set properties for parent. Log an error message. Search for
		// parent name.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"VertexAnimationTrackElementProcessor::parseElementImpl(): Can not create vertex animation track using parent animation, parent animation does not exist.");
		mVertexTrack = 0;
	}

	return mVertexTrack;
}

//***********************************************************************************************************************************
//******************************************* End of VertexAnimationTrackElementProcessor *******************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************ VertexPoseKeyFrameElementProcessor ***********************************************
//***********************************************************************************************************************************

void*	VertexPoseKeyFrameElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	VertexAnimationTrack* parentVertexTrack = 0;
	if ( (parentVertexTrack = (VertexAnimationTrack*)parent->getParsedNode()) )
	{
		// Parse attribute "timePosition".
		Real timePosition =
			parseAttribute("timePosition", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)0);

		// TODO: The following lines were written according to the Ogre TransformKeyFrame and NodeAnimationTrack documentation,
		// however they don't work, the zeroth keyframe isn't always pre-created, so they are commented out.
		// -------
			//// If timePositon == 0, then don't create key frame, just retrieve it, since the keyframe at the zeroth position always
			//// exists.
			//if ( !timePosition )	mTransformKeyFrame = (TransformKeyFrame*)parentNodeTrack->getKeyFrame(0);
			//else					mTransformKeyFrame = parentNodeTrack->createNodeKeyFrame(timePosition);
		// -------

		// try-catch block not needed, keyframe creation won't throw an exception anyway.
		mVertexPoseKeyFrame = parentVertexTrack->createVertexPoseKeyFrame(timePosition);
	}else
	{
		// An error occured during parent node creation.
		// TODO: An error occured during parent node creation, can't set properties for parent. Log an error message. Search for
		// parent name.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"VertexPoseKeyFrameElementProcessor::parseElementImpl(): Can not create vertex pose keyframe using parent vertex animation track, parent vertex animation track does not exist.");
		mVertexPoseKeyFrame = 0;
	}

	return mVertexPoseKeyFrame;
}

//***********************************************************************************************************************************
//******************************************** End of VertexPoseKeyFrameElementProcessor ********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************** PoseReferenceElementProcessor **************************************************
//***********************************************************************************************************************************

void*	PoseReferenceElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	VertexPoseKeyFrame* parentVertexPoseKeyFrame = 0;
	if ( (parentVertexPoseKeyFrame = (VertexPoseKeyFrame*)parent->getParsedNode()) )
	{
		// Parse attribute "poseIndex".
		mPoseRef.poseIndex = parseAttribute("poseIndex", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (unsigned int)0);

		// Parse attribute "influence".
		mPoseRef.influence = parseAttribute("influence", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Real)1);

		// Now add pose reference to parent key frame. Try-catch block isn't needed, VertexPoseKeyFrame::addPoseReference() won't
		// throw an exception anyway.
		parentVertexPoseKeyFrame->addPoseReference(mPoseRef.poseIndex, mPoseRef.influence);

	}else
	{
		// An error occured during parent node creation.
		// TODO: An error occured during parent node creation, can't set properties for parent. Log an error message. Search for
		// parent name.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"PoseReferenceElementProcessor::parseElementImpl(): Can not set pose reference on parent vertex pose key frame, parent vertex pose key frame does not exist.");
		return 0;
	}

	return &mPoseRef;
}

//***********************************************************************************************************************************
//*********************************************** End of PoseReferenceElementProcessor **********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************ AnimationStatesElementProcessor **************************************************
//***********************************************************************************************************************************

void* AnimationStatesElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// TODO: should "animationStates" element processor parse anything at all?

	// This is required, since AnimationStateElementProcessor instances are going to create animation states using parent entity or
	// scene manager.
	return parent->getParsedNode();
}

//***********************************************************************************************************************************
//********************************************** End of AnimationStatesElementProcessor **********************************************
//***********************************************************************************************************************************


//***********************************************************************************************************************************
//************************************************* AnimationStateElementProcessor **************************************************
//***********************************************************************************************************************************

void*	AnimationStateElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);
	
	// Parse attribute "animationName".
	String animationName =
		parseAttribute("animationName", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (String)BLANKSTRING);

	// TODO: FIXTHIS: HUGE HACK!!! Separate versions of AnimationStateElementProcessor should be implemented for xml element
	// "entity" and xml element "scene", to keep dependencies between element processors as low as possible, and to make element
	// processing compatible with the element processing theory described in the documentation of class XmlNodeProcessorTreeBuilder.
	String				parentParentsXMLNodeName(currentNode->Parent()->Parent()->Value());
	bool				parentDoesNotExist = false;
	Entity*				parentEntity = 0;
	SceneManager*		parentSceneManager = 0;
	if		( parentParentsXMLNodeName == "scene" )
	{
		if ( !(parentSceneManager = (SceneManager*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else 
		{
			try
			{
				String* prefix;
				if ( (prefix = ((String*)mTreeBuilder->getCommonParameter("NamePrefix"))) != 0 ) 
					animationName = *prefix + animationName;

				mAnimationState = parentSceneManager->createAnimationState(animationName);

				// Report created animation state to DotSceneInfo instance.
				if (mDotSceneInfo)	mDotSceneInfo->reportLoadedAnimationState(parentSceneManager, mAnimationState);
			}catch (Ogre::Exception& ex)
			{
				if (mDotSceneInfo) mDotSceneInfo->logLoadError(
					"Unable to create animation state referencing animation \"" + animationName + "\"! Exception occured: " +
					ex.getFullDescription());
				mAnimationState = 0;
			}
		}
	}else if ( parentParentsXMLNodeName == "entity" )
	{
		if ( !(parentEntity = (Entity*)parent->getParsedNode()) ) parentDoesNotExist = true;
		else 
		{
			try
			{
				mAnimationState = parentEntity->getAnimationState(animationName);

				// Report created animation state to DotSceneInfo instance.
				if (mDotSceneInfo)	mDotSceneInfo->reportLoadedAnimationState(parentEntity, mAnimationState);
			}catch (Ogre::Exception& ex)
			{
				if (mDotSceneInfo) mDotSceneInfo->logLoadError(
					"Unable to get animation state referencing animation \"" + animationName + "\" from entity \"" + parentEntity->getName() + "\"! Exception occured: " +
					ex.getFullDescription());
				mAnimationState  = 0;
			}
		}
	}

	if ( parentDoesNotExist )
	{
		// An error occured during parent node creation.
		// TODO: An error occured during parent node creation, can't set properties for parent. Log an error message. Search for
		// parent name.
		if (mDotSceneInfo) mDotSceneInfo->logLoadError(
			"AnimationStateElementProcessor::parseElementImpl(): Can not create/get animation set with parent xml node's parsed data, parent xml node's parsed data does not exist.");
		mAnimationState = 0;
	}

	if ( mAnimationState )
	{
		bool enabled	  = parseAttribute("enabled",		parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)true);
		Real timePosition = parseAttribute("timePosition",	parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)0);
		Real length		  = parseAttribute("length",		parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)0.0);
		Real weight		  = parseAttribute("weight",		parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (Real)1.0);
		bool loop		  = parseAttribute("loop",			parent, currentNode, XmlNodeProcessor::XMLADD_DEFAULTVALUEPRESENT, (bool)true);

		mAnimationState->setEnabled(enabled);
		mAnimationState->setTimePosition(timePosition);
		if (length) mAnimationState->setLength(length);
		mAnimationState->setWeight(weight);
		mAnimationState->setLoop(loop);
	}

	return mAnimationState;
}

//***********************************************************************************************************************************
//********************************************** End of AnimationStateElementProcessor **********************************************
//***********************************************************************************************************************************

//***********************************************************************************************************************************
//****************************************************** UserDataElementProcessor ******************************************************
//***********************************************************************************************************************************

void*	UserDataElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	Node* parentNode = (Node*)parent->getParsedNode();
	Any value;
	String type, name;
	UserObjectBindings* userObjects;

	if (parentNode) {
		userObjects = &parentNode->getUserObjectBindings();
		name = parseAttribute("name", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (String)"");
		type = parseAttribute("type", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (String)"none");
		
		if (type.compare("str") == 0) {
			value = parseAttribute("value", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (String)"");
			
		}
		else if (type.compare("int") == 0) {
			value = parseAttribute("value", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (int32)0);
		}
		else if (type.compare("float") == 0) {
			value = parseAttribute("value", parent, currentNode, XmlNodeProcessor::XMLADD_REQUIRED, (Ogre::Real)0.0);
		}

		if (!value.isEmpty()) { userObjects->setUserAny(name, (Ogre::Any)value); }
	}

	return &userObjects;
}

//***********************************************************************************************************************************
//************************************************** End of UserDataElementProcessor **************************************************
//***********************************************************************************************************************************




//***********************************************************************************************************************************
//***************************************************** OctreeElementProcessor ******************************************************
//***********************************************************************************************************************************

#ifdef _USE_DOT_OCTREE

//#include "DotSceneManager.h"

void* OctreeElementProcessor::parseElementImpl(XmlNodeProcessor* parent, TiXmlElement* currentNode)
{
	queryCurrElementNameAndFilenameAndDotSceneInfo(currentNode);

	// First determine if we have to load the static octree at all. Default is yes.
	bool* loadStaticOctree;
	if ( (loadStaticOctree = (bool*)mTreeBuilder->getCommonParameter("LoadStaticOctree")) && !(*loadStaticOctree) )
		return 0;


//	pSM->addWorldGeometry(sceneFileName);
//	pSM->SetOctreeVisible(0);

	
	return 0;
}
#endif

//***********************************************************************************************************************************
//************************************************** End of OctreeElementProcessor **************************************************
//***********************************************************************************************************************************


}
