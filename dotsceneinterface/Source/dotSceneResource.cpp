/*
-----------------------------------------------------------------------------
Original file:	???
New Author:		Balazs Hajdics (wolverine_@freemail.hu)

Copyright (c) 2007 Balazs Hajdics

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.

-----------------------------------------------------------------------------
*/

#include "dotSceneStableHeaders.h"
#include "dotSceneResource.h"
#include "dotSceneResourceManager.h"

namespace Ogre
{
	//***************************************************************************************************
	namespace dsi
	{
		//***************************************************************************************************
		dotSceneResource::dotSceneResource()
		{
		}
		//***************************************************************************************************
		dotSceneResource::~dotSceneResource()
		{
			unload(); 
		}
		//***************************************************************************************************
		dotSceneResource::dotSceneResource(ResourceManager *creator, const String &name, ResourceHandle handle, const String &group, bool isManual, ManualResourceLoader *loader)
			:Resource(creator, name, handle, group, isManual, loader)
		{
		}
		//***************************************************************************************************
		void dotSceneResource::loadImpl()
		{
			if (mData.isNull())
				DataStreamPtr mData = ResourceGroupManager::getSingleton().openResource(mName, mGroup);
		}
		//***************************************************************************************************
		void dotSceneResource::unloadImpl()
		{
			if (!mData.isNull())
			{
				delete mData.getPointer();
				mData.setNull();
			}
		}
		//***************************************************************************************************
		DataStreamPtr dotSceneResource::getDataStream()
		{
			if (isLoaded() && !mData.isNull())
				return mData;
			else
				return DataStreamPtr();
		}
	}
}
