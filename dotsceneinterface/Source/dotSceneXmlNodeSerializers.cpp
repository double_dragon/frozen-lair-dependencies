/*
-----------------------------------------------------------------------------
Author:		Ihor Tregubov, Daniel Banky (ViveTech Ltd., daniel.banky@vivetech.com)

Copyright (c) 2013 Ihor Tregubov and Daniel Banky (Vivetech Ltd.)

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.

-----------------------------------------------------------------------------
*/

#include "dotSceneStableHeaders.h"

#include "dotSceneXmlNodeProcessors.h"
#include "dotScenePlatformUtils.h"

#include "dotSceneInfo.h"
#include "dotSceneUtils.h"
// --------------------------------------------------- XmlNodeProcessor includes ---------------------------------------------------
#include "XNPPrerequisites.h"
#include "XNPXmlNodeProcessor.h"
#include "XNPXmlNodeProcessorTreeBuilder.h"


// -------------------------------------------------------- Tinyxml includes -------------------------------------------------------
#include "tinyxml.h"


// --------------------------------------------------------- Ogre includes ---------------------------------------------------------
#include "Ogre.h"


// -------------------------------------------------------- HDRLib includes --------------------------------------------------------
//#include "HDRCompositor.h"

namespace Ogre {

    const String& StringAttribute( const String& strValue, const String& strDefaultValue )
    {
        if( strValue.length() == 0 && strDefaultValue.length() != 0 )
        {
            return strDefaultValue;
        }
        else
        {
            return strValue;
        }
    }

    
    // ------------------------------------------------ SceneElementProcessor --------------------------------------------------------

    bool SceneElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        elementsData->push_back( parentData );
        return true;
    }

    void SceneElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        SceneManager* pSceneManager = static_cast<SceneManager*>(pElementData);
		dsi::DotSceneProcessorImpl* pDotSceneProcessor = static_cast<dsi::DotSceneProcessorImpl*>(mTreeBuilder->getCommonParameter("DotSceneProcessor"));

		mDotSceneInfo = (dsi::DotSceneInfo*)mTreeBuilder->getCommonParameter("DotSceneInfo");

		if ( pDotSceneProcessor->getVersion().compare( BLANKSTRING ) )
			SETATTRIB( "formatVersion", pDotSceneProcessor->getVersion() );

		if ( mDotSceneInfo->getSceneID().compare( BLANKSTRING ) )
			SETATTRIB( "id", mDotSceneInfo->getSceneID() );
        
        SETATTRIB( "sceneManagerType", pSceneManager->getTypeName() );
        SETATTRIB( "sceneManagerName", pSceneManager->getName() );

		if ( mDotSceneInfo->getSceneAuthor().compare( BLANKSTRING ) )
			SETATTRIB( "author", mDotSceneInfo->getSceneAuthor() );

        String sOgreVersion( 
            StringConverter::toString( OGRE_VERSION_MAJOR ) + "." + StringConverter::toString( OGRE_VERSION_MINOR  ) + "." + StringConverter::toString( OGRE_VERSION_PATCH ) );
        SETATTRIBDEFAULT( "minOgreVersion", sOgreVersion, "1.4.9" );

		SETATTRIBDEFAULT( "showBoundingBoxes", StringConverter::toString( pSceneManager->getShowBoundingBoxes() ), "false" );
    }
    // ------------------------------------------------ SceneElementProcessor --------------------------------------------------------

    // ------------------------------------------------ SceneManagerOptionElementProcessor --------------------------------------------------------
    bool SceneManagerOptionElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        bool bResult = false;

        SceneManager* pSceneManager = static_cast<SceneManager*>(parentData);

        StringVector refKeys;
        if( pSceneManager->getOptionKeys( refKeys ) )
        {
            for( size_t i = 0; i < refKeys.size(); i++ )
            {
                elementsData->push_back( (void*)new String( refKeys[i] ) ); 
                bResult = true;
            }
        }
        else
        {
            if (mDotSceneInfo)mDotSceneInfo->logLoadInfo( "Scene Manager has no Option attributes.\n" );
            
        }
        return bResult;
    }

    void SceneManagerOptionElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        String pOptionName( ((String*)pElementData)->c_str() );
        SceneManager* pSceneManager = ((SceneManager*)mTreeBuilder->getCommonParameter("SceneManager"));

        if( pOptionName.compare( "Size" )  == 0 )
        {
            /* \TODO Implement AxisAlignedBox parsing serialisation.
            AxisAlignedBox axisAlignedBox;
            pSceneManager->getOption( pOptionName, &axisAlignedBox );
            SETATTRIB( "name", pOptionName );
            SETATTRIB( "type", String( "AxisAlignedBox") );
            SETATTRIB( "value", 
                SerializeValueBasedOnType( &axisAlignedBox, "AxisAlignedBox", mElementName, mFilename, mDotSceneInfo ) );
            */
        }
        if( pOptionName.compare( "Depth" )  == 0 )
        {
            int depth;
            pSceneManager->getOption( pOptionName, &depth );

            SETATTRIB( "name", pOptionName );
            SETATTRIB( "type", String( "int" ) );
            SETATTRIB( "value", 
                SerializeValueBasedOnType( &depth, "int", mElementName, mFilename, mDotSceneInfo ) );
        }
        if( pOptionName.compare( "ShowOctree" ) == 0)
        {
            bool showOctree;
            pSceneManager->getOption( pOptionName, &showOctree );

            SETATTRIB( "name", pOptionName );
            SETATTRIB( "type", String( "bool" ) );
            SETATTRIB( "value", 
                SerializeValueBasedOnType( &showOctree, "bool", mElementName, mFilename, mDotSceneInfo ) );
        }
    }

    void SceneManagerOptionElementProcessor::serializeElementEndImpl(TiXmlElement& currentNode, void* pParentData, void* pElementData )
    {
        if( pElementData )
            delete (String*)pElementData;
    }
    // ------------------------------------------------ SceneElementProcessor --------------------------------------------------------
    
    // ------------------------------------------------ ShadowSettingsElementProcessor --------------------------------------------------------
    Ogre::String ShadowSettingsElementProcessor::serializeAttribute_shadowTechnique( TiXmlElement* currentNode, Ogre::ShadowTechnique technique )
    {
        String strShadowTechnique = "none";
        switch( technique )
        {
        case Ogre::SHADOWTYPE_NONE:
            {
                strShadowTechnique = "none";
            }break;
        case Ogre::SHADOWTYPE_STENCIL_MODULATIVE:
            {
                strShadowTechnique = "stencilModulative";
            }break;
        case Ogre::SHADOWTYPE_STENCIL_ADDITIVE:
            {
                strShadowTechnique = "stencilAdditive";
            }break;
        case Ogre::SHADOWTYPE_TEXTURE_MODULATIVE:
            {
                strShadowTechnique = "textureModulative";
            }break;
        case Ogre::SHADOWTYPE_TEXTURE_ADDITIVE:
            {
                strShadowTechnique = "textureAdditive";
            }break;
        case Ogre::SHADOWTYPE_TEXTURE_MODULATIVE_INTEGRATED:
            {
                strShadowTechnique = "textureModulativeIntegrated";
            }break;
        case Ogre::SHADOWTYPE_TEXTURE_ADDITIVE_INTEGRATED:
            {
                strShadowTechnique = "textureAdditiveIntegrated";
            }break;
        default:
            {
                if (mDotSceneInfo) mDotSceneInfo->logLoadError(
                    "Attribute shadowTechnique of element \"" + String(currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value!\n"
                    "Valid values are \"none\", \"stencilModulative\", \"stencilAdditive\", \"textureModulative\", \"textureAdditive\", \"textureAdditive\", \"textureModulativeIntegrated\" and \"textureAdditiveIntegrated\".\n"
                    "\" attribute will now default to \"none\".\n"
                    "See the dotsceneformat.dtd for more information.\n");
            }break;
        }

        return strShadowTechnique;
    }
    
    bool ShadowSettingsElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        elementsData->push_back( parentData );
        return true;
    }

    void ShadowSettingsElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        SceneManager* pSceneManager = static_cast<SceneManager*>( pElementData );

        SETATTRIB( "shadowTechnique", serializeAttribute_shadowTechnique( &currentNode, pSceneManager->getShadowTechnique() ) );
        SETATTRIBDEFAULT( "showDebugShadows", StringConverter::toString( pSceneManager->getShowDebugShadows() ), "false" );
        SETATTRIBDEFAULT( "shadowFarDistance", StringConverter::toString( pSceneManager->getShadowFarDistance() ), "0" );

        // ---------------------------------------- Attributes specific for stencil based shadows ----------------------------------------
        SETATTRIBDEFAULT( "shadowDirectionalLightExtrusionDistance", 
             StringConverter::toString( pSceneManager->getShadowDirectionalLightExtrusionDistance() ), "10000" );

        SETATTRIBDEFAULT( "shadowIndexBufferSize", 
             StringConverter::toString( pSceneManager->getShadowIndexBufferSize() ), "51200" );

        //FIXME SETATTRIBDEFAULT( "shadowUseInfiniteFarPlane", StringConverter::toString( pSceneManager->getShadowUseInfiniteFarPlane()), "true" );

        // ---------------------------------------- Attributes specific for texture based shadows ----------------------------------------

        SETATTRIBDEFAULT( "shadowTextureCount", 
             StringConverter::toString( pSceneManager->getShadowTextureCount() ), "0" );

        //! We got only the first element. All elements are the same.
        ConstShadowTextureConfigIterator itShadowTextureConfig = pSceneManager->getShadowTextureConfigIterator();
        if( itShadowTextureConfig.hasMoreElements() )
        {
            ShadowTextureConfig shadowTextureConfig = itShadowTextureConfig.getNext();

            //! pShadowTextureConfig->width and pShadowTextureConfig->height must be the same
            SETATTRIBDEFAULT( "shadowTextureSize", 
                StringConverter::toString( shadowTextureConfig.width ), "0" );

            SETATTRIBDEFAULT( "shadowTexturePixelFormat", 
                EnumPixelFormatToStringPixelFormat( shadowTextureConfig.format ), "PF_UNKNOWN" );
        }

        SETATTRIBDEFAULT( "shadowDirLightTextureOffset", 
            StringConverter::toString( pSceneManager->getShadowDirLightTextureOffset() ), "0.6" );

        //FIXME SETATTRIBDEFAULT( "shadowTextureFadeStart", StringConverter::toString( pSceneManager->getShadowTextureFadeStart() ), "0.7" );
        //FIXME SETATTRIBDEFAULT( "shadowTextureFadeEnd", StringConverter::toString( pSceneManager->getShadowTextureFadeEnd() ), "0.9" );

		SETATTRIBDEFAULT( "shadowTextureSelfShadow", 
            StringConverter::toString( pSceneManager->getShadowTextureSelfShadow() ), "false" );

//FIXME 		if ( pSceneManager->getShadowTextureCasterMaterial() != BLANKSTRING )
//FIXME 			SETATTRIB( "shadowTextureCasterMaterial", pSceneManager->getShadowTextureCasterMaterial() );

//FIXME 		if ( pSceneManager->getShadowTextureReceiverMaterial() != BLANKSTRING )
//FIXME 			SETATTRIB( "shadowTextureReceiverMaterial", pSceneManager->getShadowTextureReceiverMaterial()  );

        SETATTRIBDEFAULT( "shadowCasterRenderBackFaces", 
            StringConverter::toString( pSceneManager->getShadowCasterRenderBackFaces() ), "true" );
    }
    // ------------------------------------------------ SceneElementProcessor ------------------------

    // ------------------------------------------------ ShadowTextureConfigElementProcessor --------------------------------------------------------
    bool ShadowTextureConfigElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        bool bResult = false;

        SceneManager* pSceneManager = static_cast<SceneManager*>(parentData);
        ConstShadowTextureConfigIterator shadowTextureConfigIterator = pSceneManager->getShadowTextureConfigIterator();
        
        while( shadowTextureConfigIterator.hasMoreElements() )
        {
            elementsData->push_back( new ShadowTextureConfig( shadowTextureConfigIterator.getNext() ) );
            bResult = true;
        }
        return bResult;
    }

    void ShadowTextureConfigElementProcessor::serializeElementEndImpl(TiXmlElement& currentNode, void* pParentData, void* pElementData )
    {
        if( pElementData )
            delete (ShadowTextureConfig*)pElementData;
    }

    void ShadowTextureConfigElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        ShadowTextureConfig* pShadowTextureConfig = static_cast<ShadowTextureConfig*>( pElementData );

        SETATTRIBDEFAULT( "shadowIndex", StringConverter::toString( counter ), "0" );
        SETATTRIBDEFAULT( "width", StringConverter::toString( pShadowTextureConfig->width ), "0" );
        SETATTRIBDEFAULT( "height", StringConverter::toString( pShadowTextureConfig->height ), "0" );
        SETATTRIBDEFAULT( "pixelFormat", 
            EnumPixelFormatToStringPixelFormat( pShadowTextureConfig->format ), "PF_UNKNOWN" );
    }
    // ------------------------------------------------ ShadowTextureConfigElementProcessor ---------------------------

    // ------------------------------------------------ ResourceGroupElementProcessor --------------------------------------------------------
    bool ResourceGroupElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
		StringVector resourceGroupNames = ResourceGroupManager::getSingleton().getResourceGroups();
		
		for (StringVector::iterator it = resourceGroupNames.begin(); it != resourceGroupNames.end(); ++it)
		{
			if (   it->compare(ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME) != 0
				&& it->compare(ResourceGroupManager::INTERNAL_RESOURCE_GROUP_NAME) != 0
				&& ! it->empty()
				&& ResourceGroupManager::getSingleton().isResourceGroupLoaded( *it )
				)
			{
				elementsData->push_back( (void*) new String( *it ) );
			}
		}

        return true;
    }

    void ResourceGroupElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);
		String* pResourceGroupName = static_cast<String*>(pElementData);
		
        SETATTRIB( "name", pResourceGroupName->c_str() );
    }

	void ResourceGroupElementProcessor::serializeElementEndImpl(TiXmlElement& currentNode, void* pParentData, void* pElementData )
	{
		if( pElementData )
			delete (String*)pElementData;
	}
    // ------------------------------------------------ ResourceGroupElementProcessor ---------------------------
    

    // ------------------------------------------------ ResourceLocationElementProcessor --------------------------------------------------------
    Ogre::String ResourceLocationElementProcessor::serializAttribute_relativeTo( TiXmlElement* currentNode, RelativeTo relativeTo )
    {
        String sRelativeTo;

        switch( relativeTo )
        {
        case ResourceLocationElementProcessor::RT_DOT_SCENE_FILE_LOCATION:
            {
                sRelativeTo = "dotSceneFileLocation";
            }break;
        case ResourceLocationElementProcessor::RT_EXE_FILE_LOCATION:
            {
                sRelativeTo = "exeFileLocation";
            }break;
        case ResourceLocationElementProcessor::RT_CURRENT_WORKING_DIRECTORY:
            {
                sRelativeTo = "currentWorkingDir";
            }break;
        case ResourceLocationElementProcessor::RT_ABSOLUTE:
            {
                sRelativeTo = "absolute";
            }break;
        default:
            {
                if (mDotSceneInfo) mDotSceneInfo->logLoadError(
                    "Attribute \"relativeTo\" of element \"" + String(currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value: \"" + StringConverter::toString(relativeTo) + "\"!\n"
                    "Valid values are \"dotSceneFileLocation\", \"exeFileLocation\", \"currentWorkingDir\" and \"absolute\".\n"
                    "\"relativeTo\" attribute will now default to \"dotSceneFileLocation\".\n"
                    "See the dotsceneformat.dtd for more information.\n");
                sRelativeTo = "dotSceneFileLocation";
            }break;
        }
        return sRelativeTo;
    }

    bool ResourceLocationElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {

        bool bResult = false;

		String resourceGroupName = *( static_cast<String*>( parentData ) );

		ResourceGroupManager::LocationList resources = ResourceGroupManager::getSingleton().getResourceLocationList( resourceGroupName );
		ResourceGroupManager::LocationList::iterator resource = resources.begin();
		ResourceGroupManager::LocationList::iterator resourceEnd = resources.end();
			
		for ( ; resource != resourceEnd; ++resource )
		{
			ResourceGroupManager::ResourceLocation* res = new ResourceGroupManager::ResourceLocation();
			res->archive = (*resource)->archive;
			res->recursive = (*resource)->recursive;

			elementsData->push_back( (void*) res );

			bResult = true;
		}

        return bResult;
    }

    void ResourceLocationElementProcessor::serializeElementEndImpl(TiXmlElement& currentNode, void* pParentData, void* pElementData )
    {
        
        if( pElementData )
            delete (ResourceGroupManager::ResourceLocation*) pElementData;
        
    }

    void ResourceLocationElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);
        
        ResourceGroupManager::ResourceLocation* pResLoc = static_cast<ResourceGroupManager::ResourceLocation*>(pElementData);

		String currentDirectory = StringUtil::normalizeFilePath(mFilename, OGRE_PLATFORM == OGRE_PLATFORM_WIN32);
		currentDirectory = currentDirectory.substr( 0, currentDirectory.find_last_of( '/' ) + 1 );

		if (pResLoc)
		{
			Archive* pArchive = pResLoc->archive;
			
			if( pArchive )
			{
				String name = dsi::utils::getRelativeFilename(currentDirectory, pArchive->getName());

				SETATTRIB( "name", name );
				SETATTRIB( "type", pArchive->getType() );
				SETATTRIB( "recursive", StringConverter::toString( pResLoc->recursive ) );
				
				if ( ( name.length() >= 2 && name[1] == ':' ) || ( name.length() >= 1 && name[0] == '/' ) )
				{
					SETATTRIB( "relativeTo", serializAttribute_relativeTo( &currentNode, ResourceLocationElementProcessor::RT_ABSOLUTE ) );
				}
				else
				{
					SETATTRIB( "relativeTo", serializAttribute_relativeTo( &currentNode, ResourceLocationElementProcessor::RT_DOT_SCENE_FILE_LOCATION ) );
				}
			}
		}
    }
    // ------------------------------------------------ ResourceLocationElementProcessor ---------------------------

    // ------------------------------------------------ ResourceDeclarationElementProcessor --------------------------------------------------------
    bool ResourceDeclarationElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        bool bResult = false;

		String resourceGroupName = ( (String*) parentData )->c_str();
		
		ResourceGroupManager::ResourceDeclarationList resourceDeclarationList =
			ResourceGroupManager::getSingleton().getResourceDeclarationList( resourceGroupName );

		ResourceGroupManager::ResourceDeclarationList::iterator itDeclarationList = resourceDeclarationList.begin();
		ResourceGroupManager::ResourceDeclarationList::iterator itDeclarationListEnd = resourceDeclarationList.end();
		for( ; itDeclarationList != itDeclarationListEnd; ++itDeclarationList )
		{
			elementsData->push_back( new ResourceGroupManager::ResourceDeclaration( (*itDeclarationList) ) );
			bResult = true;
		}   

        return bResult;
    }

    void ResourceDeclarationElementProcessor::serializeElementEndImpl(TiXmlElement& currentNode, void* pParentData, void* pElementData )
    {
        if( pElementData )
            delete (ResourceGroupManager::ResourceDeclaration*)pElementData;
    }

    void ResourceDeclarationElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        ResourceGroupManager::ResourceDeclaration* pResourceDeclaration =
            static_cast<ResourceGroupManager::ResourceDeclaration*>(pElementData);

        SETATTRIB( "name", pResourceDeclaration->resourceName );
        SETATTRIB( "resourceType", pResourceDeclaration->resourceType );
    }
    // ------------------------------------------------ ResourceDeclarationElementProcessor ---------------------------

    // ------------------------------------------------ NameValuePairListElementProcessor --------------------------------------------------------
    bool NameValuePairListElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        elementsData->push_back( parentData );
        return true;
    }

    void NameValuePairListElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);
        //! nothing to do here
    }
    // ------------------------------------------------ NameValuePairListElementProcessor ---------------------------

    // ------------------------------------------------ NameValuePairElementProcessor --------------------------------------------------------
    bool NameValuePairElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        bool bResult = false;

        ResourceGroupManager::ResourceDeclaration* pResourceDeclaration =
            static_cast<ResourceGroupManager::ResourceDeclaration*>(parentData);

        NameValuePairList::iterator itNameValuePairList = pResourceDeclaration->parameters.begin();
        NameValuePairList::iterator itNameValuePairListEnd = pResourceDeclaration->parameters.end(); 

        for( ; itNameValuePairList != itNameValuePairListEnd; ++itNameValuePairList )
        {
            elementsData->push_back( new NameValuePairType( (*itNameValuePairList) ) );
            bResult = true;
        }
        return bResult;
    }

    void NameValuePairElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        NameValuePairType* pNameValuePairType = static_cast<NameValuePairType*>( pElementData );

        SETATTRIB( "value_name", pNameValuePairType->first );
        SETATTRIB( "value", pNameValuePairType->second );
    }
    // ------------------------------------------------ NameValuePairElementProcessor ---------------------------

    // ------------------------------------------------ RenderTargetsElementProcessor --------------------------------------------------------
    bool RenderTargetsElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        elementsData->push_back( parentData );
        return true;
    }

    void RenderTargetsElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);
        //! Do nothing
    }
    // ------------------------------------------------ RenderTargetsElementProcessor --------------------------------------------------------

    // ------------------------------------------------ RenderWindowElementProcessor --------------------------------------------------------
    bool RenderWindowElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        bool bResult = false;

        RenderSystem::RenderTargetIterator renderTargetIterator = Root::getSingleton().getRenderSystem()->getRenderTargetIterator();
        while ( renderTargetIterator.hasMoreElements() )
        {
			RenderTarget* renderTarget = renderTargetIterator.getNext();
			if ( dsi::utils::isRenderWindow( renderTarget ) )
			{
				elementsData->push_back( static_cast<RenderWindow*>( renderTarget ) );
				bResult = true;
			}
        }
        return bResult;
    }

    void RenderWindowElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        RenderWindow* pRenderTarget = static_cast<RenderWindow*>( pElementData );
        
		String sRenderTargetName = pRenderTarget->getName();
        SETATTRIB( "name", sRenderTargetName );
		
		unsigned int width, height, colourDepth;
		int left, top;
		pRenderTarget->getMetrics (width, height, colourDepth, left, top);
        SETATTRIB( "width", StringConverter::toString( width ) );
        SETATTRIB( "height", StringConverter::toString( height ) );
		SETATTRIB( "colourDepth", StringConverter::toString( colourDepth ) );
		SETATTRIB( "left", StringConverter::toString( left ) );
		SETATTRIB( "top", StringConverter::toString( top ) );
        
		SETATTRIB( "priority", StringConverter::toString( pRenderTarget->getPriority() ) );
        SETATTRIBDEFAULT( "active", StringConverter::toString( pRenderTarget->isActive() ), "true" );
        SETATTRIBDEFAULT( "autoUpdated", StringConverter::toString( pRenderTarget->isAutoUpdated() ), "true" );
        SETATTRIBDEFAULT( "primary", StringConverter::toString( pRenderTarget->isPrimary() ), "true" );       
        SETATTRIBDEFAULT( "fullscreen", StringConverter::toString( pRenderTarget->isFullScreen() ), "false" );
        SETATTRIBDEFAULT( "visible", StringConverter::toString( pRenderTarget->isVisible() ), "true" );
		SETATTRIBDEFAULT( "FSAA", StringConverter::toString( pRenderTarget->getFSAA() ), "0" );

#if ((OGRE_VERSION_MAJOR == 1 && OGRE_VERSION_MINOR >= 8) || OGRE_VERSION_MAJOR >= 2)
		SETATTRIBDEFAULT( "vsync", StringConverter::toString( pRenderTarget->isVSyncEnabled() ), "true" );
#endif

    }
    // ------------------------------------------------ RenderWindowElementProcessor --------------------------------------------------------
    // ------------------------------------------------ EnvironmentElementProcessor --------------------------------------------------------
    bool EnvironmentElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        elementsData->push_back( parentData );
        return true;
    }

    void EnvironmentElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);
        //! Do nothing
    }
    // ------------------------------------------------ EnvironmentElementProcessor -----------------------
    // ------------------------------------------------ FogElementProcessor --------------------------------------------------------
   Ogre::String FogElementProcessor::serializeAttribute_FogMode( TiXmlElement* currentNode, Ogre::FogMode fogMode )
   {
       String strFogMode = "none";
       switch( fogMode )
       {
       case Ogre::SHADOWTYPE_NONE:
           {
               strFogMode = "none";
           }break;
       case Ogre::FOG_EXP:
           {
               strFogMode = "exp";
           }break;
       case Ogre::FOG_EXP2:
           {
               strFogMode = "exp2";
           }break;
       case Ogre::FOG_LINEAR:
           {
               strFogMode = "linear";
           }break;
       default:
           {
               if (mDotSceneInfo) mDotSceneInfo->logLoadError(
                   "Attribute FogMode of element \"" + String(currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value!\n"
                   "Valid values are \"none\", \"exp\", \"exp2\", \"linear\" and \"textureAdditiveIntegrated\".\n"
                   "\" attribute will now default to \"none\".\n"
                   "See the dotsceneformat.dtd for more information.\n");
           }break;
       }

       return strFogMode;
   }
    
    bool FogElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        elementsData->push_back( parentData );
        return true;
    }

    void FogElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        SceneManager* pSceneManager = static_cast<SceneManager*>(pElementData);

        SETATTRIB( "mode", serializeAttribute_FogMode( &currentNode, pSceneManager->getFogMode() ) );

        ColourValue col = pSceneManager->getFogColour();
        SETATTRIBDEFAULT( "colourR", StringConverter::toString(col.r), "0.0" );
        SETATTRIBDEFAULT( "colourG", StringConverter::toString(col.g), "0.0" );
        SETATTRIBDEFAULT( "colourB", StringConverter::toString(col.b), "0.0" );

        SETATTRIBDEFAULT( "expDensity", StringConverter::toString( pSceneManager->getFogDensity()), "0.001" );
        SETATTRIBDEFAULT( "linearStart", StringConverter::toString( pSceneManager->getFogStart()), "0.0" );
        SETATTRIBDEFAULT( "linearEnd", StringConverter::toString( pSceneManager->getFogEnd()), "1.0" );
    }
    // ------------------------------------------------ FogElementProcessor --------------------------------------------------------
    // ------------------------------------------------ SkyBoxElementProcessor --------------------------------------------------------
    bool SkyBoxElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        SceneManager* pSceneManager = static_cast<SceneManager*>(parentData);
        SceneNode* pSkyBoxNode = pSceneManager->getSkyBoxNode();
        if( pSkyBoxNode )
        {
            elementsData->push_back( pSkyBoxNode );
            return true;
        }
        return false;
    }

    void SkyBoxElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        SceneNode* pSkyBoxNode = static_cast<SceneNode*>(pElementData);
        SceneManager* pSceneManager = (SceneManager*)mTreeBuilder->getCommonParameter("SceneManager");
        SceneManager::SkyBoxGenParameters skyBoxGenParameters = pSceneManager->getSkyBoxGenParameters();

        String sSkyBoxNodeName = pSkyBoxNode->getName();
        SETATTRIB( "name", sSkyBoxNodeName );
        SETATTRIBDEFAULT( "enabled", StringConverter::toString( pSceneManager->isSkyBoxEnabled() ), "true" );

        SETATTRIBDEFAULT( "distance", 
            StringConverter::toString( skyBoxGenParameters.skyBoxDistance ), "5000.0" );

//FIXME 		SETATTRIBDEFAULT( "drawFirst", StringConverter::toString(  skyBoxGenParameters.skyBoxDrawFirst ), "true" );
//FIXME 		SETATTRIBDEFAULT( "materialName", skyBoxGenParameters.skyBoxMaterialName, "" );
    }
    // ------------------------------------------------ SceneElementProcessor --------------------------------------------------------

    // ------------------------------------------------ SkyDomeElementProcessor --------------------------------------------------------
    bool SkyDomeElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        SceneManager* pSceneManager = static_cast<SceneManager*>(parentData);
        SceneNode* pSkyDomeNode = pSceneManager->getSkyDomeNode();
        if( pSkyDomeNode )
        {
            elementsData->push_back( pSkyDomeNode );
            return true;
        }
        return false;
    }

    void SkyDomeElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        SceneNode* pSkyDomeNode = static_cast<SceneNode*>(pElementData);
        SceneManager* pSceneManager = (SceneManager*)mTreeBuilder->getCommonParameter("SceneManager");
        SceneManager::SkyDomeGenParameters skyDomeGenParameters = pSceneManager->getSkyDomeGenParameters();

        String sSkyDomeNodeName = pSkyDomeNode->getName();
        SETATTRIB( "name", sSkyDomeNodeName );
        SETATTRIBDEFAULT( "enabled", StringConverter::toString( pSceneManager->isSkyDomeEnabled() ), "true" );

        Entity* pEntity = (Entity*)pSkyDomeNode->getAttachedObject( "SkyDomePlane0" );
        if( pEntity )
        {
            SubEntity* pSubEntity = pEntity->getSubEntity( 0 );
            if( pSubEntity )
            {
                SETATTRIB( "materialName", pSubEntity->getMaterialName() );
            }
        }
        SETATTRIBDEFAULT( "curvature", StringConverter::toString(  skyDomeGenParameters.skyDomeCurvature ), "10.0" );
        SETATTRIBDEFAULT( "tiling", StringConverter::toString(  skyDomeGenParameters.skyDomeTiling ), "8.0" );
        SETATTRIBDEFAULT( "distance", StringConverter::toString(  skyDomeGenParameters.skyDomeDistance ), "4000.0" );
        SETATTRIBDEFAULT( "xsegments", StringConverter::toString(  skyDomeGenParameters.skyDomeXSegments ), "16" );
        SETATTRIBDEFAULT( "ysegments", StringConverter::toString(  skyDomeGenParameters.skyDomeYSegments ), "16" );
        SETATTRIBDEFAULT( "ysegments_keep", StringConverter::toString(  skyDomeGenParameters.skyDomeYSegments_keep ), "-1" );
//FIXME 		SETATTRIBDEFAULT( "drawFirst", StringConverter::toString(  skyDomeGenParameters.skyDomeDrawFirst ), "true" );
    }
    // ------------------------------------------------ SkyDomeElementProcessor --------------------------------------------------------

    // ------------------------------------------------ SkyPlaneElementProcessor --------------------------------------------------------
    bool SkyPlaneElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        SceneManager* pSceneManager = static_cast<SceneManager*>(parentData);
        SceneNode* pSkyPlaneNode = pSceneManager->getSkyPlaneNode();
        if( pSkyPlaneNode )
        {
            elementsData->push_back( pSkyPlaneNode );
            return true;
        }
        return false;
    }

    void SkyPlaneElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        SceneNode* pSkyPlaneNode = static_cast<SceneNode*>(pElementData);
        SceneManager* pSceneManager = (SceneManager*)mTreeBuilder->getCommonParameter("SceneManager");
        SceneManager::SkyPlaneGenParameters skyPlaneGenParameters = pSceneManager->getSkyPlaneGenParameters();
//FIXME 		Plane skyPlaneGenPlane = pSceneManager->getSkyPlaneGenPlane();

        String sSkyPlaneNodeName = pSkyPlaneNode->getName();
        SETATTRIB( "name", sSkyPlaneNodeName );
        SETATTRIBDEFAULT( "enabled", StringConverter::toString( pSceneManager->isSkyPlaneEnabled() ), "true" );

        Entity* pEntity = (Entity*)pSkyPlaneNode->getAttachedObject( pSceneManager->getName() + "SkyPlane" );
        if( pEntity )
        {
            SubEntity* pSubEntity = pEntity->getSubEntity( 0 );
            if( pSubEntity )
            {
                SETATTRIB( "materialName", pSubEntity->getMaterialName() );
            }
        }

        SETATTRIBDEFAULT( "scale", StringConverter::toString(  skyPlaneGenParameters.skyPlaneScale ), "1000" );
        SETATTRIBDEFAULT( "tiling", StringConverter::toString(  skyPlaneGenParameters.skyPlaneTiling ), "10" );
        SETATTRIBDEFAULT( "bow", StringConverter::toString(  skyPlaneGenParameters.skyPlaneBow ), "0" );
        SETATTRIBDEFAULT( "xsegments", StringConverter::toString(  skyPlaneGenParameters.skyPlaneXSegments ), "1" );
        SETATTRIBDEFAULT( "ysegments", StringConverter::toString(  skyPlaneGenParameters.skyPlaneYSegments ), "1" );
//FIXME 		SETATTRIBDEFAULT( "drawFirst", StringConverter::toString(  skyPlaneGenParameters.skyPlaneDrawFirst ), "true" );
//FIXME 		SETATTRIBDEFAULT( "planeX", StringConverter::toString( skyPlaneGenPlane.normal.x ), "0"  );
//FIXME 		SETATTRIBDEFAULT( "planeY", StringConverter::toString( skyPlaneGenPlane.normal.y ), "-1" );
//FIXME 		SETATTRIBDEFAULT( "planeZ", StringConverter::toString( skyPlaneGenPlane.normal.z ), "0" );
//FIXME 		SETATTRIBDEFAULT( "planeD", StringConverter::toString( skyPlaneGenPlane.d ), "5000" );
    }
    // ------------------------------------------------ SceneElementProcessor --------------------------------------------------------

    // ------------------------------------------------ ColourAmbientElementProcessor --------------------------------------------------------
    bool ColourAmbientElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        elementsData->push_back( parentData );
        return true;
    }
    
    void ColourAmbientElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        SceneManager* pSceneManager = static_cast<SceneManager*>( pElementData );

        ColourValue col = pSceneManager->getAmbientLight();
        SETATTRIBDEFAULT( "r", StringConverter::toString(col.r), "0.0" );
        SETATTRIBDEFAULT( "g", StringConverter::toString(col.g), "0.0" );
        SETATTRIBDEFAULT( "b", StringConverter::toString(col.b), "0.0" );
    }
    // ------------------------------------------------ ColourAmbientElementProcessor ---------------------------

    // ------------------------------------------------ ColourBackgroundElementProcessor --------------------------------------------------------
    bool ColourBackgroundElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        RenderWindow* pRenderWindow = (RenderWindow*)mTreeBuilder->getCommonParameter("RenderWindow");
        if( pRenderWindow )
        {
            elementsData->push_back( pRenderWindow->getViewport( 0 ) );
            return true;
        }
        return false;
    }

    void ColourBackgroundElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        Viewport* pViewport = static_cast<Viewport*>(pElementData);

        if( pViewport )
        {
            ColourValue col = pViewport->getBackgroundColour();
            SETATTRIBDEFAULT( "r", StringConverter::toString(col.r), "0.0" );
            SETATTRIBDEFAULT( "g", StringConverter::toString(col.g), "0.0" );
            SETATTRIBDEFAULT( "b", StringConverter::toString(col.b), "0.0" );
        }
        else
        {
            if (mDotSceneInfo)mDotSceneInfo->logLoadInfo(
                "Error getting viewport.\n" );
        }
    }
    // ------------------------------------------------ ColourBackgroundElementProcessor ---------------------------

    // ------------------------------------------------ MovableObjectElementProcessor ---------------------------
    void MovableObjectElementProcessor::serializeAttribute_Visible( TiXmlElement& parentNode, bool IsVisible )
    {
        parentNode.SetAttribute( "visible", StringConverter::toString( IsVisible ) );
    }
    // ------------------------------------------------ MovableObjectElementProcessor ---------------------------
    
    // ------------------------------------------------ NodesElementProcessor --------------------------------------------------------
    bool NodesElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        SceneManager* pSceneManager = static_cast<SceneManager*>( parentData );
        elementsData->push_back( pSceneManager->getRootSceneNode() );
        return true;
    }
    void NodesElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

		SETATTRIB( "visible", "true" );
		SETATTRIB( "cascadeVisibility", "false" );
    }
    // ------------------------------------------------ NodesElementProcessor ---------------------------

    // ------------------------------------------------ PositionElementProcessor --------------------------------------------------------
    bool PositionElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        Vector3 v3Position;

        if( strcmp( strParentName, "light" ) == 0 )
        {
            Light* pLight = static_cast<Light*>( parentData );
            v3Position = pLight->getPosition();
        }
        else if( strcmp( strParentName, "camera" ) == 0 )
        {
            Camera* pCamera = static_cast<Camera*>( parentData );
            v3Position = pCamera->getPosition();
        }
        else if( strcmp( strParentName, "billboard" ) == 0 )
        {
            Billboard* pBillboard = static_cast<Billboard*>( parentData );
            v3Position = pBillboard->getPosition();
        }
        else if( strcmp( strParentName, "lookTarget" ) == 0 )
        {
            LookTargetData* pLookTargetData = static_cast<LookTargetData*>( parentData );
            v3Position = pLookTargetData->mLookPosition;
        }
        else if( strcmp( strParentName, "transformKeyFrame" ) == 0 )
        {
            TransformKeyFrame* pTransformKeyFrame = static_cast<TransformKeyFrame*>( parentData );
            v3Position = pTransformKeyFrame->getTranslate();
        }
        else // Nodes Node LookTarget
        {
            SceneNode* pNode = static_cast<SceneNode*>( parentData );
            v3Position = pNode->getPosition();
        }

        elementsData->push_back( new Vector3( v3Position ) );
        return true;
    }

    void PositionElementProcessor::serializeElementEndImpl(TiXmlElement& currentNode, void* pParentData, void* pElementData )
    {
        if( pElementData )
        {
            delete (Vector3*)pElementData;
        }
    }
    
    void PositionElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        Vector3* pVector3 = static_cast<Vector3*>(pElementData);

        SETATTRIB( "x", StringConverter::toString( pVector3->x ) );
        SETATTRIB( "y", StringConverter::toString( pVector3->y ) );
        SETATTRIB( "z", StringConverter::toString( pVector3->z ) );
    }
    // ------------------------------------------------ PositionElementProcessor ---------------------------

    // ------------------------------------------------ OrientationElementProcessor --------------------------------------------------------
    bool OrientationElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        Quaternion quOrientation;
            
        if( strcmp( strParentName, "camera" ) == 0 )
        {
            Camera* pCamera = static_cast<Camera*>( parentData );
            quOrientation = pCamera->getOrientation();
        }
        else if( strcmp( strParentName, "transformKeyFrame" ) == 0 )
        {
            TransformKeyFrame* pTransformKeyFrame = static_cast<TransformKeyFrame*>( parentData );
            quOrientation = pTransformKeyFrame->getRotation();
        }
        else // Nodes Node SkyBox SkyDome
        {
            SceneNode* pNode = static_cast<SceneNode*>( parentData );
            quOrientation = pNode->getOrientation();
        }

        elementsData->push_back( new Quaternion( quOrientation ) );
        return true;
    }

    void OrientationElementProcessor::serializeElementEndImpl(TiXmlElement& currentNode, void* pParentData, void* pElementData )
    {
        if( pElementData )
            delete (Quaternion*)pElementData;
    }

    void OrientationElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);
        //! Do nothing
    }
    // ------------------------------------------------ OrientationElementProcessor ---------------------------

    // ------------------------------------------------ NodeElementProcessor ---------------------------
    bool NodeElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        bool bResult = false;

        const SceneNode* pRootNodeConst = static_cast<const SceneNode*>( parentData );

        SceneNode::ConstChildNodeIterator childNodeIterator = pRootNodeConst->getChildIterator();
        while( childNodeIterator.hasMoreElements() )
        {
            elementsData->push_back( childNodeIterator.getNext() );
            bResult = true;
        }
        return bResult;
    }

    void NodeElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        SceneNode* pNode = static_cast<SceneNode*>( pElementData );

        SETATTRIB( "name", pNode->getName() );

        SETATTRIB( "id", StringConverter::toString( counter ) );       
        //! \TODO
        // Act as ele,ment processor. Perhaps read from DotSceneInfo.
        SETATTRIBDEFAULT( "isTarget", "false", "false" );

        //! "visible" Attributes are set by child MovableObject
    }
    // ------------------------------------------------ NodeElementProcessor ---------------------------

    // ------------------------------------------------ EntityElementProcessor ---------------------------
    Ogre::String EntityElementProcessor::serializeAttribute_Usage( TiXmlElement* currentNode, HardwareBuffer::Usage bufferUsage )
    {
        String sUsageString;
        
        switch( bufferUsage )
        {
        case HardwareBuffer::HBU_STATIC:
            {
                sUsageString = "static";
            }break;
        case HardwareBuffer::HBU_DYNAMIC:
            {
                sUsageString = "dynamic";
            }break;
        case HardwareBuffer::HBU_WRITE_ONLY:
            {
                sUsageString = "writeOnly";
            }break;
        case HardwareBuffer::HBU_DISCARDABLE:
            {
                sUsageString = "discardable";
            }break;
        case HardwareBuffer::HBU_STATIC_WRITE_ONLY:
            {
                sUsageString = "staticWriteOnly";
            }break;
        case HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY:
            {
                sUsageString = "dynamicWriteOnly";
            }break;
        case HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY_DISCARDABLE:
            {
                sUsageString = "dynamicWriteOnlyDiscardable";
            }break;
        default:
            {
                if (mDotSceneInfo) mDotSceneInfo->logLoadError(
                    "Invalid Usage attribute value!\n"
                    "Valid values are \"static\", \"dynamic\", \"writeOnly\", \"discardable\", \"staticWriteOnly\", \"dynamicWriteOnly\", \"dynamicWriteOnlyDiscardable\".\n"
                    "Attribute will now default to \"staticWriteOnly\".\n"
                    "See the dotsceneformat.dtd for more information.\n ");
                sUsageString = "staticWriteOnly";
            };
        }
        return sUsageString;
    }
    
    bool EntityElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        bool bResult = false;
		const SceneNode* pParentNodeConst = static_cast<const SceneNode*>( parentData );

		SceneNode::ConstObjectIterator constObjectIterator = pParentNodeConst->getAttachedObjectIterator();
		while( constObjectIterator.hasMoreElements() )
		{
			MovableObject* obj = constObjectIterator.getNext();
			if( obj->getMovableType() == EntityFactory::FACTORY_TYPE_NAME )
			{
				elementsData->push_back( obj );
				bResult = true;
			}
		}

        return bResult;
    }

    void EntityElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        Entity*  pEntity = static_cast<Entity*>( pElementData );
        MeshPtr  pEntityMesh = pEntity->getMesh();
		String   parent = parentElement.Value();


        SETATTRIB( "name", pEntity->getName() );
        SETATTRIB( "id", StringConverter::toString( counter ) );
        SETATTRIB( "meshFile", pEntityMesh->getName() );

		if( pEntity->getNumSubEntities() )
        {
            SETATTRIB( "materialName", pEntity->getSubEntity(0)->getMaterialName() );
        }

		if ( parent == "node" )
			serializeAttribute_Visible( parentElement, pEntity->getVisible() );

        if( pEntity->getNumSubEntities() )
        {
            SETATTRIBDEFAULT( "polygonModeOverrideable", StringConverter::toString( 
                pEntity->getSubEntity( 0 )->getPolygonModeOverrideable() ),"false" );
        }

        SETATTRIBDEFAULT( "displaySkeleton", StringConverter::toString( pEntity->getDisplaySkeleton() ),"false");
        
         
        SETATTRIB( "softwareAnimationRequests", StringConverter::toString( pEntity->getSoftwareAnimationRequests() ) );
        SETATTRIB( "softwareAnimationRequestsNormalsAlso", StringConverter::toString( pEntity->getSoftwareAnimationNormalsRequests() ) );
             
        SETATTRIBDEFAULT( "vertexBufferUsage", serializeAttribute_Usage( &currentNode, pEntityMesh->getVertexBufferUsage() ), "staticWriteOnly" );
        SETATTRIBDEFAULT( "vertexBufferUseShadow", StringConverter::toString( pEntityMesh->isVertexBufferShadowed() ), "false" );
             
        SETATTRIBDEFAULT( "indexBufferUsage", serializeAttribute_Usage( &currentNode, pEntityMesh->getIndexBufferUsage() ), "staticWriteOnly" );
        SETATTRIBDEFAULT( "indexBufferUseShadow", StringConverter::toString( pEntityMesh->isIndexBufferShadowed() ), "false" );
    }
    // ------------------------------------------------ EntityElementProcessor ---------------------------

    // ------------------------------------------------ LODBiasElementProcessor ---------------------------
    bool LODBiasElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        elementsData->push_back( parentData );
        return true;
    }

    void LODBiasElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        Entity* pEntity = static_cast<Entity*>( pElementData );
		
		Real factor;
		ushort maxDetailIndex, minDetailIndex;

//FIXME 		pEntity->getMeshLodBias(factor, maxDetailIndex, minDetailIndex);

        SETATTRIB( "factor", StringConverter::toString( factor ) );
        SETATTRIBDEFAULT( "maxDetailIndex", StringConverter::toString( maxDetailIndex ), "0" );
        SETATTRIBDEFAULT( "minDetailIndex", StringConverter::toString( minDetailIndex ), "99" );
    }
    // ------------------------------------------------ LODBiasElementProcessor ---------------------------

    // ------------------------------------------------ MeshLODBiasElementProcessor ---------------------------
    bool MeshLODBiasElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        elementsData->push_back( parentData );
        return true;
    }

    void MeshLODBiasElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);
        LODBiasElementProcessor::serializeElementImpl( currentNode, parentElement, pElementData, counter );
    }
    // ------------------------------------------------ MeshLODBiasElementProcessor ---------------------------
    // ------------------------------------------------ MaterialLODBiasElementProcessor ---------------------------
    bool MaterialLODBiasElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        elementsData->push_back( parentData );
        return true;
    }

    void MaterialLODBiasElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);
        LODBiasElementProcessor::serializeElementImpl( currentNode, parentElement, pElementData, counter );
    }
    // ------------------------------------------------ MaterialLODBiasElementProcessor ---------------------------

    // ------------------------------------------------ ParticleSystemElementProcessor ---------------------------
    bool ParticleSystemElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        bool bResult = false;

        const SceneNode* pParentNodeConst = static_cast<const SceneNode*>( parentData );

        SceneNode::ConstObjectIterator constObjectIterator = pParentNodeConst->getAttachedObjectIterator();
        while( constObjectIterator.hasMoreElements() )
        {
            MovableObject* obj = (MovableObject*)constObjectIterator.getNext();
            if( obj->getMovableType() == ParticleSystemFactory::FACTORY_TYPE_NAME )
            {
                elementsData->push_back( obj );
                bResult = true;
            }
        }

		return bResult;
    }

    void ParticleSystemElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);
		
		MovableObject* pMovableOject = static_cast<MovableObject*>( pElementData );

		ParticleSystem* pParticleSystem = ((SceneManager*)mTreeBuilder->getCommonParameter("SceneManager"))->getParticleSystem( pMovableOject->getName() );

        SETATTRIB( "name", pParticleSystem->getName() );
        SETATTRIB( "id", StringConverter::toString( counter ) );
        SETATTRIB( "templateName", pParticleSystem->getOrigin() );
        SETATTRIB( "materialName", pParticleSystem->getMaterialName() );

        serializeAttribute_Visible( parentElement, pParticleSystem->getVisible() );
    }
    // ------------------------------------------------ ParticleSystemElementProcessor ---------------------------

    // ------------------------------------------------ BillboardSetElementProcessor ---------------------------
    Ogre::String BillboardSetElementProcessor::serializeAttribute_billboardType( TiXmlElement* currentNode, BillboardType type )
    {
        String sBillboardType;

        switch( type )
        {
        case BBT_POINT:
            {
                sBillboardType = "point";
            }break;
        case BBT_ORIENTED_COMMON:
            {
                sBillboardType = "orientedCommon";
            }break;
        case BBT_ORIENTED_SELF:
            {
                sBillboardType = "orientedSelf";
            }break;
        case BBT_PERPENDICULAR_COMMON:
            {
                sBillboardType = "perpendicularCommon";
            }break;
        case BBT_PERPENDICULAR_SELF:
            {
                sBillboardType = "perpendicularSelf";
            }break;
        default:
            {
                if (mDotSceneInfo) mDotSceneInfo->logLoadError(
                    "Attribute \"billboardType\" of element \"" + String(currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value: \"" + StringConverter::toString( type ) + "\"!\n"
                    "Valid values are \"point\", \"orientedCommon\", \"orientedSelf\", \"perpendicularCommon\" and \"perpendicularSelf\".\n"
                    "\"billboardType\" attribute will now default to \"point\".\n"
                    "See the dotsceneformat.dtd for more information.\n");
                sBillboardType = "point";
            }break;
        }

        return sBillboardType;
    }

    Ogre::String BillboardSetElementProcessor::serializeAttribute_billboardOrigin( TiXmlElement* currentNode, BillboardOrigin origin )
    {
        String sBillboardOrigin;

        switch( origin )
        {
        case BBO_TOP_LEFT:
            {
                sBillboardOrigin = "topLeft";
            }break;
        case BBO_TOP_CENTER:
            {
                sBillboardOrigin = "topCenter";
            }break;
        case BBO_TOP_RIGHT:
            {
                sBillboardOrigin = "topRight";
            }break;
        case BBO_CENTER_LEFT:
            {
                sBillboardOrigin = "centerLeft";
            }break;
        case BBO_CENTER:
            {
                sBillboardOrigin = "center";
            }break;
        case BBO_CENTER_RIGHT:
            {
                sBillboardOrigin = "centerRight";
            }break;
        case BBO_BOTTOM_LEFT:
            {
                sBillboardOrigin = "bottomLeft";
            }break;
        case BBO_BOTTOM_CENTER:
            {
                sBillboardOrigin = "bottomCenter";
            }break;
        case BBO_BOTTOM_RIGHT:
            {
                sBillboardOrigin = "bottomRight";
            }break;
        default:
            {
                if (mDotSceneInfo) mDotSceneInfo->logLoadError(
                    "Attribute \"billboardOrigin\" of element \"" + String(currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value: \"" + StringConverter::toString( origin ) + "\"!\n"
                    "Valid values are \"topLef\", \"topCenter\", \"topRight\", \"left\", \"center\", \"right\", \"bottomLeft\", \"bottomCenter\" and \"bottomRight\".\n"
                    "\"billboardOrigin\" attribute will now default to \"center\".\n"
                    "See the dotsceneformat.dtd for more information.\n");
                sBillboardOrigin = "topLeft";
            }
        }
        return sBillboardOrigin;
    }

    Ogre::String BillboardSetElementProcessor::serializeAttribute_billboardRotationType( TiXmlElement* currentNode, BillboardRotationType rotationType )
    {
        String sBillboardRotationType;

        switch( rotationType )
        {
        case BBR_TEXCOORD:
            {
                sBillboardRotationType = "texcoord";
            }break;
        case BBR_VERTEX:
            {
                sBillboardRotationType = "vertex";
            }break;
        default:
            {
                if (mDotSceneInfo) mDotSceneInfo->logLoadError(
                    "Attribute \"billboardRotationType\" of element \"" + String(currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value: \"" + StringConverter::toString( rotationType ) + "\"!\n"
                    "Valid values are \"texcoord\" and \"vertex\".\n"
                    "\"billboardRotationType\" attribute will now default to \"texcoord\".\n"
                    "See the dotsceneformat.dtd for more information.\n");
                sBillboardRotationType = "texcoord";
            }
        }
        return sBillboardRotationType;
    }
    
    bool BillboardSetElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        bool bResult = false;

        const SceneNode* pParentNodeConst = static_cast<const SceneNode*>( parentData );

        SceneNode::ConstObjectIterator constObjectIterator = pParentNodeConst->getAttachedObjectIterator();
        while( constObjectIterator.hasMoreElements() )
        {
            MovableObject* obj = constObjectIterator.getNext();
            if( obj->getMovableType() == BillboardSetFactory::FACTORY_TYPE_NAME )
            {
                elementsData->push_back( obj );
                bResult = true;
            }
        }
        return bResult;
    }

    void BillboardSetElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        BillboardSet* pBillboardSet = static_cast<BillboardSet*>( pElementData );

        String sBillboardSetName = pBillboardSet->getName();
        SETATTRIB( "name", sBillboardSetName );
        SETATTRIB( "id", StringConverter::toString( counter ) );
        SETATTRIBDEFAULT( "poolSize", StringConverter::toString( pBillboardSet->getPoolSize() ), "20" );
        SETATTRIBDEFAULT( "autoextend", StringConverter::toString( pBillboardSet->getAutoextend() ), "true" );

        serializeAttribute_Visible( parentElement, pBillboardSet->getVisible() );

        SETATTRIB( "materialName", pBillboardSet->getMaterialName() );
        SETATTRIBDEFAULT( "defaultWidth", StringConverter::toString( pBillboardSet->getDefaultWidth() ), "10" );
        SETATTRIBDEFAULT( "defaultHeight", StringConverter::toString( pBillboardSet->getDefaultHeight() ), "10" );

	    SETATTRIBDEFAULT( "billboardType", serializeAttribute_billboardType( &currentNode, pBillboardSet->getBillboardType() ), "point" );
        SETATTRIBDEFAULT( "billboardOrigin", serializeAttribute_billboardOrigin( &currentNode, pBillboardSet->getBillboardOrigin() ), "center" );
        SETATTRIBDEFAULT( "billboardRotationType", serializeAttribute_billboardRotationType( &currentNode, pBillboardSet->getBillboardRotationType() ), "texcoord" );

        SETATTRIBDEFAULT( "sortingEnabled", StringConverter::toString( pBillboardSet->getSortingEnabled() ), "false" );
        SETATTRIBDEFAULT( "cullIndividually", StringConverter::toString( pBillboardSet->getCullIndividually() ), "false" );
        SETATTRIBDEFAULT( "accurateFacing", StringConverter::toString( pBillboardSet->getUseAccurateFacing() ), "false" );
        
        //FIXME SETATTRIBDEFAULT( "billboardsInWorldSpace", StringConverter::toString( pBillboardSet->isBillboardsInWorldSpace() ), "false" );
      
        SETATTRIBDEFAULT( "pointRenderingEnabled", StringConverter::toString( pBillboardSet->isPointRenderingEnabled() ), "false" );
    }
    // ------------------------------------------------ BillboardSetElementProcessor ---------------------------

    // ------------------------------------------------ BillboardElementProcessor ---------------------------
    bool BillboardElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        BillboardSet* pBillboardSet = static_cast<BillboardSet*>( parentData );
        int nBillboards = pBillboardSet->getNumBillboards();

        for( int i = 0; i < nBillboards; i++ )
        {
            elementsData->push_back( (void*)pBillboardSet->getBillboard( i ) );
        }
        return true;
    }

    void BillboardElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        Billboard* pBillboard = static_cast<Billboard*>( pElementData );

        SETATTRIB( "id", StringConverter::toString( counter ) );
        SETATTRIB( "rotation", StringConverter::toString( pBillboard->getRotation() ) );
        if( pBillboard->hasOwnDimensions() )
        {
            SETATTRIB( "width", StringConverter::toString( pBillboard->getOwnWidth() ) );
            SETATTRIB( "height", StringConverter::toString( pBillboard->getOwnHeight() ) );
        }
        SETATTRIB( "texcoordIndex", StringConverter::toString(pBillboard->getTexcoordIndex() ) );
    }
    // ------------------------------------------------ BillboardElementProcessor ---------------------------

    // ------------------------------------------------ CommonDirectionElementProcessor ---------------------------
    bool CommonDirectionElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        elementsData->push_back( parentData ); 
        return true;
    }

    void CommonDirectionElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        BillboardSet* pBillboardSet = static_cast<BillboardSet*>( pElementData );
        Vector3 vect = pBillboardSet->getCommonDirection();
        if( !vect.isZeroLength() )
        {
            SETATTRIB( "x", StringConverter::toString( vect.x ) );
            SETATTRIB( "y", StringConverter::toString( vect.y ) );
            SETATTRIB( "z", StringConverter::toString( vect.z ) );
        }
    }
    // ------------------------------------------------ CommonDirectionElementProcessor ---------------------------

    // ------------------------------------------------ CommonUpVectorElementProcessor ---------------------------
    bool CommonUpVectorElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        elementsData->push_back( parentData ); 
        return true;
    }

    void CommonUpVectorElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        BillboardSet* pBillboardSet = static_cast<BillboardSet*>( pElementData );
        Vector3 vect = pBillboardSet->getCommonUpVector();
        if( !vect.isZeroLength() )
        {
            SETATTRIB( "x", StringConverter::toString( vect.x ) );
            SETATTRIB( "y", StringConverter::toString( vect.y ) );
            SETATTRIB( "z", StringConverter::toString( vect.z ) );
        }
    }
    // ------------------------------------------------ CommonUpVectorElementProcessor ---------------------------
    // ------------------------------------------------ TextureCoordsElementProcessor ---------------------------
    bool TextureCoordsElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        bool bResult = false;

        TextureCoordSets* pTextureCoordSets = NULL;

        if(strcmp( strParentName, "billboardSet" ) == 0 )
        {
            BillboardSet* pBillboardSet = (BillboardSet*)( parentData );
            uint16 nCoordsNumber;
            const Ogre::FloatRect* pCoordsSet =  pBillboardSet->getTextureCoords( &nCoordsNumber );
            pTextureCoordSets = new TextureCoordSets();
            for( uint16 i = 0; i < nCoordsNumber; i++)
            {
                pTextureCoordSets->push_back( pCoordsSet[i] );                
            }
        }
        else if(strcmp( strParentName, "billboard" ) == 0 )
        {
            Billboard* pBillboard = (Billboard*)( parentData );
            if( pBillboard->isUseTexcoordRect() )
            {
                pTextureCoordSets = new TextureCoordSets();
                pTextureCoordSets->push_back( pBillboard->getTexcoordRect() );
            }
        }
        if( pTextureCoordSets )
        {
            elementsData->push_back( pTextureCoordSets );
            bResult = true;
        }
        return bResult;
    }    
    
    void TextureCoordsElementProcessor::serializeElementEndImpl(TiXmlElement& currentNode, void* pParentData, void* pElementData )
    {
        if( pElementData )
            delete (TextureCoordSets*)pElementData;
    }

    void TextureCoordsElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);
        //! Do nothing
    }
    // ------------------------------------------------ TextureCoordsElementProcessor ---------------------------

    // ------------------------------------------------ FloatRectElementProcessor ---------------------------
    bool FloatRectElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        TextureCoordsElementProcessor::TextureCoordSets* pTextureCoordSets = 
            static_cast<TextureCoordsElementProcessor::TextureCoordSets*>(parentData);
        TextureCoordsElementProcessor::TextureCoordSets::iterator itTextureCoordSets = pTextureCoordSets->begin();
        TextureCoordsElementProcessor::TextureCoordSets::iterator itTextureCoordSetsEnd = pTextureCoordSets->end();
        for( ; itTextureCoordSets != itTextureCoordSetsEnd ; ++itTextureCoordSets )
        {
            elementsData->push_back( new FloatRect( (*itTextureCoordSets) ) );
            return true;
        }

        return false;
    }

    void FloatRectElementProcessor::serializeElementEndImpl(TiXmlElement& currentNode, void* pParentData, void* pElementData )
    {
        if( pElementData )
            delete( (Ogre::FloatRect*) pElementData );
    }

    void FloatRectElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        Ogre::FloatRect* pCoords = static_cast<FloatRect*>( pElementData );

        SETATTRIB( "left", StringConverter::toString( pCoords->left ) );
        SETATTRIB( "top", StringConverter::toString( pCoords->top ) );
        SETATTRIB( "right", StringConverter::toString( pCoords->right ) );
        SETATTRIB( "bottom", StringConverter::toString( pCoords->bottom ) );
    }
    // ------------------------------------------------ FloatRectElementProcessor ---------------------------
    // ------------------------------------------------ TextureStacksAndSlicesElementProcessor ---------------------------
    bool TextureStacksAndSlicesElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        elementsData->push_back( parentData );
        return true;
    }


    void TextureStacksAndSlicesElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        BillboardSet* pBillboardSet = static_cast<BillboardSet*>( pElementData );
        

        if( pBillboardSet )
        {
			uchar stacks;
			uchar slices;

//FIXME 			pBillboardSet->getTextureStacksAndSlices(stacks, slices);

            SETATTRIBDEFAULT( "stacks", StringConverter::toString( static_cast<uint>( stacks ) ), "1" );
            SETATTRIBDEFAULT( "slices", StringConverter::toString( static_cast<uint>( slices ) ), "1" );
        }
    }
    // ------------------------------------------------ TextureStacksAndSlicesElementProcessor ---------------------------

    // ------------------------------------------------ TrackTargetElementProcessor ---------------------------
    bool TrackTargetElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        TrackTargetData* pTrackTargetData = NULL;

        SceneNode* pAutoTrackTargetNode;
        if( strcmp( strParentName, "camera" ) == 0 )
        {
            Camera* pCamera = static_cast<Camera*>( parentData );
            pAutoTrackTargetNode = pCamera->getAutoTrackTarget();
            if( pAutoTrackTargetNode )
            {
                pTrackTargetData = new TrackTargetData();
                pTrackTargetData->mTargetSceneNodeName = pAutoTrackTargetNode->getName();
                pTrackTargetData->mOffset = pCamera->getAutoTrackOffset();
                //! getAutoTrackLocalDirection(); is absent in camera.
            }
        }
        else // node
        {
            SceneNode* pSceneNode = static_cast<SceneNode*>( parentData );
            pAutoTrackTargetNode = pSceneNode->getAutoTrackTarget();
            
            if( pAutoTrackTargetNode )
            {
                pTrackTargetData = new TrackTargetData();
                //! Todo. Form where to receive data. Form node or ftom track target data
                pTrackTargetData->mTargetSceneNodeName = pAutoTrackTargetNode->getName();
                pTrackTargetData->mOffset = pSceneNode->getAutoTrackOffset();
                pTrackTargetData->mLocalDirectionVector = pSceneNode->getAutoTrackLocalDirection();
            }
        }

        if( pTrackTargetData )
        {
            elementsData->push_back( pTrackTargetData );
            return true;
        }
        return false;
    }

    void TrackTargetElementProcessor::serializeElementEndImpl(TiXmlElement& currentNode, void* pParentData, void* pElementData )
    {
        if( pElementData )
        {
            delete (TrackTargetData*)pElementData;
        }
    }

    void TrackTargetElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);
        TrackTargetData* pTrackTargetData = static_cast<TrackTargetData*>( pElementData );
        SETATTRIB( "nodeName", pTrackTargetData->mTargetSceneNodeName );
    }
    // ------------------------------------------------ TrackTargetElementProcessor ---------------------------

    // ------------------------------------------------ LocalDirectionVectorElementProcessor ---------------------------
    bool LocalDirectionVectorElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        if( strcmp( strParentName, "trackTarget" ) == 0 )
        {
            TrackTargetData* pTrackTargetData = static_cast<TrackTargetData*>( parentData );
            elementsData->push_back( new Vector3( pTrackTargetData->mLocalDirectionVector ) );
            return true;
        }
        else if( strcmp( strParentName, "lookTarget" ) == 0 )
        {
            LookTargetData* pLookTargetData = static_cast<LookTargetData*>( parentData );
            elementsData->push_back( new Vector3( pLookTargetData->mLocalDirectionVector) );
            return true;
        }
        return false;
    }

    void LocalDirectionVectorElementProcessor::serializeElementEndImpl(TiXmlElement& currentNode, void* pParentData, void* pElementData )
    {
        if( pElementData )
        {
            delete (Vector3*)pElementData;
        }
    }

    void LocalDirectionVectorElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        Vector3* pVector3 = static_cast<Vector3*>( pElementData );

        if( !pVector3->isZeroLength() )
        {
            SETATTRIB( "x", StringConverter::toString( pVector3->x ) );
            SETATTRIB( "y", StringConverter::toString( pVector3->y ) );
            SETATTRIB( "z", StringConverter::toString( pVector3->z ) );
        }
    }
    // ------------------------------------------------ LocalDirectionVectorElementProcessor ---------------------------
    // ------------------------------------------------ OffsetElementProcessor ---------------------------
    bool OffsetElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        elementsData->push_back( parentData );
        return true;
    }

    void OffsetElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        TrackTargetData* pTrackTargetData = static_cast<TrackTargetData*>( pElementData );

        if( !pTrackTargetData->mOffset.isZeroLength() )
        {
            SETATTRIB( "x", StringConverter::toString( pTrackTargetData->mOffset.x ) );
            SETATTRIB( "y", StringConverter::toString( pTrackTargetData->mOffset.y ) );
            SETATTRIB( "z", StringConverter::toString( pTrackTargetData->mOffset.z ) );
        }
    }
    // ------------------------------------------------ OffsetElementProcessor ---------------------------

    // ------------------------------------------------ LookTargetElementProcessor ---------------------------
    Ogre::String LookTargetElementProcessor::serializeAttribute_TransformSpaceType( TiXmlElement* currentNode, Node::TransformSpace transformSpace )
    {
        String sTransformSpace;

        switch( transformSpace )
        {
        case SceneNode::TS_LOCAL:
            {
                sTransformSpace = "local";
            }break;
        case SceneNode::TS_PARENT:
            {
                sTransformSpace = "parent";
            }break;
        case SceneNode::TS_WORLD:
            {
                sTransformSpace = "world";
            }break;
        default:
            {
                if (mDotSceneInfo) mDotSceneInfo->logLoadError(
                    "Attribute \"relativeTo\" of element \"" + String(currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value: \"" + StringConverter::toString( transformSpace ) + "\"!\n"
                    "Valid values are \"local\", \"parent\", \"world\". \"relativeTo\" attribute will now default to \"local\".\n"
                    "See the dotsceneformat.dtd for more information.\n");
                sTransformSpace = "local";
            }break;
        }
        return sTransformSpace;
    }
    
    bool LookTargetElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        bool bResult = false;
        if( strcmp( strParentName, "camera" ) == 0 )
        {
            Camera* pCamera = static_cast<Camera*>( parentData );
            
            LookTargetDataOfCameras* pLookTargetDataOfCameras = 
                (LookTargetDataOfCameras*)mTreeBuilder->getCommonParameter(CommonParName_LookTargetDataOfCameras);
            if( pLookTargetDataOfCameras )
            {
                LookTargetDataOfCameras::iterator itLookTargetDataOfCameras =
                    pLookTargetDataOfCameras->find( pCamera );
                if( itLookTargetDataOfCameras != pLookTargetDataOfCameras->end() )
                {
                    elementsData->push_back( new LookTargetData( itLookTargetDataOfCameras->second ) );
                    bResult = true;
                }
            }
        }
        else // Node
        {
            SceneNode* pNode = static_cast<SceneNode*>( parentData );

            LookTargetDataOfSceneNodes* pLookTargetDataOfSceneNodes = 
                (LookTargetDataOfSceneNodes*)mTreeBuilder->getCommonParameter(CommonParName_LookTargetDataOfSceneNodes);
            if( pLookTargetDataOfSceneNodes )
            {
                LookTargetDataOfSceneNodes::iterator itLookTargetDataOfSceneNodes =
                    pLookTargetDataOfSceneNodes->find( pNode );
                if( itLookTargetDataOfSceneNodes != pLookTargetDataOfSceneNodes->end() )
                {
                    elementsData->push_back( new LookTargetData( itLookTargetDataOfSceneNodes->second ) );
                    bResult = true;
                }
            }
        }
        return bResult;
    }

    void LookTargetElementProcessor::serializeElementEndImpl(TiXmlElement& currentNode, void* pParentData, void* pElementData )
    {
        if( pElementData )
        {
            delete (LookTargetData*)pElementData;
        }
    }

    void LookTargetElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        LookTargetData* pLookTargetData = static_cast<LookTargetData*>( pElementData );
        SETATTRIB( "nodeName", pLookTargetData->mLookTargetSceneNodeName );
        SETATTRIB( "relativeTo", serializeAttribute_TransformSpaceType( &currentNode, pLookTargetData->mRelativeTo ) );
    }
    // ------------------------------------------------ LookTargetElementProcessor ---------------------------

    // ------------------------------------------------ LightElementProcessor ---------------------------

    Ogre::String LightElementProcessor::serializeAttribute_type( TiXmlElement* currentNode, Light::LightTypes type )
    {
        String sLightType;
        switch( type)
        {
        case Light::LT_POINT:
            {
                sLightType = "point";
            }break;
        case Light::LT_DIRECTIONAL:
            {
                sLightType = "directional";
            }break;
        case Light::LT_SPOTLIGHT:
            {
                sLightType = "spotLight";
            }break;
        default:
            {
                if (mDotSceneInfo) mDotSceneInfo->logLoadError(
                    "Attribute \"type\" of element \"" + String(currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value: \"" + StringConverter::toString( type ) + "\"!\n"
                    "Valid values are \"point\", \"directional\", \"spotLight\", \"radPoint\" (currently this is unsupported).\n"
                    "\"type\" attribute will now default to \"point\".\n"
                    "See the dotsceneformat.dtd for more information.\n");
                sLightType = "point";
            }break;
        }
        return sLightType;
    }

    bool LightElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        bool bResult = false;

        if( strcmp( strParentName, "scene" ) == 0 )
        {
            SceneManager* pSceneManager = static_cast<SceneManager*>( parentData );
            SceneManager::MovableObjectIterator movableObjectIterator = 
                pSceneManager->getMovableObjectIterator( LightFactory::FACTORY_TYPE_NAME );
            
            while( movableObjectIterator.hasMoreElements() )
            {
                MovableObject* obj = movableObjectIterator.getNext();
                //! Scene manager contains all existed lights. Parse here only unattached.
                if( obj->getParentNode() == 0 )
                {
                    elementsData->push_back( obj );
                    bResult = true;
                }
            }
        }
        else // Nodes Node
        {
            const SceneNode* pSceneNode = static_cast<const SceneNode*>( parentData );

            SceneNode::ConstObjectIterator constObjectIterator = pSceneNode->getAttachedObjectIterator();
            while( constObjectIterator.hasMoreElements() )
            {
                MovableObject* obj = constObjectIterator.getNext();
                if( obj->getMovableType() == LightFactory::FACTORY_TYPE_NAME )
                {
                    elementsData->push_back( obj );
                    bResult = true;
                }
            }
        }


        return bResult;
    }

    void LightElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        Light* pLight = static_cast<Light*>( pElementData );

        SETATTRIB( "name", pLight->getName() );
        SETATTRIB( "id", StringConverter::toString( counter ) );
        SETATTRIBDEFAULT( "type", serializeAttribute_type( &currentNode, pLight->getType() ), "point" );
        SETATTRIBDEFAULT( "powerScale", StringConverter::toString( pLight->getPowerScale() ), "1.0" );
    }
    // ------------------------------------------------ LightElementProcessor ---------------------------

    // ------------------------------------------------ ColourDiffuseElementProcessor ---------------------------
    bool ColourDiffuseElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        ColourValue col;

        if( strcmp( strParentName, "shadowSettings" ) == 0  )
        {
            SceneManager* pSceneManager = static_cast<SceneManager*>( parentData );
            col = pSceneManager->getShadowColour();
        }
        else if ( strcmp( strParentName, "light" ) == 0  )
        {   
            Light* pLight = static_cast<Light*>( parentData );
            col = pLight->getDiffuseColour();
        }
        else if ( strcmp( strParentName, "billboard" ) == 0  )
        {
            Billboard* pBillboard = static_cast<Billboard*>( parentData );
            col = pBillboard->getColour();
        }

        elementsData->push_back( new ColourValue( col ) );
        return true;
    }

    void ColourDiffuseElementProcessor::serializeElementEndImpl(TiXmlElement& currentNode, void* pParentData, void* pElementData )
    {
        if( pElementData )
            delete (ColourValue*)pElementData;
    }


    void ColourDiffuseElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        ColourValue* pColourValue = static_cast<ColourValue*>( pElementData );
        
        SETATTRIBDEFAULT( "r", StringConverter::toString(pColourValue->r), "0.0" );
        SETATTRIBDEFAULT( "g", StringConverter::toString(pColourValue->g), "0.0" );
        SETATTRIBDEFAULT( "b", StringConverter::toString(pColourValue->b), "0.0" );
        SETATTRIBDEFAULT( "a", StringConverter::toString(pColourValue->a), "0.0" );
    }
    // ------------------------------------------------ ColourDiffuseElementProcessor ---------------------------

    // ------------------------------------------------ ColourSpecularElementProcessor ---------------------------
    bool ColourSpecularElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        elementsData->push_back( parentData );
        return true;
    }

    void ColourSpecularElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        Light* pLight = static_cast<Light*>( pElementData );
        ColourValue col = pLight->getSpecularColour();

        SETATTRIBDEFAULT( "r", StringConverter::toString(col.r), "0.0" );
        SETATTRIBDEFAULT( "g", StringConverter::toString(col.g), "0.0" );
        SETATTRIBDEFAULT( "b", StringConverter::toString(col.b), "0.0" );
    }
    // ------------------------------------------------ ColourSpecularElementProcessor ---------------------------
    // ------------------------------------------------ SpotLightRangeElementProcessor ---------------------------
    bool SpotLightRangeElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        Light* pLight = static_cast<Light*>( parentData );
        if( pLight->getType() == Light::LT_SPOTLIGHT )
        {
            elementsData->push_back( parentData );
            return true;
        }
        return false;
    }

    void SpotLightRangeElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        Light* pLight = static_cast<Light*>( pElementData );
        

        SETATTRIB( "inner", StringConverter::toString( pLight->getSpotlightInnerAngle() ) );
        SETATTRIB( "outer", StringConverter::toString( pLight->getSpotlightOuterAngle() ) );
        SETATTRIB( "falloff", StringConverter::toString( pLight->getSpotlightFalloff() ) );
    }
    // ------------------------------------------------ SpotLightRangeElementProcessor ---------------------------

    // ------------------------------------------------ LightAttenuationElementProcessor ---------------------------
    bool LightAttenuationElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        elementsData->push_back( parentData );
        return true;
    }

    void LightAttenuationElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        Light* pLight = static_cast<Light*>( pElementData );

        SETATTRIB( "range", StringConverter::toString( pLight->getAttenuationRange() ) );
        SETATTRIB( "constant", StringConverter::toString( pLight->getAttenuationConstant() ) );
        SETATTRIB( "linear", StringConverter::toString( pLight->getAttenuationLinear() ) );
        SETATTRIB( "quadratic", StringConverter::toString( pLight->getAttenuationQuadric() ) );
    }
    // ------------------------------------------------ LightAttenuationElementProcessor ---------------------------

    // ------------------------------------------------ CameraElementProcessor ---------------------------
    Ogre::String CameraElementProcessor::serializeAttribute_ProjectionType( TiXmlElement* currentNode, ProjectionType type )
    {
        String sProjectionType;
        switch ( type )
        {
        case PT_PERSPECTIVE:
            {
                sProjectionType = "perspective";
            }break;
        case PT_ORTHOGRAPHIC:
            {
                sProjectionType = "orthographic";
            }break;
        default:
            {
                if (mDotSceneInfo) mDotSceneInfo->logLoadError(
                    "Attribute \"projectionType\" of element \"" + String(currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value: \"" + StringConverter::toString( type ) + "\"!\n"
                    "Valid values are \"perspective\" and \"orthographic\".\n"
                    "\"projectionType\" attribute will now default to \"perspective\".\n"
                    "See the dotsceneformat.dtd for more information.\n");
                sProjectionType = "perspective";
            }break;
        }
        return sProjectionType;
    }

    Ogre::String CameraElementProcessor::serializeAttribute_PolygonMode( TiXmlElement* currentNode, PolygonMode mode )
    {
        String sPolygonMode;
        switch( mode )
        {
        case PM_SOLID:
            {
                sPolygonMode = "solid";
            }break;
        case PM_POINTS:
            {
                sPolygonMode = "points";
            }break;
        case PM_WIREFRAME:
            {
                sPolygonMode = "wireframe";
            }break;
        default:
            {
                if (mDotSceneInfo) mDotSceneInfo->logLoadError(
                    "Attribute \"polygonMode\" of element \"" + String(currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value: \"" + StringConverter::toString( mode ) + "\"!\n"
                    "Valid values are \"points\", \"wireframe\" and \"solid\".\n"
                    "\"polygonMode\" attribute will now default to \"solid\".\n"
                    "See the dotsceneformat.dtd for more information.\n");
                sPolygonMode = "solid";
            }break;
        }
        return sPolygonMode;
    }

    bool CameraElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        bool bResult = false;

        String sParentElementName;
        String sCameraParentName;

        if( strcmp( strParentName, "scene" ) == 0 )
        {
            SceneManager* pParentSceneManager = static_cast<SceneManager*>(parentData);
            sParentElementName = "";
        }
        else // "nodes" or "node
        {
            SceneNode* pParentSceneNode = static_cast<SceneNode*>(parentData);
            sParentElementName = pParentSceneNode->getName();
        }
        
        SceneManager* sceneManager = (SceneManager*)mTreeBuilder->getCommonParameter("SceneManager");
        SceneManager::CameraIterator cameraIterator = sceneManager->getCameraIterator();
        Camera* pCamera = 0;
        while( cameraIterator.hasMoreElements() )
        {
            pCamera = cameraIterator.getNext();
            Node* pSceneNode = pCamera->getParentNode();
           
            if( pSceneNode )
            {
                sCameraParentName = pSceneNode->getName();
            }
            else
            {
                sCameraParentName = "";
            }
            
            if( sCameraParentName == sParentElementName )
            {
                elementsData->push_back( pCamera);
                bResult = true;
            }
        }
        return bResult;
    }

    void CameraElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        Camera* pCamera = static_cast<Camera*>( pElementData );

        SETATTRIB( "name", pCamera->getName() );
        SETATTRIB( "id", StringConverter::toString( counter ) );
        SETATTRIBDEFAULT( "FOVy", StringConverter::toString( pCamera->getFOVy() ), "45" );

        SETATTRIBDEFAULT( "aspectRatio", StringConverter::toString( pCamera->getAspectRatio() ), "1.3333333" );
        SETATTRIBDEFAULT( "projectionType", serializeAttribute_ProjectionType( &currentNode, pCamera->getProjectionType() ), "perspective" );
        SETATTRIBDEFAULT( "polygonMode", serializeAttribute_PolygonMode( &currentNode, pCamera->getPolygonMode() ), "solid" );

        SETATTRIBDEFAULT( "useRenderingDistance", StringConverter::toString( pCamera->getUseRenderingDistance() ), "yes" );
        SETATTRIBDEFAULT( "lodBiasFactor", StringConverter::toString( pCamera->getLodBias() ), "1.0" );
    }
    // ------------------------------------------------ CameraElementProcessor ---------------------------

    // ------------------------------------------------ ClippingElementProcessor ---------------------------
    bool ClippingElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        elementsData->push_back( parentData );
        return true;
    }

    void ClippingElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        Camera* pCamera = static_cast<Camera*>( pElementData );

        SETATTRIB( "nearPlaneDist", StringConverter::toString( pCamera->getNearClipDistance() ) );
        SETATTRIB( "farPlaneDist", StringConverter::toString( pCamera->getFarClipDistance() ) );
    }
    // ------------------------------------------------ ClippingElementProcessor ---------------------------

    // ------------------------------------------------ ViewportsElementProcessor ---------------------------
    bool ViewportsElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        elementsData->push_back( parentData );
        return true;
    }

    void ViewportsElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);
        //! Do nothing
    }
    // ------------------------------------------------ ViewportsElementProcessor ---------------------------

    // ------------------------------------------------ ViewportElementProcessor ---------------------------
    Ogre::String ViewportElementProcessor::serializeAttribute_FramebufferType( TiXmlElement* currentNode, uint type )
    {
        String sFramebufferType;
        
        int IsColour = type & Ogre::FBT_COLOUR;
        int IsDepth  = type & Ogre::FBT_DEPTH;
        int IsStensil  = type & Ogre::FBT_STENCIL;

        if( IsColour )
        {
            if( sFramebufferType.length() )
            {
                sFramebufferType += "|";
            }
            sFramebufferType += "colour";
        }

        if( IsDepth )
        {
            if( sFramebufferType.length() )
            {
                sFramebufferType += "|";
            }
            sFramebufferType += "depth";
        }

        if( IsStensil )
        {
            if( sFramebufferType.length() )
            {
                sFramebufferType += "|";
            }
            sFramebufferType += "stencil";
        }
        return sFramebufferType;
    }
    
    bool ViewportElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        bool bResult = false;

        SceneManager* sceneManager = static_cast<SceneManager*>( parentData );
        SceneManager::CameraIterator cameraIterator = sceneManager->getCameraIterator();

        Camera* pCamera = 0;
        while( cameraIterator.hasMoreElements() )
        {
            if( pCamera = cameraIterator.getNext() )
            {
                Viewport* pViewport = pCamera->getViewport();
                if( pViewport )
                {
                    elementsData->push_back( pViewport );
                    bResult = true;
                }
            }
        }
        return bResult;
    }

    void ViewportElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        Viewport* pViewport = static_cast< Viewport*>(pElementData);
      
        RenderTarget* pRenderTarget = pViewport->getTarget();
        if( pRenderTarget )
        {
            SETATTRIB( "renderTargetName", pRenderTarget->getName() );
        }

        Camera* pCamera = pViewport->getCamera();
        if( pCamera )
        {
            SETATTRIB( "cameraName", pCamera->getName() );
            SETATTRIBDEFAULT( "changeCameraAspectRatio", "true", "true" );
        }
               
        SETATTRIBDEFAULT( "left", StringConverter::toString( pViewport->getLeft() ), "0" );
        SETATTRIBDEFAULT( "top", StringConverter::toString( pViewport->getTop() ), "0" );
        SETATTRIBDEFAULT( "width", StringConverter::toString( pViewport->getWidth() ), "1" );
        SETATTRIBDEFAULT( "height", StringConverter::toString( pViewport->getHeight() ), "1" );
        SETATTRIBDEFAULT( "zOrder", StringConverter::toString( pViewport->getZOrder() ), "0" );
        SETATTRIBDEFAULT( "clearEveryFrame", StringConverter::toString( pViewport->getClearEveryFrame() ), "true" );
        SETATTRIBDEFAULT( "clearBuffers", serializeAttribute_FramebufferType( &currentNode, pViewport->getClearBuffers() ), "colour|depth" ); 
        SETATTRIB( "materialSchemeName", pViewport->getMaterialScheme() );
        SETATTRIBDEFAULT( "overlaysEnabled", StringConverter::toString( pViewport->getOverlaysEnabled() ), "true" );
        SETATTRIBDEFAULT( "skiesEnabled", StringConverter::toString( pViewport->getSkiesEnabled() ), "true" );
        SETATTRIBDEFAULT( "shadowsEnabled", StringConverter::toString( pViewport->getShadowsEnabled() ), "true" );
        SETATTRIB( "visibilityMask", StringConverter::toString( pViewport->getVisibilityMask() ) );
        SETATTRIB( "renderQueueInvocationSequenceName", pViewport->getRenderQueueInvocationSequenceName() );
    }
    // ------------------------------------------------ ViewportElementProcessor ---------------------------
    // ------------------------------------------------ CompositorInstancesElementProcessor ---------------------------
    bool CompositorInstancesElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        elementsData->push_back( parentData );
        return true;
    }

    void CompositorInstancesElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);
        //! Do nothing
    }
    // ------------------------------------------------ CompositorInstancesElementProcessor ---------------------------

    // ------------------------------------------------ CompositorInstanceElementProcessor ---------------------------
    bool CompositorInstanceElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        bool bResult = false;

        Viewport* pViewport = static_cast<Viewport*>( parentData );
        CompositorChain* pCompositorChain = CompositorManager::getSingleton().getCompositorChain( pViewport );

        if( pCompositorChain )
        {
            CompositorChain::InstanceIterator compositorInstanceIterator = pCompositorChain->getCompositors();
            CompositorInstance* pCompositorInstance;
            while( compositorInstanceIterator.hasMoreElements() )
            {
                if( pCompositorInstance = compositorInstanceIterator.getNext() )
                {
                    elementsData->push_back( pCompositorInstance );
                    bResult = true;
                }
            }
        }
        return bResult;
    }

    void CompositorInstanceElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        CompositorInstance* pCompositorInstance = static_cast<CompositorInstance*>( pElementData );

        Compositor* pCompositor = pCompositorInstance->getCompositor();
        if( pCompositor )
        {
            SETATTRIB( "compositorName", pCompositor->getName() );
            //! \TODO Where to insert new  compositor instance. use default or use "counter" ?
            SETATTRIBDEFAULT( "position", StringConverter::toString( counter ), "-1" );
            SETATTRIBDEFAULT( "enabled", StringConverter::toString( pCompositorInstance->getEnabled() ), "false" );
        }
    }
    // ------------------------------------------------ CompositorInstanceElementProcessor ---------------------------
    // ------------------------------------------------ HDRCompositorInstanceElementProcessor ---------------------------
//    bool HDRCompositorInstanceElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
//    {
//        return false;
//    }

//    void HDRCompositorInstanceElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
//    {
//        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);
//        Viewport* pViewport = static_cast< Viewport*>(pElementData);
//        //! Do nothing since HDRCompositor is not used for now.
//    }
    // ------------------------------------------------ HDRCompositorInstanceElementProcessor ---------------------------
    // ------------------------------------------------ AnimationsElementProcessor ---------------------------
    bool AnimationsElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        bool bResult = false;

        if( strcmp( strParentName, "scene" ) == 0 )
        {
            SceneManager* pSceneManager = static_cast<SceneManager*>( parentData );

            SceneManager::AnimationIterator animationIterator = pSceneManager->getAnimationIterator();
            Camera* pCamera = 0;
            while( animationIterator.hasMoreElements() )
            {
                elementsData->push_back( new AnimationElementProcessor::AnimationInfo( "node", animationIterator.getNext() ) );
                bResult = true;
            }
        }
        else if( strcmp( strParentName, "entity" ) == 0 )
        {
            MovableObject* pMovableObject = static_cast<MovableObject*>( parentData );

            Entity* pEntity = static_cast<Entity*>(parentData);

            MeshPtr pMesh = pEntity->getMesh();
            if( !pMesh.isNull() )
            {
                ushort nMeshAnimation = pMesh->getNumAnimations();
                for( ushort i = 0; i < nMeshAnimation; i++ )
                {
                    elementsData->push_back( new AnimationElementProcessor::AnimationInfo( "mesh", pMesh->getAnimation( i ) ) );
                    bResult = true;
                }
            }
            Skeleton* pSkeleton = pEntity->getSkeleton();
            if( pSkeleton )
            {
                ushort nSkeletonAnimation = pSkeleton->getNumAnimations();
                for( ushort j = 0; j < nSkeletonAnimation; j++ )
                {
                    elementsData->push_back( new AnimationElementProcessor::AnimationInfo( "node", pSkeleton->getAnimation( j ) ) );
                    bResult = true;
                }
            }
        }
        return bResult;
    }

    void AnimationsElementProcessor::serializeElementEndImpl(TiXmlElement& currentNode, void* pParentData, void* pElementData )
    {
        if( pElementData )
        {
            delete (AnimationElementProcessor::AnimationInfo*)pElementData;
        }
    }

    void AnimationsElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);
        //! Do nothing
    }
    // ------------------------------------------------ AnimationsElementProcessor ---------------------------

    // ------------------------------------------------ AnimationElementProcessor ---------------------------
    Ogre::String AnimationElementProcessor::serializeAttribute_InterpolationMode( TiXmlElement* currentNode, Animation::InterpolationMode mode )
    {
        String sInterpolationMode;

        switch( mode )
        {
        case Animation::InterpolationMode::IM_LINEAR:
            {
                sInterpolationMode = "linear";
            }break;
        case Animation::InterpolationMode::IM_SPLINE:
            {
                sInterpolationMode = "spline";
            }break;
        default:
            {
                if (mDotSceneInfo) mDotSceneInfo->logLoadError(
                    "Attribute InterpolationMode of element \"" + String( currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value!\n"
                    "Valid values are \"linear\" or \"spline\".\n"
                    "\" attribute will now default to \"linear\".\n"
                    "See the dotsceneformat.dtd for more information.\n");
                sInterpolationMode = "linear";
            }break;
        }
        return sInterpolationMode;

    }

    Ogre::String AnimationElementProcessor::serializeAttribute_RotationInterpolationMode( TiXmlElement* currentNode, Animation::RotationInterpolationMode mode )
    {
        String sRotationInterpolationMode;

        switch( mode )
        {
        case Animation::RotationInterpolationMode::RIM_LINEAR:
            {
                sRotationInterpolationMode = "linear";
            }break;
        case Animation::RotationInterpolationMode::RIM_SPHERICAL:
            {
                sRotationInterpolationMode = "spherical";
            }break;
        default:
            {
                if (mDotSceneInfo) mDotSceneInfo->logLoadError(
                    "Attribute RotationInterpolationMode of element \"" + String( currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value!\n"
                    "Valid values are \"linear\" or \"spherical\".\n"
                    "\" attribute will now default to \"linear\".\n"
                    "See the dotsceneformat.dtd for more information.\n");
                sRotationInterpolationMode = "linear";
            }break;
        }
        return sRotationInterpolationMode;
    }
    
    bool AnimationElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        elementsData->push_back( parentData );
        return true;
    }

    void AnimationElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        AnimationInfo* pAnimationInfo = static_cast<AnimationInfo*>( pElementData );
        Animation* pAnimation = pAnimationInfo->m_AnimationPtr;

        SETATTRIB( "name", pAnimation->getName() );
        SETATTRIB( "type", pAnimationInfo->msAnimationType );
        SETATTRIB( "length", StringConverter::toString( pAnimation->getLength() ) );
        SETATTRIBDEFAULT( "interpolationMode", 
            serializeAttribute_InterpolationMode( &currentNode, pAnimation->getInterpolationMode() ) , "linear" );
        SETATTRIBDEFAULT( "rotationInterpolationMode", 
            serializeAttribute_RotationInterpolationMode( &currentNode, pAnimation->getRotationInterpolationMode() ), "linear" );
    }

    // ------------------------------------------------ AnimationElementProcessor ---------------------------
    // ------------------------------------------------ NodeAnimationTrackElementProcessor ---------------------------
    bool NodeAnimationTrackElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        bool bResult = false;

        AnimationElementProcessor::AnimationInfo* pAnimationInfo = 
            static_cast<AnimationElementProcessor::AnimationInfo*>(parentData);

        Animation* pAnimation = pAnimationInfo->m_AnimationPtr;
        if( pAnimation )
        {
            if( pAnimation->getNumNodeTracks() )
            {
                Animation::NodeTrackIterator nodeTrackIterator = pAnimation->getNodeTrackIterator();

                while( nodeTrackIterator.hasMoreElements() )
                {
                    elementsData->push_back( nodeTrackIterator.getNext() );
                    bResult = true;
                }
            }
        }
        return bResult;
    }

    void NodeAnimationTrackElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        NodeAnimationTrack* pNodeAnimationTrack = static_cast<NodeAnimationTrack*>( pElementData );

        SETATTRIB( "handle", StringConverter::toString( pNodeAnimationTrack->getHandle() ) );
        Node* pAssociatedNode = pNodeAnimationTrack->getAssociatedNode();
        if( pAssociatedNode  )
        {
            SETATTRIB( "associatedNodeName", pAssociatedNode->getName() );
        }
        SETATTRIBDEFAULT( "useShortestRotationPath", StringConverter::toString( pNodeAnimationTrack->getUseShortestRotationPath() ), "true" );
    }
    // ------------------------------------------------ NodeAnimationTrackElementProcessor ---------------------------

    // ------------------------------------------------ TransformKeyFrameElementProcessor ---------------------------
    bool TransformKeyFrameElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        NodeAnimationTrack* pNodeAnimationTrack = static_cast<NodeAnimationTrack*>( parentData );

        ushort nKeyFrames = pNodeAnimationTrack->getNumKeyFrames();
        for( ushort i = 0; i < nKeyFrames; i++ )
        {
            elementsData->push_back( pNodeAnimationTrack->getKeyFrame( i ) ) ;
        }
        return true;
    }

    void TransformKeyFrameElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        KeyFrame* pKeyFrame = static_cast<KeyFrame*>( pElementData );
        SETATTRIB( "timePosition", StringConverter::toString( pKeyFrame->getTime() ) );
    }
    // ------------------------------------------------ TransformKeyFrameElementProcessor ---------------------------
    
    // ------------------------------------------------ VertexAnimationTrackElementProcessor ---------------------------
    Ogre::String VertexAnimationTrackElementProcessor::serializeAttribute_VertexAnimationType( TiXmlElement* currentNode, VertexAnimationType type )
    {
        String sVertexAnimationType;

        switch( type )
        {
        //! This field is required at DotScene
        /*
        case VertexAnimationType::VAT_NONE:
            {
                sVertexAnimationType = "none"
            }break;
        */
        case VAT_MORPH:
            {
                sVertexAnimationType = "morph";
            }break;
        case VAT_POSE:
            {
                sVertexAnimationType = "pose";
            }break;
        default:
            {
                if (mDotSceneInfo) mDotSceneInfo->logLoadError(
                    "Attribute VertexAnimationType of element \"" + String( currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value!\n"
                    "Valid values are \"morph\" or \"pose\".\n"
                    "\" attribute will not receive a default value.\n"
                    "See the dotsceneformat.dtd for more information.\n");
                sVertexAnimationType = "";
            }break;
        }
        return sVertexAnimationType;
    }

    Ogre::String VertexAnimationTrackElementProcessor::serializeAttribute_TargetMode( TiXmlElement* currentNode, VertexAnimationTrack::TargetMode mode )
    {
        String sTargetMode;
        switch( mode )
        {
        case VertexAnimationTrack::TargetMode::TM_SOFTWARE:
            {
                sTargetMode = "software";
            }break;
        case VertexAnimationTrack::TargetMode::TM_HARDWARE:
            {
                sTargetMode = "hardware";
            }break;
        default:
            {
                if (mDotSceneInfo) mDotSceneInfo->logLoadError(
                    "Attribute TargetMode of element \"" + String( currentNode->Value()) + "\" in file \"" + mFilename + "\" has an invalid value!\n"
                    "Valid values are \"software\" or \"hardware\".\n"
                    "\" attribute will now default to \"linear\".\n"
                    "See the dotsceneformat.dtd for more information.\n");
                sTargetMode = "software";
            }break;
        }
        return sTargetMode;
    }

    bool VertexAnimationTrackElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        bool bResult = false;

        AnimationElementProcessor::AnimationInfo* pAnimationInfo = 
            static_cast<AnimationElementProcessor::AnimationInfo*>(parentData);

        Animation* pAnimation = pAnimationInfo->m_AnimationPtr;
        if( pAnimation )
        {
            if( pAnimation->getNumVertexTracks() )
            {
                Animation::VertexTrackIterator vertexTrackIterator = pAnimation->getVertexTrackIterator();
                while( vertexTrackIterator.hasMoreElements() )
                {
                    elementsData->push_back( vertexTrackIterator.getNext() );
                    bResult = true;
                }
            }
        }
        return bResult;
    }

    void VertexAnimationTrackElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        VertexAnimationTrack* pVertexAnimationTrack = static_cast<VertexAnimationTrack*>( pElementData );

        SETATTRIB( "handle", StringConverter::toString( pVertexAnimationTrack->getHandle() ) );
        SETATTRIB( "vertexAnimationType", 
            serializeAttribute_VertexAnimationType( &currentNode, pVertexAnimationTrack->getAnimationType() ) );
        SETATTRIBDEFAULT( "vertexAnimationType", 
            serializeAttribute_TargetMode( &currentNode, pVertexAnimationTrack->getTargetMode() ), "software" );
    }
    // ------------------------------------------------ VertexAnimationTrackElementProcessor ---------------------------
    // ------------------------------------------------ VertexPoseKeyFrameElementProcessor ---------------------------
    bool VertexPoseKeyFrameElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        VertexAnimationTrack* pVertexAnimationTrack = static_cast<VertexAnimationTrack*>( parentData );

        ushort nKeyFrames = pVertexAnimationTrack->getNumKeyFrames();
        for( ushort i = 0; i < nKeyFrames; i++ )
        {
            elementsData->push_back( pVertexAnimationTrack->getKeyFrame( i ) ) ;
        }
        elementsData->push_back( parentData );
        return true;
    }

    void VertexPoseKeyFrameElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        VertexPoseKeyFrame* pKeyFrame = static_cast<VertexPoseKeyFrame*>( pElementData );
	    SETATTRIB( "timePosition", StringConverter::toString(pKeyFrame->getTime()) );
    }
    // ------------------------------------------------ VertexPoseKeyFrameElementProcessor ---------------------------

    // ------------------------------------------------ PoseReferenceElementProcessor ---------------------------
    bool PoseReferenceElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        bool bResult = false;

        VertexPoseKeyFrame* pKeyFrame = static_cast<VertexPoseKeyFrame*>( parentData );
        VertexPoseKeyFrame::PoseRefIterator poseRefIterator = pKeyFrame->getPoseReferenceIterator();

        while( poseRefIterator.hasMoreElements() )
        {
            elementsData->push_back( new VertexPoseKeyFrame::PoseRef( poseRefIterator.getNext() ) );
            bResult = true;
        }
        return bResult;
    }

    void PoseReferenceElementProcessor::serializeElementEndImpl(TiXmlElement& currentNode, void* pParentData, void* pElementData )
    {
        if( pElementData )
            delete (VertexPoseKeyFrame::PoseRef*) pElementData;
    }

    void PoseReferenceElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        VertexPoseKeyFrame::PoseRef* pPoseRef = static_cast<VertexPoseKeyFrame::PoseRef*>( pElementData );
        SETATTRIB( "poseIndex", StringConverter::toString( pPoseRef->poseIndex ) );
        SETATTRIB( "influence", StringConverter::toString( pPoseRef->influence ) );
    }
    // ------------------------------------------------ PoseReferenceElementProcessor ---------------------------

    // ------------------------------------------------ AnimationStatesElementProcessor ---------------------------
    bool AnimationStatesElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
		// TODO: FIXTHIS: HUGE HACK!!!
		// Saving parent data for child element

		if( strcmp( strParentName, "scene" ) == 0 )
		{
			mTreeBuilder->setCommonParameter("SceneAnimationState", parentData);
		}
		else if( strcmp( strParentName, "entity" ) == 0 )// Entity
		{
			mTreeBuilder->setCommonParameter("EntityAnimationState", parentData);
		}

        elementsData->push_back( parentData );
        return true;
    }

    void AnimationStatesElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);
        //! Nothing to do here
    }

	void AnimationStatesElementProcessor::serializeElementEndImpl(TiXmlElement& currentNode, void* pParentData, void* pElementData )
	{
		if ( mTreeBuilder->getCommonParameter( "SceneAnimationState" ) )
		{
			mTreeBuilder->removeCommonParameter( "SceneAnimationState" );
		}

		if ( mTreeBuilder->getCommonParameter( "EntityAnimationState" ) )
		{
			mTreeBuilder->removeCommonParameter( "EntityAnimationState" );
		}
	}
    // ------------------------------------------------ AnimationStatesElementProcessor ---------------------------



    // ------------------------------------------------ AnimationStateElementProcessor ---------------------------
    bool AnimationStateElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        bool bResult = false;

		if( mTreeBuilder->getCommonParameter( "SceneAnimationState" ) )
        {
            SceneManager* pParentSceneManager = static_cast<SceneManager*> (mTreeBuilder->getCommonParameter( "SceneAnimationState" ) );

            AnimationStateIterator animationStateIterator = pParentSceneManager->getAnimationStateIterator();

            while( animationStateIterator.hasMoreElements() )
            {
                elementsData->push_back( animationStateIterator.getNext() );
                bResult = true;
            }
        }
        else if( mTreeBuilder->getCommonParameter( "EntityAnimationState" ) ) // Entity
        {
            const Entity* pEntity = static_cast<const Entity*>( mTreeBuilder->getCommonParameter( "EntityAnimationState" ) );

            const AnimationStateSet* pAnimationStateSet = pEntity->getAllAnimationStates();
            if( pAnimationStateSet )
            {
                ConstAnimationStateIterator animationStateIterator = pAnimationStateSet->getAnimationStateIterator();
                if( animationStateIterator.hasMoreElements() )
                {
                    elementsData->push_back( animationStateIterator.getNext() );
                    bResult = true;
                }
            }
        }
        
        return bResult;
    }

    void AnimationStateElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        AnimationState* pAnimationState = static_cast< AnimationState*>( pElementData );

        SETATTRIBDEFAULT( "enabled", StringConverter::toString( pAnimationState->getEnabled() ), "false" );
        SETATTRIB( "animationName", pAnimationState->getAnimationName() );
        SETATTRIBDEFAULT( "timePosition", StringConverter::toString( pAnimationState->getTimePosition() ), "0" );
        SETATTRIB( "length", StringConverter::toString( pAnimationState->getLength() ) );
        SETATTRIBDEFAULT( "weight", StringConverter::toString( pAnimationState->getWeight() ), "0" );
        SETATTRIBDEFAULT( "loop", StringConverter::toString( pAnimationState->getLoop() ), "true" );
    }
    // ------------------------------------------------ AnimationStateElementProcessor ---------------------------

    // ------------------------------------------------ CommonMovableObjectParamsElementProcessor ---------------------------
    bool CommonMovableObjectParamsElementProcessor::getStaticStatusForMovable( const String& sName, const String& sType, MovableObject* pMovableObject )
    {
        //! light camera entity particleSystem billboardSet
        if( sType == "light" )
        {
            dsi::DotSceneInfo::ConstLoadedLightIterator constLoadedLightIterator = mDotSceneInfo->getStaticLightsIterator();
            {
                while( constLoadedLightIterator.hasMoreElements() )
                {
                    Light* pLight = constLoadedLightIterator.getNext();
                    if( sName == pLight->getName() )
                    {
                        return true;
                    }
                }
            }
        }
        if( sType == "camera" )
        {
            dsi::DotSceneInfo::ConstLoadedCameraIterator constLoadedCameraIterator = mDotSceneInfo->getStaticCamerasIterator();
            {
                while( constLoadedCameraIterator.hasMoreElements() )
                {
                    Camera* pCamera = constLoadedCameraIterator.getNext();
                    if( sName == pCamera->getName() )
                    {
                        return true;
                    }
                }
            }
        }
        if( sType == "particleSystem" )
        {
            dsi::DotSceneInfo::ConstLoadedParticleSystemIterator constLoadedParticleSystemIterator = mDotSceneInfo->getStaticParticleSystemsIterator();
            {
                while( constLoadedParticleSystemIterator.hasMoreElements() )
                {
                    ParticleSystem* pParticleSystem = constLoadedParticleSystemIterator.getNext();
                    if( sName == pParticleSystem->getName() )
                    {
                        return true;
                    }
                }
            }
        }
        if( sType == "billboardSet" )
        {
            dsi::DotSceneInfo::ConstLoadedBillboardSetIterator constLoadedBillboardSetIterator = mDotSceneInfo->getStaticBillboardSetsIterator();
            {
                while( constLoadedBillboardSetIterator.hasMoreElements() )
                {
                    BillboardSet* pBillboardSet = constLoadedBillboardSetIterator.getNext();
                    if( sName == pBillboardSet->getName() )
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    bool CommonMovableObjectParamsElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        //! light camera entity particleSystem billboardSet
		MovableObject* pMovableObject = static_cast<MovableObject*>( parentData );
		elementsData->push_back( 
			new MovableObjectParamsInfo( pMovableObject->getName(), strParentName, pMovableObject ) );
		return true;
    }
    void CommonMovableObjectParamsElementProcessor::serializeElementEndImpl(TiXmlElement& currentNode, void* pParentData, void* pElementData )
    {
        if( pElementData )
            delete (MovableObjectParamsInfo*)pElementData;
    }

    void CommonMovableObjectParamsElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        MovableObjectParamsInfo* pMovableObjectParamsInfo = static_cast<MovableObjectParamsInfo*>( pElementData );
        MovableObject* pMovableObject = pMovableObjectParamsInfo->mpObject;

        bool IsStatic = getStaticStatusForMovable( 
            pMovableObjectParamsInfo->msName, 
            pMovableObjectParamsInfo->msType, 
            pMovableObject );

        String sObjectName = pMovableObject->getName();
        //! \TODO Find this parameter
        SETATTRIBDEFAULT( "static", StringConverter::toString( IsStatic ), "false" );
        SETATTRIBDEFAULT( "visible", StringConverter::toString( pMovableObject->getVisible() ), "true" );
        SETATTRIBDEFAULT( "castShadows", StringConverter::toString( pMovableObject->getCastShadows() ), "true" );
        SETATTRIBDEFAULT( "renderingDistance", StringConverter::toString( pMovableObject->getRenderingDistance() ), "0" );
        SETATTRIB( "queryFlags",  StringConverter::toString( pMovableObject->getQueryFlags() ) );
        SETATTRIB( "visibilityFlags",  StringConverter::toString( pMovableObject->getVisibilityFlags() ) );
    }
    // ------------------------------------------------ CommonMovableObjectParamsElementProcessor ---------------------------
    // ------------------------------------------------ QuaternionElementProcessor ---------------------------
    bool QuaternionElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
		mDotSceneInfo	= (dsi::DotSceneInfo*)mTreeBuilder->getCommonParameter("DotSceneInfo");

		if( Ogre::dsi::OT_QUATERNION == mDotSceneInfo->getOrientationSaveType() )
		{
			elementsData->push_back( parentData );
			return true;
		}

		return false;
    }

    void QuaternionElementProcessor::serializeElementEndImpl(TiXmlElement& currentNode, void* pParentData, void* pElementData )
    {
        //! Define empty function here. That serializeElementEndImpl from OrientationElementProcessor will not be called.
    }

    void QuaternionElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        Quaternion* pQuaternion = static_cast<Quaternion*>( pElementData );

        SETATTRIB( "x", StringConverter::toString( pQuaternion->x ) );
        SETATTRIB( "y", StringConverter::toString( pQuaternion->y ) );
        SETATTRIB( "z", StringConverter::toString( pQuaternion->z ) );
        SETATTRIB( "w", StringConverter::toString( pQuaternion->w ) );
    }
    // ------------------------------------------------ QuaternionElementProcessor ---------------------------

    // ------------------------------------------------ AxisXYZElementProcessor ---------------------------
    bool AxisXYZElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
		mDotSceneInfo	= (dsi::DotSceneInfo*)mTreeBuilder->getCommonParameter("DotSceneInfo");

		if( Ogre::dsi::OT_AXISXYZ == mDotSceneInfo->getOrientationSaveType() )
		{
			elementsData->push_back( parentData );
			return true;
		}
		return false;
    }

    void AxisXYZElementProcessor::serializeElementEndImpl(TiXmlElement& currentNode, void* pParentData, void* pElementData )
    {
        //! Define empty function here. That serializeElementEndImpl from OrientationElementProcessor will not be called.
    }

    void AxisXYZElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
		queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

		Quaternion* pQuaternion = static_cast<Quaternion*>( pElementData );

		Vector3	xAxis = pQuaternion->xAxis();
		Vector3	yAxis = pQuaternion->yAxis();
		Vector3	zAxis = pQuaternion->zAxis();

		SETATTRIB( "xAxisX", StringConverter::toString( xAxis.x ) );
		SETATTRIB( "xAxisY", StringConverter::toString( xAxis.y ) );
		SETATTRIB( "xAxisZ", StringConverter::toString( xAxis.z ) );

		SETATTRIB( "yAxisX", StringConverter::toString( yAxis.x ) );
		SETATTRIB( "yAxisY", StringConverter::toString( yAxis.y ) );
		SETATTRIB( "yAxisZ", StringConverter::toString( yAxis.z ) );

		SETATTRIB( "zAxisX", StringConverter::toString( zAxis.x ) );
		SETATTRIB( "zAxisY", StringConverter::toString( zAxis.y ) );
		SETATTRIB( "zAxisZ", StringConverter::toString( zAxis.z ) );
    }
    // ------------------------------------------------ AxisXYZElementProcessor ---------------------------

    // ------------------------------------------------ Angle_AxisElementProcessor ---------------------------
    
    String Angle_AxisElementProcessor::serializeAttribute_AngleUnit( TiXmlElement* currentNode, Math::AngleUnit unit )
    {
        String sAngleUnit;
        switch( unit )
        {
        case Math::AU_DEGREE:
            {
                sAngleUnit = "degree";
            }break;
        case Math::AU_RADIAN:
            {
                sAngleUnit = "radian";
            }break;
        default:
            {
                sAngleUnit = "";
            }break;
        }
        return sAngleUnit;
    }

    bool Angle_AxisElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
		mDotSceneInfo	= (dsi::DotSceneInfo*)mTreeBuilder->getCommonParameter("DotSceneInfo");

		if( Ogre::dsi::OT_ANGLEAXIS == mDotSceneInfo->getOrientationSaveType() )
		{
			elementsData->push_back( parentData );
			return true;
		}
		return false;
    }

    void Angle_AxisElementProcessor::serializeElementEndImpl(TiXmlElement& currentNode, void* pParentData, void* pElementData )
    {
        //! Define empty function here. That serializeElementEndImpl from OrientationElementProcessor will not be called.
    }

    void Angle_AxisElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
		queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

		Quaternion* pQuaternion = static_cast<Quaternion*>( pElementData );

		Degree rfAngle;
		Vector3 vec;
		pQuaternion->ToAngleAxis( rfAngle, vec );

		SETATTRIB( "angle", StringConverter::toString( rfAngle ) );
		SETATTRIB( "angleUnit", serializeAttribute_AngleUnit( &currentNode, Math::AU_DEGREE ) );
		SETATTRIB( "x", StringConverter::toString( vec.x ) );
		SETATTRIB( "y", StringConverter::toString( vec.y ) );
		SETATTRIB( "z", StringConverter::toString( vec.z ) );
    }
    // ------------------------------------------------ Angle_AxisElementProcessor ---------------------------

    // ------------------------------------------------ DirectionElementProcessor ---------------------------
    bool DirectionElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
		// For light
		if ( strcmp("light", strParentName) == 0 )
		{
			Light* pLight = static_cast<Light*>( parentData );
			elementsData->push_back( new Vector3( pLight->getDirection() ) );
			return true;
		}
		else
		{
			mDotSceneInfo	= (dsi::DotSceneInfo*)mTreeBuilder->getCommonParameter("DotSceneInfo");

			if( Ogre::dsi::OT_DIRECTION == mDotSceneInfo->getOrientationSaveType() )
			{
				Quaternion* pQuaternion = static_cast<Quaternion*>( parentData );
				elementsData->push_back( new Vector3( pQuaternion->operator*( Ogre::Vector3::NEGATIVE_UNIT_Z ) ) );
				return true;
			}
		}

		return false;
    }

    void DirectionElementProcessor::serializeElementEndImpl(TiXmlElement& currentNode, void* pParentData, void* pElementData )
    {
		if ( pElementData )
		{
			Ogre::Vector3* q = static_cast<Ogre::Vector3*>( pElementData );
			delete q;
		}
    }

    void DirectionElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
		Ogre::Vector3* pVector3 = static_cast<Ogre::Vector3*>( pElementData );
		
		SETATTRIB( "x", StringConverter::toString( pVector3->x ) );
		SETATTRIB( "y", StringConverter::toString( pVector3->y ) );
		SETATTRIB( "z", StringConverter::toString( pVector3->z ) );
		
		SETATTRIB( "relativeTo", "world" );

		SETATTRIB( "localDirX", StringConverter::toString( 0 ) );
		SETATTRIB( "localDirY", StringConverter::toString( 0 ) );
		SETATTRIB( "localDirZ", StringConverter::toString( -1 ) );
    }
    // ------------------------------------------------ DirectionElementProcessor ---------------------------

    // ------------------------------------------------ ScaleElementProcessor --------------------------------------------------------
    bool ScaleElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        bool bResult = false;

        Vector3 v3Scale;
        if( strcmp( strParentName, "transformKeyFrame") == 0 )
        {
            TransformKeyFrame* pTransformKeyFrame = static_cast<TransformKeyFrame*>( parentData );
            v3Scale = pTransformKeyFrame->getScale();
        }
        else // Nodes Node
        {
            SceneNode* pNode = static_cast<SceneNode*>( parentData );
            v3Scale = pNode->getScale();
        }

        if( !v3Scale.isZeroLength() )
        {
            elementsData->push_back( new Vector3( v3Scale ) );
            bResult = true;
        }
        return bResult;
    }

    void ScaleElementProcessor::serializeElementEndImpl(TiXmlElement& currentNode, void* pParentData, void* pElementData )
    {
        if( pElementData )
            delete (Vector3*)pElementData;
    }

    void ScaleElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);

        //! Do nothing
        Vector3* pVector3 = static_cast<Vector3*>( pElementData );

        SETATTRIB( "x", StringConverter::toString( pVector3->x ) );
        SETATTRIB( "y", StringConverter::toString( pVector3->y ) );
        SETATTRIB( "z", StringConverter::toString( pVector3->z ) );
    }
    // ------------------------------------------------ ScaleElementProcessor ---------------------------

	// ------------------------------------------------ UserDataElementProcessor --------------------------------------------------------
	bool UserDataElementProcessor::serializeElementBeginImpl(const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData)
	{
		return true;
	}

	void UserDataElementProcessor::serializeElementEndImpl(TiXmlElement& currentNode, void* pParentData, void* pElementData)
	{
		return;
	}

	void UserDataElementProcessor::serializeElementImpl(TiXmlElement& currentNode, TiXmlElement& parentElement, void* pElementData, size_t counter)
	{
		return;
	}
	// ------------------------------------------------ UserDataElementProcessor ---------------------------

#ifdef _USE_DOT_OCTREE
    // ------------------------------------------------ OctreeElementProcessor ---------------------------
    //! Discover Octree using.
    bool OctreeElementProcessor::serializeElementBeginImpl( const char* strParentName, void* parentData, XmlNodeProcessor::ElementsDataList* elementsData )
    {
        elementsData->push_back( parentData );
        return true;
    }

    void OctreeElementProcessor::serializeElementImpl( TiXmlElement& currentNode,  TiXmlElement& parentElement, void* pElementData, size_t counter)
    {
        queryCurrElementNameAndFilenameAndDotSceneInfo(&currentNode);
        // \TODO Implement here
    }
    // ------------------------------------------------ OctreeElementProcessor ---------------------------
#endif
}
